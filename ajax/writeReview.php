<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
$el = new CIBlockElement;
$PROP = array();
$PROP["PROP7"] = $_REQUEST['shop'];
$PROP["PROP1"] = $_REQUEST['name'];
$PROP["PROP2"] = $_REQUEST['email'];
$PROP["PROP3"] = $_REQUEST['writeReviewRatingVal'];
$PROP["PROP4"] = date('d.m.Y H:i:s');
$PROP["PROP8"] = $_SERVER['REMOTE_ADDR'];
$PROP["FB_FILES"] = $_REQUEST["FILE"];
$arLoadProductArray = Array(
    "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
    "IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
    "IBLOCK_ID" => 44,
    "PROPERTY_VALUES" => $PROP,
    "NAME" => $_REQUEST['name'].' '.$_REQUEST['email'],
    "CODE"=>$_REQUEST['phone'],
    "ACTIVE" => "N", // активен
    "PREVIEW_TEXT" => $_REQUEST['text'],
);

if($PRODUCT_ID = $el->Add($arLoadProductArray)){
  echo "New ID: ".$PRODUCT_ID;
}else{
  echo "Error: ".$el->LAST_ERROR;
}

?>

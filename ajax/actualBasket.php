<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Sale;
if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("iblock")){
	echo "failure";
	return;
}

if(\Bitrix\Main\Loader::IncludeModule('aspro.max'))
{
	$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
	foreach ($basket as $basketItem) {
	    $basketAddProd[$basketItem->getProductId()]['ID'] = $basketItem->getProductId();
	    $basketAddProd[$basketItem->getProductId()]['QUANTITY'] = $basketItem->getQuantity();
	}
	$iblockID=(isset($_GET["iblockID"]) ? $_GET["iblockID"] : CMaxCache::$arIBlocks[SITE_ID]['aspro_max_catalog']['aspro_max_catalog'][0] );
	$arItems=CMax::getBasketItems($iblockID, $field="PRODUCT_ID");
	foreach ($arItems['NOT_AVAILABLE'] as $arItem) {
		$basketAddProd[$arItem]['QUANTITY'] = 0;
	}
	if ($_GET['product']['quantity'] > $basketAddProd[$_GET['product']['item']]['QUANTITY'])
		$isEys = array('yes');
	?>
	<script type="text/javascript">
		<?if ($isEys) {?>
			if ($('div[data-item="<?=$_GET['product']['item']?>"]').parents('.inner_wrap').find('.item-attent').length == 0) {
				$('div[data-item="<?=$_GET['product']['item']?>"]').parents('.inner_wrap').find('.item_info').append($('<span>', {
					'class': 'item-attent',
					'text': 'Доступно: <?=$basketAddProd[$_GET['product']['item']]['QUANTITY']?>'
				}));
				$('div[data-item="<?=$_GET['product']['item']?>"]').find('span.plus').attr('data-max', '<?=$basketAddProd[$_GET['product']['item']]['QUANTITY']?>');
				if ('<?=$basketAddProd[$_GET['product']['item']]['QUANTITY']?>' == '0'){
					$('a[data-item="<?=$_GET['product']['item']?>"]').hide();
				}
			}
		<?}?>
		var arBasketAspro = <? echo CUtil::PhpToJSObject($arItems, false, true); ?>;
		if(typeof obMaxPredictions === 'object'){
			obMaxPredictions.updateAll();
		}
	</script>
<?}?>

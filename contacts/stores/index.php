<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "сок, кофе, чай, кафе");
$APPLICATION->SetPageProperty("description", "В Магнолии приняты самые высокие стандарты качества обслуживания и жесткие требования к качеству продукции");
$APPLICATION->SetPageProperty("title", "Магазины Магнолия");
$APPLICATION->SetTitle("Магазины");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH ."/css/bootstrap.css");
$APPLICATION->AddHeadScript('//api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=ad83d37c-243b-4fac-a475-b20119779199');
$APPLICATION->AddHeadScript('/contacts/stores/yandexMap.js');
$APPLICATION->SetAdditionalCSS("//use.fontawesome.com/releases/v5.0.13/css/all.css");
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/magnoliaList.js');
?><?
// 		$APPLICATION->IncludeComponent(
// 			"bitrix:search.title", 
// 			"", 
// 			array(
// 				"CATEGORY_0" => array(
// 					0 => "iblock_MapFooter",
// 				),
// 				"CATEGORY_0_TITLE" => "",
// 				"CATEGORY_0_iblock_MapFooter" => array(
// 					0 => "7",
// 				),
// 				"CATEGORY_0_iblock_service" => array(
// 					0 => "all",
// 				),
// 				"CHECK_DATES" => "N",
// 				"CONTAINER_ID" => "title-search",
// 				"CONVERT_CURRENCY" => "N",
// 				"INPUT_ID" => "title-search-input",
// 				"NUM_CATEGORIES" => "1",
// 				"ORDER" => "date",
// 				"PAGE" => "/shops-2/",
// 				"PREVIEW_HEIGHT" => "75",
// 				"PREVIEW_TRUNCATE_LEN" => "",
// 				"PREVIEW_WIDTH" => "75",
// 				"PRICE_CODE" => array(
// 				),
// 				"PRICE_VAT_INCLUDE" => "Y",
// 				"SHOW_INPUT" => "Y",
// 				"SHOW_OTHERS" => "N",
// 				"SHOW_PREVIEW" => "Y",
// 				"TEMPLATE_THEME" => "blue",
// 				"TOP_COUNT" => "5",
// 				"USE_LANGUAGE_GUESS" => "N",
// 				"COMPONENT_TEMPLATE" => "titleShopSearch"
// 			),
// 			false
// 		);?>
<?
if (!empty($_GET["q"]))
    {
        $arrFilter = array(
            "?SEARCHABLE_CONTENT" => $_GET["q"],
        );
    }
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"shops2", 
	array(
		"IBLOCK_TYPE" => "aspro_max_content",
		"IBLOCK_ID" => "12",
		"NEWS_COUNT" => "3000",
		"USE_FILTER" => "Y",
		"MAP_TYPE" => "0",
		"FILTER_NAME" => "",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/contacts/stores/",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"DISPLAY_PANEL" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"USE_PERMISSIONS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "EMAIL",
			1 => "MAP",
			2 => "METRO",
			3 => "SCHEDULE",
			4 => "PHONE",
			5 => "ADDRESS",
			6 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "EMAIL",
			1 => "MAP",
			2 => "METRO",
			3 => "SCHEDULE",
			4 => "PHONE",
			5 => "ADDRESS",
			6 => "MORE_PHOTOS",
			7 => "",
		),
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Магазины",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "shops2",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_REVIEW" => "N",
		"SET_LAST_MODIFIED" => "N",
		"ADD_ELEMENT_CHAIN" => "Y",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "METRO",
			1 => "TYPE",
			2 => "YCAFE",
			3 => "BUY",
			4 => "",
		),
		"STRICT_SECTION_CHECK" => "N",
		"GOOGLE_API_KEY" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"TEMPLATE_THEME" => "blue",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_ID#/",
		)
	),
	false
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}?>
<?$APPLICATION->IncludeComponent("aspro:wrapper.block.max", "front_sections_only3", Array(
	"IBLOCK_TYPE" => "1c_catalog",	// Тип инфоблока
		"IBLOCK_ID" => "33",	// Инфоблок
		"FILTER_NAME" => "arrPopularSections",	// Имя массива со значениями фильтра для фильтрации элементов
		"COMPONENT_TEMPLATE" => "front_sections_only",
		"SECTION_ID" => "",	// ID раздела
		"SECTION_CODE" => "",	// Код раздела
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"TITLE_BLOCK" => "Популярные категории",	// Заголовок блока
		"TITLE_BLOCK_ALL" => "Весь каталог",	// Заголовок на все элементы
		"ALL_URL" => "catalog/",	// Ссылка на все элементы
		"VIEW_MODE" => "",
		"VIEW_TYPE" => "type2",
		"NO_MARGIN" => "N",	// Не использовать отступ между блоками
		"FILLED" => "Y",	// Заливать блоки
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"SHOW_ICONS" => "N",	// Отображать иконку раздела
		"SHOW_SUBSECTIONS" => "N",	// Отображать подразделы
		"SCROLL_SUBSECTIONS" => "N",	// Отображать подразделы при наведении
		"INCLUDE_FILE" => "",	// Файл с дополнительным текстом
	),
	false
);?>
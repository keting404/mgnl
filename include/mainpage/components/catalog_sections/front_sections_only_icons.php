<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}?>
<?$APPLICATION->IncludeComponent(
	"aspro:wrapper.block.max", 
	"front_sections_only_custom", 
	array(
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "33",
		"FILTER_NAME" => "arrPopularSectionsNone",
		"COMPONENT_TEMPLATE" => "front_sections_only_custom",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"TITLE_BLOCK" => "Категории",
		"TITLE_BLOCK_ALL" => "",
		"ALL_URL" => "catalog/",
		"VIEW_MODE" => "",
		"VIEW_TYPE" => "type1",
		"NO_MARGIN" => "N",
		"FILLED" => "N",
		"SHOW_ICONS" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"TOP_DEPTH" => "1",
		"SHOW_SUBSECTIONS" => "Y",
		"SCROLL_SUBSECTIONS" => "N",
		"INCLUDE_FILE" => ""
	),
	false
);?>
<div class="inline-block">
	<!--'start_frame_cache_header-allphones-block3'-->		
	<!-- noindex -->		
	<div class="phone white sm">
		<div class="wrap">
			<div>
				<i class="fa fa-phone svg inline  svg-inline-phone" aria-hidden="true"></i>
				<a rel="nofollow" href="tel:+74959213237">
					+7 (495) 921-32-37
				</a>
			</div>
		</div>
	</div>
	<!-- /noindex -->
	<!--'end_frame_cache_header-allphones-block3'-->													
</div>
<div class="phone white sm footer-add-info">
	<div class="wrap">
		<span>Горячая линия</span>
		<div>
		<i class="fa fa-phone svg inline  svg-inline-phone" aria-hidden="true"></i>
		<a href="tel:8(800)250-62-64">8(800)250-62-64</a>
		</div>
		<span>пн — пт с 09:00 до 18:00</span>
		<span>Центральный офис</span>
		<div>
		<i class="fa fa-phone svg inline  svg-inline-phone" aria-hidden="true"></i>
		<a href="tel:8(495)921-32-36">8(495)921-32-36</a>
		</div>
		<span>пн — пт с 09:00 до 18:00</span>
		<span>м. Октябрьское поле, МЦК Панфиловская</span>
		<span>м. Сокол, ул. Алабяна, д. 25/37</span>
	</div>
</div>
<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
	<div class="inline-block callback_wrap">
		<span class="callback-block animate-load colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
	</div>
<?endif;?>
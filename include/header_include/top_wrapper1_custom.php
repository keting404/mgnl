<?global $APPLICATION, $arRegion, $arSite, $arTheme, $bIndexBot;?>

<?CMax::ShowPageType('mega_menu');?>

<?CMax::get_banners_position('TOP_HEADER');?>

<div class="header_wrap visible-lg visible-md title-v<?=$arTheme["PAGE_TITLE"]["VALUE"];?><?=($isIndex ? ' index' : '')?> <?$APPLICATION->AddBufferContent(array('CMax', 'getBannerClass'))?>">
	<header id="header">
		<?CMax::ShowPageType('header');?>
	</header>
</div>
<?CMax::get_banners_position('TOP_UNDERHEADER');?>

<?if($arTheme["TOP_MENU_FIXED"]["VALUE"] == 'Y'):?>
	<div id="headerfixed">
		<?CMax::ShowPageType('header_fixed');?>
	</div>
<?endif;?>

<div id="mobileheader" class="visible-xs visible-sm">
	<div class="mobileheader-v2">
	<div class="burger pull-left">
		<i class="fa fa-bars svg inline  svg-inline-burger" aria-hidden="true"></i>
		<?//=CMax::showIconSvg("burger dark", SITE_TEMPLATE_PATH."/images/svg/burger.svg");?>
		<?=CMax::showIconSvg("close dark", SITE_TEMPLATE_PATH."/images/svg/Close.svg");?>
	</div>
	<div class="title-block col-sm-5 col-xs-5 pull-left">
	<?$curDir=$APPLICATION->GetCurDir();
	$address = str_replace("Россия, Москва, ", "", $_COOKIE["delivery_address"]);
	?>
    <?if($curDir == "/help/delivery/"){?>
    <i class="fa fa-map-marker" aria-hidden="true"></i>Ваш адрес: <a href="#inpage-map" class="head-d-adress"><?=$address ? $address : "выбрать"?></a>
    <?}else{?>
    <i class="fa fa-map-marker" aria-hidden="true"></i>Ваш адрес: <a class="call_form head-d-adress"><?=$address ? $address : "выбрать"?></a>
    <?}?>
	<?//($APPLICATION->GetTitle() ? $APPLICATION->ShowTitle(false) : $APPLICATION->ShowTitle());?></div>
	<div class="right-icons pull-right">
		<div class="pull-right">
			<div class="wrap_icon wrap_basket">
				<?=CMax::ShowBasketWithCompareLink('', 'big white', false, false, true);?>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_cabinet">
				<?=CMax::showCabinetLink(true, false, 'big white');?>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon">
				<button class="top-btn inline-search-show twosmallfont">
					<?=CMax::showIconSvg("search", SITE_TEMPLATE_PATH."/images/svg/Search.svg");?>
				</button>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_phones">
				<?//CMax::ShowHeaderMobilePhones("big");?>
			</div>
		</div>
	</div>
</div>

	<div id="mobilemenu" class="<?=($arTheme["HEADER_MOBILE_MENU_OPEN"]["VALUE"] == '1' ? 'leftside':'dropdown')?>">
		<?CMax::ShowPageType('header_mobile_menu');?>
	</div>
</div>

<div id="mobilefilter" class="scrollbar-filter"></div>

<?$APPLICATION->ShowViewContent('section_bnr_top_content');?>
<!-- Swiper -->
    <?
    $sectFilter = array("IBLOCK_ID"=>33,"ACTIVE"=>"Y","GLOBAL_ACTIVE"=>"Y","UF_POPULAR"=>"1");
    $rsAllSections = CIBlockSection::GetList(array("SORT"=>"ASC"), $sectFilter,false, array("ID","SECTION_PAGE_URL","IBLOCK_SECTION_ID","NAME","DESCRIPTION"),false);
    while ($arSectionResItem = $rsAllSections->GetNext())
    {
        $arSectResult["ITEMS"][]=$arSectionResItem;
    }?>
    <div class="wraps hover_shine">
    <div class="maxwidth-theme">
    <div class="swiper-container swiper-focus-sections">
    <div class="swiper-wrapper">
    <?foreach ($arSectResult["ITEMS"] as $arSectionResItem) {?>
        <div class="swiper-slide section-detail-list__item item font_xs">
        <a href="<?=$arSectionResItem["SECTION_PAGE_URL"]?>"><div class="bordered  box-shadow-sm rounded3">
            <span><?=$arSectionResItem["NAME"]?></span>
        </div></a>
        </div>
    <?}?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    </div>
    </div>
    </div>

    <!-- Initialize Swiper -->
    <script>
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 10,
//         slidesPerGroup: 'auto',
        slidesPerView: 'auto',
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        }
    });
    </script>

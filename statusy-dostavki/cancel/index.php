<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отмена доставки");
?>
<?global $productId, $token;?>
Отмена доставки заказа
<?if (file_exists($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php"))
            require_once($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php");?>
<?
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.gett.com/sandbox/v1/business/rides/'.$_GET["rideId"].'/cancel?business_id=RU-25193');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);

$headers = array();
$headers[] = 'Authorization: Bearer '.$token;
$headers[] = 'Content-Type: application/json';
$headers[] = 'Content-Length: 0';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$result = json_decode($result, true);

// $fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r('Отмена заказа:
// ', TRUE));
// fwrite($fp, print_r($result, TRUE));fclose($fp);
?>
<br>
<?if($result['error_description']){
        print_r($result['error_description']);
    }else if (empty($result)){
        echo "Заказ отменен";
    }else{
        echo "Ошибка";
    }
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main;
use Bitrix\Sale;
//заказ поездки
global $productId, $token, $rideID, $itemID;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.gett.com/sandbox/v1/business/rides/'.$rideID.'?business_id=RU-25193');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

$headers = array();
$headers[] = 'Authorization: Bearer '.$token;
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
curl_close($ch);
$result = json_decode($result, true);

//запись в файл
$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r('Проверка заказа:
', TRUE));
fwrite($fp, print_r($result, TRUE));fclose($fp);

CModule::IncludeModule("iblock");
// обновление статуса заказа
CModule::IncludeModule("sale");
if($result['status']=="Completed"){
    $db_props = CIBlockElement::GetProperty(36, $itemID, array("sort" => "asc"), Array("CODE"=>"ORDER_ID"));
    if($ar_props = $db_props->Fetch())
        $orderId = $ar_props["VALUE"];

    $arOrder = CSaleOrder::GetByID($orderId);
    if ($arOrder)
    {
    $arFields = array(
        "STATUS_ID" => "F"
    );
    CSaleOrder::Update($orderId, $arFields);
    }
}
// if ($result['error']!=='Not Found'){
$willArrive='';
$droppedOff='';
$pickedUp='';
    if($result['will_arrive_at']){
        $willArrive = strtotime($result['will_arrive_at']);
        $willArrive = date('Y.m.d H:i:s', $willArrive);
    }
    $propStatus='';
    if($result['status']=="Pending"){$propStatus='308';}
    if($result['status']=="Confirmed"){$propStatus='309';}
    if($result['status']=="Completed"){$propStatus='310';}
    if($result['status']=="Driving"){$propStatus='311';}
    if($result['status']=="Routing"){$propStatus='312';}
    if($result['status']=="Waiting"){$propStatus='313';}
    if($result['status']=="Cancelled"){$propStatus='314';}
    if($result['status']=="Rejected"){$propStatus='315';}
    if($result['status']=="CareReq"){$propStatus='316';}
    if($result['droppedoff_at']){
        $droppedOff = strtotime($result['droppedoff_at']);
        $droppedOff = date('Y.m.d H:i:s', $droppedOff);
    }
    if($result['pickedup_at']){
        $pickedUp = strtotime($result['pickedup_at']);
        $pickedUp = date('Y.m.d H:i:s', $pickedUp);
    }
    $PROP = array();
    if($propStatus)
        $PROP['RIDE_STATUS'][] = $propStatus;
    // if($willArrive)
        $PROP['DRIVER_TIME'] = $willArrive;
    if($droppedOff)
        $PROP['DELIVERED_TIME'] = $droppedOff;
    if($result['driver']['name'])
        $PROP['DRIVER'] = $result['driver']['name'];
    if($result['driver']['phone_number'])
        $PROP['DRIVER_PHONE'] = $result['driver']['phone_number'];
    if($pickedUp)
        $PROP['PICKED_UP'] = $pickedUp;
    if($result['driver']['vehicle']['license_plate'])
        $PROP['LICENSE_PLATE'] = $result['driver']['vehicle']['license_plate'];

    $el = new CIBlockElement;
    CIBlockElement::SetPropertyValuesEx($itemID, 36, $PROP);
// }

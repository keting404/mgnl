<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main;
//update поездки
global $productId, $token;

$sendArray = array(
'status' => 'Confirmed',
'will_arrive_at' => '2020-05-15T07:24:31Z'
);
$sendArray = json_encode($sendArray, JSON_UNESCAPED_UNICODE);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.gett.com/sandbox/v1/business/rides/5756100571/status?business_id=RU-25193');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
curl_setopt($ch, CURLOPT_POSTFIELDS, $sendArray);

$headers = array();
$headers[] = 'Authorization: Bearer '.$token;//
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$result = json_decode($result, true);


//запись в файл
$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r($result, TRUE));fclose($fp);


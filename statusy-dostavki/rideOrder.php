<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main;
//заказ поездки
global $arOrderProps, $arStores, $productId, $token, $arOrderFields, $arOrderVals;

$arOrderTime = strtotime($arOrderProps["TIME"]["VALUE"]["VALUE"]);
$arOrderTime = date('Y-m-d H:i:s', $arOrderTime);
$arOrderTime = str_replace(" ", "T", $arOrderTime).'+00:00';
$deliveryAddress = $arOrderProps["ADDRESS"]["VALUE"]["VALUE"];
if ($arOrderProps["PODEZD"]["VALUE"]["VALUE"])
     $deliveryAddress .= ', подъезд: '.$arOrderProps["PODEZD"]["VALUE"]["VALUE"];
if ($arOrderProps["FLOOR"]["VALUE"]["VALUE"])
     $deliveryAddress .= ', этаж: '.$arOrderProps["FLOOR"]["VALUE"]["VALUE"];
if ($arOrderProps["ROOM"]["VALUE"]["VALUE"])
     $deliveryAddress .= ', квартира: '.$arOrderProps["ROOM"]["VALUE"]["VALUE"];
$sendArray = array(
'product_id' => $productId,//$productId
'rider' => array(
    'name' => $arOrderProps["FIO"]["VALUE"]["VALUE"], //имя покупателя
    'phone_number' => $arOrderProps["PHONE"]["VALUE"]["VALUE"], //тел покупателя
    ),
'pickup' => array(
    'latitude' => floatval($arStores["GPS_N"]),
    'longitude' => floatval($arStores["GPS_S"]),
    'address' => $arStores["ADDRESS"],
    ),
'destination' => array(
    'latitude' => floatval($arOrderProps["D_LONG"]["VALUE"]["VALUE"]),
    'longitude' => floatval($arOrderProps["D_LAT"]["VALUE"]["VALUE"]),
    'address' => $deliveryAddress,
    ),
'note_to_driver' => 'Заказ из магазина Магнолия, внутренний номер заказа: '.$arOrderVals['ID'].' '.$arOrderVals['USER_DESCRIPTION'],
// 'scheduled_local_time' => true,
// 'scheduled_at' => $arOrderTime
// 'third_party_order_id' => $arOrderVals["ID"]
);
$sendArray = json_encode($sendArray, JSON_UNESCAPED_UNICODE);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.gett.com/sandbox/v1/business/rides?business_id=RU-25193');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $sendArray);

$headers = array();
$headers[] = 'Authorization: Bearer '.$token;//
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$result = json_decode($result, true);

//запись в файл
$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r('Создание заказа:
', TRUE));
fwrite($fp, print_r($result, TRUE));fclose($fp);

CModule::IncludeModule("iblock");
$propStatus='';
if($result['status']=="Pending"){$propStatus='308';}
if($result['status']=="Confirmed"){$propStatus='309';}
if($result['status']=="Completed"){$propStatus='310';}
if($result['status']=="Driving"){$propStatus='311';}
if($result['status']=="Routing"){$propStatus='312';}
if($result['status']=="Waiting"){$propStatus='313';}
if($result['status']=="Cancelled"){$propStatus='314';}
if($result['status']=="Rejected"){$propStatus='315';}
if($result['status']=="CareReq"){$propStatus='316';}
$el = new CIBlockElement;

$PROP = array();
$fullName = 'Доставка Gett заказа #'.$arOrderVals['ID'];
$PROP['ORDER_ID'] = $arOrderVals['ID'];
$PROP['USER_ID'] = $arOrderVals['BX_USER_ID'];
$PROP['RIDE_ID'] = $result['ride_id'];
$PROP['RIDE_STATUS'] = $propStatus;
$PROP['PICKUP'] = $result['pickup']['address'];
$PROP['DESTINATION'] = $result['destination']['address'];
//         транслит симв кода
$params = Array(
    "max_len" => "100", // обрезает символьный код до 100 символов
    "change_case" => "L", // буквы преобразуются к нижнему регистру
    "replace_space" => "-", // меняем пробелы на нижнее подчеркивание
    "replace_other" => "-", // меняем левые символы на нижнее подчеркивание
    "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
    "use_google" => "false", // отключаем использование google
    ); 
    
$arEventFields = Array(
    "IBLOCK_ID"      => 36,
    "PROPERTY_VALUES"=> $PROP,
    "NAME"           => $fullName,
    "CODE" => CUtil::translit($fullName, "ru" , $params),
    "ACTIVE"         => "Y",            // активен
//           "PREVIEW_TEXT"   => $message,
);

if( $el->Add($arEventFields) ){
    //запись в файл
    $fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r("Элемент успешно создан
", TRUE));fclose($fp);
}
else{
    //запись в файл
    $fp = fopen($_SERVER["DOCUMENT_ROOT"].'/statusy-dostavki/get-log.txt', 'a');fwrite($fp, print_r("Элемент не создан
", TRUE));fclose($fp);
}

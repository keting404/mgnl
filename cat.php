<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle("Магнолия");
?>
<div class="row">
<div class="col-lg-20 col-md-4 col-sm-6 col-xs-6 col-xxs-12 item item-parent item_block ">
				<div class="basket_props_block" id="bx_basket_div_31386_Y" style="display: none;">
									</div>
				<div class="catalog_item_wrapp catalog_item item_wrap main_item_wrapper  product_image " id="bx_3966226736_31386_Y" style="height: 466px;">
					<div class="inner_wrap TYPE_1">
																		<div class="image_wrapper_block">
																
																																										<div class="like_icons block" data-size="1">
																														<div class="wish_item_button">
										<span title="Отложить" data-quantity="1" class="wish_item to rounded3 colored_theme_hover_bg" data-item="31386" data-iblock="33"><i class="svg inline  svg-inline-wish ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="13" viewBox="0 0 16 13"><defs><style>.clsw-1{fill:#fff;fill-rule:evenodd;}</style></defs><path class="clsw-1" d="M506.755,141.6l0,0.019s-4.185,3.734-5.556,4.973a0.376,0.376,0,0,1-.076.056,1.838,1.838,0,0,1-1.126.357,1.794,1.794,0,0,1-1.166-.4,0.473,0.473,0,0,1-.1-0.076c-1.427-1.287-5.459-4.878-5.459-4.878l0-.019A4.494,4.494,0,1,1,500,135.7,4.492,4.492,0,1,1,506.755,141.6Zm-3.251-5.61A2.565,2.565,0,0,0,501,138h0a1,1,0,1,1-2,0h0a2.565,2.565,0,0,0-2.506-2,2.5,2.5,0,0,0-1.777,4.264l-0.013.019L500,145.1l5.179-4.749c0.042-.039.086-0.075,0.126-0.117l0.052-.047-0.006-.008A2.494,2.494,0,0,0,503.5,135.993Z" transform="translate(-492 -134)"></path></svg></i></span>
										<span title="В отложенных" data-quantity="1" class="wish_item in added rounded3 colored_theme_bg" style="display: none;" data-item="31386" data-iblock="33"><i class="svg inline  svg-inline-wish ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="13" viewBox="0 0 16 13"><defs><style>.clsw-1{fill:#fff;fill-rule:evenodd;}</style></defs><path class="clsw-1" d="M506.755,141.6l0,0.019s-4.185,3.734-5.556,4.973a0.376,0.376,0,0,1-.076.056,1.838,1.838,0,0,1-1.126.357,1.794,1.794,0,0,1-1.166-.4,0.473,0.473,0,0,1-.1-0.076c-1.427-1.287-5.459-4.878-5.459-4.878l0-.019A4.494,4.494,0,1,1,500,135.7,4.492,4.492,0,1,1,506.755,141.6Zm-3.251-5.61A2.565,2.565,0,0,0,501,138h0a1,1,0,1,1-2,0h0a2.565,2.565,0,0,0-2.506-2,2.5,2.5,0,0,0-1.777,4.264l-0.013.019L500,145.1l5.179-4.749c0.042-.039.086-0.075,0.126-0.117l0.052-.047-0.006-.008A2.494,2.494,0,0,0,503.5,135.993Z" transform="translate(-492 -134)"></path></svg></i></span>
									</div>
																																																						<div class="fast_view_button">
								<span title="Быстрый просмотр" class="rounded3 colored_theme_hover_bg" data-event="jqm" data-param-form_id="fast_view" data-param-iblock_id="33" data-param-id="31386" data-param-item_href="%2Fcatalog%2Fshaurma-s-kuritsey-magnoliya-210g%2F" data-name="fast_view"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><path data-name="Ellipse 302 copy 3" class="cls-1" d="M549,146a8.546,8.546,0,0,1-8.008-6,8.344,8.344,0,0,1,16.016,0A8.547,8.547,0,0,1,549,146Zm0-2a6.591,6.591,0,0,0,5.967-4,7.022,7.022,0,0,0-1.141-1.76,4.977,4.977,0,0,1-9.652,0,7.053,7.053,0,0,0-1.142,1.76A6.591,6.591,0,0,0,549,144Zm-2.958-7.246c-0.007.084-.042,0.159-0.042,0.246a3,3,0,1,0,6,0c0-.087-0.035-0.162-0.042-0.246A6.179,6.179,0,0,0,546.042,136.753Z" transform="translate(-541 -134)"></path></svg></i></span>
							</div>
											</div>
																																																																																																																														<a href="/catalog/shaurma-s-kuritsey-magnoliya-210g/" class="thumb shine">
							<span class="section-gallery-wrapper flexbox">
																										<span class="section-gallery-wrapper__item _active">
										<span class="section-gallery-wrapper__item-nav section-gallery-wrapper__item_hidden "></span>
										<img class="lazy img-responsive" src="/upload/iblock/252/25222ea2e77dba76b2987e202a972384.jpeg" data-src="/upload/iblock/252/25222ea2e77dba76b2987e202a972384.jpeg" alt="Шаурма с курицей Магнолия  210г" title="Шаурма с курицей Магнолия  210г">
									</span>
															</span>
						</a>
																																												</div>
													<div class="item_info">
																	<div class="rating">
						<!--'start_frame_cache_dv_31386'-->															<div class="blog-info__rating--top-info">
									<div class="votes_block nstar with-text">
										<div class="ratings">
																						<div class="inner_rating" title="Нет оценок">
																									<div class="item-rating "><i class="svg inline  svg-inline-star" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13"><rect class="sscls-1" width="15" height="13"></rect><path data-name="Shape 921 copy 15" class="sscls-2" d="M1333.37,457.5l-4.21,2.408,0.11,0.346,2.07,4.745h-0.72l-4.12-3-4.09,3h-0.75l2.04-4.707,0.12-.395-4.19-2.4V457h5.12l1.53-5h0.38l1.57,5h5.14v0.5Z" transform="translate(-1319 -452)"></path></svg></i></div>
																									<div class="item-rating "><i class="svg inline  svg-inline-star" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13"><rect class="sscls-1" width="15" height="13"></rect><path data-name="Shape 921 copy 15" class="sscls-2" d="M1333.37,457.5l-4.21,2.408,0.11,0.346,2.07,4.745h-0.72l-4.12-3-4.09,3h-0.75l2.04-4.707,0.12-.395-4.19-2.4V457h5.12l1.53-5h0.38l1.57,5h5.14v0.5Z" transform="translate(-1319 -452)"></path></svg></i></div>
																									<div class="item-rating "><i class="svg inline  svg-inline-star" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13"><rect class="sscls-1" width="15" height="13"></rect><path data-name="Shape 921 copy 15" class="sscls-2" d="M1333.37,457.5l-4.21,2.408,0.11,0.346,2.07,4.745h-0.72l-4.12-3-4.09,3h-0.75l2.04-4.707,0.12-.395-4.19-2.4V457h5.12l1.53-5h0.38l1.57,5h5.14v0.5Z" transform="translate(-1319 -452)"></path></svg></i></div>
																									<div class="item-rating "><i class="svg inline  svg-inline-star" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13"><rect class="sscls-1" width="15" height="13"></rect><path data-name="Shape 921 copy 15" class="sscls-2" d="M1333.37,457.5l-4.21,2.408,0.11,0.346,2.07,4.745h-0.72l-4.12-3-4.09,3h-0.75l2.04-4.707,0.12-.395-4.19-2.4V457h5.12l1.53-5h0.38l1.57,5h5.14v0.5Z" transform="translate(-1319 -452)"></path></svg></i></div>
																									<div class="item-rating "><i class="svg inline  svg-inline-star" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13"><rect class="sscls-1" width="15" height="13"></rect><path data-name="Shape 921 copy 15" class="sscls-2" d="M1333.37,457.5l-4.21,2.408,0.11,0.346,2.07,4.745h-0.72l-4.12-3-4.09,3h-0.75l2.04-4.707,0.12-.395-4.19-2.4V457h5.12l1.53-5h0.38l1.57,5h5.14v0.5Z" transform="translate(-1319 -452)"></path></svg></i></div>
																							</div>
										</div>
									</div>
																	</div>
													<!--'end_frame_cache_dv_31386'-->					</div>
																			<div class="item-title" style="height: 48px;">
					<a href="/catalog/shaurma-s-kuritsey-magnoliya-210g/" class="dark_link option-font-bold font_sm"><span>Шаурма с курицей Магнолия  210г</span></a>
				</div>
															<div class="sa_block" data-fields="[&quot;&quot;,&quot;&quot;]" data-stores="[&quot;21&quot;]" data-user-fields="[&quot;&quot;,&quot;&quot;]" style="height: 24px;">
					<div class="item-stock " data-id="31386"><span class="icon stock"></span><span class="value font_sxs">Есть в наличии: 13</span></div>															<div class="article_block"></div>
				</div>
															<div class="cost prices clearfix" style="height: 66px;">
																																																							<div class="price_group acb496c6-9506-11ea-80bf-00505697dbd3"><div class="price_name muted font_xs">Старая</div>
												<div class="price_matrix_wrapper strike_block">
															<div class="price font-bold font_mxs" data-currency="RUB" data-value="99">
																			<span class="values_wrapper"><span class="price_value">99</span><span class="price_currency"> руб.</span></span><span class="price_measure">/шт</span>																	</div>
													</div>
													</div>
																																							<div class="price_group min 1618b9f0-7969-11ea-9fc5-40167e7389e1"><div class="price_name muted font_xs">Розничная цена</div>
												<div class="price_matrix_wrapper ">
															<div class="price font-bold font_mxs" data-currency="RUB" data-value="85">
																			<span class="values_wrapper"><span class="price_value">85</span><span class="price_currency"> руб.</span></span><span class="price_measure">/шт</span>																	</div>
													</div>
													</div>
																																					<div class="sale_block"><div class="sale_wrapper font_xxs"><div class="sale-number rounded2"><div class="value">-14%</div></div></div></div></div>
										</div>
																	<div class="footer_button  inner_content js_offers__31386_Y">
																										<div class="counter_wrapp  clearfix rounded3">
																														<div class="counter_block big" data-item="31386">
							<span class="minus dark-color" id="bx_3966226736_31386_Y_quant_down"><i class="svg inline  svg-inline-wish ncolor colored1" aria-hidden="true"><svg width="11" height="1" viewBox="0 0 11 1"><rect width="11" height="1" rx="0.5" ry="0.5"></rect></svg></i></span>
							<input type="text" class="text" id="bx_3966226736_31386_Y_quantity" name="quantity" value="1">
							<span class="plus dark-color" id="bx_3966226736_31386_Y_quant_up" data-max="13"><i class="svg inline  svg-inline-wish ncolor colored1" aria-hidden="true"><svg width="11" height="11" viewBox="0 0 11 11"><path d="M1034.5,193H1030v4.5a0.5,0.5,0,0,1-1,0V193h-4.5a0.5,0.5,0,0,1,0-1h4.5v-4.5a0.5,0.5,0,0,1,1,0V192h4.5A0.5,0.5,0,0,1,1034.5,193Z" transform="translate(-1024 -187)"></path></svg></i></span>
						</div>
																											<div id="bx_3966226736_31386_Y_basket_actions" class="button_block ">
										<!--noindex-->
											<span data-value="85" data-currency="RUB" class="btn-exlg to-cart btn btn-default transition_bg animate-load has-ripple" data-item="31386" data-float_ratio="" data-ratio="1" data-bakset_div="bx_basket_div_31386" data-props="" data-part_props="N" data-add_props="Y" data-empty_props="Y" data-offers="" data-iblockid="33" data-quantity="1"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true" title="В корзину"><svg class="" width="19" height="16" viewBox="0 0 19 16"><path data-name="Ellipse 2 copy 9" class="cls-1" d="M956.047,952.005l-0.939,1.009-11.394-.008-0.952-1-0.953-6h-2.857a0.862,0.862,0,0,1-.952-1,1.025,1.025,0,0,1,1.164-1h2.327c0.3,0,.6.006,0.6,0.006a1.208,1.208,0,0,1,1.336.918L943.817,947h12.23L957,948v1Zm-11.916-3,0.349,2h10.007l0.593-2Zm1.863,5a3,3,0,1,1-3,3A3,3,0,0,1,945.994,954.005ZM946,958a1,1,0,1,0-1-1A1,1,0,0,0,946,958Zm7.011-4a3,3,0,1,1-3,3A3,3,0,0,1,953.011,954.005ZM953,958a1,1,0,1,0-1-1A1,1,0,0,0,953,958Z" transform="translate(-938 -944)"></path></svg></i><span>В корзину</span></span><a rel="nofollow" href="/basket/" class="btn-exlg in-cart btn btn-default transition_bg" data-item="31386" style="display:none;"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true" title="В корзине"><svg xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18"><path data-name="Rounded Rectangle 906 copy 3" class="cls-1" d="M1005.97,4556.22l-1.01,4.02a0.031,0.031,0,0,0-.01.02,0.87,0.87,0,0,1-.14.29,0.423,0.423,0,0,1-.05.07,0.7,0.7,0,0,1-.2.18,0.359,0.359,0,0,1-.1.07,0.656,0.656,0,0,1-.21.08,1.127,1.127,0,0,1-.18.03,0.185,0.185,0,0,1-.07.02H993c-0.03,0-.056-0.02-0.086-0.02a1.137,1.137,0,0,1-.184-0.04,0.779,0.779,0,0,1-.207-0.08c-0.031-.02-0.059-0.04-0.088-0.06a0.879,0.879,0,0,1-.223-0.22s-0.007-.01-0.011-0.01a1,1,0,0,1-.172-0.43l-1.541-6.14H988a1,1,0,1,1,0-2h3.188a0.3,0.3,0,0,1,.092.02,0.964,0.964,0,0,1,.923.76l1.561,6.22h9.447l0.82-3.25a1,1,0,0,1,1.21-.73A0.982,0.982,0,0,1,1005.97,4556.22Zm-7.267.47c0,0.01,0,.01,0,0.01a1,1,0,0,1-1.414,0l-2.016-2.03a0.982,0.982,0,0,1,0-1.4,1,1,0,0,1,1.414,0l1.305,1.31,4.3-4.3a1,1,0,0,1,1.41,0,1.008,1.008,0,0,1,0,1.42ZM995,4562a3,3,0,1,1-3,3A3,3,0,0,1,995,4562Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,995,4566Zm7-4a3,3,0,1,1-3,3A3,3,0,0,1,1002,4562Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,1002,4566Z" transform="translate(-987 -4550)"></path></svg></i><span>В корзине</span></a>										<!--/noindex-->
									</div>
								</div>
																															<div class="sku_props ce_cmp_hidden">
											</div>
				</div>
														</div>
									</div>
			</div>
<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 item item-parent item_block ">
    <div class="basket_props_block" id="bx_basket_div_31386_block" style="display: none;"></div>
    <div class="catalog_item_wrapp catalog_item item_wrap main_item_wrapper  product_image " id="bx_3966226736_31386" style="height: 450px;">
      <div class="inner_wrap TYPE_1">
        <div class="image_wrapper_block">
          <!--<div class="stickers custom-font">-->
          <!--<div><div class="sticker_stock font_sxs rounded2">Акция</div></div>-->
          <!--</div>-->
          <div class="like_icons block" data-size="1">
            <div class="wish_item_button">
              <span title="Отложить" data-quantity="1" class="wish_item to rounded3 colored_theme_hover_bg" data-item="31386" data-iblock="33"><i class="svg inline  svg-inline-wish ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="13" viewBox="0 0 16 13"><defs><style>.clsw-1{fill:#fff;fill-rule:evenodd;}</style></defs><path class="clsw-1" d="M506.755,141.6l0,0.019s-4.185,3.734-5.556,4.973a0.376,0.376,0,0,1-.076.056,1.838,1.838,0,0,1-1.126.357,1.794,1.794,0,0,1-1.166-.4,0.473,0.473,0,0,1-.1-0.076c-1.427-1.287-5.459-4.878-5.459-4.878l0-.019A4.494,4.494,0,1,1,500,135.7,4.492,4.492,0,1,1,506.755,141.6Zm-3.251-5.61A2.565,2.565,0,0,0,501,138h0a1,1,0,1,1-2,0h0a2.565,2.565,0,0,0-2.506-2,2.5,2.5,0,0,0-1.777,4.264l-0.013.019L500,145.1l5.179-4.749c0.042-.039.086-0.075,0.126-0.117l0.052-.047-0.006-.008A2.494,2.494,0,0,0,503.5,135.993Z" transform="translate(-492 -134)"></path></svg></i></span>
              <span title="В отложенных" data-quantity="1" class="wish_item in added rounded3 colored_theme_bg" style="display: none;" data-item="31386" data-iblock="33"><i class="svg inline  svg-inline-wish ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="13" viewBox="0 0 16 13"><defs><style>.clsw-1{fill:#fff;fill-rule:evenodd;}</style></defs><path class="clsw-1" d="M506.755,141.6l0,0.019s-4.185,3.734-5.556,4.973a0.376,0.376,0,0,1-.076.056,1.838,1.838,0,0,1-1.126.357,1.794,1.794,0,0,1-1.166-.4,0.473,0.473,0,0,1-.1-0.076c-1.427-1.287-5.459-4.878-5.459-4.878l0-.019A4.494,4.494,0,1,1,500,135.7,4.492,4.492,0,1,1,506.755,141.6Zm-3.251-5.61A2.565,2.565,0,0,0,501,138h0a1,1,0,1,1-2,0h0a2.565,2.565,0,0,0-2.506-2,2.5,2.5,0,0,0-1.777,4.264l-0.013.019L500,145.1l5.179-4.749c0.042-.039.086-0.075,0.126-0.117l0.052-.047-0.006-.008A2.494,2.494,0,0,0,503.5,135.993Z" transform="translate(-492 -134)"></path></svg></i></span>
            </div>
            <div class="fast_view_button">
              <span title="Быстрый просмотр" class="rounded3 colored_theme_hover_bg" data-event="jqm" data-param-form_id="fast_view" data-param-iblock_id="33" data-param-id="31386" data-param-item_href="https://shop.mgnl.ru/catalog/shaurma-s-kuritsey-magnoliya-210g/" data-name="fast_view"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><path data-name="Ellipse 302 copy 3" class="cls-1" d="M549,146a8.546,8.546,0,0,1-8.008-6,8.344,8.344,0,0,1,16.016,0A8.547,8.547,0,0,1,549,146Zm0-2a6.591,6.591,0,0,0,5.967-4,7.022,7.022,0,0,0-1.141-1.76,4.977,4.977,0,0,1-9.652,0,7.053,7.053,0,0,0-1.142,1.76A6.591,6.591,0,0,0,549,144Zm-2.958-7.246c-0.007.084-.042,0.159-0.042,0.246a3,3,0,1,0,6,0c0-.087-0.035-0.162-0.042-0.246A6.179,6.179,0,0,0,546.042,136.753Z" transform="translate(-541 -134)"></path></svg></i></span>
            </div>
          </div>
          <a href="https://shop.mgnl.ru/catalog/shaurma-s-kuritsey-magnoliya-210g/" class="thumb shine">
            <span class="section-gallery-wrapper flexbox">
              <span class="section-gallery-wrapper__item _active">
                <span class="section-gallery-wrapper__item-nav section-gallery-wrapper__item_hidden "></span>
                <img class="lazy img-responsive" src="/upload/iblock/9e7/9e7dec04dd138f7870a98dbc2f03d185.jpeg" data-src="/upload/iblock/9e7/9e7dec04dd138f7870a98dbc2f03d185.jpeg" alt="Шаурма с курицей Магнолия  210г" title="Шаурма с курицей Магнолия  210г">
              </span>
            </span>
          </a>
        </div>
        <div class="item_info">
          <div class="item-title" style="height: 96px;">
            <a href="https://shop.mgnl.ru/catalog/shaurma-s-kuritsey-magnoliya-210g/" class="dark_link option-font-bold font_sm"><span>Шаурма с курицей Магнолия  210г</span></a>
          </div>
          <div class="cost prices clearfix" style="height: 66px;">
            <div class="price_matrix_wrapper ">
              <div class="price font-bold font_mxs" data-currency="RUB" data-value="99">
                <span class="values_wrapper"><span class="price_value">99</span><span class="price_currency"> руб.</span></span><span class="price_measure">/шт</span>																	</div>
            </div>
          </div>
        </div>
        <div class="footer_button  inner_content js_offers__31386_block">
          <div class="counter_wrapp  clearfix rounded3">
            <div class="counter_block big " data-item="31386">
              <span class="minus dark-color" id="bx_3966226736_31386_quant_down"><i class="svg inline  svg-inline-wish ncolor colored1" aria-hidden="true"><svg width="11" height="1" viewBox="0 0 11 1"><rect width="11" height="1" rx="0.5" ry="0.5"></rect></svg></i></span>
              <input type="text" class="text" id="bx_3966226736_31386_quantity" name="quantity" value="1">
              <span class="plus dark-color" id="bx_3966226736_31386_quant_up"><i class="svg inline  svg-inline-wish ncolor colored1" aria-hidden="true"><svg width="11" height="11" viewBox="0 0 11 11"><path d="M1034.5,193H1030v4.5a0.5,0.5,0,0,1-1,0V193h-4.5a0.5,0.5,0,0,1,0-1h4.5v-4.5a0.5,0.5,0,0,1,1,0V192h4.5A0.5,0.5,0,0,1,1034.5,193Z" transform="translate(-1024 -187)"></path></svg></i></span>
            </div>
            <div id="bx_3966226736_31386_basket_actions" class="button_block ">
              <!--noindex-->
              <span data-value="99" data-currency="RUB" class="btn-exlg to-cart btn btn-default transition_bg animate-load has-ripple" data-item="31386" data-float_ratio="1" data-ratio="1" data-bakset_div="bx_basket_div_31386" data-props="" data-part_props="N" data-add_props="Y" data-empty_props="Y" data-offers="" data-iblockid="33" data-quantity="1"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true" title="В корзину"><svg class="" width="19" height="16" viewBox="0 0 19 16"><path data-name="Ellipse 2 copy 9" class="cls-1" d="M956.047,952.005l-0.939,1.009-11.394-.008-0.952-1-0.953-6h-2.857a0.862,0.862,0,0,1-.952-1,1.025,1.025,0,0,1,1.164-1h2.327c0.3,0,.6.006,0.6,0.006a1.208,1.208,0,0,1,1.336.918L943.817,947h12.23L957,948v1Zm-11.916-3,0.349,2h10.007l0.593-2Zm1.863,5a3,3,0,1,1-3,3A3,3,0,0,1,945.994,954.005ZM946,958a1,1,0,1,0-1-1A1,1,0,0,0,946,958Zm7.011-4a3,3,0,1,1-3,3A3,3,0,0,1,953.011,954.005ZM953,958a1,1,0,1,0-1-1A1,1,0,0,0,953,958Z" transform="translate(-938 -944)"></path></svg></i><span>В корзину</span><span class="ripple ripple-animate" style="height: 127px; width: 127px; animation-duration: 0.6s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -17.5938px; left: 25.3438px;"></span></span><a rel="nofollow" href="/basket/" class="btn-exlg in-cart btn btn-default transition_bg" data-item="31386" style="display:none;"><i class="svg inline  svg-inline-fw ncolor colored" aria-hidden="true" title="В корзине"><svg xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18"><path data-name="Rounded Rectangle 906 copy 3" class="cls-1" d="M1005.97,4556.22l-1.01,4.02a0.031,0.031,0,0,0-.01.02,0.87,0.87,0,0,1-.14.29,0.423,0.423,0,0,1-.05.07,0.7,0.7,0,0,1-.2.18,0.359,0.359,0,0,1-.1.07,0.656,0.656,0,0,1-.21.08,1.127,1.127,0,0,1-.18.03,0.185,0.185,0,0,1-.07.02H993c-0.03,0-.056-0.02-0.086-0.02a1.137,1.137,0,0,1-.184-0.04,0.779,0.779,0,0,1-.207-0.08c-0.031-.02-0.059-0.04-0.088-0.06a0.879,0.879,0,0,1-.223-0.22s-0.007-.01-0.011-0.01a1,1,0,0,1-.172-0.43l-1.541-6.14H988a1,1,0,1,1,0-2h3.188a0.3,0.3,0,0,1,.092.02,0.964,0.964,0,0,1,.923.76l1.561,6.22h9.447l0.82-3.25a1,1,0,0,1,1.21-.73A0.982,0.982,0,0,1,1005.97,4556.22Zm-7.267.47c0,0.01,0,.01,0,0.01a1,1,0,0,1-1.414,0l-2.016-2.03a0.982,0.982,0,0,1,0-1.4,1,1,0,0,1,1.414,0l1.305,1.31,4.3-4.3a1,1,0,0,1,1.41,0,1.008,1.008,0,0,1,0,1.42ZM995,4562a3,3,0,1,1-3,3A3,3,0,0,1,995,4562Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,995,4566Zm7-4a3,3,0,1,1-3,3A3,3,0,0,1,1002,4562Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,1002,4566Z" transform="translate(-987 -4550)"></path></svg></i><span>В корзине</span></a>

              <!--/noindex-->
            </div>
          </div>
          <div class="sku_props ce_cmp_hidden">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

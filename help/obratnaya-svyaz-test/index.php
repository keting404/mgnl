<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обратная связь");
?><span style="font-size: 14pt;">Уважаемые покупатели, просим вас ознакомиться с разделом "Часто задаваемые вопросы" для наиболее оперативного разрешения вашего вопроса. Если же вы не обнаружили в списке вопросов свою тематику, то вы можете воспользоваться формой обратной связи, заполнив соответствующие поля. Просим вас обязательно указывать какой раздел наиболее близок к вашему вопросу, выбор раздела возможен из списка, если ваш вопрос не соответствует ни одному из предложенных, выберете пункт "другое" и отправьте ваш вопрос. Наши операторы оперативно обработают ваш запрос в рабочее дни с 10-00 до 18-00.&nbsp;</span>

<?php
// echo '<pre>';
// print_r($_SERVER['HTTP_HOST']);
// echo '</pre>';


if (isset($_POST['c-form__send'])){

    $p_post = htmlspecialchars(trim($_POST['name']));
    $p_sity = htmlspecialchars(trim($_POST['sity']));
    $p_emal = htmlspecialchars(trim($_POST['emal']));
    $p_phone= htmlspecialchars(trim($_POST['phone']));
    $p_text = htmlspecialchars(trim($_POST['text']));

    $p_shop = htmlspecialchars(trim($_POST['shop'])); //Тип магазина
    $p_subject = htmlspecialchars(trim($_POST['subject'])); //Тема обращения

    if (empty($p_post)){
        $m_error .= 'Поле "Как вас зовут", обзательно дя заполнения';
    }

    if (empty($p_emal)){
        $m_error .= 'Поле "Email", обзательно дя заполнения';
    }

    if (empty($p_text)){
        $m_error .= 'Поле "Обращение", обзательно дя заполнения';
    }

    // if (isset($_FILES['c-file--images']['error'])){

    //     if ($_FILES['c-file--images']['size'] == 0){
    //         $m_error .= 'Файл слишком большой'; 
    //     } else {

    //         $getMime = explode('.', $_FILES['c-file--images']['name']);
    //         $mime = strtolower(end($getMime));
    //         $types = array('jpg', 'png', 'gif', 'bmp', 'jpeg');

    //         if(!in_array($mime, $types)){
    //             $m_error .= 'Недопустимый тип файла.';
    //         }else {
    //             $name = mt_rand(0, 10000) .'-'. $_FILES['c-file--images']['name'];
    //             move_uploaded_file($_FILES['c-file--images']['tmp_name'], UPLOAD_PATH. $name);
    //             //$_POST['c-file--images'] =  '/App/upload/'.$name;
    //         }

    //     }
    
    // }

    if (empty($m_error)){
        require 'PHPMailer/PHPMailer.php';
        require 'PHPMailer/Exception.php';

        $mail = new PHPMailer\PHPMailer\PHPMailer();

        $mail->CharSet = "UTF-8";
        $mail->setFrom('order@shop.mgnl.ru', 'Интернет магазин Магнолия'); // Адрес самой почты и имя отправителя
        $mail->addAddress('polivanovde@gmail.com');  // Получатель письма

        if (!empty($_FILES['myfile']['name'][0])) {
            for ($ct = 0; $ct < count($_FILES['myfile']['tmp_name']); $ct++) {
                $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['myfile']['name'][$ct]));
                $filename = $_FILES['myfile']['name'][$ct];
                if (move_uploaded_file($_FILES['myfile']['tmp_name'][$ct], $uploadfile)) {
                    $mail->addAttachment($uploadfile, $filename);
                } else {
                    $msg .= 'Неудалось прикрепить файл ' . $uploadfile;
                }
            }   
        }

        $mail->isHTML(true);
        $mail->Subject = $p_subject;
        $mail->Body    = "<style>body{font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 14px;color: #000;}</style>
        <table style='background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;' width='850' cellspacing='0' cellpadding='0' bordercolor='#d1d1d1' border='1'>
        <tbody>
        <tr>
            <td style='border: none; padding-top: 0px; padding-right:0px; padding-bottom: 0px; padding-left: 0px;' width='850' height='83' bgcolor='#FFF'>
                <table width='100%' cellspacing='0' cellpadding='0'>
                <tbody>
                <tr>
                    <td width='160' align='left'> <img src='http://shop.mgnl.ru/upload/email_send/header-left.jpg' alt=''> </td>
                    <td style='text-align:center;' width='530' align='center'> <img src='http://shop.mgnl.ru/upload/email_send/header-logo.png' alt=''> </td>
                    <td width='160' align='right'> <img src='http://shop.mgnl.ru/upload/email_send/header-right.jpg' alt=''> </td>
                </tr>
                <tr>
                    <td colspan='3' style='font-weight: bold; text-align: center; font-size: 26px; color: #9cae2f;' height='75' bgcolor='#ffffff'> Изменение статуса заказа в магазине&nbsp;#SITE_NAME# </td>
                </tr>
                <tr><td colspan='3' height='11' bgcolor='#9cae2f'><br></td></tr>
                </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style='border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;' width='850' valign='top' bgcolor='#f7f7f7'>
        <br>
                Информационное сообщение сайта #SITE_NAME#<br>
                ------------------------------------------<br>
        <br>
                <p style='margin-top: 0; margin-bottom: 20px; line-height: 20px;'>
                    Имя: $p_post<br>
                    Город: $p_sity<br>
                    Электронная почта: $p_email<br>
                    Телефон: $p_phone<br>
                    Обращение по магазину: $p_shop<br>
                    Тема обращения: $p_subject<br>
                    <br>
                    Текст обращения:<br> $p_text<br>
                </p>
                
        <br>
            </td>
        </tr>
        <tr>
            <td style='border: none; padding-top: 35px; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;' width='850' valign='top' height='40px' bgcolor='#6f6f6f'>
                <table width='100%' cellspacing='0' cellpadding='0'>
                <tbody>
                <tr>
                    <td width='160'><img src='http://shop.mgnl.ru/upload/email_send/footer-logo.png' alt=''></td>
                    <td style='text-align:center;' width='530' align='center'>
                    </td>
                    <td width='160'>
                        <table width='100%' cellspacing='0' cellpadding='0'>
                        <tbody>
                        <tr>
                            <td><a href='https://www.facebook.com/magnoliya.store/' target='_blank'><img src='http://shop.mgnl.ru/upload/email_send/footer_facbook.png' alt=''></a></td>
                            <td><a href='https://www.instagram.com/magnoliya.store/' target='_blank'><img src='http://shop.mgnl.ru/upload/email_send/footer_inst.png' alt=''></a></td>
                            <td><a href='https://vk.com/' target='_blank'><img src='http://shop.mgnl.ru/upload/email_send/footer_vk.png' alt=''></a></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <table style='border: none; padding-top: 20px; padding-right: 0x; padding-bottom: 30px; padding-left: 0px;' width='100%' cellspacing='0' cellpadding='0'>
                <tbody>
                <tr>
                    <td width='450'><p style='color:#FFF;font-size:12px;'>Прием звонков с 9:00 до 19:00</p></td>
                    <td style='color:#FFF;' width='100' align='center'><p>© <a href='http://http://shop.mgnl.ru' style='color:#FFF;'>mgnl.ru</a> 2020</p></td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>";

        if ($mail->send()) { $m_s = "Сообщение было отправлено."; } else { $m_error = "Сообщение не было отправлено. Причина ошибки: {$mail->ErrorInfo}"; }
    }

}

?>

<?php if (!empty($m_error)){ ?>
    <div class="c_form--error">
        <?php echo $m_error;?>
    </div>
<? } ?>

<?php if (!empty($m_s)){ ?>
    <div class="c_form--s">
        <?php echo $m_s;?>
    </div>
<? } ?>
<form method="POST" id="sendform" action="" accept-charset="UTF-8" enctype="multipart/form-data" class="c-form">
    <div id="result_form"></div>
    <fieldset class="c-form__fieldset">
        <legend class="c-form__fieldset-title">Личные данные</legend>
        <div class="c-form__fieldset-body">
            <div class="c-form__row">
                <div class="c-form__col">
                    <label for="queryName" class="c-form__label">Как вас зовут
                        <span class="c-form__required-mark"></span>
                    </label>
                    <input data-required="true" id="queryName" name="name" type="text" class="c-form__input" required>
                </div>
                <div class="c-form__col">
                    <label for="querySity" class="c-form__label">Город
                        <span class="c-form__required-mark"></span>
                    </label>
                    <input data-required="true" id="querySity" name="sity" type="text" class="c-form__input">
                </div>
            </div>

            <div class="c-form__row">
                <div class="c-form__col">
                    <label for="queryEmail" class="c-form__label">Электронная почта
                        <span class="c-form__required-mark"></span>
                    </label>
                    <input data-required="true" id="queryEmail" name="emal" type="email" class="c-form__input" required>
                </div>
                <div class="c-form__col">
                    <label for="queryPhone" class="c-form__label">Телефон
                        <span class="c-form__required-mark"></span>
                    </label>
                    <input data-required="true" id="queryPhone" name="phone" type="text" class="c-form__input" placeholder="+7 (___) ___ ____" maxlength="17">
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="c-form__fieldset">
        <legend class="c-form__fieldset-title">Параметры</legend>

        <div class="c-form__fieldset-body">
            <div class="c-form__row">
                <div class="c-form__col w100">
                    <label for="queryShop" class="c-form__label">
                        Выберите магазин, по работе которого вы хотели бы создать обращение
                        <span class="c-form__required-mark"></span>
                    </label>
                    <select class="c-fotm__select" id="queryShop" name="shop" required>
                        <option value="Розничный магазин">Розничный магазин</option>
                        <option value="Онлайн-супермаркет">Онлайн-супермаркет</option>
                    </select>
                </div>
            </div>
            <div class="c-form__row">
                <div class="c-form__col w100">
                    <label for="querySubject" class="c-form__label">
                        Тема сообщения
                        <span class="c-form__required-mark"></span>
                    </label>
                    <select class="c-fotm__select" id="Subject" name="subject" required>
                        <option value="Мой вопрос касается работы сайта (не могу зарегистрироваться, сменить пароль и прочие технические вопросы по работе сайта)">Мой вопрос касается работы сайта (не могу зарегистрироваться, сменить пароль и прочие технические вопросы по работе сайта)</option>
                        <option value="Мой вопрос касается начисления бонусных баллов от покупок онлайн">Мой вопрос касается начисления бонусных баллов от покупок онлайн</option>
                        <option value="Мой вопрос касается возврата или обмена товаров (качества товаров)">Мой вопрос касается возврата или обмена товаров (качества товаров)</option>
                        <option value="Мой вопрос касается состава доставленного заказа (расхождение в цене, весе)">Мой вопрос касается состава доставленного заказа (расхождение в цене, весе)</option>
                        <option value="Мой вопрос касается доставки моего заказа">Мой вопрос касается доставки моего заказа</option>
                        <option value="Другая тематика">Другая тематика</option>
                    </select>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="c-form__fieldset">
        <legend class="c-form__fieldset-title">Обращение</legend>

        <div class="c-form__fieldset-body">
            <div class="c-form__row">
                <div class="c-form__col w100">
                    <label for="queryText" class="c-form__label">
                        Ваше обращение
                        <span class="c-form__required-mark"></span>
                    </label>
                    <textarea id="queryText" class="c-form__input --textarea" data-required="true" placeholder="Опишите интересующий вас вопрос" name="text" cols="50" rows="10"></textarea>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="c-form__block">
        <input type="checkbox" name="agree_with_terms" value="1" id="agree_with_terms" data-required="true" required="required" class="c-form__checkbox">
        <label for="agree_with_terms" class="c-form__checkbox-label">
            Cогласие на&nbsp;<a href="/include/licenses_detail.php" >обработку моих персональных данных</a>
        </label> 
    </div>
			
    <div class="c-file-upload">
        <input name="myfile[]" multiple id="myfile" type="file">
    </div>
    <div class="c-query-form__submit">
	    <input type="submit" name="c-form__send" value="Отправить" class="c-query-form__submit-button">
	    <div class="c-query-form__submit-text">Чтобы прикрепить изображение к обращению, нажмите «выбрать файл» или&nbsp;перетащите файлы на эту область. Размер файла должен быть <span>меньше 2MB</span>. Всего не более 5 файлов</div>
    </div>

</form>


<style>
.c-form {
    margin-top:20px;
}
.c-form__row {
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;
}
.c-form__col {
    display: flex;
    flex-basis: 48%;
    width: 48%;
    flex-direction: column;
}

.c-form__label {
    font-size: 16px;
    margin-bottom: 5px;
}
.c-form__input {
    padding: 10px;
}

.c-form__col.w100{
    flex-basis: 100%;
    width: 100%;
}
.c-form__fieldset {
    margin-top: 20px;
}

.c-form__input.\--textarea{
    min-height: 200px;
}

.c-query-form__submit {
    display: flex;
    justify-content: space-between;
}
.c-query-form__submit-button {
    background-color: #92a93f;
    flex-basis: 30%;
    text-align: center;
    align-items: center;
    display: flex;
    justify-content: center;
    color: #FFF;
    border: 0px;
}
.c-query-form__submit-text {
    flex-basis: 67%;
}
.c-file-upload {
    margin: 20px 0px;
}
.c-file-upload__wrap {
    display: flex;
    padding: 10px;
    border: 1px solid #c7c7c7;
    align-items: center;
}
#fileapi__choose {
    flex-basis: 23%;
    color: #FF;
}
.c-file-upload__btn-text {
    display: flex;
    align-items: center;
    padding: 10px;
    border: 1px solid #c7c7c7;
    border-radius: 3px;
}
.c-file-upload__btn-img{
    max-width:35px;
    margin-left:0px;
    margin-right:10px;
}
.c-file-upload__text{
    flex: 1;
    padding-left: 10px;
}
.c-file-upload__btn input{
    display:none;
}

.c_form--error{
    padding: 10px;
    border: 1px solid #fcdde2;
    color: #fcdde2;
    margin-top: 10px;
}

.c_form--s{
    padding: 10px;
    border: 1px solid #92a93f;
    color: #92a93f;
    margin-top: 10px;
}
</style>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


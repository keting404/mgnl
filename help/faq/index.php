<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ответы на популярные вопросы");
?><div class="col-md-12">
	<h2>Регистрация на сайте</h2>
	<div class="accordion-type-2">
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion4" href="#accordion1">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b><span style="color: #464646;"><span style="color: #363636;">Для того чтобы зарегистрироваться на сайте</span><span style="color: #363636;"> </span></span></b><span style="color: #363636;">: </span>
			</div>
			<div id="accordion1" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
					</p>
					<ul>
						<li>
						<p>
							 Воспользуйтесь формой регистрации (<a href="/auth/registration/?register=yes&backurl=/help/faq/">адрес по ссылке</a>)
						</p>
 </li>
						<li>
						<p>
							 Заполните необходимые для регистрации нового пользователя поля и нажмите на кнопку <span style="color: #588528;">«Зарегистрироваться». </span>
						</p>
 </li>
					</ul>
					<p>
					</p>
					<p>
						 Мы рекомендуем Вам пройти регистрацию на сайте и оставить актуальные контактные данные, для того, чтобы при отправке заказа мы смогли до Вас дозвониться и уточнить необходимую информацию. Зарегистрированный пользователь интернет - магазина «Магнолия» может отслеживать историю своих заказов, получать бонусы за покупки, отслеживать историю своих покупок в личном кабинете на сайте.
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion5" href="#accordion2">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><span style="color: #588528;"><b><span style="color: #464646;">Для того, чтобы изменить личные данные: </span></b></span>
			</div>
			<div id="accordion2" class="panel-collapse collapse">
				<div class="accordion-body">
					 Зайдите в свой личный кабинет, в интересующем вас разделе внесите нужные изменения и не забудьте сохранить, нажав на соответствующую кнопку. <br>
					 При восстановлении пароля вам на почту придет ссылка на восстановление пароля, пройдите по ссылке и смените ваш пароль.&nbsp;<br>
					 Если вы не видите письмо, просим вас проверить раздел "Спам" в вашем почтовом ящике.&nbsp;<br>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion6" href="#accordion4">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b><span style="color: #464646;">Если вы забыли пароль :</span></b>
			</div>
			<div id="accordion4" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 При входе в личный кабинет выберете поле «восстановление пароля», для того, чтобы данный шаг прошел успешно, мы всегда рекомендуем оставлять правильные почтовые адреса и номера телефонов при регистрации личного кабинета, для того, чтобы мы могли идентифицировать вас в случае утери вами данных о логине и пароле. (<a href="/auth/forgot-password/?forgot-password=yes&backurl=/?login=yes">адрес ссылки</a>)
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion6" href="#accordion3">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b><span style="color: #464646;">При входе в личный кабинет написано, что логин не существует, пароль не верный, отправлен запрос на новый пароль, но на почту так ничего и не пришло. Как быть?</span></b>
			</div>
			<div id="accordion3" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 В таком случае мы рекомендуем проверить вашу почту, оставленную при регистрации в разделе "спам". Если же и там ничего не обнаружилось, обращайтесь по телефону: <a href="tel:8-495-921-32-36">8-495-921-32-36</a> добавочный 1278 или напишите нам на почту: <a href="mailto:order@mgnl.ru">order@shop.mgnl.ru</a> . Обязательно укажите данные, которые вы указали при регистрации вашего личного кабинета и мы поможем вам быстро восстановить ваши доступы.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<div class="col-md-12">
	<h2>Выбор товара и оформление на сайте</h2>
	<div class="accordion-type-2">
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion6" href="#accordion6">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><span style="color: #588528;"><b>Чтобы найти интересующий Вас товар, воспользуйтесь Поиском или Каталогом.</b></span>
			</div>
			<div id="accordion6" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 Выберите интересующие Вас товары, положите их в Корзину и перейдите к Оформлению Заказа. Все выбранные Вами товары необходимо поместить в Корзину для дальнейшего оформления заказа.
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion6" href="#accordion7">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><span style="color: #588528;"><b>Оформление корзины интернет - магазина: </b></span>
			</div>
			<div id="accordion7" class="panel-collapse collapse">
				<div class="accordion-body">
					 Обратите Ваше внимание, что при оформлении заказа нужно обязательно выбрать адрес доставки из предлагаемого радиуса доставки, вскоре он будет расширен.
					<p>
					</p>
					<p>
						 Обязательно нужно указать Ваши актуальные данные, чтобы курьер смог вам дозвониться, а магазин уточнить данные по заказу. Выберите вариант доставки. Курьером или самовывозом, не забудьте указать в комментарии особенности по доставке, если они есть. (Например, более удобное время получения доставки, особенности въезда на территорию доставки и т.д.).
					</p>
					<p>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<div class="col-md-12">
	<h2>Доставка</h2>
	<div class="accordion-type-2">
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion4" href="#accordion8">
 <span style="color: #588528;"><b> </b></span><span class="arrow_open pull-right colored_theme_hover_bg-el"></span><span style="color: #588528;"><b>Как доставляются охлажденные или замороженные продукты? </b></span>
			</div>
			<div id="accordion8" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 Мы собираем заказ перед самой его отправкой, придерживаясь инструкций по хранению каждого продукта, мы создаем необходимый температурный режим для каждой категории товара. Поэтому замороженные или охлажденные продукты перевозятся в контейнерах с определенным температурным режимом.
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion5" href="#accordion9">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b>Какие варианты доставки: </b>
			</div>
			<div id="accordion9" class="panel-collapse collapse">
				<div class="accordion-body">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/faq_detail_delivery.php", array(), array("MODE" => "html", "NAME" => GetMessage('TITLE_DELIVERY')));?>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<div class="col-md-12">
	<h2>Возврат</h2>
	<div class="accordion-type-2">
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion4" href="#accordion10">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b><span style="color: #588528;">Если вы хотите оформить возврат</span></b><span style="color: #588528;">: </span>
			</div>
			<div id="accordion10" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 Покупатели интернет-магазина «Магнолия» вправе отказаться от заказа товара в любое время до его передачи, а после передачи товара - в течение семи дней.
					</p>
					<p>
 <b>
						Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид и потребительские свойства.</b> В соответствии с Постановлением Правительства РФ № 55 от 19.01.1998 (в ред. от 28.01.2019) не подлежат возврату или обмену на аналогичный товар следующие группы непродовольственных товаров надлежащего качества: средства гигиены полости рта, предметы по уходу за детьми, предметы личной гигиены, парфюмерно-косметические товары, текстильные товары, швейные и трикотажные изделия, товары бытовой химии, технически сложные товары бытового назначения, на которые установлены гарантийные сроки.
					</p>
					<p>
						 В случае получения в оплаченном заказе любого товара ненадлежащего качества, «Магнолия» производит в полном объеме возврат денег за такой товар. Для возврата денег за товары надлежащего и ненадлежащего качества покупателю следует обратиться с письменным заявлением на почту <a href="mailto:order@mgnl.ru">order@shop.mgnl.ru</a>.
					</p>
					<p>
						 В заявлении следует указать номер и дату заказа, перечень приобретенных товаров на возврат и причину возврата. Заявления от покупателей подлежат рассмотрению и удовлетворению в срок не более 10 дней с даты подачи заявления.
					</p>
					<p>
						 Возврат товара и денег производится в магазине, который выполнял Ваш заказ. Форма возврата денег за товар зависит от способа оплаты исходного заказа. Возврат средств за заказы, оплаченные банковской картой, производится в день возврата товара переводом его стоимости на карту покупателя, которой производилась оплата заказа. Срок зачисления денег на карту покупателя зависит от Банка эмитента карты покупателя и обычно не превышает 3-х рабочих дней с даты перевода денег.
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion5" href="#accordion11">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><b><span style="color: #588528;">Замена товара : </span></b>
			</div>
			<div id="accordion11" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 При нехватке тех или иных товарных позиций заказа в магазине, мы готовы заменить товар на иной, который имеется в наличии и соответствует запрашиваемым требованиям, при этом, перед отправкой заказа мы обязательно согласуем с вами изменения в заказе, от которых вы вправе отказаться при оформлении заказа. При изменениях в вашем заказе мы обязательно позвоним вам для согласования изменения позиций, либо свяжемся с вами по другим удобным каналам связи, которые вы оставите при оформлении заказа.
					</p>
				</div>
			</div>
		</div>
		<div class="item-accordion-wrapper bordered box-shadow">
			<div class="accordion-head colored_theme_hover_bg-block accordion-close font_md" data-toggle="collapse" data-parent="#accordion5" href="#accordion12">
 <span class="arrow_open pull-right colored_theme_hover_bg-el"></span><span style="color: #588528;"><b>Различие в цене : </b></span>
			</div>
			<div id="accordion12" class="panel-collapse collapse">
				<div class="accordion-body">
					<p>
						 Фактическая стоимость заказа в интернет - магазине может отличаться от предварительной в случае:
					</p>
					<li>Замены или отсутствия некоторых товаров. </li>
					<li> Присутствия в заказе весовых товаров. На сайте указана типичная цена товаров, продаваемых на вес, которая может быть меньше или больше фактической </li>
					<p>
						 При приеме заказа, пожалуйста, проверьте его комплектность по накладным документам.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
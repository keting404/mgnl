<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как купить");
?><div>
 <span style="color: #464646; font-size: 20pt;">«Условия доставки продуктов»</span><span style="font-size: 20pt;"> <br>
 </span><br>
</div>
 <span style="font-size: 13pt;">Уважаемые покупатели, перед осуществлением заказа, обязательно укажите адрес доставки, по которому вы планируете получение покупки. После чего система автоматически определит ближайший к вам адрес интернет-магазина "Магнолия".</span><span style="color: #464646;"><br>
 </span> <span style="color: #464646;"> </span><br>
 <span style="color: #464646;"> </span><span style="color: #588528;"><br>
 <span style="font-size: 20pt;"> </span><span style="color: #464646; font-size: 20pt;"> </span></span><span style="color: #464646; font-size: 20pt;"> </span><span style="color: #464646; font-size: 20pt;"> </span><span style="color: #464646; font-size: 20pt;"> </span><span style="font-size: 20pt;"> </span>
<p>
 <span style="font-size: 20pt;"> </span><span style="color: #464646; font-size: 20pt;"> </span><span style="color: #626262; font-size: 20pt;">Доставка продуктов производится ежедневно, без выходных.</span><span style="font-size: 15pt;"> </span>
</p>
 <span style="font-size: 15pt;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt;"> </span>
<ul>
	<li><span style="font-size: 13pt;">Общий вес заказа не может превышать 35 кг.</span></li>
	<li><span style="font-size: 13pt;"><b>Заказы, оформленные до 18:00 доставят сегодня</b>, заказы оформленные после 18:00 доставят на следующий день. Вы можете самостоятельно выбрать наиболее подходящий интервал для доставки.<br>
 </span></li>
	<li><span style="font-size: 13pt;">В случае отсутствия лифта подъем на этаж осуществляется до 5 этажа.</span></li>
	<li><span style="font-size: 13pt;">Стоимость доставки 99 рублей в пределах МКАД и 199 рублей за пределами МКАД. При заказе от 3000 руб. - бесплатно!<br>
 </span></li>
	<li><span style="font-size: 13pt;">При получении заказа просим вас проверять товары по накладной, при этом, обеспечьте&nbsp; курьеру возможность контроля над процессом сверки во избежание спорных ситуаций.</span></li>
	<li><span style="font-size: 13pt;">Самовывоз заказов доступен при сумме покупки от 1000 рублей.&nbsp;</span></li>
</ul>
<h3><span style="color: #464646; font-size: 20pt;">«Бонусная карта»</span><span style="color: #464646;"> </span></h3>
 <span style="color: #464646;"> </span>
<p>
 <span style="color: #464646;"> </span><span style="color: #464646; font-size: 13pt;">Оформляя заказы в магазине «Магнолия» вы можете накапливать бонусные баллы за покупки, указывая 16 - ти значный номер карты, а в случае его отсутствия штрих-код карты лояльности.</span><span style="color: #464646; font-size: 16pt;"><span style="font-size: 13pt;">&nbsp;В специальном поле введите штрих-код карты(или её номер на лицевой стороне карты), нанесенный на оборотной стороне карты постоянного покупателя «Магнолия». По правилам бонусной программы «Магнолия» начисляется :</span> </span><span style="font-size: 16pt;"> </span>
</p>
 <span style="font-size: 16pt;"> </span><span style="color: #464646; font-size: 16pt;"> </span><span style="font-size: 16pt;"> </span>
<ul>
	<li style="color: #464646;"><span style="font-size: 16pt;"><span style="font-size: 13pt;">от 1000 руб. и больше - начисляется 4% бонусов! При этом 1рубль = 1бонусу!<br>
 </span></span></li>
</ul>
<div>
	 *Списание бонусных баллов в интернет-магазине невозможно. Вы можете потратить свои бонусные баллы&nbsp;на покупки в розничных магазинах сети "Магнолия"
</div>
<div>
 <br>
</div>
 <span style="color: #464646;">
<h3><span style="font-size: 20pt;">«</span><span style="font-size: 20pt;">Процесс получения заказа»</span></h3>
 </span><span style="color: #464646; font-size: 15pt;"> </span> <span style="font-size: 15pt;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt;"> </span>
<ol>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Вы оформили заказ в интернет – магазине </span></span><a href="http://www.shop.mgnl.ru"><span style="font-size: 15pt;"><span style="font-size: 13pt;">www.shop.mgnl.ru</span></span></a><span style="font-size: 15pt;"><span style="font-size: 13pt;"> </span></span><br>
 <span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span><span style="font-size: 15pt;"><span style="font-size: 13pt;"> </span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Вы произвели оплату вашего заказа онлайн на сайте&nbsp;</span></span><a href="http://www.shop.mgnl.ru/"><span style="font-size: 15pt;"><span style="font-size: 13pt;">www.shop.mgnl.ru</span></span></a><span style="font-size: 15pt;"><span style="font-size: 13pt;">&nbsp;, ваш заказ отправлен на сборку в магазин «Магнолия»</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">При возникновении необходимости уточнения по вашему заказу, с вами связывается сотрудник магазина «Магнолия»</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Вам привозят заказ</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Вы проверяете заказ ( при этом обеспечьте возможность курьеру видеть процесс проверки заказа, во избежание разногласий ).</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">При возникновении расхождений вы имеете право отказаться от заказа прямо в момент его получения.</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">По факту оплаты заказа он будет доставлен покупателю с учетом его пожеланий, оставленных в комментарии при оформлении заказа в корзине интернет - магазина.</span></span></li>
	<li style="color: #464646;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Если вы не сможете принять заказ в обозначенное время, то у вас есть возможность отменить его или выбрать другое время доставки продуктов из интернет - магазина. Для этого, пожалуйста, напишите нам в чат на сайте, наш оператор ответит вам в кротчайшие сроки. Или напишите нам на почту: </span></span><a href="mailto:>order@mgnl.ru"><span style="font-size: 15pt;"><span style="font-size: 13pt;">order@mgnl.ru</span></span></a><span style="font-size: 15pt;"><span style="font-size: 13pt;">.</span></span></li>
	<li style="color: #464646;"><span style="font-size: 13pt;">Есть случаи, при которых курьер в праве потребовать предъявления документа удостоверяющего личность получателя покупки. Например, если в заказе содержится товар, предназначенный только для граждан РФ старше 18 лет.</span></li>
</ol>
 <span style="color: #464646;"> </span>
<h3><span style="color: #464646;"> </span><span style="color: #464646; font-size: 20pt;">«Замена товара»</span><span style="color: #464646;"> </span></h3>
 <span style="color: #464646;"> </span>
<p>
 <span style="color: #464646;"> </span><span style="color: #464646; font-size: 13pt;">При нехватке тех или иных товарных позиций заказа в магазин</span><span style="color: #464646; font-size: 13pt;">е, мы готовы заменить товар на иной, который имеется в наличии и соответствует запрашиваемым требованиям, при этом, перед отправкой заказа мы обязательно согласуем с вами изменения в заказе, от которых вы вправе отказаться при оформлении заказа.</span><span style="color: #464646;"><span style="font-size: 16pt;"><span style="font-size: 13pt;"> При изменениях в вашем заказе мы обязательно позвоним вам для согласования изменения позиций, либо свяжемся с вами по другим удобным каналам связи, которые вы оставите при оформлении заказа</span>.</span></span>
</p>
<ul>
</ul>
 <span style="color: #464646;"> </span>
<h3><span style="color: #464646;"> </span><span style="color: #464646; font-size: 20pt;">«Отмена заказа»:</span><span style="color: #464646;"> </span></h3>
 <span style="color: #464646;"> </span>
<p>
 <span style="color: #464646;"> <span style="font-size: 16pt;"><span style="font-size: 13pt;">Если наш оператор не может выйти с вами на связь по электронной почте или телефону в течение дня, то заказ подлежит расформированию. Если ваш заказ был заранее оплачен, но мы не смогли с вами связаться, отправка вашего заказа будет перенесена на другую ближайшую дату. В случае, если мы снова не смогли с вами связаться, ваш заказ будет расформирован, а денежные средства возвращены на вашу банковскую карту, с которой производилась оплата.</span></span></span>
</p>
<h3><span style="font-size: 20pt;">«Различие в цене»</span></h3>
<p>
 <span style="font-size: 15pt; color: #555555;"><span style="font-size: 13pt;">Фактическая стоимость заказа в интернет – магазине может отличаться от предварительной стоимости в случае: </span></span><span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span>
</p>
 <span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span><span style="font-size: 15pt; color: #555555;"><span style="font-size: 13pt;"> </span></span><span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span>
<p>
 <span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span><span style="font-size: 15pt; color: #555555;"><span style="font-size: 13pt;"> </span></span><span style="font-size: 15pt; color: #555555;"><span style="font-size: 13pt;"> </span></span><span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span>
</p>
 <span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span><span style="font-size: 15pt; color: #555555;"><span style="font-size: 13pt;"> </span></span><span style="font-size: 13pt;"> </span><span style="font-size: 13pt;"> </span>
<ul>
	<li style="color: #555555;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Замены или отсутствия некоторых товаров</span></span></li>
	<li style="color: #555555;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">Присутствия в заказе весовых товаров. На сайте указана типичная цена товаров, продаваемых на вес, которая может быть меньше или больше фактической</span></span></li>
	<li style="color: #555555;"><span style="font-size: 15pt;"><span style="font-size: 13pt;">При приеме заказа, пожалуйста, проверьте его комплектность по накладным документам.</span></span></li>
</ul><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
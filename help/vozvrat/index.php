<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Возврат");
?><span style="font-size: 18pt;"><span style="color: #464646; font-size: 16pt;"><b><img alt="121334.JPG" src="/upload/medialibrary/525/5250c98b901621d7567ee7449a64fc16.JPG" title="121334.JPG"><br>
</b></span></span><br>
<span style="font-size: 18pt;"><span style="color: #464646; font-size: 16pt;"><b>Е</b></span><span style="color: #464646; font-size: 16pt;"><b>сли вы хотите оформить возврат:</b></span><br>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span></span><br>
 <span style="font-size: 15pt; color: #464646;"> </span><br>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
<p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 16pt;"><span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;">Покупатели интернет-магазина «Магнолия» вправе отказаться от заказа товара в любое время до его передачи, а после передачи товара - в течение семи дней.</span><span style="font-size: 15pt; color: #464646;"> </span></span><span style="font-size: 15pt; color: #464646;"> </span>
</p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
<p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;">
	Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид и потребительские свойства.&nbsp;В соответствии с Постановлением Правительства РФ № 55 от 19.01.1998 (в ред. от 28.01.2019) не подлежат возврату или обмену на аналогичный товар следующие группы непродовольственных товаров надлежащего качества: средства гигиены полости рта, предметы по уходу за детьми, предметы личной гигиены, парфюмерно-косметические товары, текстильные товары, швейные и трикотажные изделия, товары бытовой химии, технически сложные товары бытового назначения, на которые установлены гарантийные сроки. </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
</p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
<p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 16pt;"><span style="color: #464646; font-size: 15pt;">
	В случае получения в оплаченном заказе любого товара ненадлежащего качества, «Магнолия» производит в полном объеме возврат денег за такой товар</span><span style="font-size: 15pt; color: #464646;">.</span><span style="color: #464646; font-size: 15pt;"> </span><span style="color: #464646; font-size: 15pt;">Для возврата денег за товары надлежащего и ненадлежащего качества покупателю следует обратиться с письменным заявлением на почту</span><span style="color: #464646; font-size: 15pt;">&nbsp;</span></span><a href="/upload/%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BD%D0%B0%20%D0%B2%D0%BE%D0%B7%D0%B2%D1%80%D0%B0%D1%82%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%B0.doc"><span style="font-size: 15pt; color: #464646;">order@mgnl.ru</span></a><span style="font-size: 16pt;"><span style="color: #464646; font-size: 15pt;">. Бланк заявления вы можете скачать </span><a href="https://shop.mgnl.ru/upload/%D0%97%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BD%D0%B0%20%D0%B2%D0%BE%D0%B7%D0%B2%D1%80%D0%B0%D1%82%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%B0.doc"><span style="color: #464646; font-size: 15pt;">по <span style="color: #588528;">ссылке</span></span></a></span><span style="font-size: 15pt; color: #464646;"> </span>
</p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
<p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="font-size: 16pt;"><span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;">В заявлении следует указать номер и дату заказа, перечень приобретенных товаров на возврат и причину возврата. Заявления от покупателей подлежат рассмотрению и удовлетворению в срок не более 10 дней с даты подачи заявления. </span></span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
</p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;"> </span>
<p>
 <span style="font-size: 15pt; color: #464646;"> </span><span style="color: #464646; font-size: 15pt;"> </span><span style="font-size: 15pt; color: #464646;">
	Возврат товара и денег производится в магазине, который выполнял Ваш заказ. Форма возврата денег за товар зависит от способа оплаты исходного заказа. Возврат средств за заказы, оплаченные банковской картой, производится в день возврата товара переводом его стоимости на карту покупателя, которой производилась оплата заказа. Срок зачисления денег на карту покупателя зависит от Банка эмитента карты покупателя и обычно не превышает 3-х рабочих дней с даты перевода денег.</span><span style="font-size: 16pt; color: #464646;"> </span><span style="color: #464646;"> </span>
</p>
<p>
	<span style="font-size: 15pt; color: #464646;"><img alt="121334.JPG" src="/upload/medialibrary/525/5250c98b901621d7567ee7449a64fc16.JPG" title="121334.JPG"><br>
	</span>
</p>
 <span style="color: #464646;"> </span><span style="font-size: 16pt; color: #464646;"> </span><br>
 <span style="font-size: 10pt;"> </span>
<p>
 <span style="font-size: 10pt;">
	&nbsp; </span>
</p>
 <span style="font-size: 10pt;"> </span>
<h2><span style="font-size: 10pt;">&nbsp;</span>&nbsp;</h2>
 <span style="font-size: 10pt;"> </span>
<h2 align="center"><span style="font-size: 10pt;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></h2>
 <span style="font-size: 10pt;"> </span><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
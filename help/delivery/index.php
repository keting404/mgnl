<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");
?><p>
 <b><span style="color: #555555;">Область доставки онлайн-магазина «Магнолия»</span></b>. Вскоре мы расширим область покрытия доставки! См карту:
</p>
 <?$APPLICATION->IncludeComponent(
	"ctweb:yandexdelivery",
	"ipnage",
	array(
	"MAP_HEIGHT" => "400px",	// Высота карты
		"MAP_WIDTH" => "100%",	// Ширина карты
		"MAP_ZOOM" => "14",	// Масштаб по-умолчанию
		"REGION_FILTER_NAME" => "",	// Фильтр регионов
		"REGION_OPACITY" => "0.3",	// Непрозрачность региона
		"RELOCATION" => "/personal/",	// Страница после авторизации
		"ROUTE_COLOR" => "4A84E3ff",	// Цвет дороги
		"ROUTE_OPACITY" => "0.8",	// Непрозрачность дороги
		"STORE_FILTER_NAME" => "",	// Фильтр складов
	)
);?>
<p>
 <a href="/help/faq/">Ответы на часто задаваемые вопросы</a><br>
</p>
<?$APPLICATION->IncludeFile(SITE_DIR."include/faq_detail_delivery.php", array(), array("MODE" => "html", "NAME" => GetMessage('TITLE_DELIVERY')));?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
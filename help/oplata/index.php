<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата");
?><span style="font-size: 14pt;">
<p style="text-align: left;">
 <b><span style="color: #363636;"><img width="100%" alt="121334.JPG" src="/upload/medialibrary/525/5250c98b901621d7567ee7449a64fc16.JPG" height="auto" title="121334.JPG"><br>
 </span></b>
</p>
<p style="text-align: left;">
 <b><span style="color: #363636;">В интернет-магазине "Магнолия" Вы можете оплатить свой заказ банковской картой через защищенный платежный шлюз ПАО «Сбербанк России».</span></b><span style="color: #363636;">&nbsp; </span>
</p>
 <span style="color: #363636;"> </span>
<p style="text-align: left;">
 <span style="color: #363636;"> </span>
</p>
<ul>
	<li style="color: #363636;">
	<p style="text-align: left;">
 <span style="text-align: left; font-size: 14pt;">К оплате принимаются банковские карты платежных систем VISA и MasterCard. Введенная информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. <br>
 </span>
	</p>
 </li>
	<li style="color: #363636;">
	<p style="text-align: left;">
 <span style="text-align: left; font-size: 14pt;">Списание средств с Вашей карты произойдет только после получения Вами заказа, до этого денежные средства резервируются на карте. <br>
 </span>
	</p>
 </li>
</ul>
<p style="text-align: left;">
 <span style="text-align: left; font-size: 14pt;"><span style="color: #363636;">Обратите внимание, что при получении заказа, оплаченного банковской карте в интернет-магазине "Магнолия" курьер при доставке заказа имеет право потребовать документ удостоверяющий личность, в случае отказа предъявить документ, заказ может быть аннулирован, а денежные средства возвращены покупателю. </span><br>
 </span>
</p>
<p style="text-align: left;">
 <span style="text-align: left; font-size: 14pt; color: #363636;">Возврат денежных средств на банковскую карту в случае полного или частичного отказа от покупки происходит в соответствии с Условиями предоставления услуг по разблокировке денежных средств, установленными Банком, выпустившим Вашу карту. Обратите внимание! Мы не принимаем к оплате банковские карты, выпущенные зарубежными банками за пределами Российской Федерации. Уважаемые покупатели! Обратите внимание, денежные средства при оформлении заказа резервируются на вашей карте, после согласования и изменения заказа (если это необходимо) с карты списывается фактическая стоимость заказа, а излишне зарезервированные денежные средства будут автоматически возвращены на вашу карту (согласно правилам ПАО "Сбербанк России). При этом Вы не получите дополнительного смс - уведомления о сумме не списанных средств, которые первоначально были зарезервированы на вашей карте.&nbsp;</span><br>
</p>
<p style="text-align: left;">
 <img width="100%" alt="баннер оплата-возврат.jpg" src="/upload/medialibrary/c8d/c8db14cd1336ee581ea868e4e294944a.jpg" title="баннер оплата-возврат.jpg">
</p>
 </span><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
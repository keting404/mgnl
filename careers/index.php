<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?><div class="wrapContent">
	<div class="whiteBg">
		<div class="textBox">
			<div class="textBoxImageSection">
                <div class="textBoxImageSectionBg" style="background-image: url('/upload/careers/prodavec.jpg')"></div>
                <div class="contentBox">
					<div class="contentBox">
						<div class="textBoxImageSectionContent">
							<p>
								 <? $APPLICATION->IncludeFile("/include/careers/1.php", Array(), Array("MODE" => "html")); ?>
							</p>
							<p>
								 <? $APPLICATION->IncludeFile("/include/careers/2.php", Array(), Array("MODE" => "html")); ?>
							</p>
							<p class="important">
								 <? $APPLICATION->IncludeFile("/include/careers/3.php", Array(), Array("MODE" => "html")); ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contentBox textBoxWrap">
			<div class="textBox">
 <img src="/upload/careers/11.jpg" alt="" align="right">
				<h3>
				<? $APPLICATION->IncludeFile("/include/careers/4.php", Array(), Array("MODE" => "html")); ?> </h3>
				<p>
					 <? $APPLICATION->IncludeFile("/include/careers/5.php", Array(), Array("MODE" => "html")); ?>
				</p>
			</div>
		</div>
		 <!--        <div class="contentBox">
            <div class="historyBox">
                <div class="historyTitle"><? $APPLICATION->IncludeFile("/include/careers/8.php", Array(), Array("MODE" => "html")); ?></div>
                <div class="historyImage">
                    <div class="historyText">
                        <div class="historyName"><? $APPLICATION->IncludeFile("/include/careers/9.php", Array(), Array("MODE" => "html")); ?></div>
                        <div class="historyAge"><? $APPLICATION->IncludeFile("/include/careers/10.php", Array(), Array("MODE" => "html")); ?></div>
                        <p>
                          
                            <? $APPLICATION->IncludeFile("/include/careers/11.php", Array(), Array("MODE" => "html")); ?>
                        </p>
                    </div>
                    <img src="<?php echo SITE_TEMPLATE_PATH; ?>/css/img/history1bg.jpg" alt="" class="historyImageBg" width="1170" height="700">
                    <img src="<?php echo SITE_TEMPLATE_PATH; ?>/css/img/history1.png" alt="" class="historyImageWay">
                </div>
                <div class="historyMobileText">
                    <p>
                     
                        <? $APPLICATION->IncludeFile("/include/careers/12.php", Array(), Array("MODE" => "html")); ?>
                    </p>
                </div>
            </div>
        </div>-->
	</div>
	<div class="contentBox">
		<div class="jobs">
			<div class="jobsTitle">
				 <? $APPLICATION->IncludeFile("/include/careers/7.php", Array(), Array("MODE" => "html")); ?>
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"careers", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "aspro_max_vacancy",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "arrows",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "PROP4",
			1 => "PROP3",
			2 => "PROP1",
			3 => "PROP2",
			4 => "PROP5",
			5 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "",
		"COMPONENT_TEMPLATE" => "careers",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
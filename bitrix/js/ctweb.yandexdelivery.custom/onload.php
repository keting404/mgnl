<?
// подключение служебной части пролога
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->ShowAjaxHead();?>
<div class="modal_background">
	<div class="modal_form modal_address">
		 <?$APPLICATION->IncludeComponent(
            "ctweb:yandexdelivery", 
            "onload", 
            array(
                "MAP_HEIGHT" => "400px",
                "MAP_WIDTH" => "100%",
                "MAP_ZOOM" => "10",
                "REGION_FILTER_NAME" => "",
                "REGION_OPACITY" => "0.3",
                "RELOCATION" => "/personal/",
                "ROUTE_COLOR" => "4A84E3ff",
                "ROUTE_OPACITY" => "0.8",
                "STORE_FILTER_NAME" => "",
                "COMPONENT_TEMPLATE" => "window"
            ),
            false
        );?>
	</div>
</div>


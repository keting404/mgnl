<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/fileman/prolog.php");

IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("fileman");

$APPLICATION->SetTitle(GetMessage('FM_ST_ACCESS_TITLE'));

if (!$USER->CanDoOperation('fileman_edit_all_settings'))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/fileman/classes/general/sticker.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// Get stickers tasks with names
$arTasks = CSticker::GetTasks();

//Fetch user groups
$arGroups = array();
$db_groups = CGroup::GetList($order="sort", $by="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
while($arRes = $db_groups->Fetch())
	$arGroups[] = $arRes;

$defaultAccess = COption::GetOptionString('fileman', 'stickers_default_access', false);
if ($defaultAccess === false)
	foreach ($arTasks as $id => $task)
	{
		if ($task['letter'] == 'D')
		{
			$defaultAccess = $id;
			break;
		}
	}

if($REQUEST_METHOD=="POST" && $_POST['saveperm'] == 'Y' && check_bitrix_sessid())
{
	//Clear all
	if ($_REQUEST['clear_all'] == "Y")
		CSticker::DeleteAll();

	// Settings
	COption::SetOptionString("fileman", "stickers_hide_bottom", $_REQUEST['set_hide_bottom'] == "Y" ? "Y" : "N");
	COption::SetOptionString("fileman", "stickers_start_sizes", $_REQUEST['set_sizes']);
	COption::SetOptionString("fileman", "stickers_use_hotkeys", $_REQUEST['use_hotkeys'] == "Y" ? "Y" : "N");

	// Access
	$arTaskPerm = Array();
	foreach ($arGroups as $group)
	{
		$tid = ${"TASKS_".$group["ID"]};
		if ($tid)
			$arTaskPerm[$group["ID"]] = intval($tid);
	}
	CSticker::SaveAccessPermissions($arTaskPerm);
	COption::SetOptionString('fileman', 'stickers_default_access', intval($_REQUEST['st_default_access']));
	$defaultAccess = intval($_REQUEST['st_default_access']);
}

$arTaskPerm = CSticker::GetAccessPermissions();

$strTaskOpt = "";
foreach ($arTasks as $id => $task)
	$strTaskOpt .= '<option value="'.$id.'">'.($task['letter'] <> '' ? '['.$task['letter'].'] ' : '').$task['title'].'</option>';

$strGroupsOpt = '<option value="">('.GetMessage('FM_ST_SELECT_GROUP').')</option>';
$arGroupIndex = array();
foreach ($arGroups as $group)
{
	$arGroupIndex[$group['ID']] = $group['NAME'];
	$strGroupsOpt .= '<option value="'.$group['ID'].'">'.htmlspecialcharsex($group['NAME']).' ['.intval($group['ID']).']</option>';
}
?>

<form method="POST" action="<?= $APPLICATION->GetCurPage()?>?lang=<?= LANGUAGE_ID?>" name="st_access_form">
<input type="hidden" name="site" value="<?= htmlspecialcharsbx($site) ?>">
<input type="hidden" name="saveperm" value="Y">
<input type="hidden" id="bxst_clear_all" name="clear_all" value="N">
<input type="hidden" name="lang" value="<?= LANGUAGE_ID?>">
<?= bitrix_sessid_post()?>

<?
$aTabs = array(
	array("DIV" => "stickers_settings", "TAB" => "Настройки", "ICON" => "fileman", "TITLE" => "Настройки"),
// 	array("DIV" => "stickers_access", "TAB" => GetMessage("FM_ST_ACCESS"), "ICON" => "fileman", "TITLE" => GetMessage("FM_ST_ACCESS_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>


<?$tabControl->BeginNextTab();?>
<tr>
	<td colspan="2">
		<table>
		<tr>
			<td class="adm-detail-content-cell-l" width="40%">
				<input type="checkbox" name="set_hide_bottom" id="set_hide_bottom" value="Y" <? if (COption::GetOptionString("fileman", "stickers_hide_bottom", "Y") == "Y") {echo "checked";}?>/>
			</td>
			<td class="adm-detail-content-cell-r" width="60%"><label for="set_hide_bottom"><?= GetMessage('FM_ST_SET_HIDE_BOTTOM')?></label></td>
		</tr>
		<tr style="display: none;">
			<td class="adm-detail-content-cell-l">
				<input type="checkbox" name="set_supafly" id="set_supafly" value="Y"/>
			</td>
			<td class="adm-detail-content-cell-r"><label for="set_supafly"><?= GetMessage('FM_ST_SET_SUPAFLY')?></label></td>
		</tr>
		<tr style="display: none;">
			<td class="adm-detail-content-cell-l">
				<input type="checkbox" name="set_smart_marker" id="set_smart_marker" value="Y" />
			</td>
			<td class="adm-detail-content-cell-r"><label for="set_smart_marker"><?= GetMessage('FM_ST_SET_SMART_MARKER')?></label></td>
		</tr>
		<tr>
			<td class="adm-detail-content-cell-l">
				<input type="checkbox" name="use_hotkeys" id="use_hotkeys" value="Y" <?if(COption::GetOptionString("fileman", "stickers_use_hotkeys", "Y") == "Y"){echo "checked";}?>/>
			</td>
			<td class="adm-detail-content-cell-r"><label for="use_hotkeys"><?= GetMessage('FM_ST_USE_HOTKEYS')?></label></td>
		</tr>
		<tr>
			<td class="adm-detail-content-cell-l"><label for="set_sizes"><?= GetMessage('FM_ST_SET_SIZES')?>:</label></td>
			<td class="adm-detail-content-cell-r">
				<?$size = COption::GetOptionString("fileman", "stickers_start_sizes", "350_200");?>
				<select name="set_sizes" id="set_sizes">
					<option value="280_160" <? if ($size == "280_160") {echo "selected";}?>>280 x 160</option>
					<option value="350_200" <? if ($size == "350_200") {echo "selected";}?>>350 x 200</option>
					<option value="400_250" <? if ($size == "400_250") {echo "selected";}?>>400 x 250</option>
				</select>
			</td>
		</tr>

		<tr>
			<td colSpan="2" class="adm-detail-content-cell-r">
				<a href="javascript: void('');" onclick="if (confirm('<?= GetMessage('FM_ST_CLEAR_ALL_CONFIRM');?>')) {BX('bxst_clear_all').value='Y'; document.forms.st_access_form.submit(); return false;}"><?= GetMessage('FM_ST_CLEAR_ALL');?></a>
			</td>
		</tr>
		</table>
	</td>
</tr>
<?$tabControl->BeginNextTab();?>



<?$tabControl->EndTab();?>

<?
$tabControl->Buttons(
	array(
		"disabled" => false,
		"back_url" => "/bitrix/admin/?lang=".LANGUAGE_ID."&".bitrix_sessid_get()
	)
);
?>

<?$tabControl->End();?>

</form>



<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>

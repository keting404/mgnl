<?php
//bitrix/php_interface/Sale/Salebonus.php
/**
 * Добавляет новое действие в правила работы корзины для Маркетинга
 */
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Sale;
 
if (!Loader::includeModule('catalog'))
    return;
if (!Loader::includeModule('sale'))
    return;
 
class CSaleActionCtrlBonusUser extends CSaleActionCtrl {
 
    public static function GetControlDescr() {
        $description = parent::GetControlDescr();
        $description['EXECUTE_MODULE'] = 'all';//Для сохранения в таблицу
        $description['SORT'] = 500;
 
        return $description;
    }
 
    public static function GetControlID() {
        return 'ActSaleWeight';//Уникальный идентификатор
    }
 
    public static function GetControlShow($arParams) {
        $arAtoms = static::GetAtomsEx(false, false);
        $boolCurrency = false;
        if (static::$boolInit) {
            if (isset(static::$arInitParams['CURRENCY'])) {
                $arAtoms['Unit']['values']['Cur'] = static::$arInitParams['CURRENCY'];
                $boolCurrency = true;
            } elseif (isset(static::$arInitParams['SITE_ID'])) {
                $strCurrency = Sale\Internals\SiteCurrencyTable::getSiteCurrency(static::$arInitParams['SITE_ID']);
                if (!empty($strCurrency)) {
                    $arAtoms['Unit']['values']['Cur'] = $strCurrency;
                    $boolCurrency = true;
                }
            }
        }
        if (!$boolCurrency) {
            unset($arAtoms['Unit']['values']['Cur']);
        }
        $arResult = array(
            'controlId' => static::GetControlID(),
            'group' => false,
            'label' => 'За каждый килограмм',
            'defaultText' => 'Наценка за вес',
            'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
            'control' => array(
                'За каждый килограмм',
                $arAtoms['Type'],
                $arAtoms['Value'],
                $arAtoms['Unit']
            )
        );
 
        return $arResult;
    }
 
    public static function GetAtoms() {
        return static::GetAtomsEx(false, false);
    }
 
    public static function GetAtomsEx($strControlID = false, $boolEx = false) {
 
        $boolEx = (true === $boolEx ? true : false);
        $arAtomList = array(
            'Type' => array(
                'JS' => array(
                    'id' => 'Type',
                    'name' => 'extra',
                    'type' => 'select',
                    'values' => array(
                        'Discount' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_TYPE_DISCOUNT'),
                        'DiscountZero' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_TYPE_DISCOUNT_ZERO'),
                        'Extra' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_TYPE_EXTRA'),
                    ),
                    'defaultText' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_TYPE_DEF'),
                    'defaultValue' => 'Discount',
                    'first_option' => '...'
                ),
                'ATOM' => array(
                    'ID' => 'Type',
                    'FIELD_TYPE' => 'string',
                    'FIELD_LENGTH' => 255,
                    'MULTIPLE' => 'N',
                    'VALIDATE' => 'list'
                )
            ),
            'Value' => array(
                'JS' => array(
                    'id' => 'Value',
                    'name' => 'extra_size',
                    'type' => 'input'
                ),
                'ATOM' => array(
                    'ID' => 'Value',
                    'FIELD_TYPE' => 'double',
                    'MULTIPLE' => 'N',
                    'VALIDATE' => ''
                )
            ),
            'Unit' => array(
                'JS' => array(
                    'id' => 'Unit',
                    'name' => 'extra_unit',
                    'type' => 'select',
                    'values' => array(
                        'Perc' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_PERCENT'),
                        'Cur' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_CUR')
                    ),
                    'defaultText' => Loc::getMessage('BT_SALE_ACT_DELIVERY_SELECT_UNIT_DEF'),
                    'defaultValue' => 'Perc',
                    'first_option' => '...'
                ),
                'ATOM' => array(
                    'ID' => 'Unit',
                    'FIELD_TYPE' => 'string',
                    'FIELD_LENGTH' => 255,
                    'MULTIPLE' => 'N',
                    'VALIDATE' => ''
                )
            )
        );
 
        if (!$boolEx) {
            foreach ($arAtomList as &$arOneAtom) {
                $arOneAtom = $arOneAtom['JS'];
            }
            if (isset($arOneAtom))
                unset($arOneAtom);
        }
 
        return $arAtomList;
    }
 
    public static function GetShowIn($arControls) {
        return array(CSaleActionCtrlGroup::GetControlID());
    }
 
    /**
     * Функция должна вернуть колбэк того что должно быть выполнено при наступлении условий
     * @param type $arOneCondition
     * @param type $arParams
     * @param type $arControl
     * @param type $arSubs
     * @return string
     */
    public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = false) {
 
        $mxResult = '';
 
        if (is_string($arControl)) {
            if ($arControl == static::GetControlID()) {
                $arControl = array(
                    'ID' => static::GetControlID(),
                    'ATOMS' => static::GetAtoms()
                );
            }
        }
        $boolError = !is_array($arControl);
 
        if (!$boolError) {
            $arOneCondition['Value'] = (float) $arOneCondition['Value'];
            $actionParams = array(
                'VALUE' => ($arOneCondition['Type'] == 'Extra' ? $arOneCondition['Value'] : -$arOneCondition['Value']),
                'UNIT' => ($arOneCondition['Unit'] == 'Cur' ? Sale\Discount\Actions::VALUE_TYPE_FIX : Sale\Discount\Actions::VALUE_TYPE_PERCENT),
//                 'PARAM' => \Bitrix\Sale\Discount::getUseMode(),
            );
            if ($arOneCondition['Type'] == 'DiscountZero' && $arOneCondition['Unit'] == 'Cur')
                $actionParams['MAX_BOUND'] = 'Y';
 
            //Описываем колбэк
            $mxResult .= 'include_once($_SERVER[\'DOCUMENT_ROOT\'].\'/bitrix/php_interface/Sale/_testaction.php\');' . PHP_EOL;
            $mxResult .= '\Zixnru\Framework\ActionsUserBonusBalans::applyToBonus(' . $arParams['ORDER'] . ', ' . var_export($actionParams, true) . ')';
 
            unset($actionParams);
        }
 
        return $mxResult;
    }
 
}

<?php
 
//bitrix/modules/zixnru.framework/classes/general/_testaction.php
 
namespace Zixnru\Framework;
 
class ActionsUserBonusBalans {
 
    public static function applyToBonus(array &$order, array $action) {
 
        if ($action['UNIT'] !== 'F') {
            return;
        }
 
        $action['VALUE'] = intval($action['VALUE']);
 
        if (!isset($order['ORDER_WEIGHT'])) {
            //$order['PRICE_DELIVERY'] = $action['VALUE'];
        } else {
            $order['PRICE_DELIVERY'] += $action['VALUE'];
        }
 
        AddMessage2Log('$arRows = '.print_r($action, true),'');
 
    }
 
}

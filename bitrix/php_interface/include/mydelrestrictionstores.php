<?
use Bitrix\Sale\Delivery\Restrictions;
use Bitrix\Sale\Internals\Entity;

class MyDeliveryRestrictionStores extends Restrictions\Base
{
    public static function getClassTitle()
    {
        return 'по складам (задать только одну строку На складах или На всех, кроме)';
    }

    public static function getClassDescription()
    {
        return 'доставка будет выводится только при указании складов или исколючении';
    }

public static function check($currentStore, array $restrictionParams, $deliveryId = 0)
{
    $pieces = explode(",", $restrictionParams['STORE_IN_ID']);
    $pieces2 = explode(",", $restrictionParams['STORE_OUT_ID']);
    if ($restrictionParams['STORE_IN_ID']){
        if (in_array($currentStore, $pieces))
            return true;
    }elseif ($restrictionParams['STORE_OUT_ID']){
        if (!in_array($currentStore, $pieces2))
            return true;
    }
    return false;
}
protected static function extractParams(Entity $shipment)
{
    $res = $_COOKIE['current_store'];
    return !empty($res) ? intval($res) : 0;
}
public static function getParamsStructure($entityId = 0)
    {
        return array(
            "STORE_IN_ID" => array(
                'TYPE' => 'STRING',
                'DEFAULT' => "",
                'LABEL' => 'На складах (через запятую)'
            ),
            "STORE_OUT_ID" => array(
                'TYPE' => 'STRING',
                'DEFAULT' => "",
                'LABEL' => 'На всех складах, кроме (через запятую)'
            )
        );
    }
}

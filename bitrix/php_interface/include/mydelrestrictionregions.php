<?
use Bitrix\Sale\Delivery\Restrictions;
use Bitrix\Sale\Internals\Entity;

class MyDeliveryRestrictionRegions extends Restrictions\Base
{
    public static function getClassTitle()
    {
        return 'по регионам';
    }

    public static function getClassDescription()
    {
        return 'доставка будет выводится только в указанных регионах';
    }

public static function check($currentStore, array $restrictionParams, $deliveryId = 0)
{
    $pieces = explode(",", $restrictionParams['REGION_IN_ID']);
    $pieces2 = explode(",", $restrictionParams['REGION_OUT_ID']);
    if ($restrictionParams['REGION_IN_ID']){
        if (in_array($currentStore, $pieces))
            return true;
    }elseif ($restrictionParams['REGION_OUT_ID']){
        if (!in_array($currentStore, $pieces2))
            return true;
    }
    return false;
}
protected static function extractParams(Entity $shipment)
{
    $res = $_COOKIE['regionID'];
    return !empty($res) ? intval($res) : 0;
}
public static function getParamsStructure($entityId = 0)
    {
        return array(
            "REGION_IN_ID" => array(
                'TYPE' => 'STRING',
                'DEFAULT' => "",
                'LABEL' => 'В регионах (через запятую)'
            ),
            "REGION_OUT_ID" => array(
                'TYPE' => 'STRING',
                'DEFAULT' => "",
                'LABEL' => 'В регионах, кроме (через запятую)'
            )
        );
    }
}

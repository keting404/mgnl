<?
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log/init.log");

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\EventManager;

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

session_start();

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php')) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
}

CModule::AddAutoloadClasses(
	'', // не указываем имя модуля
	[
		// ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
		'stikersUpdate'     => '/bitrix/php_interface/lib/mgnl/CatalogEvents.php',
		'mgnlOrderEvents'   => '/bitrix/php_interface/lib/mgnl/OrderEvents.php',
		'orderPaySystemMap' => '/bitrix/php_interface/lib/mgnl/OrderPaySystemChange.php',
		'saleEvents'        => '/bitrix/php_interface/lib/mgnl/saleEvents.php',
		'SberbankPay'       => '/bitrix/php_interface/lib/mgnl/SberbankPay.php',
		'mgnlTools'         => '/bitrix/php_interface/lib/mgnl/mgnlTools.php',
        'iblockEvents'      => '/bitrix/php_interface/lib/mgnl/iblockEvents.php',
        'authEvents'      => '/bitrix/php_interface/lib/mgnl/authEvents.php',
	]
);

$eventManager = EventManager::getInstance();

//перед добавлением цены товара
$eventManager->addEventHandler(
	'catalog', '\Bitrix\Catalog\Price::OnBeforeAdd',
	['saleEvents', 'OnBeforePriceAddOrUpdate']
);
//перед изменением цены товара d7
$eventManager->addEventHandler(
	'catalog', '\Bitrix\Catalog\Price::OnBeforeUpdate',
	['saleEvents', 'OnBeforePriceAddOrUpdate']
);
//после добавления элемента d7
$eventManager->addEventHandler(
    "iblock", "OnAfterIBlockElementAdd",
    ['iblockEvents', 'OnAfterIBlockElementAddOrUpdate']
);
//после изменения элемента d7
$eventManager->addEventHandler(
    "iblock", "OnAfterIBlockElementUpdate",
    ['iblockEvents', 'OnAfterIBlockElementAddOrUpdate']
);
//изменение статуса заказа
AddEventHandler("sale", "OnSaleStatusOrder", ['saleEvents', 'OnSaleStatusOrder']);
AddEventHandler("sale", "OnSaleStatusOrder", ['mgnlOrderEvents', 'OnSaleStatusOrderDelivery']);
//копирование значений стикеров в свойство список
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["stikersUpdate", "OnBeforeIBlockElementUpdateHandler"]);
//разбивка наличия по складам
AddEventHandler("catalog", "OnStoreProductUpdate", ["stikersUpdate", "OnStoreProductUpdateHandler"]);
//действия при авторизации
AddEventHandler("main", "OnAfterUserAuthorize", ["authEvents", "OnAfterUserAuthorizeHandler"]);

//ограничение платежных систем
Bitrix\Main\EventManager::getInstance()->addEventHandler(
	'sale',
        'onSalePaySystemRestrictionsClassNamesBuildList',
        'myPayFunctionD'
            );
            function myPayFunctionD()
            {
                return new \Bitrix\Main\EventResult(
                        \Bitrix\Main\EventResult::SUCCESS,
                                array(
                                    '\MyPayRestrictionD' => '/bitrix/php_interface/include/MyPayRestrictionD.php',
                                    )
                                                );
            }
//ограничения службы доставки
Bitrix\Main\EventManager::getInstance()->addEventHandler(
	'sale',
	'onSaleDeliveryRestrictionsClassNamesBuildList',
	'myDeliveryFunction'
);
function myDeliveryFunction()
{
	return new \Bitrix\Main\EventResult(
		\Bitrix\Main\EventResult::SUCCESS,
		[
			'\MyDeliveryRestriction'       => '/bitrix/php_interface/include/mydelrestriction.php',
			'\MyDeliveryRestrictionStores' => '/bitrix/php_interface/include/mydelrestrictionstores.php',
			'\MyDeliveryRestrictionRegions' => '/bitrix/php_interface/include/mydelrestrictionregions.php',
		]
	);
}

//добавление ID товара в заказ
AddEventHandler("sale", "OnBeforeBasketAdd", ["stikersUpdate", "OnBeforeBasketAddHand"]);
// запрет на создание внутренних счетов
AddEventHandler("sale", "OnBeforeUserAccountAdd", "OnBeforeUserAccountAddHandler");
function OnBeforeUserAccountAddHandler(&$arFields)
{
	return false;
}

// работа с регионами на карте при оформлении
AddEventHandler("ctweb.yandexdelivery", "OnYandexDeliveryCalculatePrice", "OnYandexDeliveryCalculatePriceF");
global $regionForPartnerDelivery;
// $regionForPartnerDelivery = 507; // id региона для партнерской доставки
function OnYandexDeliveryCalculatePriceF(array &$arResult, array $arParams) /* ctweb.yandexdelivery */
{
	global $regionId, $storeId, $storeName, $APPLICATION, $regionForPartnerDelivery;
	$storeId = $arParams["STORE_ID"];
	$regionId = $arParams["REGION_ID"];
	// деактивация оплаты на руки если доставка 505 (т.е. курьеру а не работнику)
// 	if ($regionId == $regionForPartnerDelivery) {
// 		AddEventHandler("sale", "OnSaleComponentOrderOneStepPaySystem", ["orderPaySystemMap", "DeliveryPriceEdit"]);
// 	}
	// AddMessage2Log('$arRows = '.print_r($arParams, true),'');
}

//отлавливаем измененеие заказа
$eventManager->addEventHandler('sale', 'OnSaleOrderBeforeSaved', ['mgnlOrderEvents', 'handleSaleOrderBeforeSaved']);
$eventManager->addEventHandler('sale', 'OnSaleOrderSaved', ['mgnlOrderEvents', 'handleSaleOrderSaved']);



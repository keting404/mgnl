<?php

/**
 * Утилиты для обработчиков событий.
 */
class mgnlTools
{
	/**
	 * Получение значения свойства товара RATIO.
	 *
	 * @param int $productId Идентификатор товара.
	 * @return double
	 */
	public static function getRatioElement(int $productId)
	{
		$element = CIBlockElement::GetList([], ['ID' => $productId], false, false, ['PROPERTY_RATIO'])->Fetch();
		if($element['PROPERTY_RATIO_VALUE']){
            $ratio = (double)str_replace(',', '.', $element['PROPERTY_RATIO_VALUE']);
            return $ratio > 0 ? $ratio : 1;
		}
	}

	/**
	 * Изменение значения коэфф единицы измерения.
	 *
	 * @param int $id Идентификатор товара.
	 * @param string $ratio Значение коэфф единицы измерения.
	 * @return bool
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function setMeasureRatio(int $id, string $ratio): bool
	{
		if (!(int)$id
			|| !\Bitrix\Main\Loader::includeModule('catalog')
			|| !\Bitrix\Main\Loader::includeModule('sale')
			|| !($measure = \CCatalogMeasureRatio::getList(
				[],
				[
					'PRODUCT_ID' => $id,
				],
				false,
				false
			)->Fetch())
			|| ($measure['RATIO'] == $ratio)
		) {
			return false;
		}
		return \CCatalogMeasureRatio::update(
			$measure['ID'],
			[
				'PRODUCT_ID' => $id,
				'RATIO'      => $ratio,
			]
		);
	}

	/**
	 * Изменение значения коэфф единицы измерения из свойства RATIO.
	 *
	 * @param int $productId Идентификатор товара.
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function setProductRatioFromProperty(int $productId)
	{
		$ratio = self::getRatioElement($productId);
		if ($ratio > 0) {
			self::setMeasureRatio($productId, $ratio);
		}
	}
}

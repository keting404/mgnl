<?php
// namespace mgnl;
class orderPaySystemMap
{
    // деактивация оплаты на руки если доставка 505 (т.е. курьеру а не работнику)
    function DeliveryPriceEdit(&$arResult, &$arUserResult, $arParams)
    {
        
        foreach($arResult["PAY_SYSTEM"] as $k=>$paySysytem){
            if($paySysytem["ID"] == 1){ // деактивируем систему оплаты с ID
                
                $arResult["PAY_SYSTEM"][$k]["CHECKED"] = "N";
                unset($arResult["PAY_SYSTEM"][$k]); 
            }
        }
        $arResult["PAY_SYSTEM"][0]["CHECKED"] = "Y"; // отмечаем первую систему
    }
}

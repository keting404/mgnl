<?php

// namespace mgnl;
class stikersUpdate
{
	// создаем обработчик события "OnBeforeIBlockElementUpdate"
	public static function OnBeforeIBlockElementUpdateHandler(&$arFields)
	{
		if (isset($arFields["PROPERTY_VALUES"])) {
			$arFields["PROPERTY_VALUES"]["699"] = [];
			//Акция, ид значения 298, ид свойства 703
			foreach ($arFields["PROPERTY_VALUES"]["703"] as $hitProp) {
				if ($hitProp["VALUE"] == "true") {
					$checOption = false;
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "298") {
							$checOption = true;
						}
					}
					if ($checOption == false) {
						if (is_array($arFields["PROPERTY_VALUES"]["699"])) {
							array_push($arFields["PROPERTY_VALUES"]["699"], ["VALUE" => "298"]);
						}
					}
				} else {
					$k = "0";
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "298") {
							$arFields["PROPERTY_VALUES"]["699"][$k]["VALUE"] = "";
						}
						$k++;
					}
				}
			}

			//Новинка, ид значения 297, ид свойства 702
			foreach ($arFields["PROPERTY_VALUES"]["702"] as $hitProp) {
				if ($hitProp["VALUE"] == "true") {
					$checOption = false;
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "297") {
							$checOption = true;
						}
					}
					if ($checOption == false) {
						if (is_array($arFields["PROPERTY_VALUES"]["699"])) {
							array_push($arFields["PROPERTY_VALUES"]["699"], ["VALUE" => "297"]);
						}
					}
				} else {
					$k = "0";
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "297") {
							$arFields["PROPERTY_VALUES"]["699"][$k]["VALUE"] = "";
						}
						$k++;
					}
				}
			}

			//Советуем, ид значения 296, ид свойства 701
			foreach ($arFields["PROPERTY_VALUES"]["701"] as $hitProp) {
				if ($hitProp["VALUE"] == "true") {
					$checOption = false;
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "296") {
							$checOption = true;
						}
					}
					if ($checOption == false) {
						if (is_array($arFields["PROPERTY_VALUES"]["699"])) {
							array_push($arFields["PROPERTY_VALUES"]["699"], ["VALUE" => "296"]);
						}
					}
				} else {
					$k = "0";
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "296") {
							$arFields["PROPERTY_VALUES"]["699"][$k]["VALUE"] = "";
						}
						$k++;
					}
				}
			}

			//хит, ид значения 295, ид свойства 653
			foreach ($arFields["PROPERTY_VALUES"]["653"] as $hitProp) {
				if ($hitProp["VALUE"] == "true") {
					$checOption = false;
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "295") {
							$checOption = true;
						}
					}
					if ($checOption == false) {
						if (is_array($arFields["PROPERTY_VALUES"]["699"])) {
							array_push($arFields["PROPERTY_VALUES"]["699"], ["VALUE" => "295"]);
						}
					}
				} else {
					$k = "0";
					foreach ($arFields["PROPERTY_VALUES"]["699"] as $hitPropFodel) {
						if ($hitPropFodel["VALUE"] == "295") {
							$arFields["PROPERTY_VALUES"]["699"][$k]["VALUE"] = "";
						}
						$k++;
					}
				}
			}
		}
	}

	public static function OnStoreProductUpdateHandler($ID, $arFields)
	{
		$IBLOCK_ID = 33;
		$properties = CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], ["IBLOCK_ID" => $IBLOCK_ID, "CODE" => "STORE_AMOUNT_" . $arFields['STORE_ID']]);
		$PROPERTY_CODE = "STORE_AMOUNT_" . $arFields['STORE_ID'];  // код свойства
//             $PROPERTY_VALUE = $arFields["AMOUNT"];  // значение свойства
		$PROPERTY_VALUE = $arFields["AMOUNT"] > "0" ? "Y" : "N";  // значение свойства единое
		$ELEMENT_ID = $arFields["PRODUCT_ID"];  // код элемента
		if ($prop_fields = $properties->Fetch()) {
			// Установим новое значение для данного свойства данного элемента
			CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);

		} else {
			// Создадим свойство и новое значение для данного свойства данного элемента
			$arFields = [
				"NAME"          => "Остатки склада ID: " . $arFields['STORE_ID'],
				"ACTIVE"        => "Y",
				"SORT"          => "9999",
				"CODE"          => "STORE_AMOUNT_" . $arFields['STORE_ID'],
				"PROPERTY_TYPE" => "S",
				"IBLOCK_ID"     => $IBLOCK_ID,
			];

			$ibp = new CIBlockProperty;
			$PropID = $ibp->Add($arFields);
			//снимаем галочу Показывать на странице редактирования элемента
			CIBlockSectionPropertyLink::Delete(0, $PropID);

			// Установим новое значение для данного свойства данного элемента
			CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);

		}
//         AddMessage2Log('$arRows = '.print_r($arFields, true),'');
	}

	// создаем обработчик события "OnBeforeBasketAdd"
	public static function OnBeforeBasketAddHand(&$arFields)
	{
		$arFields["PROPS"]["TOVAR_ID"]["CODE"] = "TOVAR_ID";
		$arFields["PROPS"]["TOVAR_ID"]["NAME"] = "ID товара";
		$arFields["PROPS"]["TOVAR_ID"]["VALUE"] = $arFields["PRODUCT_ID"];
	}
}

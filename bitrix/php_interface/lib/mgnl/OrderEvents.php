<?php
// namespace mgnl;
use Bitrix\Sale;
use Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Application;
use \Bitrix\Catalog;
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

class mgnlOrderEvents
{
    public static function handleSaleOrderBeforeSaved(Main\Event $event)
    {
    global $regionId, $storeId, $regionForPartnerDelivery, $arStores, $arOrderVals;
    // получим объект заказа
        $order = $event->getParameter("ENTITY");
        $oldValues = $event->getParameter("VALUES"); //старые значения из которых перешел заказ
        $isNew = $order->isNew();
        $arOrderVals = $order->getFields()->getValues();
        $propertyCollection = $order->getPropertyCollection();
        $shipmentCollection = $order->getShipmentCollection();
        $discountCollection = $order->getDiscount();
        $discounts = $discountCollection->getApplyResult();
        /** @var \Bitrix\Sale\PropertyValue $obProp */
        
        //получаем ID сбера при сохранении после успешной оплаты
        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();
        
        //формируем поле - значение
        if($isNew == '1'){
            foreach ($propertyCollection as $obProp) {
                $arProp = $obProp->getProperty();
                $arProps[] = $arProp;
                //магазины
                $storeId = $_COOKIE["current_store"];
                $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$storeId),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeChatId = $arStore['UF_CHAT_ID'];
                        $storePhone = $arStore['PHONE'];
                        $storePartnerDelivery = $arStore['UF_PART_DELIVERY'];
                        $storeLongitude = $arStore['GPS_N'];
                        $storeLatitude = $arStore['GPS_S'];
                foreach ($shipmentCollection as $ship) {
                            $store_self_id = $ship->getStoreId();//ID склада
                            if ($store_self_id) {
                                break;
                            }
                        }
                        $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$store_self_id),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeLongitudeS = $arStore['GPS_N'];
                        $storeLatitudeS = $arStore['GPS_S'];
                if($store_self_id && $storeId != $store_self_id){
                    $lat1 = $storeLongitude;
                    $lon1 = $storeLatitude;
                    $lat2 = $storeLongitudeS;
                    $lon2 = $storeLatitudeS;

                    $theta = $lon1 - $lon2;
                    $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    $meters = $miles * 1.609344 * 1000;
                }
                
                if(in_array($arProp["CODE"], ["PARTNER_DELIVERY"])) {
                    if ($storePartnerDelivery)
                        $obProp->setValue("Y");
                }
                if(in_array($arProp["CODE"], ["REGION_ID"])) {
                    $obProp->setValue($_COOKIE["regionID"]);
                }
                if(in_array($arProp["CODE"], ["STORE_ID"])) {
                    $obProp->setValue($_COOKIE["current_store"]);
                }
                if(in_array($arProp["CODE"], ["STORE_NAME"])) {
                    $obProp->setValue($_COOKIE["store_name"]);
                }
                if(in_array($arProp["CODE"], ["STORE_PHONE"])) {
                    $obProp->setValue($storePhone);
                }
                if(in_array($arProp["CODE"], ["D_LONG"])) {
                    $obProp->setValue($_COOKIE["delivery_lang"]);
                }
                if(in_array($arProp["CODE"], ["D_LAT"])) {
                    $obProp->setValue($_COOKIE["delivery_lat"]);
                }
                
                if(in_array($arProp["CODE"], ["DISTANCE"])) {
                    if($meters){
                        $obProp->setValue($meters);
                    }else{
                        $obProp->setValue($_COOKIE["distance"]);
                    }
                }
                if(in_array($arProp["CODE"], ["CHAT_ID"])) {
                    $obProp->setValue($storeChatId);
                }
                    if (in_array($arProp["CODE"], ["PERIOD"])) {
                        $propPeriodValue = $obProp->getValue();
                    }
                    if (in_array($arProp["CODE"], ["PERIOD_TODAY"])) {
                        if ($obProp->getValue() == 'express'){
                            $propPeriodValue = date('H:i')." - ".date('H:i',  time()+ 90*60);
                        }else{
                            $propPeriodValue = $obProp->getValue();
                        }
                    }
                    if (in_array($arProp["CODE"], ["DATE"])) {
                        $propDateValue = $obProp->getValue();
                    }
                    if (in_array($arProp["CODE"], ["DATE_SELF"])) {
                        $propDateValue = $obProp->getValue();
                        if ($propDateValue == "Сегодня")
                            $propDateValue = date('d.m.Y');
                        if ($propDateValue == "Завтра"){
                            $propDateValue = date('d.m.Y');
                            $propDateValue = date('d.m.Y', strtotime("+1 days", strtotime($propDateValue)));
                        }
                        if ($propDateValue == "Послезавтра"){
                            $propDateValue = date('d.m.Y');
                            $propDateValue = date('d.m.Y', strtotime("+2 days", strtotime($propDateValue)));
                        }
                    }
                if(in_array($arProp["CODE"], ["COUPONS"])) {
                    //примененные купоны
                    $arrayCoupons=[];
                    foreach ($discounts["COUPON_LIST"] as $coupon) {
                        $arrayCoupons[]=$coupon["COUPON"];
                    }
                    $couponsString = implode("; ", $arrayCoupons);
                    $obProp->setValue($couponsString);
                }
            }
            if(!$propDateValue){
                $propDateValue = date('d.m.Y');
            }
            foreach ($propertyCollection as $obProp) {
                $arProp = $obProp->getProperty();
                $arProps[] = $arProp;
                if (in_array($arProp["CODE"], ["TIME"])) {
                    $obProp->setValue($propDateValue.' '.$propPeriodValue);
                }
            }
        }else{
            if($arOrderVals['CANCELED']!=="Y"){
                foreach ($shipmentCollection as $ship) {
                            $store_self_id = $ship->getStoreId();//ID склада
                            if ($store_self_id) {
                                break;
                            }
                        }
                        $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$store_self_id),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeLongitudeS = $arStore['GPS_N'];
                        $storeLatitudeS = $arStore['GPS_S'];
                        
                foreach ($propertyCollection as $obProp) {
                        $arProp = $obProp->getProperty();
                        $arProps[$arProp["CODE"]] = $arProp;
                        $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
                        
                        //подстановка ID сбера при сохранении после успешной оплаты
                        if($request["PAYMENT"] == "SBERBANK" && $request["orderId"] && $request["ORDER_ID"]){
                        if(in_array($arProp["CODE"], ["SBER_ID"])) {
                            $obProp->setValue($request["orderId"]);
                        }
                        $_SESSION['ORDER_ID'] = $request["ORDER_ID"];
                        }
                        if(in_array($arProp["CODE"], ["DISTANCE"])) {
                            if($store_self_id && $arProps["STORE_ID"]["VALUE"]["VALUE"] != $store_self_id){
                            $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                                    'filter'=>array('=ID'=>$arProps["STORE_ID"]["VALUE"]["VALUE"]),
                                    'select'=>array('GPS_N','GPS_S'),
                                ))->fetch();
                                $storeLongitude = $arStore['GPS_N'];
                                $storeLatitude = $arStore['GPS_S'];
                                $lat1 = $storeLongitude;
                                $lon1 = $storeLatitude;
                                $lat2 = $storeLongitudeS;
                                $lon2 = $storeLatitudeS;

                                $theta = $lon1 - $lon2;
                                $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
                                $miles = acos($miles);
                                $miles = rad2deg($miles);
                                $miles = $miles * 60 * 1.1515;
                                $meters = $miles * 1.609344 * 1000;
                            }
                            if($meters){
                                $obProp->setValue($meters);
                            }
                        }
                    
                }
                
                
            }
            //емэйл директору магазина
            if($oldValues['STATUS_ID']=="N" && $arOrderVals['STATUS_ID']=="O" && $arOrderVals['CANCELED']!=="Y"){
                foreach ($propertyCollection as $obProp) {
                    $arProp = $obProp->getProperty();
                    $arProps[$arProp["CODE"]] = $arProp;
                    $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
                }
                $storeId = $arProps["STORE_ID"]["VALUE"]["VALUE"];
                $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$storeId),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeName = $arStore['TITLE'];
                        $storeLongitude = $arStore['GPS_N'];
                        $storeLatitude = $arStore['GPS_S'];
                        $storeEmail = $arStore['EMAIL'];
                        $storePhone = $arStore['PHONE'];
                        $arStores = $arStore;
                        
                    $arEventFields = array(
                        "EMAIL" => $storeEmail,
                        "ORDER_ID" => $arOrderVals["ID"],
                        "STORE_NAME" => $storeName,
                        "PRICE" => $arOrderVals["PRICE"],
                );
                // CEvent::Send("SALE_STATUS_CHANGED_O", "s1", $arEventFields);
            }
            
            if($arOrderVals['STATUS_ID']=="OA" && $arOrderVals['CANCELED']!=="Y"){
                foreach ($propertyCollection as $obProp) {
                    $arProp = $obProp->getProperty();
                    $arProps[$arProp["CODE"]] = $arProp;
                    $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
                }
                $storeId = $arProps["STORE_ID"]["VALUE"]["VALUE"];
                $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$storeId),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeName = $arStore['TITLE'];
                        $storeLongitude = $arStore['GPS_N'];
                        $storeLatitude = $arStore['GPS_S'];
                        $storeEmail = $arStore['EMAIL'];
                        $storePhone = $arStore['PHONE'];
                        $arStores = $arStore;
            }
            
            if($oldValues['CANCELED']!=="Y" && $arOrderVals['CANCELED']=="Y" && $arOrderVals['STATUS_ID']!=="N"){
                foreach ($propertyCollection as $obProp) {
                    $arProp = $obProp->getProperty();
                    $arProps[$arProp["CODE"]] = $arProp;
                    $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
                }
                $storeId = $arProps["STORE_ID"]["VALUE"]["VALUE"];
                $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$storeId),
                            'select'=>array('*','UF_*'),
                        ))->fetch();

                        $storeName = $arStore['TITLE'];
                        $storeLongitude = $arStore['GPS_N'];
                        $storeLatitude = $arStore['GPS_S'];
                        $storeEmail = $arStore['EMAIL'];
                        $storePhone = $arStore['PHONE'];
                        $arStores = $arStore;
                        
                    $arEventFields = array(
                        "EMAIL_ADD" => $storeEmail,
                        "ORDER_ID" => $arOrderVals["ID"],
                        "ORDER_CANCEL_DESCRIPTION" => $arOrderVals["REASON_CANCELED"],
                );
//                 CEvent::Send("SALE_ORDER_CANCEL", "s1", $arEventFields);
            }
        }
        
    }
    public static function OnSaleStatusOrderDelivery(int $orderId, string $orderStatus)
    {
    global $arOrderProps, $arOrderVals;
    $regionForPartnerDelivery = Option::get("gettDelivery", "region_id");
    $deliveryId = Option::get("gettDelivery", "delivery_id");
    $gettIsEnabled = Option::get("gettDelivery", "enabled");
    $statusId = Option::get("gettDelivery", "status_id");
        if ($gettIsEnabled == "Y") {
            if($orderStatus == $statusId){
            // получим объект заказа
            $order = Sale\Order::load($orderId);
            $arOrderVals = $order->getFields()->getValues();
            $propertyCollection = $order->getPropertyCollection();
            $shipmentCollection = $order->getShipmentCollection();
            /** @var \Bitrix\Sale\PropertyValue $obProp */
                    
            if($arOrderVals['CANCELED']!=="Y"){
                    foreach ($propertyCollection as $obProp) {
                        $arProp = $obProp->getProperty();
                        $arProps[$arProp["CODE"]] = $arProp;
                        $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
                    }
                    $arOrderProps = $arProps;
                    //вызываем доставку
                    
                    if($arOrderVals['DELIVERY_ID']==$deliveryId && $arProps["REGION_ID"]["VALUE"]["VALUE"] == $regionForPartnerDelivery && $arProps["PARTNER_DELIVERY"]["VALUE"]["VALUE"] == "Y" && 
                        $arProps["DISTANCE"]["VALUE"]["VALUE"] > 500
                        ){
                        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php") && file_exists($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/rideOrder.php")){
                            AddMessage2Log('$arRows = '.print_r($arProps["PARTNER_DELIVERY"]["VALUE"]["VALUE"], true),'');
                            require_once($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php");
                            require($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/rideOrder.php");
                        }
                    }
                    else if(
                        $arOrderVals['DELIVERY_ID']==2 && 
                        $arProps["REGION_ID"]["VALUE"]["VALUE"] == $regionForPartnerDelivery && 
                        $arProps["PARTNER_DELIVERY"]["VALUE"]["VALUE"] == "Y"
                    ){
                        foreach ($shipmentCollection as $ship) {
                            $store_id = $ship->getStoreId();//ID склада
                            if ($store_id) {
                                break;
                            }
                        }
                        $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                            'filter'=>array('=ID'=>$store_id),
                            'select'=>array('*','UF_*'),
                        ))->fetch();
                        $storeName = $arStore['TITLE'];
                        $storeEmail = $arStore['EMAIL'];
                        $storePhone = $arStore['PHONE'];
                        
                        $arOrderProps["ADDRESS"]["VALUE"]["VALUE"] = $arStore['ADDRESS'];
                        $arOrderProps["D_LONG"]["VALUE"]["VALUE"] = $arStore['GPS_N'];
                        $arOrderProps["D_LAT"]["VALUE"]["VALUE"] = $arStore['GPS_S'];
                        $arOrderVals['USER_DESCRIPTION'] = "Доставка в пункт самовывоза (маг. Магнолия) ".$arOrderVals['USER_DESCRIPTION'];
                        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php") && file_exists($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/rideOrder.php") && $store_id != $arProps["STORE_ID"]["VALUE"]["VALUE"]){
                            AddMessage2Log('$arRows = '.print_r($storeChat, true),'');
//                             require_once($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/getToken.php");
//                             require($_SERVER["DOCUMENT_ROOT"]."/statusy-dostavki/rideOrder.php");
                        }
                    }
                }
            }
        }
    }
    function handleSaleOrderSaved(Main\Event $event)
    {
        /** @var Order $order */
        $order = $event->getParameter("ENTITY");
        $oldValues = $event->getParameter("VALUES");
        $arOrderVals = $order->getFields()->getValues();
        $isNew = $event->getParameter("IS_NEW");
        $propertyCollection = $order->getPropertyCollection();
        if ($isNew)
        {
            foreach ($propertyCollection as $obProp) {
                $arProp = $obProp->getProperty();
                $arProps[] = $arProp;

                if($arOrderVals['DELIVERY_ID']=="2"){
                    // foreach ($propertyCollection as $obProp) {
                    //     $arProp = $obProp->getProperty();
                    //     $arProps[] = $arProp;

                        
                    //     if (in_array($arProp["CODE"], ["DATE_SELF"])) {
                    //         $propValue = $obProp->getValue();
                    //         $arFields = array(
                    //         "ORDER_ID" => $order->getId(),
                    //         "ORDER_PROPS_ID" => 41,
                    //         "NAME" => "Дата доставки",
                    //         "CODE" => "DATE",
                    //         "VALUE" => $propValue
                    //         );

                    //         CSaleOrderPropsValue::Add($arFields);
                    //     }
                        
                    // }
                    
                } else if (!in_array($arProp["CODE"], ["DATE"])) {
                    $arFields = array(
                    "ORDER_ID" => $order->getId(),
                    "ORDER_PROPS_ID" => 41,
                    "NAME" => "Дата доставки",
                    "CODE" => "DATE",
                    "VALUE" => date('d.m.Y')
                    );

                    CSaleOrderPropsValue::Add($arFields);
                }
                if (!in_array($arProp["CODE"], ["PERIOD"])) {
                    if (in_array($arProp["CODE"], ["PERIOD_TODAY"])) {
                        $propValue = $obProp->getValue();
                        $arFields = array(
                        "ORDER_ID" => $order->getId(),
                        "ORDER_PROPS_ID" => 40,
                        "NAME" => "Период доставки",
                        "CODE" => "PERIOD",
                        "VALUE" => $propValue
                        );

                        CSaleOrderPropsValue::Add($arFields);
                    }
                }
            }
        }
    }
}

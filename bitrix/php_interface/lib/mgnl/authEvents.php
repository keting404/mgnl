<?php

use Bitrix\Main\Loader;

class authEvents
{
	function OnAfterUserAuthorizeHandler($arUser)
    {
        // Выберем все профили покупателя для текущего пользователя, 
        // упорядочив результат по дате последнего изменения
        $db_sales = CSaleOrderUserProps::GetList(array("DATE_UPDATE" => "DESC"),array("USER_ID" => $arUser["user_fields"]["ID"]));
        while ($ar_sales = $db_sales->Fetch())
        {
            $ar_sales_arr[]=$ar_sales;
        }
        $actualProfile = $ar_sales_arr[0];
        // Выведем все свойства профиля покупателя с кодом $ID
        $db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID"=>$actualProfile["ID"]));
        while ($arPropVals = $db_propVals->Fetch())
        {
            $arProps[$arPropVals["PROP_CODE"]] = $arPropVals;
        }
        if ($arProps['ADDRESS']['VALUE'] != "" && !$_COOKIE["delivery_address"]){
                setcookie("delivery_address",$arProps['ADDRESS']['VALUE'],time()+60*60*24*1,"/");
            }
    }
}

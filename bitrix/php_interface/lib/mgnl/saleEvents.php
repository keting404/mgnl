<?php

use Bitrix\Sale;
use Bitrix\Main\Loader;

/**
 * Обработчики событий для модуля sale и catalog.
 */
class saleEvents
{
	/**
	 * Смена статуса заказа.
	 *
	 * @param int $orderId Идентификатора заказа.
	 * @param string $orderStatus Статус заказа.
	 * @return bool
	 */
	public static function OnSaleStatusOrder(int $orderId, string $orderStatus)
	{
		//addMessage2log('OnSaleStatusOrder');

		if ('Y' !== \Bitrix\Sale\BusinessValue::get("SBERBANK_HANDLER_TWO_STAGE", "PAYSYSTEM_7")) {
			// addMessage2log('Двухстадийные оплаты не активны');
			return true;
		}

		if ($orderStatus !== 'OM') {
			//addMessage2log('Статус заказа не нуждается в обработке: $orderStatus: ' . $orderStatus);
			return true;
		}

		if (!($order = Sale\Order::load($orderId))) {
			addMessage2log('Не удалось загрузить информацияю по заказу: $orderId: ' . $orderId);
			return true;
		}

		if ($order->getSumPaid() <= 0) {
			addMessage2log('Заказ не был предоплачен');
			return true;
		}

		$orderSumPaid = $order->getSumPaid() * 100;
		addMessage2log('Уже оплачено: ' . $orderSumPaid);

		$orderPrice = $order->getPrice() * 100;
		addMessage2log('Новая стоимость заказа: ' . $orderPrice);

// 		после подключения рекуррентных платежей возможность оплатить больше должна появиться
		if ($orderPrice - $orderSumPaid > 0) {
			addMessage2log('Списывать больше предоплаты нет возможности, дельта: ' . ($orderPrice - $orderSumPaid));
			return true;
		}

		$propertyCollection = $order->getPropertyCollection();
		$sberOrderId = $propertyCollection->getItemByOrderPropertyId(32)->getValue();

		addMessage2log('$sberOrderId: ' . $sberOrderId);

		$sberbankPay = new SberbankPay();
		$sberOrder = $sberbankPay->getOrderStatusExtended($sberOrderId);

		if (1 != $sberOrder['orderStatus']) {
			addMessage2log('Плохой статус заказа из шлюза: ' . $sberOrder['orderStatus']);
			addMessage2log($sberOrder);
			return true;
		}

		if (!Loader::includeModule('sberbank.ecom2')) {
			addMessage2log('Модуль платежной системы не установлен');
			return true;
		}

		$RBS_Gateway = new \Sberbank\Payments\Gateway();

		/* Корзина для чека */
		$basket = $order->getBasket();
		$basketItems = $basket->getBasketItems();
		$cart = [];
		foreach ($basketItems as $key => $basketItem) {
			$cart[] = [
				'positionId' => $key,
				'name'       => $basketItem->getField('NAME'),
				'quantity'   => [
					'value'   => $basketItem->getQuantity(),
					'measure' => $basketItem->getField('MEASURE_NAME'),
				],
				'itemAmount' => $basketItem->getFinalPrice() * 100,
				'itemCode'   => $basketItem->getProductId(),
				'tax'        => [
					'taxType' => $RBS_Gateway->getTaxCode($basketItem->getField('VAT_RATE') * 100),
				],
				'itemPrice'  => $basketItem->getPrice() * 100,
			];
		}

		$depositAnswer = $sberbankPay->deposit($sberOrderId, $orderPrice, $cart);

		if (!empty($depositAnswer['errorCode'])) {
			addMessage2log($depositAnswer['errorMessage']);
		} else {
			addMessage2log('Оплата завершена');
		}
	}

	/**
	 * Обработчик события, вызываемый после добавления/изменения цены товара.
	 *
	 * @param $event экземпляр объекта события Entity\DataManagerEvent $event.
	 */
	public static function OnBeforePriceAddOrUpdate(Bitrix\Main\Event $event)
	{
		$arParams = $event->getParameters();
		AddMessage2Log('$arRows = '.print_r($arParams, true),'');
		mgnlTools::setProductRatioFromProperty($arParams['fields']["PRODUCT_ID"]);
		
		if ((double)$arParams['fields']['PRICE'] <= 0) {
			if ((int)$arParams['primary']['ID']) {
				\Bitrix\Catalog\Model\Price::delete($arParams['primary']['ID']);
			}
			$obResult = new \Bitrix\Main\Entity\EventResult();
			$obResult->addError(new \Bitrix\Main\Entity\EntityError('Запрещено добавлять нулевую цену'));
			return $obResult;
		}
	}
}

<?php

use Bitrix\Main\Loader;

/**
 * Обработчики событий для модуля iblock (ИБ).
 */
class iblockEvents
{
	/**
	 * Обработчик события добавления/изменения элемента ИБ.
	 *
	 * @param $elementFields {array} Массив полей элемента ИБ.
	 */
	public static function OnAfterIBlockElementAddOrUpdate($elementFields)
	{
		switch ($elementFields['IBLOCK_ID']) {
			case 33: // Каталог товаров
				$elementSections = [];
				$res = \CIBlockElement::GetProperty(
					$elementFields['IBLOCK_ID'],
					$elementFields['ID'],
					"sort",
					"asc",
					[
						"CODE" => "SORTIROVKI",
					]
				);
				while ($multipleSortProp = $res->GetNext()) {
// 					if (empty($multipleSortProp['DESCRIPTION']) || empty($multipleSortProp['VALUE'])) {
// 						continue;
// 					}
					$sectionXmlId = trim($multipleSortProp['DESCRIPTION']);
					$elementSections[$sectionXmlId] = [
						'ELEMENT_SORT' => (int)$multipleSortProp['VALUE'],
					];
				}
				if (empty($elementSections)) {
					return;
				}
				$ibp = new \CIBlockProperty();
				$sortProps = [];
				$dbProps = \CIBlockProperty::GetList(
					null,
					[
						"ACTIVE"    => "Y",
						"IBLOCK_ID" => $elementFields['IBLOCK_ID'],
						"CODE"      => "SORT_%",
					]
				);
				while ($sortProperty = $dbProps->GetNext()) {
					// Пропускаем исходное множественное свойство из 1С, т.к по коду попало под фильтр:
					if ($sortProperty['CODE'] == 'SORTIROVKI') {
						continue;
					}
					$sortProps[] = $sortProperty['CODE'];
				}
				$res = \CIBlockSection::GetList(
					null,
					[
						"IBLOCK_ID" => $elementFields['IBLOCK_ID'],
						"ACTIVE"    => 'Y',
						"XML_ID"    => array_keys($elementSections),
					],
					false,
					[
						'ID',
						'XML_ID',
						'NAME',
					]
				);
				while ($section = $res->Fetch()) {
					$elementSections[$section['XML_ID']]['ID'] = $section['ID'];
					$elementSections[$section['XML_ID']]['NAME'] = $section['NAME'];
				}
				$newSortProps = [];
				foreach ($elementSections as $section) {
// 					if (!$section['ELEMENT_SORT']) {
// 						continue;
// 					}
					if (!in_array("SORT_" . $section['ID'], $sortProps)) {
						if ($newPropId = $ibp->Add([
							'CODE'             => "SORT_" . $section['ID'],
							'IBLOCK_ID'        => $elementFields['IBLOCK_ID'],
							'NAME'             => 'Сортировка товара в разделе "' . $section['NAME'] . '"',
							'ACTIVE'           => 'Y',
							'SORT'             => 5000,
							'PROPERTY_TYPE'    => 'N',
							'SECTION_PROPERTY' => 'N',
						])) {
							$sortProps[] = "SORT_" . $section['ID'];
						}
					}
					$newSortProps["SORT_" . $section['ID']] = $section['ELEMENT_SORT'];
				}
				if (!empty($newSortProps)) {
					\CIBlockElement::SetPropertyValuesEx(
						$elementFields['ID'],
						$elementFields['IBLOCK_ID'],
						$newSortProps
					);
				}
				break;
            case 44: // Отзывы кастом
                if (!empty($elementFields['PROPERTY_VALUES']['1335'])):
                    $email = array_shift($elementFields['PROPERTY_VALUES']['1331']);
                    $email = $email['VALUE'];
                    $text = $elementFields['DETAIL_TEXT'];
                    $arEventFields['EMAIL'] = $email;
                    $arEventFields['TEXT'] = $text;
                    CEvent::Send("SEND_REVIEWS", 's1', $arEventFields);
                    CIBlockElement::SetPropertyValues($elementFields['ID'], $elementFields['IBLOCK_ID'], '', 'PROP6');
                endif;
                if (!empty($elementFields['PROPERTY_VALUES']['1339']) && !empty($elementFields['PROPERTY_VALUES']['1338'])):
                    $email = array_shift($elementFields['PROPERTY_VALUES']['1331']);
                    $email = $email['VALUE'];
                    $name = array_shift($elementFields['PROPERTY_VALUES']['1338']);
                    $f = CIBlockElement::GetByID($name['VALUE'])->fetch();
                    $text = $elementFields['PREVIEW_TEXT'];
                    $arEventFields['EMAIL'] = $f['PREVIEW_TEXT'];
                    $arEventFields['EMAIL2'] = $email;
                    $arEventFields['TEXT'] = $text;
                    $arEventFields['PHONE'] = $elementFields['CODE'];
                    CEvent::Send("SEND_REVIEWS_MN", 's1', $arEventFields);
                    CIBlockElement::SetPropertyValues($elementFields['ID'], $elementFields['IBLOCK_ID'], '', 'PROP10');
                endif;
                break;
		}
	}
}

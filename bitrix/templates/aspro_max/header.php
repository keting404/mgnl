<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if($_GET["debug"] == "y")
	error_reporting(E_ERROR | E_PARSE);
IncludeTemplateLangFile(__FILE__);
CJSCore::Init(array("fx"));
global $APPLICATION, $arRegion, $arSite, $arTheme, $bIndexBot, $bIframeMode;
$arSite = CSite::GetByID(SITE_ID)->Fetch();
$htmlClass = ($_REQUEST && isset($_REQUEST['print']) ? 'print' : false);
$bIncludedModule = (\Bitrix\Main\Loader::includeModule("aspro.max"));?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>" <?=($htmlClass ? 'class="'.$htmlClass.'"' : '')?> <?=($bIncludedModule ? CMax::getCurrentHtmlClass() : '')?>>
<head>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<?$APPLICATION->ShowHead();?>
<?
//очищаем корзину если не выбран адрес
// if( !$_COOKIE["current_store"] ){
// CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
// }
$resStores = \CCatalogStore::GetList(
        [],
        ['ID' => $_COOKIE["current_store"]],
        false,
        false,
        ['ACTIVE']
    );
    while ($arStore = $resStores->Fetch()) {
            if($arStore["ACTIVE"] == "N"){
                setcookie("delivery_address","",time()-3600,"/");
                setcookie("current_region","",time()-3600,"/");
                setcookie("current_store","",time()-3600,"/");
            }

    }
if(!$_COOKIE["current_store"]){
    	setcookie("current_region","",time()-3600,"/");
//     	setcookie("delivery_address","",time()-3600,"/");
    }
use Bitrix\Sale;
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems();
foreach ($basket as $basketItem) {
    $basketProdId = $basketItem->getProductId();
    $basketItemId = $basketItem->getId();
    $basketItemFields = $basketItem->getPropertyCollection();
    $basketItemFieldsValues = $basketItemFields->getPropertyValues();
    if(!$basketItemFieldsValues['TOVAR_ID']['VALUE']){
        $basketItemFields->setProperty(array(
            array(
            'NAME' => 'ID товара',
            'CODE' => 'TOVAR_ID',
            'VALUE' => $basketProdId,
            ),
        ));
    }
    if(!CModule::IncludeModule("iblock"))
    return; 
    
    $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$basketProdId, 'ACTIVE' => 'Y', 'STORE_ID' => $_COOKIE["current_store"]), false, false, array('AMOUNT', 'ACTIVE')); 
    if ($arStore = $rsStore->Fetch())
        $arStore['AMOUNT'];
    if(!$arStore['AMOUNT'] || $arStore['AMOUNT']=="0"){
        $basketItem->setField('DELAY', 'Y');
        $unavalaibleItem=$basketItemId;
		//$basketItem->delete();?>
    <style>#basket-item-<?=$basketItemId?> a[data-entity=basket-item-remove-delayed]{display:none !important}</style>
    <?}
    $basket->save();
}?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowMeta("viewport");?>
	<?$APPLICATION->ShowMeta("HandheldFriendly");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style");?>
	<?$APPLICATION->ShowMeta("SKYPE_TOOLBAR");?>
	
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/swiper.min.css');?>

	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/swiper.min.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.min.js');?>
	<?$APPLICATION->AddHeadString('<script>BX.message('.CUtil::PhpToJSObject( $MESS, false ).')</script>', true);?>
	<?if($bIncludedModule)
		CMax::Start(SITE_ID);?>
	<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.SITE_DIR.'include/header_include/head.php'));?>
	<meta name="theme-color" content="#92a93f">
    <link href="https://fonts.googleapis.com/css2?family=Philosopher:wght@400;700&subset=cyrilic" rel="stylesheet" rel="preload" as="font" crossorigin>
</head>
<?$bIndexBot = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false);?>
<body class="<?=($bIndexBot ? "wbot" : "");?> site_<?=SITE_ID?> <?=($bIncludedModule ? CMax::getCurrentBodyClass() : '')?>" id="main" data-site="<?=SITE_DIR?>">
	<?if(!$bIncludedModule):?>
		<?$APPLICATION->SetTitle(GetMessage("ERROR_INCLUDE_MODULE_ASPRO_MAX_TITLE"));?>
		<center><?$APPLICATION->IncludeFile(SITE_DIR."include/error_include_module.php");?></center></body></html><?die();?>
	<?endif;?>
	
	<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.SITE_DIR.'include/header_include/body_top.php'));?>

	<?$arTheme = $APPLICATION->IncludeComponent("aspro:theme.max", ".default", array("COMPONENT_TEMPLATE" => ".default"), false, array("HIDE_ICONS" => "Y"));?>
	<?include_once('defines.php');?>
	<?CMax::SetJSOptions();?>

	<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.SITE_DIR.'include/header_include/under_wrapper1.php'));?>
	<div class="wrapper1 <?=($isIndex && $isShowIndexLeftBlock ? "with_left_block" : "");?> <?=CMax::getCurrentPageClass();?> <?$APPLICATION->AddBufferContent(array('CMax', 'getCurrentThemeClasses'))?>  ">
		<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.SITE_DIR.'include/header_include/top_wrapper1.php'));?>
        
		<div class="wraps hover_<?=$arTheme["HOVER_TYPE_IMG"]["VALUE"];?>" id="content">
			<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.SITE_DIR.'include/header_include/top_wraps.php'));?>
        
			<?if($isIndex):?>
				<?$APPLICATION->ShowViewContent('front_top_big_banner');?>
				<div class="wrapper_inner front <?=($isShowIndexLeftBlock ? "" : "wide_page");?> <?=$APPLICATION->ShowViewContent('wrapper_inner_class')?>">
			<?elseif(!$isWidePage):?>
				<div class="wrapper_inner <?=($isHideLeftBlock ? "wide_page" : "");?> <?=$APPLICATION->ShowViewContent('wrapper_inner_class')?>">
			<?endif;?>
				
				<div class="container_inner clearfix <?=$APPLICATION->ShowViewContent('container_inner_class')?>">
				<?if(($isIndex && ($isShowIndexLeftBlock || $bActiveTheme)) || (!$isIndex && !$isHideLeftBlock)):?>
					<div class="right_block <?=(defined("ERROR_404") ? "error_page" : "");?> wide_<?=CMax::ShowPageProps("HIDE_LEFT_BLOCK");?> <?=$APPLICATION->ShowViewContent('right_block_class')?>">
				<?endif;?>
					<div class="middle <?=($is404 ? 'error-page' : '');?> <?=$APPLICATION->ShowViewContent('middle_class')?>">
						<?CMax::get_banners_position('CONTENT_TOP');?>
						<?if(!$isIndex):?>
							<div class="container">
								<?//h1?>
								<?if($isHideLeftBlock && !$isWidePage):?>
									<div class="maxwidth-theme">
								<?endif;?>
						<?endif;?>
						<?CMax::checkRestartBuffer();?>

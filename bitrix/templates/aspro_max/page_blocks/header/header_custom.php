<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme, $arRegion, $bLongHeader2, $bColoredHeader;
$arRegions = CMaxRegionality::getRegions();
if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
$bLongHeader2 = true;
$bColoredHeader = true;
?>
<div class="header-wrapper fix-logo header-v6">
	<div class="logo_and_menu-row">
		<div class="logo-row">
			<div class="maxwidth-theme">
				<div class="row">
					<div class="col-md-12">
						<div class="logo-block">
							<div class="logo<?=$logoClass?>">
								<?=CMax::ShowLogo();?>
							</div>
						</div>
						<div class="content-block">
							<div class="row">
								<div class="col-md-9">
									<div class="subtop lines-block">
										<div class="row">
											<div class="col-md-12">
												<?if($arRegions):?>
													<div class="inline-block pull-left">
														<div class="top-description no-title wicons">
		                                                    <?$curDir=$APPLICATION->GetCurDir();
		                                                    $address = str_replace("Россия, Москва, ", "", $_COOKIE["delivery_address"]);
		                                                    ?>
		                                                    <?if($curDir == "/help/delivery/"){?>
		                                                    <i class="fa fa-map-marker header-delivery-icon" aria-hidden="true"></i>Ваш адрес: <a href="#inpage-map" class="head-d-adress"><?=$address ? $address : "выбрать"?></a>
		                                                    <?}else{?>
		                                                    <i class="fa fa-map-marker header-delivery-icon" aria-hidden="true"></i>Ваш адрес: <a class="call_form head-d-adress"><?=$address ? $address : "выбрать"?></a>
		                                                    <?}?>
															<?//\Aspro\Functions\CAsproMax::showRegionList();?>
														</div>
														<div class="wrap_icon inner-table-block">
															<?$APPLICATION->IncludeComponent(
																	"bitrix:menu",
																	"top_addition_menu",
																	Array(
																		"ALLOW_MULTI_SELECT" => "N",
																		"CHILD_MENU_TYPE" => "",
																		"DELAY" => "N",
																		"MAX_LEVEL" => "1",
																		"MENU_CACHE_GET_VARS" => array(""),
																		"MENU_CACHE_TIME" => "3600",
																		"MENU_CACHE_TYPE" => "N",
																		"MENU_CACHE_USE_GROUPS" => "Y",
																		"ROOT_MENU_TYPE" => "top_addition",
																		"USE_EXT" => "N"
																	)
																);?>
															<div class="phone-block icons">
																<?if($bPhone):?>
																	<?//CMax::ShowHeaderPhones('');?>
																<?endif?>
																<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
																	<div class="inline-block">
																		<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
																	</div>
																<?endif;?>
															</div>
														</div>
													</div>
												<?endif;?>
											</div>
										</div>
									</div>
									<div class="subbottom">

										<div class="menu">
											<div class="menu-only">
												<nav class="mega-menu sliced">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
														array(
															"COMPONENT_TEMPLATE" => ".default",
															"PATH" => SITE_DIR."include/menu/menu.subtop_content.php",
															"AREA_FILE_SHOW" => "file",
															"AREA_FILE_SUFFIX" => "",
															"AREA_FILE_RECURSIVE" => "Y",
															"EDIT_TEMPLATE" => "include_area.php"
														),
														false, array("HIDE_ICONS" => "Y")
													);?>
												</nav>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="auth">
										<div class="header-user-menu">
											<?//=CMax::showCabinetLink(true, true, 'big');?>
											<!--'start_frame_cache_header-auth-block2'-->
											<!-- noindex -->
											<div class="header-user-menu-item">
												<a href="/personal/"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
												<a class="header-user-menu-link" href="/personal/orders/">МОИ ЗАКАЗЫ</a>
											</div>
											<?global $USER;
											if($USER->IsAuthorized()){?>
											<div class="header-user-menu-item">
												<a href="/personal/"><i class="fa fa-user" aria-hidden="true"></i></a>
												<a class="header-user-menu-link" href="/personal/">ЛИЧНЫЙ КАБИНЕТ</a>
											</div>
											<?}else{?>
											<div class="header-user-menu-item">
												<a data-event="jqm" data-param-type="auth" data-param-backurl="/" data-name="auth" href="/personal/"><i class="fa fa-user" aria-hidden="true"></i></a>
												<a class="header-user-menu-link" data-event="jqm" data-param-type="auth" data-param-backurl="/" data-name="auth" href="/personal/">ВОЙТИ</a>
											</div>
											<?}?>
											<!-- /noindex -->
											<!--'end_frame_cache_header-auth-block2'-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?// class=logo-row?>
	</div>
	<div class="menu-row middle-block bg<?=strtolower($arTheme["MENU_COLOR"]["VALUE"]);?>">
		<div class="maxwidth-theme">
			<div class="row">
				<div class="col-md-12 menu-only">
					<div class="right-icons pull-right">
						<div class="pull-right">
							<?=CMax::ShowBasketWithCompareLink('', '', false, 'wrap_icon inner-table-block');?>
						</div>
					</div>
					<div class="menu-only-wr pull-left">
						<nav class="mega-menu">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/menu/menu.only_catalog.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "include_area.php"
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						</nav>
					</div>
					<div class="search-block">
						<div class="inner-table-block">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/top_page/search.title.catalog.php",
									"EDIT_TEMPLATE" => "include_area.php",
									'SEARCH_ICON' => 'Y'
								)
							);?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line-row visible-xs"></div>
</div>

/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
$(document).ready(function(){
	//убрать открытие меню по свайпу
    $("body").addClass("swipeignore");
    
//     var $header = $('#header');
//     setInterval(function() {
//         if( $(window).scrollTop() > 150 ) {
//             $header.addClass('sticky-header');
//         } else {
//             $header.removeClass('sticky-header');
//         }
//     }, 250);
    
    // правила для выбора магазинов
    function replaceCartButton() {
        if ( !$.cookie('current_store') ){
            $(".counter_wrapp .button_block").addClass("wide");
            $(".counter_wrapp .button_block.wide").html('<span class="btn-exlg call_form btn no-address btn-default transition_bg animate-load has-ripple"><span>Выбрать адрес доставки</span></span>');
            $(".item-stock ").css("visibility", "hidden");
            $(".counter_block").addClass("d-none");
            $(".counter_block_inner").addClass("d-none");
        }
    }
    function basketCheck() {
        for(key in arBasketAspro.NOT_AVAILABLE) {
            $('.js_offers__'+key+'_block.footer_button').hide();
        }
    }
	function insertPercents() {
        var newPercentList = $('.cost.prices');
        if ($('.cost.prices .price_matrix_block').children().hasClass('acb496c6-9506-11ea-80bf-00505697dbd3')) {
            newPercentList = $('.cost.prices .price_matrix_block');
        }
        $.each(newPercentList, function(){
            var priceListElemntOld = $(this).children('.acb496c6-9506-11ea-80bf-00505697dbd3');
            
            
            if (priceListElemntOld.length > 0) {
                var priceListElemntNew = $(this).children('.1618b9f0-7969-11ea-9fc5-40167e7389e1');
                
                var oldPrice = priceListElemntOld.find('.price_value').text();
                    oldPrice = parseFloat(oldPrice.replace(/\s+/g, ""));
                var newPrice = priceListElemntNew.find('.price_value').text();
                    newPrice = parseFloat(newPrice.replace(/\s+/g, ''));
                var percenPrice = Math.round(((newPrice / oldPrice) - 1) * 100);
                if (percenPrice && $(this).children('.sale_block').length < 1){
                    $(this).append('<div class="sale_block"><div class="sale_wrapper font_xxs"><div class="sale-number rounded2"><div class="value">'+percenPrice+'</span>%</div></div></div></div>');
                }
            }
        });
    }
    insertPercents();
	replaceCartButton();
//     basketCheck();
    BX.addCustomEvent('onAjaxSuccess', function(){
        replaceCartButton();
        insertPercents();
        basketCheck();
    });
    BX.addCustomEvent('onAjaxSuccess', function(){
        replaceCartButton();
        insertPercents();
        basketCheck();
    });
    BX.addCustomEvent('onAjaxSuccessFilter', function(){
        replaceCartButton();
        insertPercents();
        basketCheck();
    });
    BX.addCustomEvent('onCompleteAction', function(){
        replaceCartButton();
        insertPercents();
        basketCheck();
    });



});

//товары 18+
var date = new Date();
date.setDate(date.getDate()+2);
//date.setTime(date.getTime()+(60000));
function track_user_adult() {
    document.cookie = "adult=Y; expires="+ date.toGMTString() + "; path=/";
}
function user_adult_alert() {
        $('body').append('<div class="item-adult-content"><div class="item-adult-text-cont"><p class="item-adult-text">Данный раздел не рекомендован для лиц, не достигших возраста совершеннолетия.</p><span class="item-adult-appruve">Мне исполнилось 18 лет</span><p class="item-noadult-text"><a href="/catalog/">Мне еще нет 18 лет</a></p></div></div>');
}
function cookie_adult_check() {
    if ( $.cookie('adult') !== "Y" ){
        user_adult_alert();
    }
    $(".item-adult-appruve").click(function() {
        $('.item-adult-content').remove();
        track_user_adult();
    });
}


//попап выбора адреса
$(function() {
    // вызываем форму
    $(document).on("click", ".call_form", function(e) {
    	e.preventDefault();// отменяем переход по ссылке

    	var this_ = $(this);
    	if(this_.hasClass("disabled"))
    		return false;
        
        this_.addClass("disabled");

        $.ajax({
            url: "/bitrix/js/ctweb.yandexdelivery.custom/delivery.php",
            type: "POST",
            data: {},
            success: function(data) {
            	$("body").append(data);
            	$(".modal_background").css({"display":"flex"});
            	$(".modal_form").fadeIn();
        	this_.removeClass("disabled");
            }
        });
    });

        //вызов на заказе
    $(document).on("click", ".modal-map-order", function(e) {
        e.preventDefault();// отменяем переход по ссылке
        $(".modal_background.order").addClass("visible");
    });
    $(document).on("click", ".success-btn.choose,.close_form_nosave_order", function(e) {
        $(".modal_background.order").removeClass("visible");
    });
    
    // а это для закрытия формы
//     $(document).on("click", ".close_form", function(e) {
//         e.preventDefault();
//         $(".modal_background").hide().remove();
//         location.reload();
//         return false;
// 
//     });
    $(document).on("click", ".close_form_nosave", function(e) {
        e.preventDefault();
        $(".modal_background").hide().remove();
        
        return false;

    });
});

// if(!funcDefined('checkMinPrice')){
	checkMinPrice = function(){
		if(arAsproOptions["PAGES"]["BASKET_PAGE"]){
			var summ_raw=0,
				summ=0;
			if($('#allSum_FORMATED').length)
			{
				summ_raw=$('#allSum_FORMATED').text().replace(/[^0-9\.,]/g,'');
				summ=parseFloat(summ_raw);
				if($('#basket_items').length)
				{
					var summ = 0;
					$('#basket_items tr').each(function(){
						if(typeof ($(this).data('item-price')) !== 'undefined' && $(this).data('item-price'))
							summ += $(this).data('item-price')*$(this).find('#QUANTITY_INPUT_'+$(this).attr('id')).val();
					})
				}
				if(!$('.catalog_back').length)
					$('.bx_ordercart_order_pay_center').prepend('<a href="'+arAsproOptions["PAGES"]["CATALOG_PAGE_URL"]+'" class="catalog_back btn btn-default btn-lg white grey">'+BX.message("BASKET_CONTINUE_BUTTON")+'</a>');
			}

			if(arAsproOptions["THEME"]["SHOW_ONECLICKBUY_ON_BASKET_PAGE"] == "Y")
				$('.basket-coupon-section').addClass('smallest');

			if(typeof BX.Sale !== "undefined")
			{
				if(typeof BX.Sale.BasketComponent !== "undefined" && typeof BX.Sale.BasketComponent.result !== "undefined")
					summ = BX.Sale.BasketComponent.result.allSum;
			}
            var svgMinPrice = '<i class="svg  svg-inline-price colored_theme_svg" aria-hidden="true"><svg id="Group_278_copy" data-name="Group 278 copy" xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38"><path id="Ellipse_305_copy_2" data-name="Ellipse 305 copy 2" class="clswm-1" d="M1851,561a19,19,0,1,1,19-19A19,19,0,0,1,1851,561Zm0-36a17,17,0,1,0,17,17A17,17,0,0,0,1851,525Zm3.97,10.375-0.03.266c-0.01.062-.02,0.127-0.03,0.188l-0.94,7.515h0a2.988,2.988,0,0,1-5.94,0H1848l-0.91-7.525c-0.01-.041-0.01-0.086-0.02-0.128l-0.04-.316h0.01c-0.01-.125-0.04-0.246-0.04-0.375a4,4,0,0,1,8,0c0,0.129-.03.25-0.04,0.375h0.01ZM1851,533a2,2,0,0,0-2,2,1.723,1.723,0,0,0,.06.456L1850,543a1,1,0,0,0,2,0l0.94-7.544A1.723,1.723,0,0,0,1853,535,2,2,0,0,0,1851,533Zm0,14a3,3,0,1,1-3,3A3,3,0,0,1,1851,547Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,1851,551Z" transform="translate(-1832 -523)"></path>  <path class="clswm-2 op-cls" d="M1853,543l-1,1h-2l-1-1-1-8,1-2,1-1h2l1,1,1,2Zm-1,5,1,1v2l-1,1h-2l-1-1v-2l1-1h2Z" transform="translate(-1832 -523)"></path></svg></i>';
            
			if(arAsproOptions["PRICES"]["MIN_PRICE"]){
				if(arAsproOptions["PRICES"]["MIN_PRICE"]>summ || BX.Sale.BasketComponent.result.allWeight > 35000){
					
					if($('.oneclickbuy.fast_order').length)
						$('.oneclickbuy.fast_order').remove();

					if($('.basket-checkout-container').length)
					{
						if(!$('.icon_error_wrapper').length && arAsproOptions["PRICES"]["MIN_PRICE"]>summ){
							$('.basket-checkout-block.basket-checkout-block-btn').html('<div class="icon_error_wrapper"><div class="icon_error_block">'+svgMinPrice+BX.message("MIN_ORDER_PRICE_TEXT").replace("#PRICE#", jsPriceFormat(arAsproOptions["PRICES"]["MIN_PRICE"]))+'</div></div>');
						}else if(!$('.icon_error_wrapper').length && BX.Sale.BasketComponent.result.allWeight > 35000){
							$('.basket-checkout-block.basket-checkout-block-btn').html('<div class="icon_error_wrapper"><div class="icon_error_block">'+svgMinPrice+'<p style="margin-top: 10px;margin-bottom: 0;font-weight: 600;" >Максимальный вес заказа 35 кг</p></div></div>');
						}
					}
					else
					{
						if(!$('.icon_error_wrapper').length  && typeof jsPriceFormat !== 'undefined'){
							$('.bx_ordercart_order_pay_center').prepend('<div class="icon_error_wrapper"><div class="icon_error_block">'+svgMinPrice+BX.message("MIN_ORDER_PRICE_TEXT").replace("#PRICE#", jsPriceFormat(arAsproOptions["PRICES"]["MIN_PRICE"]))+'</div></div>');
						}
						if($('.bx_ordercart_order_pay .checkout').length)
							$('.bx_ordercart_order_pay .checkout').remove();
					}
				}else{
					if($('.icon_error_wrapper').length)
						$('.icon_error_wrapper').remove();

					if($('.basket-checkout-container').length)
					{
						if(!$('.oneclickbuy.fast_order').length && arAsproOptions["THEME"]["SHOW_ONECLICKBUY_ON_BASKET_PAGE"] == "Y" && !$('.basket-btn-checkout.disabled').length)
							$('.basket-checkout-section-inner').append('<div class="fastorder"><span class="oneclickbuy btn btn-lg fast_order btn-transparent-border-color" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+'</span></div>');
					}
					else
					{
						if($('.bx_ordercart_order_pay .checkout').length)
							$('.bx_ordercart .bx_ordercart_order_pay .checkout').css('opacity','1');
						else
							$('.bx_ordercart_order_pay_center').append('<a href="javascript:void(0)" onclick="checkOut();" class="checkout" style="opacity: 1;">'+BX.message("BASKET_ORDER_BUTTON")+'</a>');
						if(!$('.oneclickbuy.fast_order').length && arAsproOptions["THEME"]["SHOW_ONECLICKBUY_ON_BASKET_PAGE"] == "Y")
							$('.bx_ordercart_order_pay_center').append('<span class="oneclickbuy btn btn-lg fast_order btn-transparent-border-color" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+'</span>');
					}
				}
			}else{
				if($('.basket-checkout-container').length)
				{
					if(!$('.oneclickbuy.fast_order').length && arAsproOptions["THEME"]["SHOW_ONECLICKBUY_ON_BASKET_PAGE"] == "Y" && !$('.basket-btn-checkout.disabled').length)
						$('.basket-checkout-section-inner').append('<div class="fastorder"><span class="oneclickbuy btn btn-lg fast_order btn-transparent-border-color" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+'</span></div>');
				}
				else
				{
					$('.bx_ordercart .bx_ordercart_order_pay .checkout').css('opacity','1');
					if(!$('.oneclickbuy.fast_order').length && arAsproOptions["THEME"]["SHOW_ONECLICKBUY_ON_BASKET_PAGE"] == "Y")
						$('.bx_ordercart_order_pay_center').append('<span class="oneclickbuy btn btn-lg fast_order btn-transparent-border-color" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+'</span>');
				}
			}

			$('#basket-root .basket-checkout-container .basket-checkout-section .basket-checkout-block .basket-btn-checkout');
			$('#basket-root .basket-checkout-container').addClass('visible');
		}
	}
// }
// стилизация селектов с уточнением чтобы не визулизировать - сласс no-visual
function initSelects(target){
	var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	if ( iOS ) return;
	if($("#bx-soa-order").length)
		return;
	// SELECT STYLING
	$(target).find('.wrapper1 select:visible:not(.no-visual)').ikSelect({
		syntax: '<div class="ik_select_link"> \
					<span class="ik_select_link_text"></span> \
					<div class="trigger"></div> \
				</div> \
				<div class="ik_select_dropdown"> \
					<div class="ik_select_list"> \
					</div> \
				</div>',
		dynamicWidth: true,
		ddMaxHeight: 112,
		customClass: 'common_select',
		//equalWidths: true,
		onShow: function(inst){
			inst.$dropdown.css('top', (parseFloat(inst.$dropdown.css('top'))-5)+'px');
			if ( inst.$dropdown.outerWidth() < inst.$link.outerWidth() ){
				inst.$dropdown.css('width', inst.$link.outerWidth());
			}
			if ( inst.$dropdown.outerWidth() > inst.$link.outerWidth() ){
				inst.$dropdown.css('width', inst.$link.outerWidth());
			}
			var count=0,
				client_height=0;
			inst.$dropdown.css('left', inst.$link.offset().left);
			$(inst.$listInnerUl).find('li').each(function(){
				if(!$(this).hasClass('ik_select_option_disabled')){
					++count;
					client_height+=$(this).outerHeight();
				}
			})
			if(client_height<112){
				inst.$listInner.css('height', 'auto');
			}else{
				inst.$listInner.css('height', '112px');
			}
			inst.$link.addClass('opened');
			inst.$listInner.addClass('scroller');
			if($('.confirm_region').length)
				$('.confirm_region').remove();
		},
		onHide: function(inst){
			inst.$link.removeClass('opened');
		}
	});
	// END OF SELECT STYLING

	var timeout;
	$(window).on('resize', function(){
		ignoreResize.push(true);
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				//$('select:visible').ikSelect('redraw');
				var inst='';
				if(inst=$('.common_select-link.opened + select').ikSelect().data('plugin_ikSelect')){
					inst.$dropdown.css('left', inst.$link.offset().left+'px');
				}
			}, 20);
		ignoreResize.pop();
	});
}
// максимально доступное количество товара
// $(document).ready(function(){
//    $(document).on("click", ".counter_block:not(.basket) .plus", function(){
// 		if(!$(this).parents('.basket_wrapp').length){
// 			if($(this).parent().data("offers")!="Y"){
// 				if(curValue>$(this).data('max'))
//                     
//                     console.log($(this).data('max'));
// 			}
// 		}
// 	}); 
// });




$(document).ready(function(){

	$("body").on('click', 'form[name=regform] button[name=register_submit_button1]', function(){
		var form = $(this).closest('form');

		console.log("Регистрация");

		$.post( "/lead.php", { 
			lead:1, 
			metrika_client_id:yaCounter64330144.getClientID(), 
			name: $("#input_NAME", form).val(), 
            phone: $("#input_PERSONAL_PHONE", form).val(),
            email: $("#input_EMAIL", form).val(),
			description: "Решистрация"
		})

		.done(function(id) {
			if(id) yaCounter64330144.params({'wbooster':id})
	    });

	})

	$("body").on('click', 'form[id=sendform] input[name=c-form__send]', function(){
		var form = $(this).closest('form');

		console.log('Форма обратной связт');

		$.post( "/lead.php", { 
			lead:1, 
			metrika_client_id:yaCounter64330144.getClientID(), 
			name: $("input[name=name]", form).val(), 
			email: $("input[name=emal]", form).val(),
			phone: $("input[name=phone]", form).val(),
			description: "Форма обратной связт"
		})

		.done(function(id) {
			if(id) yaCounter64330144.params({'wbooster':id})
	    });
    })
    


	$("body").on('click', 'form[name=form_comment] input[name=sub-post]', function(){
		var form = $(this).closest('form');

		console.log('Отзывы');

		$.post( "/lead.php", { 
			lead:1, 
			metrika_client_id:yaCounter64330144.getClientID(), 
			name: $("input[name=user_name]", form).val(), 
			email: $("input[name=user_email]", form).val(),
			description: "Отзывы"
		})

		.done(function(id) {
			if(id) yaCounter64330144.params({'wbooster':id})
	    });

    })
        

    $("body").on('click', 'form[name=ORDER_FORM] a.btn-order-save', function(){
		var form = $(this).closest('form');

        console.log('Заказ');
		$.post( "/lead.php", { 
			lead:1, 
			metrika_client_id:yaCounter64330144.getClientID(), 
			name: $("input[name=ORDER_PROP_1]", form).val(), 
			email: $("input[name=ORDER_PROP_2]", form).val(),
			phone: $("input[name=ORDER_PROP_3]", form).val(),
            value: $("#bx-soa-total-mobile .bx-soa-cart-total-line-total .bx-soa-cart-d").text().replace(' руб.',''),
			description: "Заказ"
		})
		console.log($("#bx-soa-total-mobile .bx-soa-cart-total-line-total .bx-soa-cart-d").text().replace(' руб.',''))

		.done(function(id) {
			if(id) yaCounter64330144.params({'wbooster':id})
	    });

	})


});

function jivo_onLoadCallback() {
    setInterval(function(){ 
    if (document.cookie.indexOf('wbsend=1') === -1) {  
  
      wbci = jivo_api.getContactInfo()
  
      if(wbci.client_name && (wbci.phone || wbci.email)) {
        var now = new Date(); now.setTime(now.getTime() + 30 * 3600 * 1000 * 24);
        document.cookie = "wbsend=1; expires=" + now.toUTCString() + "; path=/";
        $.post( "/lead.php", {
          lead:1, 
          metrika_client_id:yaCounter64330144.getClientID(), 
          name: wbci.client_name, 
          phone: wbci.phone, 
          email:wbci.email, 
          description: 'jivosite.com'
        })
        .done(function( id ) {
          if(id) yaCounter64330144.params({'wbooster':id})
        });   
      }}
	}, 3000)
}

// //добавление в корзину
// $('span.to-cart').on('click', function() {
//     ym(64330144,'reachGoal','add_cart')
// });

// ///basket посещение страницы 

// //переход к этапу оформить заказ
// $('button.basket-btn-checkout').on('click', function() {
//     ym(64330144,'reachGoal','place_order')
// });

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use \Bitrix\Main\Localization\Loc as Loc;
if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}

$mainID = $this->GetEditAreaId('');
$arResult['JSPARAMS']['TEMPLATE']['MODAL'] = $mainID . "modal";
$arResult['JSPARAMS']['TEMPLATE']['MODAL_CANCEL'] = $mainID . "cancel";
$arResult['JSPARAMS']['TEMPLATE']['MODAL_SAVE'] = $mainID . "save";
$arResult['JSPARAMS']['TEMPLATE']['LINK'] = $mainID . "link"; // for delivery

$templateIDs = &$arResult['JSPARAMS']['TEMPLATE'];
?>

<div class="modal_background order">
	<div class="modal_form">
	<span class="close_form_nosave_order" id="<?= $templateIDs['MODAL_CANCEL']; ?>" title="Закрыть">X</span>
	<div class="dilevery-wrapper">
    <div class="ctweb-yandexdelivery">
        
        <div class="body">
            <div class="delivery-left-block">
                
                <p><b>Введите адрес для доставки и поиска ближайшего магазина</b> <br>или выберите место на карте двойным нажатием</p>
                <input type="text" id="suggest" class="yamaps-suggest-input"/>
            </div>
            <div id="<?= $templateIDs['SPINNER']; ?>" class="loader-wrap"><div class="loader"><i class="fa fa-spinner fa-spin"></i></div></div>
            <div id="<?= $templateIDs['MAP']; ?>" class="map-body" style="width: auto; height: 400px;"></div>
        </div>
        <div class="footer">
            <div class="ctweb-yandexdelivery__calculates">
                <span id="<?= $templateIDs['ADDRESS']; ?>" ></span>
                <span id="<?= $templateIDs['DISTANCE']; ?>" ></span>
                <span id="<?= $templateIDs['PRICE']; ?>" ></span>
            </div>
            <button id="<?= $templateIDs['MODAL_SAVE']; ?>" class="success-btn choose d-none"></button>
            <a href="/basket/?addressChanged=y" class="btn btn-default close_form has-ripple">Сохранить</a>
        </div>
    </div>
    </div>
    </div>
</div>
<? include "script.php"; ?>
<script>
    //
    //  Example handlers
    //

    BX.addCustomEvent('yandexdelivery.initialized', function (controller) {
        // Set focus to nearest store
        if (controller.arStores.length) {
            controller.CenterToStore(controller.arStores[0].ID);
            ymaps.geolocation.get({
                provider: 'yandex',
                mapStateAutoApply: true
            }).then(function (res) {
                controller.CenterToStore(controller.getNearestStore(res.geoObjects.position).ID);
            }, function () {
                controller.CenterToStore(controller.getNearestStore([0, 0]).ID);
            });
        }
        
        
    });


    // display calculate results example
    BX.addCustomEvent('yandexdelivery.calculate', function(e) {
        var calcInput = document.querySelector('.ctweb-yandexdelivery__calculates');

        if (!e.error && calcInput)
            calcInput.innerHTML = BX.message('CW_YD_DISTANCE') + ': <b>' + e.result.DISTANCE + '</b>; ' + BX.message('CW_YD_PRICE') + ': <b>' + e.result.PRICE_FORMATTED + '</b>';
    });

    // display address result
    BX.addCustomEvent('yandexdelivery.address_response', function (e) {
        var calcInput = document.querySelector('.ctweb-yandexdelivery__calculates');

        if (!e.error && calcInput)
            $(".address-popup").remove();
            calcInput.innerHTML = "<span class='address-popup'>Текущий адрес: <b>" + e.ADDRESS + "</b><br></span>" + calcInput.innerHTML;
    });
</script>


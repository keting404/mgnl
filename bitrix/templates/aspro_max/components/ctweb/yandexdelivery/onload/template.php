<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use \Bitrix\Main\Localization\Loc as Loc;

if (method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}

$templateIDs = &$arResult['JSPARAMS']['TEMPLATE'];
?>
<div class="dilevery-wrapper">
    <div class="ctweb-yandexdelivery">
        <div id="<?= $templateIDs['SPINNER']; ?>" class="loader-onlogin"><i class="fa fa-spinner fa-spin"></i>Загружаем информацию о Вашем адресе</div>
        <div class="body">
            <div class="delivery-left-block">
                <input type="text" id="suggest" style="display:none" class="yamaps-suggest-input"/>
            </div>
            
            <div id="<?= $templateIDs['MAP']; ?>" class="map-body" style="display:none"></div>
        </div>
        
    </div>
</div>
<? include "script.php"; ?>
<script>
    // display formatted calculate results example
    BX.addCustomEvent('yandexdelivery.calculate', function(e) {
        var calcInput = document.querySelector('.ctweb-yandexdelivery__calculates');

        if (!e.error) {
            calcInput.innerHTML = '';
            }
    });
    // display address result
    BX.addCustomEvent('yandexdelivery.address_response', function (e) {
        BX.reload(false);
    });
</script>


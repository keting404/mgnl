<?php

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID" => IntVal(12), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, Array("nPageSize" => 5000), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();

    $arResult['shops'][$arFields['ID']] = $arFields['NAME'];
}

<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * @var string $templateFolder
 */
CJSCore::Init();
// $APPLICATION->AddHeadScript($templateFolder.'/js/jquery.fileupload.js');
CModule::IncludeModule("iblock"); 
?>

<div class="reviews">
    <div class="reviewsList">
        <?php foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <div class="reviewsItem">
                <div class="reviewsItemHeader">
                    <div class="reviewsItemName"><?php echo $arItem['PROPERTIES']['PROP1']['VALUE']; ?></div>
                    <div class="reviewsItemDateTime"><?php echo $arItem['PROPERTIES']['PROP4']['VALUE']; ?></div>
                    <div class="reviewsItemShop">
                    <span class="reviewsItemName">Магазин: </span>
                    <?
                        $res = CIBlockElement::GetByID($arItem['PROPERTIES']['PROP7']['VALUE']);
                        if($ar_res = $res->GetNext())
                            echo $ar_res['NAME'];
                    ?></div>
                    <div class="reviewsItemRating">
                        <?php for ($x = 0; $x < 5; $x++): ?>

                            <i <?php if ($x < $arItem['PROPERTIES']['PROP3']['VALUE']): ?> class="active"<?php endif; ?>></i>

                        <?php endfor; ?>
                    </div>
                </div>
                <div class="reviewsItemText">
                    <?php echo $arItem['PREVIEW_TEXT']; ?>
                </div>
                <?php if ($arItem['DETAIL_TEXT'] != ''): ?>
                    <div class="reviewsItem">
                        <div class="reviewsItemHeader">
                            <div class="reviewsItemName reviewsItemName_admin">Администрация</div>
                            <div class="reviewsItemDateTime"><?php echo $arItem['PROPERTIES']['PROP5']['VALUE']; ?></div>
                        </div>
                        <div class="reviewsItemText">
                            <?php echo $arItem['DETAIL_TEXT']; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <? endforeach; ?> 

        <a  name="add"></a>
        <div class="pagination" >
            <?= $arResult["NAV_STRING"] ?>
        </div>

    </div>
</div>

<form class="writeReview" action="/ajax/writeReview.php" id="writeReview" method="POST">
    <div class="writeReviewTitle">
        Оставьте свой отзыв
    </div>

    <div class="writeReviewTop success_hiding">
        <div class="writeReviewEl writeReviewEl_wide">
            <input type="text" value="" name="name" placeholder="Представьтесь, пожалуйста">
        </div>

        <div class="writeReviewEl">
            <input type="email" value="" name="email" required placeholder="Электронная почта (обязательно)">
        </div>

        <div class="writeReviewEl">
            <input type="text" value="" name="phone" placeholder="Номер телефона">
        </div>

    </div>
	<p class="success_hiding">Выберите магазин</p>
    <select name="shop" required class="no-visual">
        <option value=""></option>
        <?php foreach ( $arResult['shops'] as $key => $arItem): ?>
        <option value="<?php echo $key;?>"><?php echo $arItem;?></option>
        <? endforeach; ?> 

    </select>
    
    <textarea class="success_hiding" name="text" placeholder="Опишите свои впечатления от посещения магазина «Магнолия»"></textarea>
    <?
    $APPLICATION->IncludeComponent(
	"bitrix:main.file.input", 
	"mobile", 
	array(
		"INPUT_NAME" => "FILE",
		"MULTIPLE" => "Y",
		"MODULE_ID" => "main",
		"MAX_FILE_SIZE" => "10000000",
		"ALLOW_UPLOAD" => "I",
		"ALLOW_UPLOAD_EXT" => "",
		"maxCount" => "1",
		"COMPONENT_TEMPLATE" => "mobile"
	),
	false
);
    ?>
    <div class="writeReviewBottom success_hiding">
        <div class="writeReviewRating">
            <input type="hidden" value="0" name="writeReviewRatingVal" class="writeReviewRatingVal">
            <div class="writeReviewRatingTitle">Оставьте свою оценку!</div>
            <div class="writeReviewRatingList">
                <span data-val="1"></span>
                <span data-val="2"></span>
                <span data-val="3"></span>
                <span data-val="4"></span>
                <span data-val="5"></span>
            </div>
        </div>

        <button type="submit" class="btn-small btn btn-default writeReviewSubmit has-ripple"><span>Отправить</span></button>
    </div>
</form>

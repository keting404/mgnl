var jobs = {
    initialized: false,
    init: function() {
        this.initialized = true;
        $(".accordionItemHeader").click(function() {
            $(this).closest(".accordionItem").toggleClass("active");
        });
        $(".jobsTabs span").click(function() {
            var id = $(this).data("id");
            $(this).closest(".jobs").find(".jobsPanel").removeClass("active");
            $(this).closest(".jobs").find("#" + id).addClass("active");
            $(".jobsTabs span").removeClass('active');
            $("." + id).addClass('active');
        });
    }
};
$( document ).ready(function() {
    jobs.init();
});
<?php
foreach ($arResult['ITEMS'] as $key => $arItem):
    $n = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID'])->fetch();
    $arResult['RESULT'][$arItem['IBLOCK_SECTION_ID']]['NAME'] = $n['NAME'];
    $arResult['RESULT'][$arItem['IBLOCK_SECTION_ID']]['CODE'] = $n['CODE'];
    $arResult['RESULT'][$arItem['IBLOCK_SECTION_ID']]['ITEMS'][] = $arItem;
endforeach;
?> 
<div class="jobsTabs">
    <i>Вакансии:</i>
    <?php $b = 0;?>
    <?php foreach ($arResult['RESULT'] as $key => $arItem): ?>
        <span  class="<?php if ($b == 0): ?>active<?php endif; ?>  <?php echo $arItem['CODE']; ?>"  data-id="<?php echo $arItem['CODE']; ?>"><?php echo $arItem['NAME']; ?></span>
    <?php $b++;?>
            <? endforeach; ?> 
</div>
<?php
$a = 0;
foreach ($arResult['RESULT'] as $key1 => $arItem1): ?>
    <div class="jobsPanel<?php if($a==0):?> active<?php endif;?>" id="<?php echo $arItem1['CODE'];?>">
        <div class="accordion">
            <?php foreach ($arItem1['ITEMS'] as $key => $arItem): ?>
                <div class="accordionItem">
                    <div class="accordionItemHeader">
                        <div class="accordionItemName"><?php echo $arItem['NAME']; ?></div>
                        <div class="accordionItemSalary"><?php echo $arItem['PROPERTIES']['PAY']['VALUE']; ?></div>
                    </div>
                    <div class="accordionItemContent jobsItemContent">
                        <table>
                            <tr>
                                <td>
                                    <b>Требования:</b>
                                </td>
                                <td><?php echo $arItem['PROPERTIES']['PROP1']['~VALUE']['TEXT']; ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Условия:</b>
                                </td>
                                <td><?php echo $arItem['PROPERTIES']['PROP2']['~VALUE']['TEXT']; ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Обязанности:</b>
                                </td>
                                <td><?php echo $arItem['PROPERTIES']['PROP3']['~VALUE']['TEXT']; ?></td>
                            </tr>
                        </table>
                        <span class="btn btn-default btn-lg animate-load has-ripple getIt showPopup" data-event="jqm" data-param-form_id="RESUME" data-name="RESUME" data-autoload-post="<?=$arItem['NAME']?>"><span>Заполнить анкету</span></span>
                    </div>
                </div>
            <? endforeach; ?> 
        </div>
    </div>
<?php $a++;?>
<? endforeach; ?> 

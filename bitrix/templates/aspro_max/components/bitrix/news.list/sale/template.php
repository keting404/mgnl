<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
$templateFolder = $this->getFolder();
$APPLICATION->SetAdditionalCSS($templateFolder.'/swiper.min.css', true);
$this->addExternalJs($templateFolder.'/swiper.min.js');
?>
<div class="swiper-block"><a class="btn btn-lg btn-default basket-btn-checkout has-ripple catalog-skidok-href" href="/upload/combined.pdf" download>CКАЧАТЬ</a></div>
<!-- Swiper -->
<div class="swiper-block">
  <div class="swiper-inner">
  <div class="swiper-container-cat sale-swiper">
      <div class="swiper-wrapper gallery">
          <?php foreach ($arResult['ITEMS'] as $key => $arItem): ?>
              <div class="swiper-slide">
                  <?php   $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>674, 'height'=>953), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                  
                  <img href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" data-fancybox="gallery" class="gallery__item"  src="<?php echo $file['src'];?>" alt="">
              </div>
          <? endforeach; ?> 
      </div>
  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination sale-katalog"></div>
  <!-- Add Arrows -->
  <div class="slick-next"></div>
  <div class="slick-prev"></div>


<!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container-cat', {
      slidesPerView: 2,
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.slick-next',
        prevEl: '.slick-prev',
      },
    });
  </script>
  <div>
</div>
<?
if (max($timesArr) > filemtime($_SERVER['DOCUMENT_ROOT'].'/upload/catalog-skidok.pdf')) {
  $pdf = new Imagick($pdfImages);
  $pdf->setImageFormat('pdf');
  $pdf->writeImages($_SERVER['DOCUMENT_ROOT'].'/upload/catalog-skidok.pdf', true);
}
?>

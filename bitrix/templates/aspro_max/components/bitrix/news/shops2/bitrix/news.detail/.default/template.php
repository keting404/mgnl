<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID" => IntVal(6), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 500), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $metros[$arFields['ID']]['NAME'] = $arFields['NAME'];
    $metros[$arFields['ID']]['X'] = $arProps['X']['VALUE'];
    $metros[$arFields['ID']]['Y'] = $arProps['Y']['VALUE'];
    $metros[$arFields['ID']]['COLOR'] = $arProps['COLOR']['VALUE'];
}
?>
<script>
var shops = {"list":[
    <? $imgPrev = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>144, 'height'=>104), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
    {"coord":"<?=$arResult['PROPERTIES']['X']['VALUE']?>, <?=$arResult['PROPERTIES']['Y']['VALUE']?>",
    "addr":"<?=$arResult['PROPERTIES']['ADDR']['VALUE']?>",
    "phone":[
    <?php foreach ($arResult['PROPERTIES']["PHOTE"]["VALUE"] as $key => $arItemEl):
        echo '"'.$arItemEl.'",';
    endforeach;?>
    ],
    "metro":"<?=$metros[$arResult['PROPERTIES']['METRO']['VALUE']]['NAME']?>",
    "metroColor":"<?=$metros[$arResult['PROPERTIES']['METRO']['VALUE']]['COLOR']?>",
    "type":"<?=$arResult['PROPERTIES']['TYPE']['VALUE_XML_ID']?>",
    "link":"<?=$arResult['DETAIL_PAGE_URL']?>",
    "img":"<?=$imgPrev['src']?>",
    "canBy":["konditerskiy-tsekh"],
    "option":[
    <?php foreach ($arResult['PROPERTIES']["BUY"]["VALUE"] as $key => $arItemEl):
        echo '"'.$options[$arItemEl].'",';
    endforeach;?>
    ]
    },
]};
ymaps.ready(init);
</script>
<div class="NewsDetail NewsContent">
    <div class="row shops-detail-content">
        <div class="col-md-5 shop-image-container">
        <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
            <? $imgPrev = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>487, 'height'=>484), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
            <img
                class="detail_picture"
                border="0"
                src="<?=$imgPrev["src"]?>"
                width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
                height="auto"
                alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
                />
        <?endif?>
        </div>
        <div class="col-md-7 shop-content-container">
        <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
            <span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
        <?endif;?>
        <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
            <h3><?=$arResult["NAME"]?></h3>
        <?endif;?>
        <div class="row">
            <?if($arResult["PROPERTIES"]["METRO"]["VALUE"]){?>    
            <div class="col-12 col-md-4">
                <div class="metroStation <?php echo $metros[$arResult["PROPERTIES"]["METRO"]["VALUE"]]['COLOR']; ?>"><?php echo $metros[$arResult["PROPERTIES"]["METRO"]["VALUE"]]['NAME']; ?></div>
            </div>
            <?}?>
            <?if($arResult["PROPERTIES"]["TYPE"]["VALUE"]){?>    
            <div class="col shop-opener-block">
                <div class="<?=$arResult["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]?>"><?=$arResult["PROPERTIES"]["TYPE"]["VALUE"]?></div>
            </div>
            <?}?>
            <?if($arResult["PROPERTIES"]["YCAFE"]["VALUE_XML_ID"] == "Y"){?>    
            <div class="col-6 col-md-4 shop-opener-block">
                <div class="open"><?=$arResult["PROPERTIES"]["YCAFE"]["NAME"]?></div>
            </div>
            <?}?>
        </div>
        <?if(count($arResult["PROPERTIES"]["PHOTE"]["VALUE"])>0){?>
            <div class="phones-container">
                <span>Телефон<?if(count($arResult["PROPERTIES"]["PHOTE"]["VALUE"])>1){?>ы<?}?>: </span>
                <span class="phones-values">
                <?foreach($arResult["PROPERTIES"]["PHOTE"]["VALUE"] as $telItem){?>   
                    <a href="tel:<?=$telItem?>"><?=$telItem?></a><br>
                <?}?>
                </span>
            </div>
        <?}?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
        <?endif;?>
        <div id="map" class="d-none" style="width:100%; height:250px"></div>
        <div id="indexMap" class="shop-element-map" style="width:100%; height:250px"></div>
        <?if($arResult["NAV_RESULT"]):?>
            <?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
            <?echo $arResult["NAV_TEXT"];?>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
        <?endif;?>
        <div style="clear:both"></div>
        <br />
        <?foreach($arResult["FIELDS"] as $code=>$value):
            if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
            {
                ?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
                if (!empty($value) && is_array($value))
                {
                    ?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
                }
            }
            else
            {
                ?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
            }
            ?><br />
        <?endforeach;?>
        <?if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
        {
            ?>
            <div class="news-detail-share">
                <noindex>
                <?
                $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                        "PAGE_TITLE" => $arResult["~NAME"],
                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                        "HIDE" => $arParams["SHARE_HIDE"],
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
                ?>
                </noindex>
            </div>
            <?
        }
        ?>
        </div>
        <div class="col-md-5 shop-item-options">
            <ul class="ks-cboxtags">
            <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                <div class="shop-item-options-title-block">
                    <img src="/upload/logo_mgnl.png" /><h2 class="shop-item-options-title"><?=$arProperty["NAME"]?>:&nbsp;</h2>
                </div>
                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                <?foreach($arProperty["DISPLAY_VALUE"] as $value):?>
                    <li>
                        <input
                                type="checkbox"
                                checked
                                disabled
                            />
                        <label data-role="" class="bx-filter-param-label" for=""><?=$value?></label>
                    </li>
                <?endforeach;?>
                <?else:?>
                    <li>
                        <input
                                type="checkbox"
                                checked
                                disabled
                            />
                        <label data-role="" class="bx-filter-param-label" for=""><?=$arProperty["DISPLAY_VALUE"];?></label>
                    </li>
                <?endif?>
                <br />
            <?endforeach;?>
            </ul>
        </div>
        <div class="col-md-7 shop-item-options">
            <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
                <?echo $arResult["DETAIL_TEXT"];?>
            <?else:?>
                <?echo $arResult["PREVIEW_TEXT"];?>
            <?endif?>
        </div>
	</div>
</div>

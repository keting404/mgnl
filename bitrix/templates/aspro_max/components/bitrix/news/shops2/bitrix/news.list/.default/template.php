<?php
CModule::IncludeModule("iblock");
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID" => IntVal(46), "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 5000), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $metros[$arFields['ID']]['NAME'] = $arFields['NAME'];
    $metros[$arFields['ID']]['X'] = $arProps['X']['VALUE'];
    $metros[$arFields['ID']]['Y'] = $arProps['Y']['VALUE'];
    $metros[$arFields['ID']]['COLOR'] = $arProps['COLOR']['VALUE'];
}
?>
<?
if (CModule::IncludeModule('highloadblock')) {

    $ID = 4; // ИД 

    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById($ID)->fetch();
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $hlDataClass = $hldata["NAME"] . "Table";

    $result = $hlDataClass::getList(array(
                "select" => array("ID", "UF_NAME", "UF_XML_ID"), // Поля для выборки
                "order" => array("UF_SORT" => "ASC"),
                "filter" => array(),
    ));

    while ($res = $result->fetch()) {
        $options[$res['UF_XML_ID']] = $res['UF_NAME'];
    }
}?>
<?if(count($arResult['ITEMS'])<=0){?>
    <div class="container">
        <div class="catalogItemName">Магазины с такой комбинацией не найдены, пожалуйста, выберите другую опцию</div>
    </div>
<?}?>
<script>
var shops = {"list":[
    <?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
    <? $imgPrev = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>144, 'height'=>104), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
    <?if($arItem['PROPERTIES']['MAP']['VALUE']){?>
        {"coord":"<?=$arItem['PROPERTIES']['MAP']['VALUE']?>",
        "addr":"<?=$arItem['PROPERTIES']['ADDRESS1']['VALUE']?>",
        "phone":[
        <?php foreach ($arItem['PROPERTIES']["PHONE"]["VALUE"] as $key => $arItemEl):
            echo '"'.$arItemEl.'",';
        endforeach;?>
        ],
        "metro":"<?=$metros[$arItem['PROPERTIES']['METRO']['VALUE']]['NAME']?>",
        "metroColor":"<?=$metros[$arItem['PROPERTIES']['METRO']['VALUE']]['COLOR']?>",
        "type":"<?=$arItem['PROPERTIES']['TYPE']['VALUE_XML_ID']?>",
        "link":"<?=$arItem['DETAIL_PAGE_URL']?>",
        "img":"<?=$imgPrev['src']?>",
        "canBy":["konditerskiy-tsekh"],
        "option":[
        <?php foreach ($arItem['PROPERTIES']["BUY"]["VALUE"] as $key => $arItemEl):
            echo '"'.$options[$arItemEl].'",';
        endforeach;?>
        ]
        },
    <?}?>
    <?endforeach;?>
]};
ymaps.ready(init);
</script>


<div class="indexMapYandex active">
            <div id="map" class="d-none" style="width:100%; height:580px"></div>
            <div id="indexMap" style="width:100%; height:580px"></div>
        </div>
<div class="indexMapList">
            <div class="indexMapListContent">
<table>
    <thead>
        <tr>
            <th><span>Адрес</span></th>
            <th><span>Телефон</span></th>
            <th><span>Метро</span></th>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <tr data-type="<?php echo $arItem['PROPERTIES']['TYPE']['VALUE_XML_ID']?>">
                <td class="indexMapListName"><?php echo $arItem['PROPERTIES']['ADDRESS1']['VALUE']; ?></td>
                <td class="indexMapListPhone">
					<?if(is_array($arItem['PROPERTIES']['PHONE']['VALUE'])){?>
                    
                        <?php foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $key2 => $arItem2): ?>
                        <a href="tel:<?php echo $arItem2; ?>">
                            <?php echo $arItem2; ?>;
                        </a>
                        <? endforeach; ?> 
                    
                    <?}else{?>
                    <a href="tel:<?php echo $arItem['PROPERTIES']['PHONE']['VALUE']; ?>"><?php echo $arItem['PROPERTIES']['PHONE']['VALUE']; ?></a>    
                    <?}?>
                </td>
                <td class="indexMapListMetro">
                    <div class="metroStation <?php echo $metros[$arItem['PROPERTIES']['METRO']['VALUE']]['COLOR']; ?>"><?php echo $metros[$arItem['PROPERTIES']['METRO']['VALUE']]['NAME']; ?></div>
                </td>
            </tr>
        <? endforeach; ?> 
    </tbody>
</table>
</div>
</div>

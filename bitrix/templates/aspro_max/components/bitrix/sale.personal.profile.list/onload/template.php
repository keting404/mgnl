<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult['PROFILES']) {
foreach ($arResult['PROFILES'] as $arProfile) {
    if ($arProfile['PERSON_TYPE_ID'] == 1){
        $profileId = $arProfile['ID'];
        break;
    }
}
$APPLICATION->IncludeComponent(
		"bitrix:sale.personal.profile.detail",
		"onload",
		array(
			"PATH_TO_LIST" => "/personal/profiles/",
			"PATH_TO_DETAIL" => "/personal/profiles/".$USER->GetID(),
			"SET_TITLE" =>"N",
			"USE_AJAX_LOCATIONS" => "N",
			"CACHE_TYPE" => "N",
			"ID" => $profileId,
		),
		$component
	);
}
?>

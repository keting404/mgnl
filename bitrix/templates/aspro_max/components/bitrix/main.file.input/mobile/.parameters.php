<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"maxCount" => array(
		"NAME" => "Максимальное количество файлов",
		"TYPE" => "STRING",
		"PARENT" => "BASE"
	),
	"MAX_FILE_SIZE" => array(
		"NAME" => "Максимальный размер файла",
		"TYPE" => "STRING",
		"PARENT" => "BASE"
	),
);

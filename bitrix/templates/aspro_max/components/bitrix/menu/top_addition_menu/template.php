<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if($arResult){?>
	<div class="submenu_top_addition">
		<?if (is_array($arResult) && !empty($arResult)):?>
			<?foreach( $arResult as $arItem ){?>
					<div class="header-info-menu"><a href="<?=$arItem["LINK"]?>" class="dark_link"><?=$arItem["TEXT"]?></a></div>
			<?}?>
		<?endif;?>
	</div>
<?}?>
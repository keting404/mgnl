<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?if(strlen($arResult["ERROR_MESSAGE"])):?>
		<?=ShowError($arResult["ERROR_MESSAGE"]);?>
	<?else:?>	
		<?if($arParams["SHOW_ORDER_BASE"]=='Y' || $arParams["SHOW_ORDER_USER"]=='Y' || $arParams["SHOW_ORDER_PARAMS"]=='Y' || $arParams["SHOW_ORDER_BUYER"]=='Y' || $arParams["SHOW_ORDER_DELIVERY"]=='Y' || $arParams["SHOW_ORDER_PAYMENT"]=='Y'):?>
			<?if($arParams["SHOW_ORDER_PAYMENT"]=='Y'):?>
				<?
				foreach ($arResult["SHIPMENT"] as $shipment)
				{
					$titleParams = [
						"#ACCOUNT_NUMBER#" => htmlspecialcharsbx($shipment['ACCOUNT_NUMBER']),
						"#DELIVERY_PRICE#" => $shipment['PRICE_DELIVERY_FORMATTED'],
					];
					?>
					
					<p style="margin: 0;"><?=GetMessage("SPOD_ORDER_DELIVERY")?>: <b><?=strlen($shipment["DELIVERY_NAME"]) ? htmlspecialcharsbx($shipment["DELIVERY_NAME"]) : GetMessage("SPOD_NONE")?></b></p>
					<?if(isset($shipment['STORE_ID']) && !empty($arResult["DELIVERY"]["STORE_LIST"][(int)$shipment['STORE_ID']])):?>

									<?$store = $arResult["DELIVERY"]["STORE_LIST"][$shipment['STORE_ID']];?>
									<div class="bx_ol_store">
										
										<?if(!empty($store['ADDRESS'])):?>
											<div class="bx_old_s_row">
												<b><?=GetMessage('SPOD_STORE_ADDRESS')?></b>: <?=htmlspecialcharsbx($store['ADDRESS'])?>
											</div>
										<?endif?>

										<?if(!empty($store['SCHEDULE'])):?>
											<div class="bx_old_s_row">
												<b><?=GetMessage('SPOD_STORE_WORKTIME')?></b>: <?=htmlspecialcharsbx($store['SCHEDULE'])?>
											</div>
										<?endif?>

										

										<?if(!empty($store['EMAIL'])):?>
											<div class="bx_old_s_row">
												<b><?=GetMessage('SPOD_STORE_EMAIL')?></b>: <a href="mailto:<?=htmlspecialcharsbx($store['EMAIL'])?>"><?=htmlspecialcharsbx($store['EMAIL'])?></a>
											</div>
										<?endif?>
									</div>

								<?endif?>

					<?if($shipment["TRACKING_NUMBER"]):?>
						<tr>
							<td><?=GetMessage('SPOD_ORDER_TRACKING_NUMBER')?>:</td>
							<td><?=htmlspecialcharsbx($shipment["TRACKING_NUMBER"])?></td>
						</tr>
					<?endif?>
					<?
				}
				?>
			<?endif?>
		<?endif?>
	<?endif?>

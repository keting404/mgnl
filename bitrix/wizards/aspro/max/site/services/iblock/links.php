<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock")) return;
if(!CModule::IncludeModule("aspro.max")) return;

if(!defined("WIZARD_SITE_ID")) return;
if(!defined("WIZARD_SITE_DIR")) return;
if(!defined("WIZARD_SITE_PATH")) return;
if(!defined("WIZARD_TEMPLATE_ID")) return;
if(!defined("WIZARD_TEMPLATE_ABSOLUTE_PATH")) return;
if(!defined("WIZARD_THEME_ID")) return;

$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."/";


// iblocks ids
$partnersIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_partners"][0];
$tizersIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_tizers"][0];
$brandsIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_brands"][0];
$newsIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_news"][0];
$stockIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_stock"][0];
$servicesIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_services"][0];
$catalogIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_catalog"]["aspro_max_catalog"][0];
$projectsIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_projects"][0];
$staffIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_staff"][0];
$regionsIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_regionality"]["aspro_max_regions"][0];
$articlesIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_content"]["aspro_max_articles"][0];
$landingIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_catalog"]["aspro_max_landing"][0];
$searchIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_catalog"]["aspro_max_search"][0];
$cross_salesIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_catalog"]["aspro_max_cross_sales"][0];
$megamenuIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_catalog"]["aspro_max_megamenu"][0];
$banners_innerIBlockID = CMaxCache::$arIBlocks[WIZARD_SITE_ID]["aspro_max_adv"]["aspro_max_banners_inner"][0];


// elements ids
$arCatalog = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($catalogIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $catalogIBlockID), false, false, array("ID", "XML_ID"));
$arBrands = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($brandsIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $brandsIBlockID), false, false, array("ID", "XML_ID"));
$arStock = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($stockIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $stockIBlockID), false, false, array("ID", "XML_ID"));
$arServices = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($servicesIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $servicesIBlockID), false, false, array("ID", "XML_ID"));
$arNews = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($newsIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $newsIBlockID), false, false, array("ID", "XML_ID"));
$arArticles = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($articlesIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $articlesIBlockID), false, false, array("ID", "XML_ID"));
$arLanding = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($landingIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $landingIBlockID), false, false, array("ID", "XML_ID"));
$arProjects = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($projectsIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $projectsIBlockID), false, false, array("ID", "XML_ID"));
$arStaff = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($staffIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $staffIBlockID), false, false, array("ID", "XML_ID"));
$arPartners = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($partnersIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $partnersIBlockID), false, false, array("ID", "XML_ID"));
$arTizers = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($tizersIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $tizersIBlockID), false, false, array("ID", "XML_ID"));
$arSearch = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($searchIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $searchIBlockID), false, false, array("ID", "XML_ID"));
$arRegions = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($regionsIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $regionsIBlockID), false, false, array("ID", "XML_ID"));
$arCross_sales = CMaxCache::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => CMaxCache::GetIBlockCacheTag($cross_salesIBlockID), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => $cross_salesIBlockID), false, false, array("ID", "XML_ID"));


//update brand property in catalog
$propertiesCat = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$catalogIBlockID, "CODE"=>"BRAND"));
if($prop_brand = $propertiesCat ->GetNext())
{
	$ibpBrand = new CIBlockProperty();
	$ibpBrand ->Update($prop_brand ["ID"], array("LINK_IBLOCK_ID" => $brandsIBlockID));
}


// update links in aspro_max_services
CIBlockElement::SetPropertyValuesEx($arStock["3423"], $stockIBlockID, array("LINK_GOODS" => array($arCatalog["49580"], $arCatalog["49729"], $arCatalog["49872"], $arCatalog["49738"]), "LINK_BRANDS" => array($arBrands["3307"], $arBrands["49591"], $arBrands["49588"], $arBrands["49581"])));
CIBlockElement::SetPropertyValuesEx($arServices["10630"], $servicesIBlockID, array("LINK_BRANDS" => array($arBrands["3307"], $arBrands["49581"], $arBrands["49588"], $arBrands["49591"])));

// update links in aspro_max_catalog
CIBlockElement::SetPropertyValuesEx($arCatalog["49738"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "EXPANDABLES" => array($arCatalog["49732"], $arCatalog["3699"], $arCatalog["49875"], $arCatalog["3700"], $arCatalog["3701"]), "ASSOCIATED" => array($arCatalog["49595"], $arCatalog["49744"], $arCatalog["49741"], $arCatalog["3425"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49744"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "EXPANDABLES" => array($arCatalog["3699"], $arCatalog["3700"], $arCatalog["49875"], $arCatalog["49732"]), "ASSOCIATED" => array($arCatalog["3426"], $arCatalog["49738"], $arCatalog["49595"], $arCatalog["49741"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49595"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49902"]), "LINK_BLOG" => array($arArticles["49914"]), "BRAND" => array($arBrands["3307"]), "EXPANDABLES" => array($arCatalog["49875"], $arCatalog["3699"], $arCatalog["3700"], $arCatalog["3719"]), "ASSOCIATED" => array($arCatalog["3426"], $arCatalog["49741"], $arCatalog["49744"], $arCatalog["49738"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49741"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "EXPANDABLES" => array($arCatalog["3700"], $arCatalog["3699"], $arCatalog["49875"], $arCatalog["49732"], $arCatalog["49729"]), "ASSOCIATED" => array($arCatalog["3425"], $arCatalog["49738"], $arCatalog["49744"], $arCatalog["49595"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3425"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"]), "EXPANDABLES" => array($arCatalog["3699"], $arCatalog["3698"], $arCatalog["49875"], $arCatalog["49732"], $arCatalog["3700"]), "ASSOCIATED" => array($arCatalog["3426"], $arCatalog["49738"], $arCatalog["49744"], $arCatalog["49741"], $arCatalog["49595"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3426"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["49581"]), "EXPANDABLES" => array($arCatalog["3700"], $arCatalog["3698"], $arCatalog["49732"], $arCatalog["49875"], $arCatalog["3699"]), "ASSOCIATED" => array($arCatalog["49738"], $arCatalog["49744"], $arCatalog["49741"], $arCatalog["3425"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49599"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["10633"]), "BRAND" => array($arBrands["49591"]), "EXPANDABLES" => array($arCatalog["49877"], $arCatalog["49872"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3688"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3689"], $catalogIBlockID, array("BRAND" => array($arBrands["3759"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3690"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3691"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3692"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3693"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["3759"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3697"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["3759"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3698"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49909"]), "BRAND" => array($arBrands["49581"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3699"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "PODBORKI" => array($arLanding["3382"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3700"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["3759"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3701"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3702"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3703"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3704"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3705"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49580"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3706"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3707"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3708"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49725"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49909"]), "BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49771"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49767"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3709"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49732"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "PODBORKI" => array($arLanding["3382"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3710"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3714"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3715"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49729"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3724"], $arCatalog["3719"], $arCatalog["3716"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3716"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["49581"]), "ASSOCIATED" => array($arCatalog["3719"], $arCatalog["3724"], $arCatalog["49729"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3719"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["49588"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3724"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3725"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"]), "ASSOCIATED" => array($arCatalog["3728"], $arCatalog["3727"], $arCatalog["3726"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3726"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"]), "ASSOCIATED" => array($arCatalog["3725"], $arCatalog["3727"], $arCatalog["3728"]), "PODBORKI" => array($arLanding["3382"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3727"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3728"], $arCatalog["3725"], $arCatalog["3726"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3728"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["3759"]), "ASSOCIATED" => array($arCatalog["3725"], $arCatalog["3726"], $arCatalog["3727"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49877"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["3729"], $arCatalog["3730"], $arCatalog["3731"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3729"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["49877"], $arCatalog["3730"], $arCatalog["3731"]), "PODBORKI" => array($arLanding["3382"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3730"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3731"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["49877"], $arCatalog["3729"], $arCatalog["3730"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49875"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["49872"], $arCatalog["3732"], $arCatalog["3733"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["49872"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3732"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["10633"]), "BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["49875"], $arCatalog["49872"], $arCatalog["3733"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3733"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49909"]), "BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3732"], $arCatalog["49872"], $arCatalog["49875"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3734"], $catalogIBlockID, array("BRAND" => array($arBrands["49581"]), "ASSOCIATED" => array($arCatalog["3735"], $arCatalog["3736"], $arCatalog["3737"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3735"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3737"], $arCatalog["3734"], $arCatalog["3736"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3736"], $catalogIBlockID, array("LINK_BLOG" => array($arArticles["268"]), "BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3734"], $arCatalog["3735"], $arCatalog["3737"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3737"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3736"], $arCatalog["3735"], $arCatalog["3734"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3738"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"]), "ASSOCIATED" => array($arCatalog["3741"], $arCatalog["3740"], $arCatalog["3739"]), "PODBORKI" => array($arLanding["3382"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3739"], $catalogIBlockID, array("BRAND" => array($arBrands["49591"]), "ASSOCIATED" => array($arCatalog["3738"], $arCatalog["3740"], $arCatalog["3741"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3740"], $catalogIBlockID, array("BRAND" => array($arBrands["3759"]), "ASSOCIATED" => array($arCatalog["3737"], $arCatalog["3738"], $arCatalog["3739"], $arCatalog["3741"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3741"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["3759"]), "ASSOCIATED" => array($arCatalog["3740"], $arCatalog["3739"], $arCatalog["3738"], $arCatalog["3734"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3742"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"]), "ASSOCIATED" => array($arCatalog["3743"], $arCatalog["3744"], $arCatalog["3745"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3743"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3744"], $arCatalog["3745"], $arCatalog["3742"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3744"], $catalogIBlockID, array("BRAND" => array($arBrands["49588"]), "ASSOCIATED" => array($arCatalog["3742"], $arCatalog["3743"], $arCatalog["3745"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3745"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["49907"]), "BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3742"], $arCatalog["3743"], $arCatalog["3744"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3746"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3747"], $arCatalog["3748"], $arCatalog["3749"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3747"], $catalogIBlockID, array("BRAND" => array($arBrands["3760"]), "ASSOCIATED" => array($arCatalog["3746"], $arCatalog["3748"], $arCatalog["3749"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3748"], $catalogIBlockID, array("LINK_NEWS" => array($arNews["10633"]), "BRAND" => array($arBrands["3760"]), "ASSOCIATED" => array($arCatalog["3747"], $arCatalog["3749"], $arCatalog["3746"])));
CIBlockElement::SetPropertyValuesEx($arCatalog["3749"], $catalogIBlockID, array("BRAND" => array($arBrands["3307"]), "ASSOCIATED" => array($arCatalog["3747"], $arCatalog["3746"], $arCatalog["3748"])));

// update links in aspro_max_projects
CIBlockElement::SetPropertyValuesEx($arProjects["152"], $projectsIBlockID, array("LINK_GOODS" => array($arCatalog["49741"]), "LINK_PROJECTS" => array($arProjects["215"]), "LINK_STAFF" => array($arStaff["230"]), "LINK_NEWS" => array($arNews["49909"]), "LINK_PARTNERS" => array($arPartners["69"]), "LINK_TIZERS" => array($arTizers["3389"])));

// update links in aspro_max_articles
CIBlockElement::SetPropertyValuesEx($arArticles["49914"], $articlesIBlockID, array("LINK_GOODS" => array($arCatalog["49741"], $arCatalog["49744"], $arCatalog["3426"], $arCatalog["3425"])));

// update links in aspro_max_search
CIBlockElement::SetPropertyValuesEx($arSearch["3363"], $searchIBlockID, array("SIMILAR" => array($arSearch["3362"])));
CIBlockElement::SetPropertyValuesEx($arSearch["3365"], $searchIBlockID, array("SIMILAR" => array($arSearch["3364"])));

// update links in aspro_max_partners
CIBlockElement::SetPropertyValuesEx($arPartners["27"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3213"], $arRegions["3214"])));
CIBlockElement::SetPropertyValuesEx($arPartners["28"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3213"], $arRegions["3212"], $arRegions["3215"])));
CIBlockElement::SetPropertyValuesEx($arPartners["1762"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3213"], $arRegions["3212"], $arRegions["3215"], $arRegions["3214"])));
CIBlockElement::SetPropertyValuesEx($arPartners["67"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3213"], $arRegions["3212"], $arRegions["3215"])));
CIBlockElement::SetPropertyValuesEx($arPartners["69"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3213"], $arRegions["3212"], $arRegions["3215"], $arRegions["3214"])));
CIBlockElement::SetPropertyValuesEx($arPartners["70"], $partnersIBlockID, array("LINK_REGION" => array($arRegions["3212"], $arRegions["3215"], $arRegions["3214"])));


// get sections
$newPropSectionsXML = array (
  403 => '403',
  397 => '397',
  406 => '406',
  402 => '402',
  401 => '401',
  196 => '537',
  400 => '400',
  398 => '398',
);
$newPropSectionsRes = CIBlockSection::GetList(array(), array("XML_ID" => $newPropSectionsXML, "IBLOCK_ID" => $catalogIBlockID), false, array("ID", "XML_ID"));
while($newPropSection = $newPropSectionsRes->Fetch()) {
	$arNewPropSections[$newPropSection["XML_ID"]] = $newPropSection["ID"];
}

// get props
$newPropPropsXML = array (
  520 => '1199',
);
foreach($newPropPropsXML as $xml) {
	$resNewProps = CIBlockProperty::GetList(
		array(),
		array("XML_ID" => $xml, "IBLOCK_ID" => $catalogIBlockID)
	);
	$newProp = $resNewProps->Fetch();
	$arNewPropProps[$newProp["XML_ID"]] = $newProp["ID"];
}

// update values custom filter
CIBlockElement::SetPropertyValuesEx($arSearch["3362"], $searchIBlockID, array("CUSTOM_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":["{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":{"1":{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["403"].'"}},"2":{"CLASS_ID":"CondIBProp:'.$catalogIBlockID.':'.$arNewPropProps["1199"].'","DATA":{"logic":"Equal","value":"'.iconv(LANG_CHARSET, "UTF-8", "Беспроводные").'"}}}}"]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3753"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["403"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["403"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3754"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["397"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["397"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3755"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["397"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["406"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3757"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["402"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["402"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3756"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["401"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["401"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3758"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["537"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["397"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3660"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["400"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["400"].'"}}]}'));
CIBlockElement::SetPropertyValuesEx($arCross_sales["3752"], $cross_salesIBlockID, array("PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["398"].'"}}]}', "EXT_PRODUCTS_FILTER" => '{"CLASS_ID":"CondGroup","DATA":{"All":"AND","True":"True"},"CHILDREN":[{"CLASS_ID":"CondIBSection","DATA":{"logic":"Equal","value":"'.$arNewPropSections["398"].'"}}]}'));

if($_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])
{
	if($_SESSION["WIZARD_MAXIMUM_STOCK_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arStockProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_STOCK_IBLOCK_ID"], "CODE" => "LINK_GOODS_FILTER"))->Fetch();
		if($arStockProps["ID"])
		{
			$ibp->Update($arStockProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_STOCK_IBLOCK_ID"]);
		}
	}

	if($_SESSION["WIZARD_MAXIMUM_ARTICLES_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arArticlesProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_ARTICLES_IBLOCK_ID"], "CODE" => "LINK_GOODS_FILTER"))->Fetch();
		if($arArticlesProps["ID"])
		{
			$ibp->Update($arArticlesProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_ARTICLES_IBLOCK_ID"]);
		}
	}

	if($_SESSION["WIZARD_MAXIMUM_NEWS_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arNewsProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_NEWS_IBLOCK_ID"], "CODE" => "LINK_GOODS_FILTER"))->Fetch();
		if($arNewsProps["ID"])
		{
			$ibp->Update($arNewsProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_NEWS_IBLOCK_ID"]);
		}
	}

	if($_SESSION["WIZARD_MAXIMUM_PROJECTS_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arProjectsProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_PROJECTS_IBLOCK_ID"], "CODE" => "LINK_GOODS_FILTER"))->Fetch();
		if($arProjectsProps["ID"])
		{
			$ibp->Update($arProjectsProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_PROJECTS_IBLOCK_ID"]);
		}
	}

	if($_SESSION["WIZARD_MAXIMUM_CROSS_SALE_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arCrossProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_CROSS_SALE_IBLOCK_ID"], "CODE" => "PRODUCTS_FILTER"))->Fetch();
		if($arCrossProps["ID"])
		{
			$ibp->Update($arCrossProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp);
		}

		$ibp = new CIBlockProperty;
		$arCrossProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_CROSS_SALE_IBLOCK_ID"], "CODE" => "EXT_PRODUCTS_FILTER"))->Fetch();
		if($arCrossProps["ID"])
		{
			$ibp->Update($arCrossProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_CROSS_SALE_IBLOCK_ID"]);
		}
	}

	if($_SESSION["WIZARD_MAXIMUM_SEARCH_IBLOCK_ID"])
	{
		$ibp = new CIBlockProperty;
		$arCrossProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $_SESSION["WIZARD_MAXIMUM_SEARCH_IBLOCK_ID"], "CODE" => "CUSTOM_FILTER"))->Fetch();
		if($arCrossProps["ID"])
		{
			$ibp->Update($arCrossProps["ID"], array('USER_TYPE' => 'SAsproCustomFilterMax','USER_TYPE_SETTINGS' => array('IBLOCK_ID' => $_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"])));
			unset($ibp, $_SESSION["WIZARD_MAXIMUM_SEARCH_IBLOCK_ID"]);
		}
	}
	
	unset($_SESSION["WIZARD_MAXIMUM_CATALOG_IBLOCK_ID"]);
}

$UserFields = array(
	$catalogIBlockID => array(
		'UF_SECTION_DESCR',
		'UF_SECTION_TEMPLATE',
		'UF_TIZERS',
		'UF_POPULAR',
		'UF_CATALOG_ICON',
		'UF_OFFERS_TYPE',
		'UF_TABLE_SIZES',
		'UF_ELEMENT_DETAIL',
		'UF_SECTION_BG_IMG',
		'UF_SECTION_BG_DARK',
		'UF_SECTION_TIZERS' => array(
			'LINK_IBLOCK' => $tizersIBlockID,
		),
		'UF_HELP_TEXT',
		'UF_MENU_BANNER' => array(
			'LINK_IBLOCK' => $banners_innerIBlockID,
		),
		'UF_REGION' => array(
			'LINK_IBLOCK' => $regionsIBlockID,
		),
		'UF_PICTURE_RATIO',
		'UF_LINE_ELEMENT_CNT',
		'UF_LINKED_BLOG' => array(
			'LINK_IBLOCK' => $articlesIBlockID,
		),
		'UF_BLOG_WIDE',
		'UF_BLOG_BOTTOM',
		'UF_BLOG_MOBILE',
		'UF_MENU_BRANDS' => array(
			'LINK_IBLOCK' => $brandsIBlockID,
		),
	),
	$megamenuIBlockID = array(
		"UF_MENU_LINK",
		"UF_MEGA_MENU_LINK",
	),
);

// iblock user fields
$langFile = $_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/aspro/max/site/services/iblock/lang/ru/links.php';
include($langFile);

foreach($UserFields as $iblockId => $fields) {
	foreach ($fields as $fieldKey => $fieldInfo) {

		$fieldCode = is_array($fieldInfo) ? $fieldKey : $fieldInfo;

		$arLangs = array(
			"EDIT_FORM_LABEL"   => array(
		        "ru"    => $MESS[$fieldCode],
		        "en"    => $fieldCode,
		    ),
		    "LIST_COLUMN_LABEL" => array(
		        "ru"    => $MESS[$fieldCode],
		        "en"    => $fieldCode,
		    )
		);

		if( isset($fieldInfo['LINK_IBLOCK']) ) {
			$arLangs['SETTINGS'] = array(
				'DISPLAY' => 'LIST',
				'LIST_HEIGHT' => '5',
				'IBLOCK_ID' => $fieldInfo['LINK_IBLOCK'],
			);
		}

		$arUserField = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => "IBLOCK_".$iblockId."_SECTION", "FIELD_NAME" => $fieldCode))->Fetch();
		
		if($arUserField) {
			$ob = new CUserTypeEntity();
			$ob->Update($arUserField["ID"], $arLangs);
		}

	}
}
?>
<?
$MESS["WZD_OPTION_0"] = "Тип баннеров";
$MESS["WZD_OPTION_1"] = "*Тип баннеров";
$MESS["WZD_OPTION_2"] = "Активность";
$MESS["WZD_OPTION_3"] = "*Активность";
$MESS["WZD_OPTION_4"] = "*Название";
$MESS["WZD_OPTION_5"] = "**Название";
$MESS["WZD_OPTION_6"] = "Символьный код";
$MESS["WZD_OPTION_7"] = "*Символьный код";
$MESS["WZD_OPTION_8"] = "Сортировка";
$MESS["WZD_OPTION_9"] = "*Сортировка";
$MESS["WZD_OPTION_10"] = "";
$MESS["WZD_OPTION_11"] = "*";
$MESS["WZD_OPTION_12"] = "Баннер";
$MESS["WZD_OPTION_13"] = "*Баннер";
$MESS["WZD_OPTION_14"] = "Начало активности";
$MESS["WZD_OPTION_15"] = "*Начало активности";
$MESS["WZD_OPTION_16"] = "Окончание активности";
$MESS["WZD_OPTION_17"] = "*Окончание активности";
$MESS["WZD_OPTION_18"] = "Заголовок баннера h1";
$MESS["WZD_OPTION_19"] = "*Заголовок баннера h1";
$MESS["WZD_OPTION_20"] = "Тип баннера";
$MESS["WZD_OPTION_21"] = "*Тип баннера";
$MESS["WZD_OPTION_22"] = "Активная картинка";
$MESS["WZD_OPTION_23"] = "*Активная картинка";
$MESS["WZD_OPTION_24"] = "Фон";
$MESS["WZD_OPTION_25"] = "*Фон";
$MESS["WZD_OPTION_26"] = "Тип перехода (target)";
$MESS["WZD_OPTION_27"] = "*Тип перехода (target)";
$MESS["WZD_OPTION_28"] = "Ссылка с баннера";
$MESS["WZD_OPTION_29"] = "*Ссылка с баннера";
$MESS["WZD_OPTION_30"] = "Кнопки и текст";
$MESS["WZD_OPTION_31"] = "*Кнопки и текст";
$MESS["WZD_OPTION_32"] = "Цвет текста";
$MESS["WZD_OPTION_33"] = "*Цвет текста";
$MESS["WZD_OPTION_34"] = "Темный цвет меню";
$MESS["WZD_OPTION_35"] = "*Темный цвет меню";
$MESS["WZD_OPTION_36"] = "Расположение текста";
$MESS["WZD_OPTION_37"] = "*Расположение текста";
$MESS["WZD_OPTION_38"] = "Текст над заголовком";
$MESS["WZD_OPTION_39"] = "*Текст над заголовком";
$MESS["WZD_OPTION_40"] = "Описание";
$MESS["WZD_OPTION_41"] = "*Описание";
$MESS["WZD_OPTION_42"] = "Кнопки";
$MESS["WZD_OPTION_43"] = "*Кнопки";
$MESS["WZD_OPTION_44"] = "Класс первой кнопки";
$MESS["WZD_OPTION_45"] = "*Класс первой кнопки";
$MESS["WZD_OPTION_46"] = "Текст первой кнопки";
$MESS["WZD_OPTION_47"] = "*Текст первой кнопки";
$MESS["WZD_OPTION_48"] = "Ссылка с первой кнопки";
$MESS["WZD_OPTION_49"] = "*Ссылка с первой кнопки";
$MESS["WZD_OPTION_50"] = "Класс второй кнопки";
$MESS["WZD_OPTION_51"] = "*Класс второй кнопки";
$MESS["WZD_OPTION_52"] = "Текст второй кнопки";
$MESS["WZD_OPTION_53"] = "*Текст второй кнопки";
$MESS["WZD_OPTION_54"] = "Ссылка с второй кнопки";
$MESS["WZD_OPTION_55"] = "*Ссылка с второй кнопки";
$MESS["WZD_OPTION_56"] = "Видео";
$MESS["WZD_OPTION_57"] = "*Видео";
$MESS["WZD_OPTION_58"] = "Показывать видео";
$MESS["WZD_OPTION_59"] = "*Показывать видео";
$MESS["WZD_OPTION_60"] = "Источник видео";
$MESS["WZD_OPTION_61"] = "*Источник видео";
$MESS["WZD_OPTION_62"] = "Файл видео";
$MESS["WZD_OPTION_63"] = "*Файл видео";
$MESS["WZD_OPTION_64"] = "Автовоспроизведение";
$MESS["WZD_OPTION_65"] = "*Автовоспроизведение";
$MESS["WZD_OPTION_66"] = "Заполнить видео по размеру";
$MESS["WZD_OPTION_67"] = "*Заполнить видео по размеру";
$MESS["WZD_OPTION_68"] = "Отключить звук для видео";
$MESS["WZD_OPTION_69"] = "*Отключить звук для видео";
$MESS["WZD_OPTION_70"] = "Повторять видео";
$MESS["WZD_OPTION_71"] = "*Повторять видео";
$MESS["WZD_OPTION_72"] = "Ссылка на видео";
$MESS["WZD_OPTION_73"] = "*Ссылка на видео";
$MESS["WZD_OPTION_74"] = "Текст подсказки кнопки воспроизведения видео";
$MESS["WZD_OPTION_75"] = "*Текст подсказки кнопки воспроизведения видео";
$MESS["WZD_OPTION_76"] = "Цвет кнопки воспроизведения видео";
$MESS["WZD_OPTION_77"] = "*Цвет кнопки воспроизведения видео";
$MESS["WZD_OPTION_78"] = "Настройка связанного товара";
$MESS["WZD_OPTION_79"] = "*Настройка связанного товара";
$MESS["WZD_OPTION_80"] = "Товар";
$MESS["WZD_OPTION_81"] = "*Товар";
$MESS["WZD_OPTION_82"] = "Отображать стикеры";
$MESS["WZD_OPTION_83"] = "*Отображать стикеры";
$MESS["WZD_OPTION_84"] = "Отображать рейтинг";
$MESS["WZD_OPTION_85"] = "*Отображать рейтинг";
$MESS["WZD_OPTION_86"] = "Отображать срок действия скидки";
$MESS["WZD_OPTION_87"] = "*Отображать срок действия скидки";
$MESS["WZD_OPTION_88"] = "Отображать цены";
$MESS["WZD_OPTION_89"] = "*Отображать цены";
$MESS["WZD_OPTION_90"] = "Отображать старую цену";
$MESS["WZD_OPTION_91"] = "*Отображать старую цену";
$MESS["WZD_OPTION_92"] = "Отображать экономию";
$MESS["WZD_OPTION_93"] = "*Отображать экономию";
$MESS["WZD_OPTION_94"] = "Отображать блок с кнопками";
$MESS["WZD_OPTION_95"] = "*Отображать блок с кнопками";
$MESS["WZD_OPTION_96"] = "Связи";
$MESS["WZD_OPTION_97"] = "*Связи";
$MESS["WZD_OPTION_98"] = "Регион";
$MESS["WZD_OPTION_99"] = "*Регион";
$MESS["WZD_OPTION_100"] = "Разделы";
$MESS["WZD_OPTION_101"] = "*Разделы";
$MESS["WZD_OPTION_102"] = "*Внешний код";
$MESS["WZD_OPTION_103"] = "**Внешний код";
$MESS["WZD_OPTION_104"] = "Тизер";
$MESS["WZD_OPTION_105"] = "*Тизер";
$MESS["WZD_OPTION_106"] = "Закрашивать иконку в цвет темы";
$MESS["WZD_OPTION_107"] = "*Закрашивать иконку в цвет темы";
$MESS["WZD_OPTION_108"] = "Ссылка";
$MESS["WZD_OPTION_109"] = "*Ссылка";
$MESS["WZD_OPTION_110"] = "Показывать на главной";
$MESS["WZD_OPTION_111"] = "*Показывать на главной";
$MESS["WZD_OPTION_112"] = "Картинки";
$MESS["WZD_OPTION_113"] = "*Картинки";
$MESS["WZD_OPTION_114"] = "Иконка";
$MESS["WZD_OPTION_115"] = "*Иконка";
$MESS["WZD_OPTION_116"] = "Изображение";
$MESS["WZD_OPTION_117"] = "*Изображение";
$MESS["WZD_OPTION_118"] = "Элемент";
$MESS["WZD_OPTION_119"] = "*Элемент";
$MESS["WZD_OPTION_120"] = "*Адрес";
$MESS["WZD_OPTION_121"] = "**Адрес";
$MESS["WZD_OPTION_122"] = "Телефон";
$MESS["WZD_OPTION_123"] = "*Телефон";
$MESS["WZD_OPTION_124"] = "Email";
$MESS["WZD_OPTION_125"] = "*Email";
$MESS["WZD_OPTION_126"] = "Режим работы";
$MESS["WZD_OPTION_127"] = "*Режим работы";
$MESS["WZD_OPTION_128"] = "Метро";
$MESS["WZD_OPTION_129"] = "*Метро";
$MESS["WZD_OPTION_130"] = "Способы оплаты";
$MESS["WZD_OPTION_131"] = "*Способы оплаты";
$MESS["WZD_OPTION_132"] = "Детальное описание";
$MESS["WZD_OPTION_133"] = "*Детальное описание";
$MESS["WZD_OPTION_134"] = "Координаты на карте";
$MESS["WZD_OPTION_135"] = "*Координаты на карте";
$MESS["WZD_OPTION_136"] = "Картинка для анонса";
$MESS["WZD_OPTION_137"] = "*Картинка для анонса";
$MESS["WZD_OPTION_138"] = "Детальная картинка";
$MESS["WZD_OPTION_139"] = "*Детальная картинка";
$MESS["WZD_OPTION_140"] = "Галерея";
$MESS["WZD_OPTION_141"] = "*Галерея";
$MESS["WZD_OPTION_142"] = "ID склада (привязка)";
$MESS["WZD_OPTION_143"] = "*ID склада (привязка)";
$MESS["WZD_OPTION_144"] = "SEO";
$MESS["WZD_OPTION_145"] = "*SEO";
$MESS["WZD_OPTION_146"] = "Шаблон META TITLE";
$MESS["WZD_OPTION_147"] = "*Шаблон META TITLE";
$MESS["WZD_OPTION_148"] = "Шаблон META KEYWORDS";
$MESS["WZD_OPTION_149"] = "*Шаблон META KEYWORDS";
$MESS["WZD_OPTION_150"] = "Шаблон META DESCRIPTION";
$MESS["WZD_OPTION_151"] = "*Шаблон META DESCRIPTION";
$MESS["WZD_OPTION_152"] = "Заголовок элемента";
$MESS["WZD_OPTION_153"] = "*Заголовок элемента";
$MESS["WZD_OPTION_154"] = "Настройки для картинок анонса элементов";
$MESS["WZD_OPTION_155"] = "*Настройки для картинок анонса элементов";
$MESS["WZD_OPTION_156"] = "Шаблон ALT";
$MESS["WZD_OPTION_157"] = "*Шаблон ALT";
$MESS["WZD_OPTION_158"] = "Шаблон TITLE";
$MESS["WZD_OPTION_159"] = "*Шаблон TITLE";
$MESS["WZD_OPTION_160"] = "Шаблон имени файла";
$MESS["WZD_OPTION_161"] = "*Шаблон имени файла";
$MESS["WZD_OPTION_162"] = "Настройки для детальных картинок элементов";
$MESS["WZD_OPTION_163"] = "*Настройки для детальных картинок элементов";
$MESS["WZD_OPTION_164"] = "Дополнительно";
$MESS["WZD_OPTION_165"] = "*Дополнительно";
$MESS["WZD_OPTION_166"] = "Теги";
$MESS["WZD_OPTION_167"] = "*Теги";
$MESS["WZD_OPTION_168"] = "Бренд";
$MESS["WZD_OPTION_169"] = "*Бренд";
$MESS["WZD_OPTION_170"] = "Показывать на главной странице";
$MESS["WZD_OPTION_171"] = "*Показывать на главной странице";
$MESS["WZD_OPTION_172"] = "Отображать в верхнем меню";
$MESS["WZD_OPTION_173"] = "*Отображать в верхнем меню";
$MESS["WZD_OPTION_174"] = "Документы";
$MESS["WZD_OPTION_175"] = "*Документы";
$MESS["WZD_OPTION_176"] = "Описание для анонса";
$MESS["WZD_OPTION_177"] = "*Описание для анонса";
$MESS["WZD_OPTION_178"] = "Баннер сверху";
$MESS["WZD_OPTION_179"] = "*Баннер сверху";
$MESS["WZD_OPTION_180"] = "Фоновая картинка";
$MESS["WZD_OPTION_181"] = "*Фоновая картинка";
$MESS["WZD_OPTION_182"] = "Заголовок баннера";
$MESS["WZD_OPTION_183"] = "*Заголовок баннера";
$MESS["WZD_OPTION_184"] = "Описание баннера";
$MESS["WZD_OPTION_185"] = "*Описание баннера";
$MESS["WZD_OPTION_186"] = "Код цвета текста";
$MESS["WZD_OPTION_187"] = "*Код цвета текста";
$MESS["WZD_OPTION_188"] = "Заголовок товара";
$MESS["WZD_OPTION_189"] = "*Заголовок товара";
$MESS["WZD_OPTION_190"] = "Управление";
$MESS["WZD_OPTION_191"] = "*Управление";
$MESS["WZD_OPTION_192"] = "Очистить кеш вычисленных значений";
$MESS["WZD_OPTION_193"] = "*Очистить кеш вычисленных значений";
$MESS["WZD_OPTION_194"] = "Новость";
$MESS["WZD_OPTION_195"] = "*Новость";
$MESS["WZD_OPTION_196"] = "Значения свойств";
$MESS["WZD_OPTION_197"] = "*Значения свойств";
$MESS["WZD_OPTION_198"] = "Показывать в боковой колонке";
$MESS["WZD_OPTION_199"] = "*Показывать в боковой колонке";
$MESS["WZD_OPTION_200"] = "Расположение фото";
$MESS["WZD_OPTION_201"] = "*Расположение фото";
$MESS["WZD_OPTION_202"] = "Задать вопрос";
$MESS["WZD_OPTION_203"] = "*Задать вопрос";
$MESS["WZD_OPTION_204"] = "Заказ услуги";
$MESS["WZD_OPTION_205"] = "*Заказ услуги";
$MESS["WZD_OPTION_206"] = "Редирект";
$MESS["WZD_OPTION_207"] = "*Редирект";
$MESS["WZD_OPTION_208"] = "Период";
$MESS["WZD_OPTION_209"] = "*Период";
$MESS["WZD_OPTION_210"] = "Анонс";
$MESS["WZD_OPTION_211"] = "*Анонс";
$MESS["WZD_OPTION_212"] = "Подробно";
$MESS["WZD_OPTION_213"] = "*Подробно";
$MESS["WZD_OPTION_214"] = "Полное описание";
$MESS["WZD_OPTION_215"] = "*Полное описание";
$MESS["WZD_OPTION_216"] = "Товары";
$MESS["WZD_OPTION_217"] = "*Товары";
$MESS["WZD_OPTION_218"] = "Услуги";
$MESS["WZD_OPTION_219"] = "*Услуги";
$MESS["WZD_OPTION_220"] = "Связанные товары по фильтру";
$MESS["WZD_OPTION_221"] = "*Связанные товары по фильтру";
$MESS["WZD_OPTION_222"] = "Акция";
$MESS["WZD_OPTION_223"] = "*Акция";
$MESS["WZD_OPTION_224"] = "ID";
$MESS["WZD_OPTION_225"] = "*ID";
$MESS["WZD_OPTION_226"] = "Отображать на главной странице";
$MESS["WZD_OPTION_227"] = "*Отображать на главной странице";
$MESS["WZD_OPTION_228"] = "Размер скидки";
$MESS["WZD_OPTION_229"] = "*Размер скидки";
$MESS["WZD_OPTION_230"] = "Картинка сбоку от текста";
$MESS["WZD_OPTION_231"] = "*Картинка сбоку от текста";
$MESS["WZD_OPTION_232"] = "Отображение картинки сбоку";
$MESS["WZD_OPTION_233"] = "*Отображение картинки сбоку";
$MESS["WZD_OPTION_234"] = "Картинка анонса";
$MESS["WZD_OPTION_235"] = "*Картинка анонса";
$MESS["WZD_OPTION_236"] = "Описание анонса";
$MESS["WZD_OPTION_237"] = "*Описание анонса";
$MESS["WZD_OPTION_238"] = "Сотрудники";
$MESS["WZD_OPTION_239"] = "*Сотрудники";
$MESS["WZD_OPTION_240"] = "Проекты";
$MESS["WZD_OPTION_241"] = "*Проекты";
$MESS["WZD_OPTION_242"] = "Новости";
$MESS["WZD_OPTION_243"] = "*Новости";
$MESS["WZD_OPTION_244"] = "Отзывы";
$MESS["WZD_OPTION_245"] = "*Отзывы";
$MESS["WZD_OPTION_246"] = "Тизеры";
$MESS["WZD_OPTION_247"] = "*Тизеры";
$MESS["WZD_OPTION_248"] = "Бренды";
$MESS["WZD_OPTION_249"] = "*Бренды";
$MESS["WZD_OPTION_250"] = "Вакансии";
$MESS["WZD_OPTION_251"] = "*Вакансии";
$MESS["WZD_OPTION_252"] = "Коллекции";
$MESS["WZD_OPTION_253"] = "*Коллекции";
$MESS["WZD_OPTION_254"] = "Партнеры";
$MESS["WZD_OPTION_255"] = "*Партнеры";
$MESS["WZD_OPTION_256"] = "Статьи";
$MESS["WZD_OPTION_257"] = "*Статьи";
$MESS["WZD_OPTION_258"] = "Темный цвет меню (баннер)";
$MESS["WZD_OPTION_259"] = "*Темный цвет меню (баннер)";
$MESS["WZD_OPTION_260"] = "Баннер на половину";
$MESS["WZD_OPTION_261"] = "*Баннер на половину";
$MESS["WZD_OPTION_262"] = "Эффект наезда на шапку";
$MESS["WZD_OPTION_263"] = "*Эффект наезда на шапку";
$MESS["WZD_OPTION_264"] = "Статья";
$MESS["WZD_OPTION_265"] = "*Статья";
$MESS["WZD_OPTION_266"] = "Текст на всю ширину";
$MESS["WZD_OPTION_267"] = "*Текст на всю ширину";
$MESS["WZD_OPTION_268"] = "Осмотр и консультация";
$MESS["WZD_OPTION_269"] = "*Осмотр и консультация";
$MESS["WZD_OPTION_270"] = "Настройка";
$MESS["WZD_OPTION_271"] = "*Настройка";
$MESS["WZD_OPTION_272"] = "Установка";
$MESS["WZD_OPTION_273"] = "*Установка";
$MESS["WZD_OPTION_274"] = "Создан";
$MESS["WZD_OPTION_275"] = "*Создан";
$MESS["WZD_OPTION_276"] = "Изменен";
$MESS["WZD_OPTION_277"] = "*Изменен";
$MESS["WZD_OPTION_278"] = "В наличии";
$MESS["WZD_OPTION_279"] = "*В наличии";
$MESS["WZD_OPTION_280"] = "Товар дня";
$MESS["WZD_OPTION_281"] = "*Товар дня";
$MESS["WZD_OPTION_282"] = "Текст с акцией";
$MESS["WZD_OPTION_283"] = "*Текст с акцией";
$MESS["WZD_OPTION_284"] = "Наши предложения";
$MESS["WZD_OPTION_285"] = "*Наши предложения";
$MESS["WZD_OPTION_286"] = "Примечаниеподсказка";
$MESS["WZD_OPTION_287"] = "*Примечаниеподсказка";
$MESS["WZD_OPTION_288"] = "Большой блок";
$MESS["WZD_OPTION_289"] = "*Большой блок";
$MESS["WZD_OPTION_290"] = "Картинка для большого блока";
$MESS["WZD_OPTION_291"] = "*Картинка для большого блока";
$MESS["WZD_OPTION_292"] = "Артикул";
$MESS["WZD_OPTION_293"] = "*Артикул";
$MESS["WZD_OPTION_294"] = "Базовая единица";
$MESS["WZD_OPTION_295"] = "*Базовая единица";
$MESS["WZD_OPTION_296"] = "ID товара на Яндекс.Маркет";
$MESS["WZD_OPTION_297"] = "*ID товара на Яндекс.Маркет";
$MESS["WZD_OPTION_298"] = "Характеристики";
$MESS["WZD_OPTION_299"] = "*Характеристики";
$MESS["WZD_OPTION_300"] = "Тип";
$MESS["WZD_OPTION_301"] = "*Тип";
$MESS["WZD_OPTION_302"] = "Цвет";
$MESS["WZD_OPTION_303"] = "*Цвет";
$MESS["WZD_OPTION_304"] = "Вес, кг";
$MESS["WZD_OPTION_305"] = "*Вес, кг";
$MESS["WZD_OPTION_306"] = "Состав";
$MESS["WZD_OPTION_307"] = "*Состав";
$MESS["WZD_OPTION_308"] = "Общий объем";
$MESS["WZD_OPTION_309"] = "*Общий объем";
$MESS["WZD_OPTION_310"] = "Габариты (ВхШхГ), см";
$MESS["WZD_OPTION_311"] = "*Габариты (ВхШхГ), см";
$MESS["WZD_OPTION_312"] = "Размер";
$MESS["WZD_OPTION_313"] = "*Размер";
$MESS["WZD_OPTION_314"] = "Материал";
$MESS["WZD_OPTION_315"] = "*Материал";
$MESS["WZD_OPTION_316"] = "Тип управления";
$MESS["WZD_OPTION_317"] = "*Тип управления";
$MESS["WZD_OPTION_318"] = "Автоматический режим";
$MESS["WZD_OPTION_319"] = "*Автоматический режим";
$MESS["WZD_OPTION_320"] = "Антискользящее покрытие";
$MESS["WZD_OPTION_321"] = "*Антискользящее покрытие";
$MESS["WZD_OPTION_322"] = "Возрастное ограничение";
$MESS["WZD_OPTION_323"] = "*Возрастное ограничение";
$MESS["WZD_OPTION_324"] = "Гарантия";
$MESS["WZD_OPTION_325"] = "*Гарантия";
$MESS["WZD_OPTION_326"] = "Назначение";
$MESS["WZD_OPTION_327"] = "*Назначение";
$MESS["WZD_OPTION_328"] = "Особенности";
$MESS["WZD_OPTION_329"] = "*Особенности";
$MESS["WZD_OPTION_330"] = "Напряжение, В";
$MESS["WZD_OPTION_331"] = "*Напряжение, В";
$MESS["WZD_OPTION_332"] = "Комплектация";
$MESS["WZD_OPTION_333"] = "*Комплектация";
$MESS["WZD_OPTION_334"] = "Общая мощность, Вт";
$MESS["WZD_OPTION_335"] = "*Общая мощность, Вт";
$MESS["WZD_OPTION_336"] = "Длина, см";
$MESS["WZD_OPTION_337"] = "*Длина, см";
$MESS["WZD_OPTION_338"] = "Ширина, см";
$MESS["WZD_OPTION_339"] = "*Ширина, см";
$MESS["WZD_OPTION_340"] = "Работа от аккумулятора, часов";
$MESS["WZD_OPTION_341"] = "*Работа от аккумулятора, часов";
$MESS["WZD_OPTION_342"] = "Размеры, см";
$MESS["WZD_OPTION_343"] = "*Размеры, см";
$MESS["WZD_OPTION_344"] = "Тип подключения";
$MESS["WZD_OPTION_345"] = "*Тип подключения";
$MESS["WZD_OPTION_346"] = "Страна производства";
$MESS["WZD_OPTION_347"] = "*Страна производства";
$MESS["WZD_OPTION_348"] = "Стиль";
$MESS["WZD_OPTION_349"] = "*Стиль";
$MESS["WZD_OPTION_350"] = "Диагональ дисплея, см";
$MESS["WZD_OPTION_351"] = "*Диагональ дисплея, см";
$MESS["WZD_OPTION_352"] = "WiFi";
$MESS["WZD_OPTION_353"] = "*WiFi";
$MESS["WZD_OPTION_354"] = "Встроенная память (ROM), Гб";
$MESS["WZD_OPTION_355"] = "*Встроенная память (ROM), Гб";
$MESS["WZD_OPTION_356"] = "Жанр";
$MESS["WZD_OPTION_357"] = "*Жанр";
$MESS["WZD_OPTION_358"] = "Подсветка панели управления";
$MESS["WZD_OPTION_359"] = "*Подсветка панели управления";
$MESS["WZD_OPTION_360"] = "Количество SIMкарт";
$MESS["WZD_OPTION_361"] = "*Количество SIMкарт";
$MESS["WZD_OPTION_362"] = "Материал амбушюров";
$MESS["WZD_OPTION_363"] = "*Материал амбушюров";
$MESS["WZD_OPTION_364"] = "Количество кнопок";
$MESS["WZD_OPTION_365"] = "*Количество кнопок";
$MESS["WZD_OPTION_366"] = "Оперативная память (RAM), Гб";
$MESS["WZD_OPTION_367"] = "*Оперативная память (RAM), Гб";
$MESS["WZD_OPTION_368"] = "Угол поворота руля";
$MESS["WZD_OPTION_369"] = "*Угол поворота руля";
$MESS["WZD_OPTION_370"] = "Операционная система";
$MESS["WZD_OPTION_371"] = "*Операционная система";
$MESS["WZD_OPTION_372"] = "Платформа";
$MESS["WZD_OPTION_373"] = "*Платформа";
$MESS["WZD_OPTION_374"] = "Система шумоподавления";
$MESS["WZD_OPTION_375"] = "*Система шумоподавления";
$MESS["WZD_OPTION_376"] = "Поддержка Smart TV";
$MESS["WZD_OPTION_377"] = "*Поддержка Smart TV";
$MESS["WZD_OPTION_378"] = "Частотный диапазон, кГц";
$MESS["WZD_OPTION_379"] = "*Частотный диапазон, кГц";
$MESS["WZD_OPTION_380"] = "Чувствительность, дБ";
$MESS["WZD_OPTION_381"] = "*Чувствительность, дБ";
$MESS["WZD_OPTION_382"] = "Разрешение основной камеры, Мп";
$MESS["WZD_OPTION_383"] = "*Разрешение основной камеры, Мп";
$MESS["WZD_OPTION_384"] = "Разрешение фронтальной камеры, Мп";
$MESS["WZD_OPTION_385"] = "*Разрешение фронтальной камеры, Мп";
$MESS["WZD_OPTION_386"] = "Разрешение экрана, пикс";
$MESS["WZD_OPTION_387"] = "*Разрешение экрана, пикс";
$MESS["WZD_OPTION_388"] = "Язык";
$MESS["WZD_OPTION_389"] = "*Язык";
$MESS["WZD_OPTION_390"] = "USBразъем";
$MESS["WZD_OPTION_391"] = "*USBразъем";
$MESS["WZD_OPTION_392"] = "Вид наушников";
$MESS["WZD_OPTION_393"] = "*Вид наушников";
$MESS["WZD_OPTION_394"] = "Вид стекла";
$MESS["WZD_OPTION_395"] = "*Вид стекла";
$MESS["WZD_OPTION_396"] = "Время зарядки, минут";
$MESS["WZD_OPTION_397"] = "*Время зарядки, минут";
$MESS["WZD_OPTION_398"] = "Вставка";
$MESS["WZD_OPTION_399"] = "*Вставка";
$MESS["WZD_OPTION_400"] = "Емкость аккумулятора, мАч";
$MESS["WZD_OPTION_401"] = "*Емкость аккумулятора, мАч";
$MESS["WZD_OPTION_402"] = "Изогнутый экран";
$MESS["WZD_OPTION_403"] = "*Изогнутый экран";
$MESS["WZD_OPTION_404"] = "Микрофон";
$MESS["WZD_OPTION_405"] = "*Микрофон";
$MESS["WZD_OPTION_406"] = "Регулятор громкости";
$MESS["WZD_OPTION_407"] = "*Регулятор громкости";
$MESS["WZD_OPTION_408"] = "Режимы";
$MESS["WZD_OPTION_409"] = "*Режимы";
$MESS["WZD_OPTION_410"] = "Способ крепления";
$MESS["WZD_OPTION_411"] = "*Способ крепления";
$MESS["WZD_OPTION_412"] = "Таймер включения/выключения";
$MESS["WZD_OPTION_413"] = "*Таймер включения/выключения";
$MESS["WZD_OPTION_414"] = "Форма";
$MESS["WZD_OPTION_415"] = "*Форма";
$MESS["WZD_OPTION_416"] = "Шум, дБ";
$MESS["WZD_OPTION_417"] = "*Шум, дБ";
$MESS["WZD_OPTION_418"] = "Эффект";
$MESS["WZD_OPTION_419"] = "*Эффект";
$MESS["WZD_OPTION_420"] = "Фотогалерея";
$MESS["WZD_OPTION_421"] = "*Фотогалерея";
$MESS["WZD_OPTION_422"] = "Видео в попапе";
$MESS["WZD_OPTION_423"] = "*Видео в попапе";
$MESS["WZD_OPTION_424"] = "Видео (код из Youtube)";
$MESS["WZD_OPTION_425"] = "*Видео (код из Youtube)";
$MESS["WZD_OPTION_426"] = "Аксессуары по фильтру";
$MESS["WZD_OPTION_427"] = "*Аксессуары по фильтру";
$MESS["WZD_OPTION_428"] = "Аксессуары";
$MESS["WZD_OPTION_429"] = "*Аксессуары";
$MESS["WZD_OPTION_430"] = "Похожие товары по фильтру";
$MESS["WZD_OPTION_431"] = "*Похожие товары по фильтру";
$MESS["WZD_OPTION_432"] = "Похожие товары";
$MESS["WZD_OPTION_433"] = "*Похожие товары";
$MESS["WZD_OPTION_434"] = "Файлы";
$MESS["WZD_OPTION_435"] = "*Файлы";
$MESS["WZD_OPTION_436"] = "Акции";
$MESS["WZD_OPTION_437"] = "*Акции";
$MESS["WZD_OPTION_438"] = "Подборки";
$MESS["WZD_OPTION_439"] = "*Подборки";
$MESS["WZD_OPTION_440"] = "Основной раздел";
$MESS["WZD_OPTION_441"] = "*Основной раздел";
$MESS["WZD_OPTION_442"] = "Системные";
$MESS["WZD_OPTION_443"] = "*Системные";
$MESS["WZD_OPTION_444"] = "Минимальная цена";
$MESS["WZD_OPTION_445"] = "*Минимальная цена";
$MESS["WZD_OPTION_446"] = "Максимальная цена";
$MESS["WZD_OPTION_447"] = "*Максимальная цена";
$MESS["WZD_OPTION_448"] = "Количество комментариев к элементу";
$MESS["WZD_OPTION_449"] = "*Количество комментариев к элементу";
$MESS["WZD_OPTION_450"] = "Количество проголосовавших";
$MESS["WZD_OPTION_451"] = "*Количество проголосовавших";
$MESS["WZD_OPTION_452"] = "Рейтинг";
$MESS["WZD_OPTION_453"] = "*Рейтинг";
$MESS["WZD_OPTION_454"] = "Реквизиты";
$MESS["WZD_OPTION_455"] = "*Реквизиты";
$MESS["WZD_OPTION_456"] = "Ставки налогов";
$MESS["WZD_OPTION_457"] = "*Ставки налогов";
$MESS["WZD_OPTION_458"] = "Сумма оценок";
$MESS["WZD_OPTION_459"] = "*Сумма оценок";
$MESS["WZD_OPTION_460"] = "Тема форума для комментариев";
$MESS["WZD_OPTION_461"] = "*Тема форума для комментариев";
$MESS["WZD_OPTION_462"] = "Торговый каталог";
$MESS["WZD_OPTION_463"] = "*Торговый каталог";
$MESS["WZD_OPTION_464"] = "Внешний код";
$MESS["WZD_OPTION_465"] = "*Внешний код";
$MESS["WZD_OPTION_466"] = "Объём";
$MESS["WZD_OPTION_467"] = "*Объём";
$MESS["WZD_OPTION_468"] = "Элемент каталога";
$MESS["WZD_OPTION_469"] = "*Элемент каталога";
$MESS["WZD_OPTION_470"] = "Открыть в окне";
$MESS["WZD_OPTION_471"] = "*Открыть в окне";
$MESS["WZD_OPTION_472"] = "Позиция";
$MESS["WZD_OPTION_473"] = "*Позиция";
$MESS["WZD_OPTION_474"] = "Показ в разделах";
$MESS["WZD_OPTION_475"] = "*Показ в разделах";
$MESS["WZD_OPTION_476"] = "Показ на странице";
$MESS["WZD_OPTION_477"] = "*Показ на странице";
$MESS["WZD_OPTION_478"] = "Показ в пункте меню";
$MESS["WZD_OPTION_479"] = "*Показ в пункте меню";
$MESS["WZD_OPTION_480"] = "Скрывать на мобильном";
$MESS["WZD_OPTION_481"] = "*Скрывать на мобильном";
$MESS["WZD_OPTION_482"] = "Скрывать на планшетах";
$MESS["WZD_OPTION_483"] = "*Скрывать на планшетах";
$MESS["WZD_OPTION_484"] = "Уменьшение";
$MESS["WZD_OPTION_485"] = "*Уменьшение";
$MESS["WZD_OPTION_486"] = "Цвет фона";
$MESS["WZD_OPTION_487"] = "*Цвет фона";
$MESS["WZD_OPTION_488"] = "Проект";
$MESS["WZD_OPTION_489"] = "*Проект";
$MESS["WZD_OPTION_490"] = "Заказать проект";
$MESS["WZD_OPTION_491"] = "*Заказать проект";
$MESS["WZD_OPTION_492"] = "Сфера";
$MESS["WZD_OPTION_493"] = "*Сфера";
$MESS["WZD_OPTION_494"] = "Сайт";
$MESS["WZD_OPTION_495"] = "*Сайт";
$MESS["WZD_OPTION_496"] = "Дата";
$MESS["WZD_OPTION_497"] = "*Дата";
$MESS["WZD_OPTION_498"] = "Автор";
$MESS["WZD_OPTION_499"] = "*Автор";
$MESS["WZD_OPTION_500"] = "Галерея большая";
$MESS["WZD_OPTION_501"] = "*Галерея большая";
$MESS["WZD_OPTION_502"] = "Задача";
$MESS["WZD_OPTION_503"] = "*Задача";
$MESS["WZD_OPTION_504"] = "Связанные элементы";
$MESS["WZD_OPTION_505"] = "*Связанные элементы";
$MESS["WZD_OPTION_506"] = "Город";
$MESS["WZD_OPTION_507"] = "*Город";
$MESS["WZD_OPTION_508"] = "Название";
$MESS["WZD_OPTION_509"] = "*Название";
$MESS["WZD_OPTION_510"] = "Город по умолчанию";
$MESS["WZD_OPTION_511"] = "*Город по умолчанию";
$MESS["WZD_OPTION_512"] = "Основной домен";
$MESS["WZD_OPTION_513"] = "*Основной домен";
$MESS["WZD_OPTION_514"] = "Избранное местоположение";
$MESS["WZD_OPTION_515"] = "*Избранное местоположение";
$MESS["WZD_OPTION_516"] = "Домены";
$MESS["WZD_OPTION_517"] = "*Домены";
$MESS["WZD_OPTION_518"] = "Склонение города";
$MESS["WZD_OPTION_519"] = "*Склонение города";
$MESS["WZD_OPTION_520"] = "Название города в родительном падеже";
$MESS["WZD_OPTION_521"] = "*Название города в родительном падеже";
$MESS["WZD_OPTION_522"] = "Название города в предложном падеже";
$MESS["WZD_OPTION_523"] = "*Название города в предложном падеже";
$MESS["WZD_OPTION_524"] = "Название города в творительном падеже";
$MESS["WZD_OPTION_525"] = "*Название города в творительном падеже";
$MESS["WZD_OPTION_526"] = "Информация";
$MESS["WZD_OPTION_527"] = "*Информация";
$MESS["WZD_OPTION_528"] = "Телефоны";
$MESS["WZD_OPTION_529"] = "*Телефоны";
$MESS["WZD_OPTION_530"] = "Адрес";
$MESS["WZD_OPTION_531"] = "*Адрес";
$MESS["WZD_OPTION_532"] = "Текст для главной страницы";
$MESS["WZD_OPTION_533"] = "*Текст для главной страницы";
$MESS["WZD_OPTION_534"] = "Цены";
$MESS["WZD_OPTION_535"] = "*Цены";
$MESS["WZD_OPTION_536"] = "Цена для сортировки";
$MESS["WZD_OPTION_537"] = "*Цена для сортировки";
$MESS["WZD_OPTION_538"] = "Склады";
$MESS["WZD_OPTION_539"] = "*Склады";
$MESS["WZD_OPTION_540"] = "Местоположение";
$MESS["WZD_OPTION_541"] = "*Местоположение";
$MESS["WZD_OPTION_542"] = "Текст баннера";
$MESS["WZD_OPTION_543"] = "*Текст баннера";
$MESS["WZD_OPTION_544"] = "Вопрос";
$MESS["WZD_OPTION_545"] = "*Вопрос";
$MESS["WZD_OPTION_546"] = "Название кнопки";
$MESS["WZD_OPTION_547"] = "*Название кнопки";
$MESS["WZD_OPTION_548"] = "Ссылка для кнопки";
$MESS["WZD_OPTION_549"] = "*Ссылка для кнопки";
$MESS["WZD_OPTION_550"] = "Отображать картинку на фоне";
$MESS["WZD_OPTION_551"] = "*Отображать картинку на фоне";
$MESS["WZD_OPTION_552"] = "Положение блока";
$MESS["WZD_OPTION_553"] = "*Положение блока";
$MESS["WZD_OPTION_554"] = "Картинка";
$MESS["WZD_OPTION_555"] = "*Картинка";
$MESS["WZD_OPTION_556"] = "Показывать блок 'Задать вопрос'";
$MESS["WZD_OPTION_557"] = "*Показывать блок 'Задать вопрос'";
$MESS["WZD_OPTION_558"] = "Раздел";
$MESS["WZD_OPTION_559"] = "*Раздел";
$MESS["WZD_OPTION_560"] = "Описание сверху";
$MESS["WZD_OPTION_561"] = "*Описание сверху";
$MESS["WZD_OPTION_562"] = "Описание снизу";
$MESS["WZD_OPTION_563"] = "*Описание снизу";
$MESS["WZD_OPTION_564"] = "Текст на главной";
$MESS["WZD_OPTION_565"] = "*Текст на главной";
$MESS["WZD_OPTION_566"] = "URL фильтр";
$MESS["WZD_OPTION_567"] = "*URL фильтр";
$MESS["WZD_OPTION_568"] = "Заголовок H3 перед товарами";
$MESS["WZD_OPTION_569"] = "*Заголовок H3 перед товарами";
$MESS["WZD_OPTION_570"] = "Текс в баннере";
$MESS["WZD_OPTION_571"] = "*Текс в баннере";
$MESS["WZD_OPTION_572"] = "Надпись в кнопке";
$MESS["WZD_OPTION_573"] = "*Надпись в кнопке";
$MESS["WZD_OPTION_574"] = "Шаблон поискового запроса";
$MESS["WZD_OPTION_575"] = "*Шаблон поискового запроса";
$MESS["WZD_OPTION_576"] = "Участвует в индексации";
$MESS["WZD_OPTION_577"] = "*Участвует в индексации";
$MESS["WZD_OPTION_578"] = "Человекопонятный URL (обработка адреса)";
$MESS["WZD_OPTION_579"] = "*Человекопонятный URL (обработка адреса)";
$MESS["WZD_OPTION_580"] = "Редирект на URL";
$MESS["WZD_OPTION_581"] = "*Редирект на URL";
$MESS["WZD_OPTION_582"] = "Подмена поискового запроса";
$MESS["WZD_OPTION_583"] = "*Подмена поискового запроса";
$MESS["WZD_OPTION_584"] = "Дополнительный фильтр товаров";
$MESS["WZD_OPTION_585"] = "*Дополнительный фильтр товаров";
$MESS["WZD_OPTION_586"] = "Способ применения дополнительного фильтра";
$MESS["WZD_OPTION_587"] = "*Способ применения дополнительного фильтра";
$MESS["WZD_OPTION_588"] = "Похожие запросы";
$MESS["WZD_OPTION_589"] = "*Похожие запросы";
$MESS["WZD_OPTION_590"] = "Краткое описание";
$MESS["WZD_OPTION_591"] = "*Краткое описание";
$MESS["WZD_OPTION_592"] = "Служебное";
$MESS["WZD_OPTION_593"] = "*Служебное";
$MESS["WZD_OPTION_594"] = "МетаХэш (служебное)";
$MESS["WZD_OPTION_595"] = "*МетаХэш (служебное)";
$MESS["WZD_OPTION_596"] = "МетаДанные (служебное)";
$MESS["WZD_OPTION_597"] = "*МетаДанные (служебное)";
$MESS["WZD_OPTION_598"] = "Должность";
$MESS["WZD_OPTION_599"] = "*Должность";
$MESS["WZD_OPTION_600"] = "Логотип";
$MESS["WZD_OPTION_601"] = "*Логотип";
$MESS["WZD_OPTION_602"] = "Основной заголовок";
$MESS["WZD_OPTION_603"] = "*Основной заголовок";
$MESS["WZD_OPTION_604"] = "Текст кнопки подробнее";
$MESS["WZD_OPTION_605"] = "*Текст кнопки подробнее";
$MESS["WZD_OPTION_606"] = "Тип 1";
$MESS["WZD_OPTION_607"] = "*Тип 1";
$MESS["WZD_OPTION_608"] = "Тип 2";
$MESS["WZD_OPTION_609"] = "*Тип 2";
$MESS["WZD_OPTION_610"] = "Тип 3";
$MESS["WZD_OPTION_611"] = "*Тип 3";
$MESS["WZD_OPTION_612"] = "Тип 4";
$MESS["WZD_OPTION_613"] = "*Тип 4";
$MESS["WZD_OPTION_614"] = "Преимущества";
$MESS["WZD_OPTION_615"] = "*Преимущества";
$MESS["WZD_OPTION_616"] = "Текст сверху";
$MESS["WZD_OPTION_617"] = "*Текст сверху";
$MESS["WZD_OPTION_618"] = "Тип блока";
$MESS["WZD_OPTION_619"] = "*Тип блока";
$MESS["WZD_OPTION_620"] = "Картинка сверху";
$MESS["WZD_OPTION_621"] = "*Картинка сверху";
$MESS["WZD_OPTION_622"] = "Правило";
$MESS["WZD_OPTION_623"] = "*Правило";
$MESS["WZD_OPTION_624"] = "Место показа";
$MESS["WZD_OPTION_625"] = "*Место показа";
$MESS["WZD_OPTION_626"] = "Приоритет применимости";
$MESS["WZD_OPTION_627"] = "*Приоритет применимости";
$MESS["WZD_OPTION_628"] = "Индекс сортировки в уровне приоритета";
$MESS["WZD_OPTION_629"] = "*Индекс сортировки в уровне приоритета";
$MESS["WZD_OPTION_630"] = "Прекратить применение правил на текущем уровне приоритетов";
$MESS["WZD_OPTION_631"] = "*Прекратить применение правил на текущем уровне приоритетов";
$MESS["WZD_OPTION_632"] = "Прекратить дальнейшее применение правил";
$MESS["WZD_OPTION_633"] = "*Прекратить дальнейшее применение правил";
$MESS["WZD_OPTION_634"] = "Условия";
$MESS["WZD_OPTION_635"] = "*Условия";
$MESS["WZD_OPTION_636"] = "Основные товары";
$MESS["WZD_OPTION_637"] = "*Основные товары";
$MESS["WZD_OPTION_638"] = "Товары для допродажи";
$MESS["WZD_OPTION_639"] = "*Товары для допродажи";
$MESS["WZD_OPTION_640"] = "Ограничения";
$MESS["WZD_OPTION_641"] = "*Ограничения";
$MESS["WZD_OPTION_642"] = "Группы пользователей";
$MESS["WZD_OPTION_643"] = "*Группы пользователей";
$MESS["WZD_OPTION_644"] = "Обязательные поля";
$MESS["WZD_OPTION_645"] = "*Обязательные поля";
$MESS["WZD_OPTION_646"] = "URL";
$MESS["WZD_OPTION_647"] = "*URL";
$MESS["WZD_OPTION_648"] = "Карта";
$MESS["WZD_OPTION_649"] = "*Карта";
$MESS["WZD_OPTION_650"] = "Изображение на странице контактов";
$MESS["WZD_OPTION_651"] = "*Изображение на странице контактов";
$MESS["WZD_OPTION_652"] = "Текст на странице контактов";
$MESS["WZD_OPTION_653"] = "*Текст на странице контактов";
$MESS["WZD_OPTION_654"] = "Режим работы";
$MESS["WZD_OPTION_655"] = "*Режим работы";
$MESS["WZD_OPTION_656"] = "Задержка появления окна (сек)";
$MESS["WZD_OPTION_657"] = "Периодичность показа окна (сек)";
$MESS["WZD_OPTION_658"] = "Условие показа";
$MESS["WZD_OPTION_659"] = "Картинка для <768px";
$MESS["WZD_OPTION_660"] = "*Картинка для <768px";
$MESS["WZD_OPTION_661"] = "Скрывать заголовок";
$MESS["WZD_OPTION_662"] = "*Скрывать заголовок";
$MESS["WZD_OPTION_663"] = "Тип";
$MESS["WZD_OPTION_664"] = "*Тип";
$MESS["WZD_OPTION_665"] = "Позиция";
$MESS["WZD_OPTION_666"] = "*Позиция";
?>

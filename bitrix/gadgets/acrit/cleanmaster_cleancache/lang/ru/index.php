<?
$MESS['GT_RM_NAME'] = 'Мастер очистки сайта - Очистка кеша';
$MESS['GT_RM_DESCRIPTION'] = 'Быстрое удаление кеша';

$MESS['GT_RM_UNGOVERNABLE_CACHE'] = 'Неуправляемый кеш';
$MESS['GT_RM_MANAGED_CACHE'] = 'Управляемый кеш';
$MESS['GT_RM_STACK_CACHE'] = 'Кэш стека';
$MESS['GT_RM_RESIZE_CACHE'] = 'Миниатюры изображений';
$MESS['GT_RM_SEO_CACHE'] = 'SEO кеш';
$MESS['GT_RM_CACHE_SEO_INFO'] = '( хранится в бд )';

$MESS['GT_RM_CACHE_TYPE'] = 'Тип кеша';
$MESS['GT_RM_CACHE_PATH'] = 'Путь';
$MESS['GT_RM_CACHE_DIR_FILES_COUNT'] = 'Файлы';
$MESS['GT_RM_CACHE_DIR_SIZE'] = 'Размер';

$MESS['GT_RM_CACHE_SIZE_MB'] = 'Мб';
$MESS['GT_RM_CLEAN'] = 'Очистить';
$MESS['GT_RM_CLEAN_CACHE'] = 'Очистить кеш';
$MESS['GT_RM_CLEAN_ALL'] = 'Очистить все';
$MESS['GT_RM_CLEAN_CACHE_TITLE'] = 'Очистить неуправляемый и управляемый кеш, кеш стека';
$MESS['GT_RM_CLEAN_ALL_TITLE'] = 'Очистить все кеши и изображения';

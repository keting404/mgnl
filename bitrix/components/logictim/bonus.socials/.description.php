<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("LOGICTIM_BONUS_SOCIALS_NAME"),
	"DESCRIPTION" => GetMessage("LOGICTIM_BONUS_SOCIALS_DESCRIPTION"),
	"ICON" => "/images/sale_order_full.gif",
	"PATH" => array(
		"ID" => "logictim",
	),
);
?>
<?
$MESS["LOGICTIM_BONUS_BALLANCE_CHANGE"] = "Изменение баланса администратором";
$MESS["LOGICTIM_BONUS_FIELD_ID"] = "Код";
$MESS["LOGICTIM_BONUS_FIELD_DATE"] = "Дата";
$MESS["LOGICTIM_BONUS_FIELD_TYPE"] = "Операция";
$MESS["LOGICTIM_BONUS_FIELD_SUM"] = "Сумма операции";
$MESS["LOGICTIM_BONUS_FIELD_BEFORE"] = "Баланс До";
$MESS["LOGICTIM_BONUS_FIELD_AFTER"] = "Баланс После";
$MESS["LOGICTIM_BONUS_FIELD_DETAIL"] = "Состав начисления";
$MESS["LOGICTIM_BONUS_BALANCE"] = "Состав начисления";
$MESS["LOGICTIM_BONUS_HAVE"] = "Доступно бонусов:";
$MESS["LOGICTIM_BONUS_HISTORY"] = "История начислений и списаний:";
$MESS["LOGICTIM_BONUS_ACTIVE_TO"] = "Активны до ";
$MESS["LOGICTIM_BONUS_HAVE_H"] = "Доступно: ";
$MESS["LOGICTIM_BONUS_PAID"] = "Использовано: ";
$MESS["LOGICTIM_BONUS_LIVE_END"] = "Срок активности закончился";
$MESS["LOGICTIM_BONUS_END"] = "Использованы";
$MESS["LOGICTIM_BONUS_ADD_FROM_REGISTER"] = "Бонус при регистрации";
$MESS["LOGICTIM_BONUS_ADD_FROM_BIRTHDAY"] = "Начисление на день рождения";
$MESS["LOGICTIM_BONUS_SUBSCRIBE_TEXT"] = "Получать предупреждения о сгорании бонусов: ";
$MESS["LOGICTIM_BONUS_YES"] = "Да";
$MESS["LOGICTIM_BONUS_NO"] = "Нет";
$MESS["LOGICTIM_BONUS_SUBSCRIBE"] = "Подписаться";
$MESS["LOGICTIM_BONUS_UNSUBSCRIBE"] = "Отписаться";
$MESS["LOGICTIM_BONUS_REF_LINK"] = "Ссылка для привлечения посетителей";
$MESS["LOGICTIM_REFERALS_REF_COUPON"] = "Код реферального купона";
$MESS["LOGICTIM_REFERALS_REF_COUPON_GENERATE"] = "Сгенерировать";
?>
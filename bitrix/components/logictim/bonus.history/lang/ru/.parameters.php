<?
$MESS["LOGICTIM_BONUS_DATA"] = "Основные настройки";
$MESS["LOGICTIM_BONUS_PROPERTY_ID"] = "ID операции";
$MESS["LOGICTIM_BONUS_DATE"] = "Дата";
$MESS["LOGICTIM_BONUS_PROPERTY_NAME"] = "Название операции";
$MESS["LOGICTIM_BONUS_PROPERTY_OPERATION_SUM"] = "Сумма операции";
$MESS["LOGICTIM_BONUS_BALLANCE_BEFORE"] = "Балланс до";
$MESS["LOGICTIM_BONUS_BALLANCE_AFTER"] = "Балланс после";
$MESS["LOGICTIM_BONUS_ADD_DETAIL"] = "Состав начисления бонусов";
$MESS["LOGICTIM_BONUS_PAGE_NAVIG_TEMP"] = "Шаблон постраничной навигации";
$MESS["LOGICTIM_BONUS_PAGE_NAVIG"] = "Количество операций на странице";
$MESS["LOGICTIM_BONUS_SORT_ASC"] = "Последние вконце";
$MESS["LOGICTIM_BONUS_SORT_DESC"] = "Последние вначале";
$MESS["LOGICTIM_BONUS_ORDER_URL"] = "url страницы с комонентом информации о заказах";
$MESS["LOGICTIM_BONUS_ORDER_LINK"] = "Показывать ссылку на заказ по которому совершена операция";
$MESS["LOGICTIM_BONUS_SEE_FIELDS"] = "Какие поля показывать";
?>
<?
$MESS["logictim.balls_SELECT_METOD_1"] = "Установка баллов на участвующий товар"; //old
$MESS["logictim.balls_SELECT_METOD_2"] = "Установка % на участвующий товар"; //old
$MESS["logictim.balls_SELECT_METOD_3"] = "Установка % на сумму заказа и доставки"; //old
$MESS["logictim.balls_SELECT_METOD_4"] = "Установка % только на сумму товаров в заказе"; //old
$MESS["logictim.balls_SELECT_METOD_5"] = "Начисление бонусов за товары";
$MESS["logictim.balls_SELECT_METOD_6"] = "Начисление бонусов за заказ";

$MESS["logictim.balls_SELECT_BONUS_BILL"] = "<b>Используемый счет</b>";
$MESS["logictim.balls_BONUS_BILL_DESCRIPTION"] = "ВАЖНО!!!<br />Не изменяйте используемый счет в процессе использования модуля!<br />Выбрать тип счета необходимо один раз, до начала использования модуля<br />Иначе потом сами запутаетесь!";
$MESS["logictim.balls_BILL_BONUS"] = "Бонусный счет модуля";
$MESS["logictim.balls_BILL_BITRIX"] = "Внутренний счет битрикс (не рекомендуется)";
$MESS["logictim.balls_SELECT_BONUS_CURRENCY"] = "Валюта счета<br />(при использовании внутреннего счета битрикс)";

$MESS["logictim.balls_SITE_SELECT"] = "Сайты, на которых доступна бонусная система<br />(все, если не выделено)";

$MESS["logictim.balls_EVENT_USER_REGISTER"] = "Регистрация пользователей <a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/api_modulya_4/user_register/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_EVENT_USER_REGISTER_TITLE"] = "Событием регистрации считается";
$MESS["logictim.balls_EVENT_USER_REGISTER_ADD"] = "Любое добавление пользователя";
$MESS["logictim.balls_EVENT_USER_REGISTER_REGISTER"] = "Регистрация пользователя через форму регистрации";

$MESS["logictim.balls_TAB_2"] = "Настройки начисления бонусов";
$MESS["logictim.balls_MEDOD_3_4_PROPERTY"] = "Настройки начисления бонусов за заказ <a href='https://youtu.be/oCVbKV8i_s0?t=491' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_MEDOD_5_PROPERTY"] = "Настройки начисления бонусов за товары <a href='https://youtu.be/oCVbKV8i_s0?t=491' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_ORDERS_SUM_VARIANTS"] = "Настройки зависимости бонусов от суммы оплаченных заказов <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/zavisimost_bonusov_ot_summy_oplachennykh_zakazov/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_ORDERS_SUM_RANGE_TYPE"] = "Вариант использования";
$MESS["logictim.balls_ORDERS_SUM_RANGE_TYPE_NO_USE"] = "Не использовать";
$MESS["logictim.balls_ORDERS_SUM_RANGE_TYPE_PLUS"] = "Добавлять к основному проценту";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PERIOD"] = "Учитывать заказы за последние";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PERIOD_TYPE_D"] = "Дней";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PERIOD_TYPE_M"] = "Месяцев";
$MESS["logictim.balls_ORDERS_SUM_RANGE_1"] = "Сумма заказов";
$MESS["logictim.balls_ORDERS_SUM_RANGE_RATE"] = "Добавить";
$MESS["logictim.balls_ORDERS_SUM_RANGE_2"] = "Сумма заказов";
$MESS["logictim.balls_ORDERS_SUM_RANGE_3"] = "Сумма заказов";
$MESS["logictim.balls_ORDERS_SUM_RANGE_4"] = "Сумма заказов";
$MESS["logictim.balls_ORDERS_SUM_RANGE_5"] = "Сумма заказов";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PLACE_1"] = "Например: 0-5000";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PLACE_2"] = "Например: 5001-10000";
$MESS["logictim.balls_ORDERS_SUM_RANGE_PLACE_3"] = "Например: 10001-0";

$MESS["logictim.balls_CART_SUM_VARIANTS"] = "Настройки зависимости бонусов от суммы товаров в заказе";
$MESS["logictim.balls_CART_SUM_RANGE_TYPE"] = "Вариант использования";
$MESS["logictim.balls_CART_SUM_RANGE_TYPE_NO_USE"] = "Не использовать";
$MESS["logictim.balls_CART_SUM_RANGE_TYPE_PLUS"] = "Добавлять к основному проценту";
$MESS["logictim.balls_CART_SUM_RANGE_1"] = "Сумма товаров";
$MESS["logictim.balls_CART_SUM_RANGE_RATE"] = "Добавить";
$MESS["logictim.balls_CART_SUM_RANGE_2"] = "Сумма товаров";
$MESS["logictim.balls_CART_SUM_RANGE_3"] = "Сумма товаров";
$MESS["logictim.balls_CART_SUM_RANGE_4"] = "Сумма товаров";
$MESS["logictim.balls_CART_SUM_RANGE_5"] = "Сумма товаров";
$MESS["logictim.balls_CART_SUM_RANGE_PLACE_1"] = "Например: 0-5000";
$MESS["logictim.balls_CART_SUM_RANGE_PLACE_2"] = "Например: 5001-10000";
$MESS["logictim.balls_CART_SUM_RANGE_PLACE_3"] = "Например: 10001-0";

$MESS["logictim.balls_BONUS_REFERAL_SYSTEM"] = "Реферальная программа <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/nastroyka_referalnoy_programmy/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_REFERAL_SYSTEM_TYPE"] = "Вариант начисления";
$MESS["logictim.balls_REFERAL_SYSTEM_TYPE_NO_USE"] = "Не использовать";
$MESS["logictim.balls_REFERAL_SYSTEM_TYPE_FIX_ORDER"] = "% от суммы заказа, включая доставку";
$MESS["logictim.balls_REFERAL_SYSTEM_TYPE_FIX_CART"] = "% от суммы заказа, исключая доставку";
$MESS["logictim.balls_REFERAL_SYSTEM_TYPE_MODULE"] = "% от суммы бонусов, расчитанных модулем";
$MESS["logictim.balls_REFERAL_SYSTEM_BONUS"] = "Процент";
$MESS["logictim.balls_REFERAL_USE_COUPONS"] = "Использовать купоны для привлечения рефералов";
$MESS["logictim.balls_REFERAL_USE_COUPONS_Y"] = "Да";
$MESS["logictim.balls_REFERAL_USE_COUPONS_N"] = "Нет";
$MESS["logictim.referals_REFERAL_COUPON_DISCOUNT"] = "Скидка по купонам";
$MESS["logictim.referals_REFERAL_COUPON_DISCOUNT_NO"] = "Не установлено";
$MESS["logictim.balls_REFERAL_COUPON_PREFIX"] = "Правило генерации купонов";
$MESS["logictim.balls_REFERAL_COUPON_PREFIX_NOTES"] = "Доступные переменные:<br />#USER_ID# - ID пользователя, <br />#AUTO# - автоматическая генерация кода<br /><br />Если пусто, или не указано ни одной переменной, то купон генерируется в виде: PARTNER_1, где 1 - ID пользователя<br /><br />Вы можете использовать собственную логику формирования купона, используя АПИ модуля. <a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/api_modulya_4/sobytie_pri_generatsii_referalnogo_kupona_dlya_partnerov/' target='_blank' class='lgb_instruction'>Инструкция</a>";
$MESS["logictim.balls_REFERAL_LEVELS"] = "Количество уровней реферальной системы";


$MESS["logictim.balls_MEDOD_5_ALL_PRODUCT"] = "<b>Начислять на все товары</b>";
$MESS["logictim.balls_BONUS_FOR_PRODUCT_TYPE"] = "<b>Тип начисления</b>";
$MESS["logictim.balls_TYPE_PROC"] = "В процентах";
$MESS["logictim.balls_TYPE_FIX"] = "Фиксированная сумма";
$MESS["logictim.balls_BONUS_MINUS_DISCOUNT_PROD"] = "Не начислять бонусы на товары со скидкой";
$MESS["logictim.balls_BONUS_MINUS_BONUS"] = "Не начислять бонусы на сумму, оплаченную баллами";
$MESS["logictim.balls_BONUS_FOR_DELIVERY"] = "<b>Учитывать стоимость доставки</b>";

$MESS["logictim.balls_OPTIONS_EVENTS_ORDER"] = "Когда начислять бонусы за заказ <a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyki_modulya/kogda_nachislyat_bonusy_za_zakaz/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_EVENT_ORDER_END"] = "Перевод заказа в статус Выполнен (код F)";
$MESS["logictim.balls_EVENT_ORDER_PAYED"] = "При полной оплате заказа";

$MESS["logictim.balls_SELECT_MAX_BONUS_TYPE_1"] = "Нет ограничений";
$MESS["logictim.balls_SELECT_MAX_BONUS_TYPE_2"] = "Ограничение по максимальной сумме оплаты бонусами";
$MESS["logictim.balls_SELECT_MAX_BONUS_TYPE_3"] = "Ограничение в % от суммы заказа, исключая доставку";
$MESS["logictim.balls_SELECT_MAX_BONUS_TYPE_4"] = "Ограничение в % от суммы заказа, включая доставку";

$MESS["logictim.balls_SELECT_MIN_BONUS_TYPE_1"] = "Нет ограничений";
$MESS["logictim.balls_SELECT_MIN_BONUS_TYPE_2"] = "Ограничение по минимальной сумме оплаты бонусами";
$MESS["logictim.balls_SELECT_MIN_BONUS_TYPE_3"] = "Ограничение в % от суммы заказа, исключая доставку";
$MESS["logictim.balls_SELECT_MIN_BONUS_TYPE_4"] = "Ограничение в % от суммы заказа, включая доставку";

$MESS["logictim.balls_BONUS_REPOST_GROUP"] = "Бонусы за репосты <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/bonusy_za_reposty/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_BONUS_REPOST_VK"] = "<b>Бонусы за репост Вконтакте</b>";
$MESS["logictim.balls_BONUS_REPOST_FB"] = "<b>Бонусы за репост Facebook</b>";
$MESS["logictim.balls_BONUS_REPOST_TW"] = "<b>Бонусы за репост Twitter</b><br />(не работает по причине ошибки в АПИ социальной сети)";
$MESS["logictim.balls_BONUS_REPOST_OK"] = "<b>Бонусы за репост Одноклассники</b>";
$MESS["logictim.balls_BONUS_REPOST_WORD"] = "репостов";
$MESS["logictim.balls_BONUS_FROM_WORD"] = "за";
$MESS["logictim.balls_BONUS_MINUTE_WORD"] = "мин.";
$MESS["logictim.balls_VK_APP_ID"] = "Вконтакте APP id";
$MESS["logictim.balls_FACEBOOK_APP_ID"] = "Facebook APP id";
$MESS["logictim.balls_BONUS_ALL_REPOST_SAVE"] = "<b>Сохранять все репосты в истории</b><br />(включая репосты без начислений)";
$MESS["logictim.balls_BONUS_REPOST_ALL_COUNT"] = "<b>Максимальное количество платных репостов<br />(для одного пользователя)</b>";
$MESS["logictim.balls_BONUS_REPOST_PAGE_COUNT"] = "<b>Максимальное количество платных репостов каждой страницы</b><br />(для одного пользователя)";

$MESS["logictim.balls_BONUS_REFERAL_GROUP"] = "Бонусы за переход по репостам <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/bonusy_za_perekhody_po_repostam/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_BONUS_REFERAL"] = "<b>Бонусы за переход по репосту</b>";
$MESS["logictim.balls_BONUS_REFERAL_NAME_VK"] = "Вконтакте";
$MESS["logictim.balls_BONUS_REFERAL_NAME_FB"] = "Facebook";
$MESS["logictim.balls_BONUS_REFERAL_NAME_OK"] = "Одноклассники";
$MESS["logictim.balls_BONUS_REFERAL_NAME_MM"] = "МойМир";
$MESS["logictim.balls_BONUS_REFERAL_NAME_GP"] = "Google+";
$MESS["logictim.balls_BONUS_REFERAL_NAME_TW"] = "Twitter";
$MESS["logictim.balls_BONUS_REFERAL_NAME_LJ"] = "Livejournal";
$MESS["logictim.balls_BONUS_REFERAL_NAME_VB"] = "Viber ";
$MESS["logictim.balls_BONUS_REFERAL_NAME_WA"] = "WhatsApp";
$MESS["logictim.balls_BONUS_REFERAL_NAME_SK"] = "Skype";
$MESS["logictim.balls_BONUS_REFERAL_NAME_TE"] = "Telegram";
$MESS["logictim.balls_BONUS_SOCIALS_NETWORK"] = "<b>Социальные сети</b>";

$MESS["logictim.balls_REVIEW_BLOG"] = "Блог с отзывами";
$MESS["logictim.balls_REVIEW_IBLOCK"] = "Инфоблок с отзывами";
$MESS["logictim.balls_REVIEW_FORUM"] = "Форум с отзывами";
$MESS["logictim.balls_REVIEWS_BONUS_GROUP"] = "Бонусы за отзывы и комментарии";
$MESS["logictim.balls_BONUS_REVIEW"] = "<b>Начислить бонусов за отзыв, или комментарий<b>";
$MESS["logictim.balls_BONUS_REVIEW_COMMENT"] = "Выберите блог, если отзывы реализованы через функционал блога<br />Выберите инфоблок, если отзывы реализованы через инфоблоки";
$MESS["logictim.balls_BONUS_REVIEW_COUNT_HEADER"] = "<b>Максимальное количество начислений за отзыв в промежуток времени</b>";
$MESS["logictim.balls_BONUS_REVIEW_COUNT"] = "<b>Отзывов<b>";



$MESS["logictim.balls_OPTIONS"] = "Настройки";
$MESS["logictim.balls_OPTIONS_MAIN"] = "Основные настройки <a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyki_modulya/nastroyki_ispolzuemyy_schet_i_sposob_oplaty_bonusami/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_OPTIONS_MIN_PAYMENT_BONUS"] = "Настройки минимальной суммы оплаты бонусами";
$MESS["logictim.balls_OPTIONS_MAX_PAYMENT_BONUS"] = "Настройки максимальной суммы оплаты бонусами";
$MESS["logictim.balls_OPTIONS_TEXT"] = "Текстовые настройки";

$MESS["logictim.balls_MIN_PAYMENT_BONUS_TYPE"] = "<b>Тип ограничения</b>";
$MESS["logictim.balls_MIN_PAYMENT_BONUS_LABEL"] = "<b>Сумма, или % ограничения</b><br />(в зависимости от выбранного типа)";

$MESS["logictim.balls_MAX_PAYMENT_BONUS_TYPE"] = "<b>Тип ограничения</b>";
$MESS["logictim.balls_MAX_PAYMENT_BONUS_SUM"] = "<b>Сумма, или % ограничения</b><br />(в зависимости от выбранного типа)";
$MESS["logictim.balls_MAX_PAYMENT_DISCOUNT"] = "<b>Запретить оплачивать бонусами товары со скидкой</b>";
$MESS["logictim.balls_SELECT_METOD_LABEL"] = "<b>Способ начисления баллов</b>";
$MESS["logictim.balls_BONUS_PROC"] = "<b>Процент начисления от заказа</b><br />(если выбран способ начисления баллов <Установка % на сумму заказа>)";
$MESS["logictim.balls_BONUS_ROUND"] = "<b>Округление бонусов</b><br />(знаков после запятой)";

$MESS["logictim.balls_USER_GROUPS_TYPE"] = "<b>Группам пользователей ниже</b>";
$MESS["logictim.balls_USER_GROUPS_TYPE_1"] = "Разрешить использование бонусной системы";
$MESS["logictim.balls_USER_GROUPS_TYPE_2"] = "Запретить использование бонусной системы";

$MESS["logictim.balls_USER_GROUPS"] = "Группы пользователей";

$MESS["logictim.balls_BONUS_TIME_GROUP"] = "<b>Настройки срока жизни бонусов <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/nastroyki_sroka_zhizni_bonusov/' target='_blank' class='lgb_instruction'>(инструкция)</a></b>";
$MESS["logictim.balls_LIVE_BONUS"] = "<b>Включить время жизни бонусов</b>";
$MESS["logictim.balls_LIVE_BONUS_TIME"] = "<b>Время жизни бонусов (дней)</b>";
$MESS["logictim.balls_LIVE_BONUS_ALL"] = "<b>Дата окончания всех бонусов</b><br />(устанавливает дату сгорания уже начисленных бонусов, если ранее время жизни бонусов не использовалось)";
$MESS["logictim.balls_BONUS_DELAY"] = "<b>Через сколько дней активировать начисленные бонусы</b><br />(при начислении бонусов за заказ)";

$MESS["logictim.balls_DISCOUNT_METOD"] = "<b>Оплата бонусами это:</b>";
$MESS["logictim.balls_DISCOUNT_METOD_MOMENT_DISCOUNT"] = "Моментальная скидка";
$MESS["logictim.balls_DISCOUNT_METOD_DISCOUNT"] = "Скидка при сохранении заказа";
$MESS["logictim.balls_DISCOUNT_METOD_PAYMENT"] = "Частичная оплата заказа";

$MESS["logictim.balls_OPTIONS_TEXT_TEXT"] = "Текст";
$MESS["logictim.balls_TEXT_BONUS_BALLS"] = "Бонусные баллы:";
$MESS["logictim.balls_HAVE_BONUS_TEXT"] = "Доступно накопительных бонусов: ";
$MESS["logictim.balls_CAN_BONUS_TEXT"] = "Вы можете оплатить часть заказа накопительными бонусами ";
$MESS["logictim.balls_MIN_BONUS_TEXT"] = "минимальная сумма оплаты бонусами: ";
$MESS["logictim.balls_MAX_BONUS_TEXT"] = "максимальная сумма оплаты бонусами: ";
$MESS["logictim.balls_PAY_BONUS_TEXT"] = "Оплатить бонусами:";
$MESS["logictim.balls_TEXT_BONUS_PAY"] = "Оплата бонусами:";
$MESS["logictim.balls_ERROR_1_TEXT"] = "Не достаточно баллов на счету для списания";
$MESS["logictim.balls_TEXT_BONUS_FOR_ITEM"] = "на счет";
$MESS["logictim.balls_TEXT_BONUS_FOR_PAYMENT"] = "Оплата бонусами";

$MESS["logictim.balls_TAB_4"] = "Начисление бонусов при других событиях";
$MESS["logictim.balls_BONUS_REGISTRATION"] = "<b>Начислить бонусов при регистрации</b>";
$MESS["logictim.balls_BONUS_BIRTHDAY"] = "<b>Начислить бонусов на день рождения</b>";
$MESS["logictim.balls_FREE_BONUS"] = "Акт доброй воли";
$MESS["logictim.balls_FREE_BONUS_CHECK"] = "Начислить бонусы всем";
$MESS["logictim.balls_FREE_BONUS_GROUPS"] = "<b>Начислить всем пользователям из выделенных групп</b><br /> Держите зажатой клавишу ctrl, чтобы выделить несколько";
$MESS["logictim.balls_FREE_BONUS_BONUS"] = "Начислить бонусов";
$MESS["logictim.balls_FREE_BONUS_GO"] = "Начислить";

$MESS["logictim.balls_EVENTS_MAIL_GROUP"] = "Почтовые события по стандарту 1С-Битрикс:";
$MESS["logictim.balls_EVENTS_MAIL_DESCRIPTION_LABEL"] = "Описание:";
$MESS["logictim.balls_EVENTS_MAIL_DESCRIPTION"] = "<b>Доступны следующие почтовые события:</b><br />
	1. Начисление бонусов за заказ <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_ORDER_ADD'>Открыть</a><br />
	2. Начисление бонусов при регистрации <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_REGISTER_ADD'>Открыть</a><br />
	3. Начисление бонусов на день рождения <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_BIRTHDAY_ADD'>Открыть</a><br />
	4. Предупреждение о сгорании бонусов <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_WARNING_END_TIME'>Открыть</a><br />
	5. Начисление бонусов за репост <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_REPOST'>Открыть</a><br />
	6. Начисление бонусов за переход по ссылке <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_LINK'>Открыть</a><br />
	7. Начисление бонусов за отзывы и комментарии <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_REVIEW'>Открыть</a><br />
	8. Начисление бонусов за заказ реферала <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_FROM_REFERAL_ADD'>Открыть</a><br />
	9. Новая заявка на вывод бонусов <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_EXIT_BONUS_INSERT'>Открыть</a><br />
	10. Заявка на вывод бонусов отклонена <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_EXIT_BONUS_CANCEL'>Открыть</a><br />
	11. Закрыта заявка на вывод бонусов <a target='_blank' href='/bitrix/admin/type_edit.php?EVENT_NAME=LOGICTIM_BONUS_EXIT_BONUS_CLOSE'>Открыть</a>
	";
$MESS["logictim.balls_COUNT_DAY_WARNING"] = "<b>За сколько дней предупреждать пользователей о сгорании бонусов</b><br />Через запятую. Например: <1, 3, 5> (0 - не предупреждать)";

$MESS["logictim.balls_TAB_3"] = "Настройка уведомлений";
$MESS["logictim.balls_SEND_MAIL_GROUP"] = "Уведомления пользователей о начислении бонусов за заказ";
$MESS["logictim.balls_SEND_MAIL_CHECK"] = "Отправлять уведомления";
$MESS["logictim.balls_SEND_MAIL_SUBJECT"] = "Тема письма";
$MESS["logictim.balls_SEND_MAIL_SUBJECT_DEFAULT"] = "Бонусы за заказ №#ORDER_ID#";
$MESS["logictim.balls_SEND_MAIL_FROM"] = "Отправитель (E-mail)";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA"] = "Текст письма";
$MESS["logictim.balls_SEND_MAIL_FORMAT"] = "Формат письма";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_NOTES"] = "<b>Доступные переменные:</b><br /><br />#ORDER_ID# - Номер заказа (ID)<br />#ORDER_NUM# - Номер заказа при использовании шаблона генерации номера заказа<br />#BONUS# - Начислено бонусов<br />#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления<br />#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления<br />#NAME# - Имя пользователя<br />#LAST_NAME# - Фамилия пользователя<br />#SECOND_NAME# - Отчество пользователя<br />#LOGIN# - Логин пользователя<br />#EMAIL# - E-mail пользователя<br />#DETAIL# - Состав начисления баллов<br />#SITE# - Адрес сайта<br />#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_DEFAULT"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!

Вам начислены бонусы по заказу №#ORDER_ID# на сайте <a href="http://#SITE#">#SITE#</a> !

Начислено бонусов: #BONUS#
Начисленные бонусы активны до: #BONUS_LIVE_DATE#
Всего бонусов на Вашем счете: #BALLANCE_AFTER#

Ваш логин на сайте: #LOGIN# ';


$MESS["logictim.balls_SEND_MAIL_REGISTER_GROUP"] = "Уведомления пользователей о начислении бонусов при регистрации";
$MESS["logictim.balls_SEND_MAIL_SUBJECT_REGISTER_DEFAULT"] = "Вам начислены бонусы за регистрацию на сайте #SITE#";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_REGISTER_NOTES"] = "<b>Доступные переменные:</b><br /><br />#BONUS# - Начислено бонусов<br />#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления<br />#NAME# - Имя пользователя<br />#LAST_NAME# - Фамилия пользователя<br />#SECOND_NAME# - Отчество пользователя<br />#LOGIN# - Логин пользователя<br />#EMAIL# - E-mail пользователя<br />#SITE# - Адрес сайта<br />#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_REGISTER_DEFAULT"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!

Вам начислены бонусы за регистрацию на сайте <a href="http://#SITE#">#SITE#</a> !

Начислено бонусов: #BONUS#
Начисленные бонусы активны до: #BONUS_LIVE_DATE#
Всего бонусов на Вашем счете: #BALLANCE_AFTER#

Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_SEND_MAIL_BIRTHDAY_GROUP"] = "Уведомления пользователей о начислении бонусов на день рождения";
$MESS["logictim.balls_SEND_MAIL_SUBJECT_BIRTHDAY_DEFAULT"] = "Вам начислены бонусы на день рождения на сайте #SITE#";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_BIRTHDAY_NOTES"] = "<b>Доступные переменные:</b><br /><br />#BONUS# - Начислено бонусов<br />#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления<br />#NAME# - Имя пользователя<br />#LAST_NAME# - Фамилия пользователя<br />#SECOND_NAME# - Отчество пользователя<br />#LOGIN# - Логин пользователя<br />#EMAIL# - E-mail пользователя<br />#SITE# - Адрес сайта<br />#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_SEND_MAIL_TEXTAREA_BIRTHDAY_DEFAULT"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!

Вам начислены бонусы на день рождения, на сайте <a href="http://#SITE#">#SITE#</a> !

Начислено бонусов: #BONUS#
Начисленные бонусы активны до: #BONUS_LIVE_DATE#
Всего бонусов на Вашем счете: #BALLANCE_AFTER#

Ваш логин на сайте: #LOGIN# ';


$MESS["logictim.balls_TAB_5"] = "Настройка интеграции в шаблоны";
$MESS["logictim.balls_OPTIONS_ORDER_FORM"] = "Настройки страницы оформления заказа <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/avtomaticheskaya_integratsiya_v_koriznu_i_oformlenie_zakaza/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_INTEGRATE_IN_SALE_ORDER_AJAX"] = "<b>Интегрировать бонусы в шаблон компонента оформления заказа sale.order.ajax</b><br />(При использовании стандартного шаблона битрикс)";
$MESS["logictim.balls_INTEGRATE_IN_SALE_ORDER_AJAX_NOTE"] = "После изменения параметра требуется очистка кэша сайта <a href='/bitrix/admin/cache.php' target='_blank'>Перейти</a>";
$MESS["logictim.balls_ORDER_TOTAL_BONUS"] = "<b>Показывать сумму заказа с учетом бонусов</b><br />(Показывать 'Итого' с учетом оплаты бонусами)";
$MESS["logictim.balls_ORDER_PAY_BONUS_AUTO"] = "<b>Автоматическое заполнение поля <Оплатить бонусами:></b><br />(В поле <Оплатить бонусами:> подставляется доступная сумма)";

$MESS["logictim.balls_INTEGRATE_IN_SALE_BASKET"] = "Настройки страницы корзины <a href='https://logictim.ru/marketplace/nakopitelnaya_sistema_bonusov_ballov/instruktsii_po_nastroyke/avtomaticheskaya_integratsiya_v_koriznu_i_oformlenie_zakaza/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_INTEGRATE_IN_SALE_BASKET_Y"] = "<b>Интегрировать бонусы в шаблон компонента корзины sale.basket.basket</b><br />(При использовании стандартного шаблона битрикс версии выше 17.8)";

$MESS["logictim.balls_INTEGRATE_IN_CATALOG"] = "Настройки интеграции в каталог";
$MESS["logictim.balls_INTEGRATE_AJAX_IN_CATALOG"] = '<b>AJAX вывод бонусов в каталоге</b><br />(При включенной опции нагрузка на сервер сильно возрастает)<br /> <a href="https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/vnedrenie_v_shablony_4/ajax_vyvod_bonusov_v_kataloge/" target="_blank" class="lgb_instruction">(инструкция)</a>';

//VERSION 4
$MESS["logictim.balls_TAB_ACCESS"] = "Доступ";
$MESS["logictim.balls_MODULE_VERSION"] = "Версия модуля";
$MESS["logictim.balls_MODULE_VERSION_3"] = "Без правил (старая)";
$MESS["logictim.balls_MODULE_VERSION_4"] = "С правилами (новая)";
$MESS["logictim.balls_VERSION_CHANGE_NOTES"] = "<b>ВАЖНО!!! Ознакомьтесь с инструкцией перед сменой версий!</b><br />
Настройки в различных версиях отличаются!<br /><a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyki_modulya/perekhod_s_versii_3/' target='_blank' class='lgb_instruction'>Смотреть инструкцию</a>";
$MESS["logictim.balls_EVENT_ORDER_STATUS"] = "При переводе заказа в статус";
$MESS["logictim.balls_EVENT_ORDER_STATUS_NO"] = "Не учитывать";
$MESS["logictim.balls_MAIN_OPTIONS_TAB"] = "Основные настройки";
$MESS["logictim.balls_REFERAL_OPTIONS_TAB"] = "Реферальные настройки";
$MESS["logictim.balls_REFERAL_OPTIONS_SECTION"] = "Настройки реферальной программы <a href='https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyki_modulya/nastroyki_referalnoy_sistemy/' target='_blank' class='lgb_instruction'>(инструкция)</a>";
$MESS["logictim.balls_REFERAL_OPTIONS_PARTNER_GROUP"] = "При генерации купона добавлять пользователя в группу:<br /><div class='comment'>(чтобы в настройках скидки запретить ее применение владельцу купона)</div>";

?>
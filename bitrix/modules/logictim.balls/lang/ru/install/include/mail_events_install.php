<?
$MESS["logictim.balls_ME_BONUS_FROM_ORDER_NAME"] = "Начислены бонусы за заказ";
$MESS["logictim.balls_ME_BONUS_FROM_ORDER_DESCRIPTION"] = "#ORDER_ID# - Номер заказа (ID)
#ORDER_NUM# - Номер заказа при использовании шаблона генерации номера заказа
#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#DETAIL# - Состав начисления баллов
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_FROM_ORDER_SUBJECT"] = "Бонусы за заказ №#ORDER_ID#";
$MESS["logictim.balls_MT_BONUS_FROM_ORDER_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы по заказу №#ORDER_ID# на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_FROM_REFERAL_NAME"] = "Начислены бонусы за заказ реферала";
$MESS["logictim.balls_ME_BONUS_FROM_REFERAL_DESCRIPTION"] = "#ORDER_ID# - Номер заказа (ID)
#ORDER_NUM# - Номер заказа при использовании шаблона генерации номера заказа
#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#DETAIL# - Состав начисления баллов
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_FROM_REFERAL_SUBJECT"] = "Бонусы за заказ реферала";
$MESS["logictim.balls_MT_BONUS_FROM_REFERAL_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы за заказ реферала на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';


$MESS["logictim.balls_ME_BONUS_REGISTER_ADD_NAME"] = "Начислены бонусы за регистрацию";
$MESS["logictim.balls_ME_BONUS_REGISTER_ADD_DESCRIPTION"] = "#BONUS# - Начислено бонусов
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_REGISTER_ADD_SUBJECT"] = "Вам начислены бонусы за регистрацию на сайте #SITE#";
$MESS["logictim.balls_MT_BONUS_REGISTER_ADD_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы за регистрацию на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_BIRTHDAY_ADD_NAME"] = "Начислены бонусы на день рождения";
$MESS["logictim.balls_ME_BONUS_BIRTHDAY_ADD_DESCRIPTION"] = "#BONUS# - Начислено бонусов
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_BIRTHDAY_ADD_SUBJECT"] = "Вам начислены бонусы на день рождения на сайте #SITE#";
$MESS["logictim.balls_MT_BONUS_BIRTHDAY_ADD_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы на день рождения, на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_FREE_ADD_NAME"] = "Ручное начисление бонусов по группе (Акт доброй воли)";
$MESS["logictim.balls_ME_BONUS_FREE_ADD_DESCRIPTION"] = "#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_FREE_ADD_SUBJECT"] = "Вам начислены бонусы на сайте #SITE#";
$MESS["logictim.balls_MT_BONUS_FREE_ADD_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Наименование начисления: #OPERATION_NAME#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_WARNING_END_TIME_NAME"] = "Предупреждение о сгорании бонусов";
$MESS["logictim.balls_ME_BONUS_WARNING_END_TIME_DESCRIPTION"] = "#BONUS# - Сколько бонусов сгорит в ближайшее время
#BALLANCE_USER# - Бонусов на счету пользователя
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_WARNING_END_TIME_SUBJECT"] = "Предупреждение об окончании срока действия бонусов на сайте #SITE#";
$MESS["logictim.balls_MT_BONUS_WARNING_END_TIME_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
У Вас имеются не использованные бонусы на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Подходит к концу срок действия бонусов суммой: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br><br>
Всего бонусов на Вашем счете: #BALLANCE_USER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_FROM_REPOST_NAME"] = "Начислены бонусы за репост";
$MESS["logictim.balls_ME_BONUS_FROM_REPOST_DESCRIPTION"] = "#ORDER_ID# - Номер заказа (ID)
#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов
#SOCIAL_NETWORK# - Социальная сеть
#PAGE# - Страница сайта, которую репостили
#SOCIAL_POST_DATE# - Дата репоста";
$MESS["logictim.balls_MT_BONUS_FROM_REPOST_SUBJECT"] = "Бонусы за репост #SOCIAL_NETWORK#";
$MESS["logictim.balls_MT_BONUS_FROM_REPOST_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы за репост #SOCIAL_NETWORK# на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Дата репоста: #SOCIAL_POST_DATE#<br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';

$MESS["logictim.balls_ME_BONUS_FROM_REVIEW_NAME"] = "Начислены бонусы за отзыв";
$MESS["logictim.balls_ME_BONUS_FROM_REVIEW_DESCRIPTION"] = "#ORDER_ID# - Номер заказа (ID)
#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";
$MESS["logictim.balls_MT_BONUS_FROM_REVIEW_SUBJECT"] = "Бонусы за отзыв";
$MESS["logictim.balls_MT_BONUS_FROM_REVIEW_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы за отзыв на сайте <a href="http://#SITE#">#SITE#</a> !<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';



$MESS["logictim.balls_ME_BONUS_FROM_LINK_NAME"] = "Начислены бонусы за переход по ссылке";
$MESS["logictim.balls_ME_BONUS_FROM_LINK_DESCRIPTION"] = "#BONUS# - Начислено бонусов
#BALLANCE_BEFORE# - Бонусов на счету пользователя до начисления
#BALLANCE_AFTER# - Бонусов на счету пользователя после начисления
#OPERATION_NAME# - Наименование операции
#NAME# - Имя пользователя
#LAST_NAME# - Фамилия пользователя
#SECOND_NAME# - Отчество пользователя
#LOGIN# - Логин пользователя
#EMAIL# - E-mail пользователя
#URL# - Страница на которую ведет ссылка
#REF_LINK# - Сайт с размещенной ссылкой
#SITE# - Адрес сайта
#BONUS_LIVE_DATE# - Дата окончания срока активности бонусов";

$MESS["logictim.balls_MT_BONUS_FROM_LINK_SUBJECT"] = "Бонусы за переход по ссылке";
$MESS["logictim.balls_MT_BONUS_FROM_LINK_MESSAGE"] = 'Здравствуйте, <b>#LAST_NAME# #NAME#  #SECOND_NAME#</b>!<br><br>
Вам начислены бонусы на сайте <a href="http://#SITE#">#SITE#</a> за переход по размещенной Вами ссылке!<br><br>
Начислено бонусов: #BONUS#<br>
Начисленные бонусы активны до: #BONUS_LIVE_DATE#<br>
Всего бонусов на Вашем счете: #BALLANCE_AFTER#<br><br>
Ваш логин на сайте: #LOGIN# ';


$MESS["logictim.balls_ME_EXIT_BONUS_CLOSE"] = "Закрыта заявка на вывод бонусов";
$MESS["logictim.balls_ME_EXIT_BONUS_CLOSE_DESCRIPTION"] = "#EXIT_ID# - ID запроса на вывод бонусов
#EXIT_NAME# - Наименование запроса на вывод бонусов
#EXIT_SUM# - Сумма запроса на вывод бонусов
#DATE_INSERT# - Дата создания запроса на вывод бонусов
#DATE_CLOSE# - Дата закрытия запроса на вывод бонусов
#COMMENT_ADMIN# - Комментарий администратора
#USER_NAME# - Имя пользователя
#USER_LAST_NAME# - Фамилия пользователя
#USER_SECOND_NAME# - Отчество пользователя
#USER_LOGIN# - Логин пользователя
#USER_EMAIL# - E-mail пользователя
#SITE# - Адрес сайта";

$MESS["logictim.balls_MT_EXIT_BONUS_CLOSE_SUBJECT"] = "Закрыта заявка на вывод бонусов";
$MESS["logictim.balls_MT_EXIT_BONUS_CLOSE_MESSAGE"] = 'Здравствуйте, <b>#USER_LAST_NAME# #USER_NAME#  #USER_SECOND_NAME#</b>!<br><br>
Ваша заявка на вывод бонусов №#EXIT_ID# от #DATE_INSERT# на сайте <a href="http://#SITE#">#SITE#</a> закрыта!<br><br>
Сумма вывода со счета: #EXIT_SUM#<br>
Комментарий администратора: #COMMENT_ADMIN#<br>
Ваш логин на сайте: #USER_LOGIN# ';


$MESS["logictim.balls_ME_EXIT_BONUS_CANCEL"] = "Заявка на вывод бонусов отклонена";
$MESS["logictim.balls_ME_EXIT_BONUS_CANCEL_DESCRIPTION"] = "#EXIT_ID# - ID запроса на вывод бонусов
#EXIT_NAME# - Наименование запроса на вывод бонусов
#EXIT_SUM# - Сумма запроса на вывод бонусов
#DATE_INSERT# - Дата создания запроса на вывод бонусов
#DATE_CANCEL# - Дата отклонения запроса на вывод бонусов
#COMMENT_ADMIN# - Комментарий администратора
#USER_NAME# - Имя пользователя
#USER_LAST_NAME# - Фамилия пользователя
#USER_SECOND_NAME# - Отчество пользователя
#USER_LOGIN# - Логин пользователя
#USER_EMAIL# - E-mail пользователя
#SITE# - Адрес сайта";

$MESS["logictim.balls_MT_EXIT_BONUS_CANCEL_SUBJECT"] = "Заявка на вывод бонусов отклонена";
$MESS["logictim.balls_MT_EXIT_BONUS_CANCEL_MESSAGE"] = 'Здравствуйте, <b>#USER_LAST_NAME# #USER_NAME#  #USER_SECOND_NAME#</b>!<br><br>
Ваша заявка на вывод бонусов №#EXIT_ID# от #DATE_INSERT# на сайте <a href="http://#SITE#">#SITE#</a> отклонена.<br><br>
Сумма вывода со счета: #EXIT_SUM#<br>
Комментарий администратора: #COMMENT_ADMIN#<br>
Ваш логин на сайте: #USER_LOGIN# ';

$MESS["logictim.balls_ME_EXIT_BONUS_INSERT"] = "Новая заявка на вывод бонусов";
$MESS["logictim.balls_ME_EXIT_BONUS_INSERT_DESCRIPTION"] = "#EXIT_ID# - ID запроса на вывод бонусов
#EXIT_NAME# - Наименование запроса на вывод бонусов
#EXIT_SUM# - Сумма запроса на вывод бонусов
#DATE_INSERT# - Дата создания запроса на вывод бонусов
#USER_NAME# - Имя пользователя
#USER_LAST_NAME# - Фамилия пользователя
#USER_SECOND_NAME# - Отчество пользователя
#USER_LOGIN# - Логин пользователя
#USER_EMAIL# - E-mail пользователя
#SITE# - Адрес сайта";

$MESS["logictim.balls_MT_EXIT_BONUS_INSERT_SUBJECT"] = "Создана новая заявка на вывод бонусов со счета";
$MESS["logictim.balls_MT_EXIT_BONUS_INSERT_MESSAGE"] = 'Здравствуйте!<br><br>
На сайте <a href="http://#SITE#">#SITE#</a> создана новая заявка на вывод бонусов со счета<br><br>
ID заявки: #EXIT_ID#<br>
Дата создания заявки: #DATE_INSERT#<br>
Сумма вывода со счета: #EXIT_SUM#<br><br>
Данные пользователя:<br>
Логин: #USER_LOGIN#<br>
Имя: #USER_LAST_NAME# #USER_NAME#  #USER_SECOND_NAME#';
?>
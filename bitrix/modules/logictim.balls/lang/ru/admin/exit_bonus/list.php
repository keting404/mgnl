<?
$MESS["logictim.balls_BONUS_OPERATION_NAME"] = "Название";
$MESS["logictim.balls_BONUS_OPERATION_SUM"] = "Сумма вывода";
$MESS["logictim.balls_BONUS_OPERATION_DATE_INSERT"] = "Дата создания";
$MESS["logictim.balls_BONUS_OPERATION_DATE_UPDATE"] = "Дата обновления";
$MESS["logictim.balls_BONUS_OPERATION_DATE_CLOSE"] = "Дата закрытия";
$MESS["logictim.balls_BONUS_OPERATION_STATUS"] = "Статус";
$MESS["logictim.balls_BONUS_OPERATION_USER"] = "Пользователь";

$MESS["logictim.balls_BONUS_PROFILE_COPY"] = "Копировать";
$MESS["logictim.balls_BONUS_PROFILE_EDIT"] = "Изменить";
$MESS["logictim.balls_BONUS_PROFILE_DEL"] = "Удалить";
$MESS["logictim.balls_EXIT_STATUS_P"] = "Получена";
$MESS["logictim.balls_EXIT_STATUS_F"] = "Выполнена";
$MESS["logictim.balls_EXIT_STATUS_C"] = "Отклонена";
$MESS["MAIN_ADMIN_LIST_CLOSE"] = "Завершить";
$MESS["MAIN_ADMIN_LIST_CANCEL"] = "Отклонить";
$MESS["logictim.balls_EXIT_RETURN_OPERATION_NAME"] = "Возврат бонусов по отклоненной заявке на вывод №";
$MESS["logictim.balls_BONUS_PROFILE_NAV"] = "Заявки";
$MESS["logictim.balls_BONUS_EXIT_LIST"] = 'Заявки на вывод';
$MESS["logictim.balls_BONUS_EXIT_STATUS_ALL"] = 'Все';
$MESS["logictim.balls_BONUS_USER_ID"] = 'ID пользователя';
?>
<?
$MESS["logictim.balls_PROFILE_ORDER_TAB_1"] = "Основные настройки";
$MESS["logictim.balls_PROFILE_ORDER_TAB_2"] = "Условия применения правила";
$MESS["logictim.balls_PROFILE_ORDER_TAB_3"] = "Условия начислений";
$MESS["logictim.balls_PROFILE_TYPE"] = "Тип правила:";
$MESS["logictim.balls_PROFILE_ID"] = "ID правила:";
$MESS["logictim.balls_PROFILE_ACTIVE"] = "Активность:";
$MESS["logictim.balls_PROFILE_NAME"] = "Название правила:";
$MESS["logictim.balls_PROFILE_SORT"] = "Приоритет:";
$MESS["logictim.balls_PROFILE_PERIOD"] = "Период активности правила:";
$MESS["logictim.balls_PROFILE_PERIOD_PLACEHOLDER"] = "дд.мм.гггг";
$MESS["logictim.balls_BONUS_PERIOD"] = "Период активности бонусов";
$MESS["logictim.balls_BONUS_ACTIVE_AFTER"] = "Активировать бонусы через:";
$MESS["logictim.balls_BONUS_LIVE_TIME"] = "Время жизни бонусов:";
$MESS["logictim.balls_BONUS_DAYS"] = "Дней";
$MESS["logictim.balls_BONUS_MONTHS"] = "Месяцев";
$MESS["logictim.balls_PROFILE_COND_COMMENT"] = '<p>
                	- Если не указано ни одно условие, то правило действует для всех пользователей
                    </p>
                    <p>
                    - Если добавлено условие из группы "Дополнительные параметры", то бонусы могут отображаться только на этапе корзины и оформления.
                    </p>';
$MESS["logictim.balls_PROFILE_COND_REFERAL_COMMENT"] = '<p>
                	- Партнер - пользователь, который привел другого пользователя (реферала)
                    </p>
                    <p>
                    - Реферал - пользователь, с заказов которого начисляется бонус партнеру
                    </p>
					<p>
                    - Если в условии не указан уровень реферала, то бонус будет начисляться за заказы рефералов любого уровня
                    </p>
					<p>
                    <b>Пример:</b><br>
					Если в условии указано: "Уровень реферала" = 2, то бонусы согласно настройкам правила будут начисляться партнеру за заказ рефералов 2-го уровня.
                    </p>';
$MESS["logictim.balls_PROFILE_COND_ABSOLUTE"] = 'Отображать в каталоге бонусы этого правила даже если указаны дополнительные пармаметры (требует включения ajax интеграции в каталог) <a href="https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/vnedrenie_v_shablony_4/ajax_vyvod_bonusov_v_kataloge/" target="_blank" class="lgb_instruction">(инструкция)</a>';
$MESS["logictim.balls_PROFILE_COND_SECT"] = 'Условия применения правила <a href="https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyka_pravil/usloviya_primeneniya_pravila/" target="_blank" class="lgb_instruction">(инструкция)</a>';
$MESS["logictim.balls_PRODUCTS_COND_SECT"] = 'Условия начислений <a href="https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyka_pravil/usloviya_otbora_tovarov/" target="_blank" class="lgb_instruction">(инструкция)</a>';
$MESS["logictim.balls_BONUS_ADD"] = "Начислить бонусов:";
$MESS["logictim.balls_BONUS_INSTRUCTION"] = "Инструкция";

$MESS["logictim.balls_BONUS_FROM_ORDER"] = "Бонусы за заказ пользователя";
$MESS["logictim.balls_BONUS_FROM_REGISTRATION"] = "Бонусы за регистрацию";
$MESS["logictim.balls_BONUS_FROM_BIRTHDAY"] = "Бонусы на день рождения";
$MESS["logictim.balls_BONUS_FROM_REVIEW"] = "Бонусы за отзыв на сайте";
$MESS["logictim.balls_BONUS_REVIEW_MAX"] = "Максимальное количество отзывов для начислений";
$MESS["logictim.balls_BONUS_REVIEW_MAX_FROM"] = "отзывов за";
$MESS["logictim.balls_BONUS_MUST_SAVE_RULE"] = "Сохраните правило для вступления данной настройки в силу";
$MESS["logictim.balls_BONUS_REVIEW_COND"] = "Где хранятся отзывы";
$MESS["logictim.balls_BONUS_FROM_REFLINK"] = "Бонусы за переход по репосту (реферальной ссылке)";
$MESS["logictim.balls_BONUS_FROM_ORDER_REFERAL"] = "Бонусы за заказ реферала";
$MESS["logictim.balls_BONUS_PAY_ORDER"] = "Оплата заказа бонусами";
$MESS["logictim.balls_BONUS_PAY_LIMIT"] = "Ограничения оплаты бонусами";
$MESS["logictim.balls_BONUS_PAY_MIN"] = "Минимальная сумма оплаты бонусами:";
$MESS["logictim.balls_BONUS_BONUS"] = "бонусов";
$MESS["logictim.balls_BONUS_PERCENT"] = "%";
$MESS["logictim.balls_BONUS_INCLUDE"] = "включая";
$MESS["logictim.balls_BONUS_NO_INCLUDE"] = "исключая";
$MESS["logictim.balls_BONUS_PAY_MAX"] = "Максимальная сумма оплаты бонусами:";
$MESS["logictim.balls_PRODUCTS_COND_PAY_TAB"] = 'Условия оплаты товаров';
$MESS["logictim.balls_PRODUCTS_COND_PAY_SECT"] = 'Условия оплаты товаров <a href="https://logictim.ru/marketplace/nakopitelnaya_referalnaya_sistema_versii_4_0_i_vyshe/instruktsii_po_nastroyke_4/nastroyka_pravil/usloviya_otbora_tovarov/" target="_blank" class="lgb_instruction">(инструкция)</a>';
$MESS["logictim.balls_BONUS_EXIT_BONUS"] = "Запрос на вывод бонусов";
$MESS["logictim.balls_BONUS_EXIT_LIMIT"] = "Ограничения на вывод бонусов";
$MESS["logictim.balls_BONUS_EXIT_MIN"] = "Минимальная сумма для вывода";
$MESS["logictim.balls_BONUS_EXIT_MAX"] = "Максимальная сумма для вывода";
$MESS["logictim.balls_MAX_SHARING_BONUS"] = 'Максимальное количество оплачиваемых репостов для одного пользователя';
$MESS["logictim.balls_MAX_SHARING_BONUS_PAGE"] = 'Максимальное количество оплачиваемых репостов <b>одной страницы</b>';
$MESS["logictim.balls_MAX_SHARING_BONUS_ALL"] = 'Максимальное количество оплачиваемых репостов <b>всего</b>';
$MESS["logictim.balls_MAX_SHARING_REPOSTS"] = 'репостов';
$MESS["logictim.balls_MAX_SHARING_FROM"] = 'за';
$MESS["logictim.balls_BONUS_MINS"] = 'минут';
$MESS["logictim.balls_BONUS_HOURS"] = 'часов';
$MESS["logictim.balls_MAX_SHARING_SAVE_ALL"] = 'Сохранять все репосты в истории(включая репосты без начислений)';
$MESS["logictim.balls_SHARING_BONUS"] = 'Начислять бонусы';
$MESS["logictim.balls_SHARING_BONUS_VK"] = 'за репост Вконтакте	';
$MESS["logictim.balls_SHARING_BONUS_FB"] = 'за репост Facebook	';
$MESS["logictim.balls_SHARING_BONUS_OK"] = 'за репост Одноклассники	';
$MESS["logictim.balls_SHARING_TECH"] = 'Технические настройки';
$MESS["logictim.balls_SHARING_VK_APP"] = 'Вконтакте APP id	';
$MESS["logictim.balls_SHARING_FB_APP"] = 'Facebook APP id';
$MESS["logictim.balls_BONUS_FROM_REPOST"] = 'Бонусы за репост';

//CONDITIONS
$MESS["logictim.balls_SELECT_COND"] = "Выберите условие";
$MESS["logictim.balls_ADD_PROFILE_COND"] = "Добавить условие применения правила";
$MESS["logictim.balls_DEL_COND"] = "Удалить условие";
$MESS["logictim.balls_ADD_COND"] = "Добавить условие";
$MESS["logictim.balls_PARTNER_GROUP"] = "Группа партнера";
$MESS["logictim.balls_COND_EQUAL"] = "равно";
$MESS["logictim.balls_COND_NOT"] = "не равно";
$MESS["logictim.balls_COND_GREAT"] = "больше";
$MESS["logictim.balls_COND_LESS"] = "меньше";
$MESS["logictim.balls_COND_EQGR"] = "больше либо равно";
$MESS["logictim.balls_COND_EQLS"] = "меньше либо равно";
$MESS["logictim.balls_PARTNER_LEVEL"] = "Уровень реферала";
$MESS["logictim.balls_ORDER_USER_GROUP"] = "Группа реферала (сделавшего заказ)";
$MESS["logictim.balls_ORDER_SITE"] = "Сайт на котором сделан заказ";
$MESS["logictim.balls_COND_MAIN_PARAMS"] = "Основные параметры";
$MESS["logictim.balls_COND_SITE"] = "Сайт";
$MESS["logictim.balls_COND_USER_GROUP"] = "Группа пользователей";
$MESS["logictim.balls_COND_OTHER_PARAMS"] = "Дополнительные параметры";
$MESS["logictim.balls_COND_USER_ORDERS_SUM"] = "Сумма покупок пользователя";
$MESS["logictim.balls_COND_FOR_PERIOD"] = "за период";
$MESS["logictim.balls_COND_DAY"] = "день";
$MESS["logictim.balls_COND_MONTH"] = "месяц";
$MESS["logictim.balls_COND_YEAR"] = "год";
$MESS["logictim.balls_COND_CART_SUM"] = "Сумма товаров в заказе";
$MESS["logictim.balls_COND_ORDER_SUM"] = "Сумма заказа (с учетом доставки)";
$MESS["logictim.balls_COND_PAY_SYSTEM"] = "Платежная система";
$MESS["logictim.balls_COND_DELIVERY_SYSTEM"] = "Способ доставки";
$MESS["logictim.balls_COND_PERSON_TYPE"] = "Тип плательщика";


$MESS["logictim.balls_SELECT_COND_GROUP"] = "Добавить начисление";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS_GROUP"] = "Добавить начисление бонусов";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS"] = "За покупку товаров";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS_GROUP_FROM_PROPS"] = "За покупку товаров (бонусы из свойств)";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS_ORDER"] = "За заказ";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS_TEXT"] = "Начислять ";
$MESS["logictim.balls_SELECT_COND_ADD_BONUS_ORDER_TEXT"] = "Начислять за заказ ";
$MESS["logictim.balls_FIX_IF"] = " если ";
$MESS["logictim.balls_SELECT_ADD_BONUS_PERCENT"] = "процентов";
$MESS["logictim.balls_SELECT_ADD_BONUS_BONUS"] = "бонусов";
$MESS["logictim.balls_ROUND_LABEL"] = "стоимости товаров с округлением до";
$MESS["logictim.balls_ROUND_LABEL_SHORT"] = "с округлением до";
$MESS["logictim.balls_ROUND_SYMBOLS"] = "знаков после запятой, на товары для которых";
$MESS["logictim.balls_AND_CONDS"] = "все условия";
$MESS["logictim.balls_OR_CONDS"] = "любое из условий";
$MESS["logictim.balls_CONDS_TRUE"] = "выполнено(ы)";
$MESS["logictim.balls_CONDS_FALSE"] = "не выполнено(ы)";
$MESS["logictim.balls_COND_IBLOCK"] = "Инфоблок";
$MESS["logictim.balls_COND_SECTION"] = "Раздел";
$MESS["logictim.balls_COND_PRODUCT"] = "Товар";
$MESS["logictim.balls_COND_PRICE"] = "Цена";

$MESS["logictim.balls_COND_DISCOUNT"] = "Наличие скидки";
$MESS["logictim.balls_COND_WITHOUT_DISCOUNT"] = "Только товары без скидки";
$MESS["logictim.balls_COND_WITH_DISCOUNT"] = "Только товары со скидкой";
$MESS["logictim.balls_COND_DISCOUNT_SIZE"] = "Размер скидки (на единицу товара)";
$MESS["logictim.balls_COND_PERCENT"] = "%";
$MESS["logictim.balls_COND_EDINIC"] = "рублей (валюты цены)";

$MESS["logictim.balls_COND_PARTNER_GROUP"] = "Группа Рефералодателя";
$MESS["logictim.balls_COND_REFERAL_PARAMS"] = "Реферальные параметры";
$MESS["logictim.balls_COND_ADD_REVIEW_PLACE"] = "Указать где хранятся отзывы";
$MESS["logictim.balls_COND_DEL_REVIEW_PLACE"] = "Удалить место хранения отзывов";
$MESS["logictim.balls_COND_AND"] = "И";
$MESS["logictim.balls_COND_OR"] = "ИЛИ";
$MESS["logictim.balls_COND_ADD_BONUS_FROM_PROP"] = "Начислять бонусы из свойства с кодом";
$MESS["logictim.balls_COND_ADD_BONUS_FROM_PROP_TYPE"] = "в виде";
$MESS["logictim.balls_COND_BLOG"] = "Блог";
$MESS["logictim.balls_COND_FORUM"] = "Форум";
$MESS["logictim.balls_ADD_PAY_COND"] = "Добавить правило оплаты бонусами";
$MESS["logictim.balls_CAN_PAY"] = "Разрешить оплачивать ";
$MESS["logictim.balls_COND_PAY_BONUS"] = "Оплата заказа бонусами";
$MESS["logictim.balls_COND_HAVE"] = "Только заказы с оплатой бонусами";
$MESS["logictim.balls_COND_HAVE_NO"] = "Только заказы без оплаты бонусами";
$MESS["logictim.balls_COND_PART_ORDER"] = "стоимости заказа";
$MESS["logictim.balls_COND_PART_INCLUDE_DELIVERY"] = "доставку";

$MESS["logictim.balls_COND_TYPE_USERS"] = "Тип пользователя";
$MESS["logictim.balls_COND_HAVE_REFERALS_LABEL"] = "Наличие рефералов у пользователя";
$MESS["logictim.balls_COND_HAVE_REFERALS"] = "У пользователя есть рефералы";
$MESS["logictim.balls_COND_HAVE_REFERALS_Y"] = "Да";
$MESS["logictim.balls_COND_HAVE_REFERALS_N"] = "Нет";
?>
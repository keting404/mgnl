<?
$MESS["logictim.balls_BONUS_PROFILES_ADD_ACTION"] = "Добавить начисление бонусов";
$MESS["logictim.balls_BONUS_PROFILES_ADD_REFERAL"] = "Добавить реферальное начисление";
$MESS["logictim.balls_BONUS_PROFILES_ADD_PAYMENT"] = "Добавить правило оплаты бонусами";
$MESS["logictim.balls_BONUS_PROFILE_ORDER"] = "За заказ";
$MESS["logictim.balls_BONUS_PROFILE_REGISTER"] = "За регистрацию";
$MESS["logictim.balls_BONUS_PROFILE_BIRTHDAY"] = "На день рождения";
$MESS["logictim.balls_BONUS_PROFILE_REVIEW"] = "За отзыв на сайте";
$MESS["logictim.balls_BONUS_PROFILE_REFLINK"] = "За переход по репосту (реф. сссылке)";
$MESS["logictim.balls_BONUS_PROFILE_SHARING"] = "За репост в социальные сети";
$MESS["logictim.balls_BONUS_PROFILE_ORDER_REFERAL"] = "За заказ реферала";
$MESS["logictim.balls_BONUS_PROFILE_REGISTER_REFERAL"] = "За регистрацию реферала";
$MESS["logictim.balls_BONUS_PROFILE_PAY"] = "Оплата заказа бонусами";
$MESS["logictim.balls_BONUS_PROFILE_EEXIT_BONUS"] = "Запрос на вывод бонусов";
$MESS["logictim.balls_BONUS_PROFILE_NAV"] = "Правила";
$MESS["logictim.balls_BONUS_PROFILE_NAME"] = "Название";
$MESS["logictim.balls_BONUS_PROFILE_SORT"] = "Приоритет";
$MESS["logictim.balls_BONUS_PROFILE_ACTIVE"] = "Активность";
$MESS["logictim.balls_BONUS_PROFILE_TYPE"] = "Тип профиля";

$MESS["logictim.balls_BONUS_PROFILE_TYPE_order"] = "Бонусы за заказ";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_registration"] = "Бонусы за регистрацию";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_birthday"] = "Бонусы на день рождения";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_review"] = "Бонусы за отзыв на сайте";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_reflink"] = "Бонусы за переход по репосту";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_sharing"] = "Бонусы за репост";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_order_referal"] = "Бонусы за заказ реферала";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_pay_bonus"] = "Оплата заказа бонусами";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_exit_bonus"] = "Запрос на вывод бонусов";
$MESS["logictim.balls_BONUS_PROFILE_TYPE_ALL"] = "Все";
$MESS["logictim.balls_BONUS_PROFILE_ACTIVE_Y"] = "Да";
$MESS["logictim.balls_BONUS_PROFILE_ACTIVE_N"] = "Нет";
$MESS["logictim.balls_BONUS_PROFILES_LIST"] = "Список правил (профилей)";

$MESS["logictim.balls_BONUS_PROFILE_COPY"] = "Копировать";
$MESS["logictim.balls_BONUS_PROFILE_EDIT"] = "Изменить";
$MESS["logictim.balls_BONUS_PROFILE_DEL"] = "Удалить";

?>
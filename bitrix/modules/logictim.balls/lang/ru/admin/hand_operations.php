<?
$MESS["logictim.balls_BONUS_OPERATION_TITLE"] = "Ручное проведение операций";
$MESS["logictim.balls_BONUS_OPERATION_TYPE"] = "Тип операции:";
$MESS["logictim.balls_BONUS_OPERATION_PLUS"] = "Начислить";
$MESS["logictim.balls_BONUS_OPERATION_MINUS"] = "Списать";
$MESS["logictim.balls_BONUS_OPERATION_NAME"] = "Название операции:";
$MESS["logictim.balls_BONUS_OPERATION_BONUS"] = "Бонусов:";
$MESS["logictim.balls_BONUS_LIVE_TIME"] = "Время жизни бонусов:";
$MESS["logictim.balls_BONUS_PERIOD_DAY"] = "Дней";
$MESS["logictim.balls_BONUS_PERIOD_MONTH"] = "Месяцев";
$MESS["logictim.balls_BONUS_USER_TYPE"] = "Кому:";
$MESS["logictim.balls_BONUS_USER_TYPE_GROUPS"] = "Группе пользователей";
$MESS["logictim.balls_BONUS_USER_TYPE_USERS"] = "Выборочным пользователям";
$MESS["logictim.balls_BONUS_GROUPS"] = "Группа пользователей:";
$MESS["logictim.balls_BONUS_USERS"] = "Выбранные пользователи";
$MESS["logictim.balls_ADD_USER"] = "Добавить пользователя";
$MESS["logictim.balls_BONUS_TITLE"] = "Начисление / списание бонусов";
$MESS["logictim.balls_OK_MESSAGE"] = "Задание выполнено, о повелитель бонусов!";
$MESS["logictim.balls_ERROR_NO_USERS"] = "Не указано ни одного пользователя";
$MESS["logictim.balls_ERROR_NO_OPERATION_NAME"] = "Не указано название операции";
$MESS["logictim.balls_ERROR_NO_LIVE_PERIOD"] = "Не указано время жизни бонусов";
$MESS["logictim.balls_ERROR_NO_BONUS"] = "Не казано количество бонусов";
$MESS["logictim.balls_USER_BALLANCE"] = "Текущий баланс";
$MESS["logictim.balls_HAND_OPERATION_WAIT"] = "Не закрывайте и не обновляйте страницу, пока процесс не завершится";
$MESS["logictim.balls_HAND_OPERATION_COMPLITE"] = "Выполнено операций:";
$MESS["logictim.balls_HAND_OPERATION_ELSE"] = "Продолжить";
?>
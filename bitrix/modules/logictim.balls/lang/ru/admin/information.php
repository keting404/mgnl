<?
$MESS["logictim.balls_INFO"] = "Информация";
$MESS["logictim.balls_INFO_OPTIONS_MODULE"] = "Глобальные настройки модуля";
$MESS["logictim.balls_INFO_OPTIONS_TAB"] = "Инструкции";
$MESS["logictim.balls_INFO_OPTIONS_SEE"] = "Смотреть";
$MESS["logictim.balls_BONUS_INFO_MAIN_OPTIONS"] = "Основные настройки: Используемый счет, Способ оплаты бонусами";
$MESS["logictim.balls_BONUS_INFO_WHEN_ORDER"] = "Когда начсиялять бонусы за заказ";
$MESS["logictim.balls_BONUS_INFO_REFERAL_OPTIONS"] = "Настройки реферальной системы";
$MESS["logictim.balls_BONUS_INFO_INEGRATE_OPTIONS"] = "Настройка автоматической интеграции в стандартные шаблоны корзины и оформления заказа";

$MESS["logictim.balls_INFO_RULES"] = "Настройка правил начисления и оплаты бонусами";
$MESS["logictim.balls_INFO_RULES_MAIN"] = "Основы настройки правил. Создание, копирование, редактирование, порядок применения правил.";
$MESS["logictim.balls_INFO_RULES_FROM_ORDER"] = "Бонусы за заказ";
$MESS["logictim.balls_INFO_RULES_FROM_REGISTER"] = "Бонусы за регистрацию";
$MESS["logictim.balls_INFO_RULES_FROM_BIRTHDAY"] = "Бонусы на день рождения";
$MESS["logictim.balls_INFO_RULES_FROM_REVIEW"] = "Бонусы за отзыв на сайте";
$MESS["logictim.balls_INFO_RULES_FROM_REF_LINK"] = "Бонусы за переход по репосту (реф. ссылке)";
$MESS["logictim.balls_INFO_RULES_FROM_REFERAL_ORDER"] = "Бонусы за заказ реферала";
$MESS["logictim.balls_INFO_RULES_PAY_BONUS"] = "Оплата заказа бонусами";
$MESS["logictim.balls_INFO_RULES_EXIT_BONUS"] = "Запрос на вывод бонусов";
$MESS["logictim.balls_INFO_RULES_CONDITIONS"] = "Условия применения правила";
$MESS["logictim.balls_INFO_RULES_CONDITIONS_PRODUCT"] = "Условия отбора товаров";

$MESS["logictim.balls_INFO_INSERT_TEMPLATE"] = "Внедрение в шаблоны";
$MESS["logictim.balls_INFO_AUTO_INSERT"] = "Автоматическая интеграция в коризну и оформление заказа";
$MESS["logictim.balls_INFO_INSERT_ASPRO_NEXT"] = "Интеграция в шаблон ASPRO - NEXT";
$MESS["logictim.balls_INFO_INSERT_CATALOG"] = "Вывод бонусов в шаблоне каталога и корзине";
$MESS["logictim.balls_INFO_INSERT_SALE_ORDER_AJAX"] = "Интеграция в кастомный шаблон оформления заказа битрикс";
$MESS["logictim.balls_INFO_INSERT_SALE_ORDER_AJAX_OLD"] = "Интеграция в старый шаблон оформления заказа";
?>
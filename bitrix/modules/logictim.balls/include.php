<?php
//CModule::IncludeModule("logictim.balls");
global $DBType;

$arClasses=array(
	'CModuleOptionsLogictimBonus'=>'classes/module-options/CModuleOptions.php',
	
	'cLTBBeforeBuferContent'=>'classes/general/BeforeBuferContent.php',
	
    'cUpdateElement'=>'classes/general/cUpdateElement.php',
	'cLBBeforeElementAdd'=>'classes/general/cBeforeElementAdd.php',
	'cAddElement'=>'classes/general/cAddElement.php',
	'cBeforeIBlockElementDelete'=>'classes/general/cBeforeIBlockElementDelete.php',
	'cBeforeUserUpdate'=>'classes/general/cBeforeUserUpdate.php',
	
	'cLogictimBlogComment'=>'classes/general/cLogictimBlogComment.php',
	'cLogictimBlogPost'=>'classes/general/cLogictimBlogPost.php',
	
	'cLogictimForumMessage'=>'classes/general/cLogictimForumMessage.php',

	'cAfterUserRegister'=>'classes/general/cAfterUserRegister.php',
	'cBonusFromRegister'=>'classes/helpers/cBonusFromRegister.php',
	'cBonusFromBirthday'=>'classes/helpers/cBonusFromBirthday.php',
	'cBonusDeactivateFromDate'=>'classes/helpers/cBonusDeactivateFromDate.php',
	'cBonusActivateFromDate'=>'classes/helpers/cBonusActivateFromDate.php',
	'cSaleOrderBeforeSaved'=>'classes/module_sale_16/cSaleOrderBeforeSaved.php',
	'cSaleOrderSaved'=>'classes/module_sale_16/cSaleOrderSaved.php',
	'cSaleStatusOrderChange'=>'classes/module_sale_16/cSaleStatusOrderChange.php',
	'BonusFromOrderAdd'=>'classes/module_sale_16/addBonus/BonusFromOrderAdd.php',
	'cSaleOrderPaid'=>'classes/module_sale_16/cSaleOrderPaid.php',
	'cSalePaymentPaid' => 'classes/module_sale_16/cSalePaymentPaid.php',
	'cChangePaid'=>'classes/module_sale_16/changePaid/cChangePaid.php',
	'cLogictimBeforeUserAccountUpdate'=>'classes/module_sale_16/cBeforeUserAccountUpdate.php',
	'cLTSaleOrderDeleted'=>'classes/module_sale_16/cSaleOrderDeleted.php',
	'cLTSaleComponentOrderOneStepComplete'=>'classes/module_sale_16/cSaleComponentOrderOneStepComplete.php',
	'cLTSaleOrderCanceled'=>'classes/module_sale_16/cSaleOrderCanceled.php',
	
	'cSaleOrderAjax'=>'classes/module_sale_16/cSaleOrderAjax.php',
	'cLTBOnSaleComponentOrderCreated'=>'classes/module_sale_16/cSaleComponentOrderCreated.php',
	
	'cHelper'=>'classes/module_sale_16/cHelper.php',
	'cHelperCalc'=>'classes/module_sale_16/cHelperCalc.php',
	'logictimBonusApi'=>'classes/module_sale_16/logictimBonusApi.php',
	
	'cRefLink'=>'classes/module_sale_16/cRefLink.php',
	'cBonusFromReferal'=>'classes/module_sale_16/addBonus/BonusFromReferal.php',
	'cLTBAfterUserAdd'=>'classes/general/cAfterUserAdd.php',
	
	'LBReferalsApi' =>  'classes/module_sale_16/logictimReferalsApi.php',
	
);
CModule::AddAutoloadClasses("logictim.balls",$arClasses);

\Bitrix\Main\Loader::registerAutoLoadClasses('logictim.balls', array(
	'Logictim\Balls\AddBonus\FromOrder' => '/lib/addbonus/fromorder.php',
	'Logictim\Balls\AddBonus\FromReferalOrder' => '/lib/addbonus/fromreferalorder.php',
	'Logictim\Balls\PayBonus\OrderBeforeSaved' => '/lib/paybonus/orderbeforesaved.php',
	'Logictim\Balls\PayBonus\OrderAfterSaved' => '/lib/paybonus/orderaftersaved.php',
	'Logictim\Balls\PayBonus\ExitBonus' => '/lib/paybonus/exitbonus.php',
	'Logictim\Balls\Events\ChangeOrderStatus' => '/lib/events/changeorderstatus.php',
	'Logictim\Balls\Events\OrderBeforeSaved' => '/lib/events/orderbeforesaved.php',
	'Logictim\Balls\Events\OrderAfterSaved' => '/lib/events/orderaftersaved.php',
	'Logictim\Balls\Events\OnSaleComponentOrderCreated' => '/lib/events/onsalecomponentordercreated.php',
	'Logictim\Balls\Ajax\SaleOrderAjax' => '/lib/ajax/saleorderajax.php',
	'Logictim\Balls\Conditions\Products' => '/lib/conditions/products.php',
	'Logictim\Balls\Conditions\ProfileOrder' => '/lib/conditions/profileorder.php',
	'Logictim\Balls\Conditions\Profile' => '/lib/conditions/profile.php',
	'Logictim\Balls\Conditions\Review' => '/lib/conditions/review.php',
	'Logictim\Balls\Conditions\PayBonus' => '/lib/conditions/paybonus.php',
));
?>
BX.ready(function(){
	logictimBonusOrder();
});

function updateBonusField() {
	if(document.getElementById('paybonus_input').value == '0')
		document.getElementById('paybonus_input').value = '00';
	BX.Sale.OrderAjaxComponent.sendRequest();
}

BX.addCustomEvent('onAjaxSuccess', function() {logictimBonusOrder('ajax');});

function logictimBonusOrder(type) {
	
	if(!BX.Sale || BX.Sale.OrderAjaxComponent == undefined)
		return;
	
	var result = BX.Sale.OrderAjaxComponent.result.LOGICTIM_BONUS;
	
	if(BX('ORDER_PROP_'+result.ORDER_PROP_PAYMENT_BONUS_ID))
		return;
	/*udalyaem slujebnie polya*/
	BX.remove(BX.findParent(BX('soa-property-'+result.ORDER_PROP_PAYMENT_BONUS_ID), {'tag': 'div', 'class': 'bx-soa-customer-field'}));
	BX.remove(BX.findParent(BX('soa-property-'+result.ORDER_PROP_ADD_BONUS_ID), {'tag': 'div', 'class': 'bx-soa-customer-field'}));
	/*udalyaem slujebnie polya*/
	
	var total_block = BX.findChild(BX('bx-soa-total'), {'tag': 'div', 'class': 'bx-soa-cart-total'});
	var itog_block = BX.findChild(BX('bx-soa-total'), {'tag': 'div', 'class': 'bx-soa-cart-total-line-total'}, true);
	
	/* Dobavlyaem blok s infoy - skolko nachisleno bonusov */
	if(result.ADD_BONUS > 0) {
		BX.remove(BX('bonus_add_block'));
		var addBonusAll_block = BX.create('DIV', {attrs: {className: 'bx-soa-cart-total-line', id: 'bonus_add_block'},
					html: '<div id="bonus_add_sum"><span class="bx-soa-cart-t">'+result.TEXT_BONUS_BALLS+'</span><span class="bx-soa-cart-d">'+result.ADD_BONUS+'</span></div>'
			});
		BX.insertAfter(addBonusAll_block, itog_block);
	}
	/* Dobavlyaem blok s infoy - skolko nachisleno bonusov */
	
	
	if(BX('bx-soa-total-mobile'))
	{
		var total_block_mobile = BX.findChild(BX('bx-soa-total-mobile'), {'tag': 'div', 'class': 'bx-soa-cart-total'});
		var itog_block_mobile = BX.findChild(BX('bx-soa-total-mobile'), {'tag': 'div', 'class': 'bx-soa-cart-total-line-total'}, true);
		
		/* Dobavlyaem blok s infoy - skolko nachisleno bonusov */
		if(result.ADD_BONUS > 0) {
			BX.remove(BX('bonus_add_block_mobile'));
			var addBonusAll_block_mobile = BX.create('DIV', {attrs: {className: 'bx-soa-cart-total-line', id: 'bonus_add_block_mobile'},
						html: '<div id="bonus_add_sum_mobile"><span class="bx-soa-cart-t">'+result.TEXT_BONUS_BALLS+'</span><span class="bx-soa-cart-d">'+result.ADD_BONUS+'</span></div>'
				});
			BX.insertAfter(addBonusAll_block_mobile, itog_block_mobile);
		}
		/* Dobavlyaem blok s infoy - skolko nachisleno bonusov */
	}
	
	
	
	/* Dobavlyaem blok s polem dlya oplati */
	var payment_block = BX.create('DIV', {props: {id: 'bonus_payment_block', className: 'bx-soa-section'}});
	var payment_title = BX.create('DIV', {attrs: {className: 'bx-soa-section-title-container'},
									html: '<h2 class="bx-soa-section-title col-sm-9"><span class="bx-soa-section-title-count"></span>'+result.MODULE_LANG.TEXT_BONUS_FOR_PAYMENT+'</h2>'
								});
	payment_block.appendChild(payment_title);
	var payment_block_content = BX.create('DIV', {props: {className: 'bx-soa-section-content'}});
	payment_block.appendChild(payment_block_content);
	
	
	
	var comment_block = BX.create('DIV', {
											props: {className: 'bonus_comment'},
											children: [
														BX.create('strong', {text: result.MODULE_LANG.HAVE_BONUS_TEXT + ' ' + result.USER_BONUS})
														]
									});
	if(parseFloat(result.MIN_BONUS) > 0)
		comment_block.appendChild(BX.create('span', {html: '<br />'+result.MODULE_LANG.MIN_BONUS_TEXT}));
	if(parseFloat(result.MAX_BONUS) > 0)
		comment_block.appendChild(BX.create('span', {html: '<br />'+result.MODULE_LANG.MAX_BONUS_TEXT}));
	
		comment_block.appendChild(BX.create('div', {html: '<br />'}));
	if(parseFloat(result.USER_BONUS) > 0)
		payment_block_content.appendChild(comment_block);
	
	
	var pay_field_block = BX.create('DIV', {props: {id: 'bonus_payfield_block'}, children: [BX.create('strong', {text: result.MODULE_LANG.PAY_BONUS_TEXT})]});
	
	
	if(type == 'ajax')
	{
		var logictimPayBonus = result.PAY_BONUS;
		if(result.INPUT_BONUS == '-' || result.INPUT_BONUS == '00')
			var logictimPayBonus = '0';
	}
	else
		var logictimPayBonus = result.PAY_BONUS;
	
	
	var input_field_block = BX.create('DIV', {props: {className: 'bx-soa-coupon-input'}});
	var input_field = BX.create('input', {
      attrs: {
		  type: 'text',
		  id: 'paybonus_input',
		  className: 'form-control bx-ios-fix',
         onchange: 'updateBonusField();',
		 value: logictimPayBonus,
		 name: 'ORDER_PROP_'+result.ORDER_PROP_PAYMENT_BONUS_ID
      },
   });
    input_field_block.appendChild(input_field);
	pay_field_block.appendChild(input_field_block);
	payment_block_content.appendChild(pay_field_block);
	
	BX.remove(BX('bonus_payment_block'));
	
	//if(parseFloat(result.PAY_BONUS) > 0 || type == 'ajax' && result.PAY_BONUS != undefined)
	if(parseFloat(result.USER_BONUS) > 0 && parseFloat(result.USER_BONUS) >= parseFloat(result.MIN_BONUS) && parseFloat(result.MAX_BONUS) > 0)
	{
		//$(payment_block).insertAfter($(".bx-soa-section.bx-active:last")); metod jquery
		var last_block = document.querySelectorAll('.bx-soa-section.bx-active');
		last_block = last_block[last_block.length -1];
		BX.insertAfter(payment_block, last_block);
	}
		
	
	
	//BX.insertAfter(payment_block, BX('bx-soa-orderSave'));
	/*if(BX('bx-soa-paysystem') && BX.isNodeHidden(BX('bx-soa-paysystem')) != true)
		$(payment_block).insertAfter($("#bx-soa-paysystem"));
	else*/
		
	
	
	/* Dobavlyaem blok s polem dlya oplati */
	
	
	/* Dobavlyaem infu - ckolkimi bonusami oplatili zakaz */
	if(logictimPayBonus > 0 && result.DISCOUNT_TO_PRODUCTS != 'B') {
		BX.remove(BX('bonus_pay_block'));
		var addPay_info_block = BX.create('DIV', {attrs: {className: 'bx-soa-cart-total-line', id: 'bonus_pay_block'},
					html: '<div id="bonus_pay_sum"><span class="bx-soa-cart-t">'+result.TEXT_BONUS_PAY+'</span><span class="bx-soa-cart-d">'+logictimPayBonus+'</span></div>'
			});
		var before_itog_block = BX.findPreviousSibling(itog_block, {'tag': 'div', 'class': 'bx-soa-cart-total-line'});
		BX.insertAfter(addPay_info_block, before_itog_block);
		
		//for mobile
		BX.remove(BX('bonus_pay_block_mobile'));
		var addPay_info_block_mobile = BX.create('DIV', {attrs: {className: 'bx-soa-cart-total-line', id: 'bonus_pay_block_mobile'},
					html: '<div id="bonus_pay_sum_mobile"><span class="bx-soa-cart-t">'+result.TEXT_BONUS_PAY+'</span><span class="bx-soa-cart-d">'+logictimPayBonus+'</span></div>'
			});
		var before_itog_block_mobile = BX.findPreviousSibling(itog_block_mobile, {'tag': 'div', 'class': 'bx-soa-cart-total-line'});
		BX.insertAfter(addPay_info_block_mobile, before_itog_block_mobile);
	}
	/* Dobavlyaem infu - ckolkimi bonusami oplatili zakaz */
	
   //console.log(result);
}
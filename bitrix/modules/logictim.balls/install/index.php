<?
IncludeModuleLangFile(__FILE__);
Class logictim_balls extends CModule
{
	const MODULE_ID = 'logictim.balls';
	var $MODULE_ID = 'logictim.balls'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("logictim.balls_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("logictim.balls_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("logictim.balls_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("logictim.balls_PARTNER_URI");
	}
	
	function InstallDB()
	{
		global $DB;
		$DB->Query('CREATE TABLE IF NOT EXISTS logictim_balls_users (
				id INT NOT NULL AUTO_INCREMENT,
				active VARCHAR(1) DEFAULT NULL,
				user_id INT DEFAULT NULL,
				change_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				partner_coupon VARCHAR(50) DEFAULT NULL,
				PRIMARY KEY (id)
			);');
			
		$DB->Query('CREATE TABLE IF NOT EXISTS logictim_balls_profiles (
				id INT NOT NULL AUTO_INCREMENT,
				sort INT NOT NULL,
				active VARCHAR(1) DEFAULT NULL,
				name VARCHAR(50) DEFAULT NULL,
				active_from DATETIME DEFAULT NULL,
				active_to DATETIME DEFAULT NULL,
				add_bonus VARCHAR(50) DEFAULT NULL,
				active_after_period INT DEFAULT NULL,
				active_after_type TEXT DEFAULT NULL,
				deactive_after_period INT DEFAULT NULL,
				deactive_after_type TEXT DEFAULT NULL,
				type VARCHAR(50) DEFAULT NULL,
				change_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				conditions TEXT NOT NULL,
				profile_conditions TEXT NOT NULL,
				other_conditions TEXT NOT NULL,
				PRIMARY KEY (id)
			);');
			
		$DB->Query('CREATE TABLE IF NOT EXISTS logictim_balls_exit_bonus (
				id INT NOT NULL AUTO_INCREMENT,
				name VARCHAR(50) DEFAULT NULL,
				sum VARCHAR(50) DEFAULT NULL,
				status VARCHAR(50) DEFAULT NULL,
				date_insert DATETIME DEFAULT NULL,
				date_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				date_close DATETIME DEFAULT NULL,
				user INT NOT NULL,
				insert_user INT NOT NULL,
				comment_user TEXT NOT NULL,
				comment_admin TEXT NOT NULL,
				operation_output INT DEFAULT NULL,
				operation_refund INT DEFAULT NULL,
				PRIMARY KEY (id)
			);');
	}
	function UnInstallDB()
	{
		global $DB;
		$DB->Query('DROP TABLE IF EXISTS logictim_balls_users;');
		$DB->Query('DROP TABLE IF EXISTS logictim_balls_profiles;');
		$DB->Query('DROP TABLE IF EXISTS logictim_balls_exit_bonus;');
	}
	
	 function DoInstall()
    {
		$this->InstallDB();
		
        global $DOCUMENT_ROOT, $APPLICATION;
		//Add order props
		include(dirname(__FILE__)."/include/order_props.php");
		
		//Add User Prop Bonus Count
		include(dirname(__FILE__)."/include/user_props.php");
		
		//Add Prop Bonus to infobloks
		//include(dirname(__FILE__)."/include/catalog_props.php");
		
		//Install iblok type, iblok, iblok propertys
		include(dirname(__FILE__)."/include/iblok_install.php");
		
		//Install Pay System bonus
		include(dirname(__FILE__)."/include/paysystem_install.php");
		
		//Install MAIL EVENTS AND TEMPLATES
		include(dirname(__FILE__)."/include/mail_events_install.php");
		
		//Install PROFILES
		include(dirname(__FILE__)."/include/profiles_install.php");
		
		//Install PUBLIC
		include(dirname(__FILE__)."/include/public_install.php");
		
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", true, true);

        // Install events
		RegisterModuleDependences("iblock","OnBeforeIBlockElementAdd","logictim.balls","cLBBeforeElementAdd","OnBeforeIBlockElementAdd"); //add element
		RegisterModuleDependences("iblock","OnAfterIBlockElementAdd","logictim.balls","cAddElement","OnAfterIBlockElementAdd"); //add element
        RegisterModuleDependences("iblock","OnBeforeIBlockElementUpdate","logictim.balls","cUpdateElement","onBeforeElementUpdateHandler"); //update element
		RegisterModuleDependences("iblock","OnBeforeIBlockElementDelete","logictim.balls","cBeforeIBlockElementDelete","BeforeIBlockElementDelete"); //before element delete
		
		RegisterModuleDependences("main","OnBeforeUserUpdate","logictim.balls","cBeforeUserUpdate","BeforeUserUpdate"); //before update user
		RegisterModuleDependences("main","OnAfterUserRegister","logictim.balls","cAfterUserRegister","AfterUserRegister"); //After user register
		RegisterModuleDependences("main","OnBeforeEndBufferContent","logictim.balls","cRefLink","RefLink"); //Dlya otlova referalov
		
		RegisterModuleDependences("blog","OnCommentAdd","logictim.balls","cLogictimBlogComment","OnCommentAdd"); //add comment
		RegisterModuleDependences("blog","OnCommentUpdate","logictim.balls","cLogictimBlogComment","OnCommentUpdate"); //update element
		RegisterModuleDependences("blog","OnPostAdd","logictim.balls","cLogictimBlogPost","OnPostAdd"); //add post
		RegisterModuleDependences("blog","OnPostUpdate","logictim.balls","cLogictimBlogPost","OnPostUpdate"); //update post
		
		RegisterModuleDependences("forum","onAfterMessageAdd","logictim.balls","cLogictimForumMessage","AfterMessageAdd"); //add message to forum
		RegisterModuleDependences("forum","onAfterMessageUpdate","logictim.balls","cLogictimForumMessage","AfterMessageUpdate"); //update message to forum
		
		
		// Install events for module sale up 16.0
		RegisterModuleDependences("sale","OnSaleOrderPaid","logictim.balls","cSaleOrderPaid","SaleOrderPaid"); //Change order paid
		RegisterModuleDependences("sale","OnSaleOrderCanceled","logictim.balls","cLTSaleOrderCanceled","CangeOrderCanceled"); //Change order canceled
		RegisterModuleDependences('sale', 'OnSaleOrderDeleted', 'logictim.balls', 'cLTSaleOrderDeleted', 'AfterOrderDeleted'); //After order deleted
		RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepComplete', 'logictim.balls', 'cLTSaleComponentOrderOneStepComplete', 'AddOrderSaleOrderAjax'); //sale.order.ajax - after order add
		RegisterModuleDependences("main","OnAfterUserAdd","logictim.balls","cLTBAfterUserAdd","AfterUserAdd");
		
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->registerEventHandler('sale', 'OnBeforeSalePaymentSetField', 'logictim.balls', 'cChangePaid', 'ChangePaid');
		$eventManager->registerEventHandler('sale', 'OnSaleComponentOrderOneStepProcess', 'logictim.balls', 'cSaleOrderAjax', 'SaleOrderAjaxEvent');
		$eventManager->registerEventHandler('sale', 'OnBeforeUserAccountUpdate', 'logictim.balls', 'cLogictimBeforeUserAccountUpdate', 'BeforeUserAccountUpdate');
		$eventManager->registerEventHandler('main', 'OnBeforeEndBufferContent', 'logictim.balls', 'cLTBBeforeBuferContent', 'BeforeEndBufferContent');
		
		//VERSION 4
		RegisterModuleDependences("sale","OnSaleStatusOrderChange","logictim.balls", "\Logictim\Balls\Events\ChangeOrderStatus", "ChangeOrderStatus"); //Change order status
		RegisterModuleDependences("sale","OnSaleOrderBeforeSaved","logictim.balls","\Logictim\Balls\Events\OrderBeforeSaved","OrderBeforeSaved"); //Sale Order Before Saved
		$eventManager->registerEventHandler('sale', 'OnSaleComponentOrderCreated', 'logictim.balls', '\Logictim\Balls\Events\OnSaleComponentOrderCreated', 'OnSaleComponentOrderCreated');
		RegisterModuleDependences("sale","OnSaleOrderSaved","logictim.balls","\Logictim\Balls\Events\OrderAfterSaved","OrderAfterSaved", 1); //Sale Order After Saved
		
		//Set default options
		include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/logictim.balls/options.php');
		foreach($arOptions as $optionName => $arOption)
			{
				$val=$arOption["DEFAULT"];
				//if ($arOption["TYPE"] == "STRING" || $arOption["TYPE"] == "INT") {
					COption::SetOptionString($this->MODULE_ID, $optionName, $val, $arOption["TITLE"]);
				//}
			}
		
			
			
        RegisterModule($this->MODULE_ID);
		
		//INSTALL AGENTS
		CModule::IncludeModule("main");
		CAgent::AddAgent(
			"cBonusFromBirthday::BonusFromBirthday();",
			"logictim.balls",
			"N",
			86400,
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			"Y",
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			10);
		CAgent::AddAgent(
			"cBonusDeactivateFromDate::BonusDeactivateFromDate();",
			"logictim.balls",
			"N",
			3600,
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			"Y",
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			10);
		CAgent::AddAgent(
			"cBonusDeactivateFromDate::BonusWarningFromDate();",
			"logictim.balls",
			"N",
			86400,
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			"Y",
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			10);
		CAgent::AddAgent(
			"cBonusActivateFromDate::BonusActivateFromDate();",
			"logictim.balls",
			"N",
			3600,
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			"Y",
			ConvertTimeStamp(time() + 86400,'SHORT').' 00:00:01',
			10);

		
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
		
		$this->UnInstallDB();
		
		//Delete Order Props and Groups
		include(dirname(__FILE__)."/include/del_order_props.php");
		//Delette User Prop
		include(dirname(__FILE__)."/include/del_user_props.php");
		//Delette Prop Bonus to infobloks
		include(dirname(__FILE__)."/include/del_catalog_props.php");
		//Delette Pay System bonus
		include(dirname(__FILE__)."/include/del_paysystem.php");
		//Delette MAIL EVENTS AND TEMPLATES
		include(dirname(__FILE__)."/include/del_mail_events.php");

		UnRegisterModuleDependences("iblock","OnBeforeIBlockElementAdd","logictim.balls","cLBBeforeElementAdd","OnBeforeIBlockElementAdd"); //add element
		UnRegisterModuleDependences("iblock","OnAfterIBlockElementAdd","logictim.balls","cAddElement","OnAfterIBlockElementAdd"); //add element
		UnRegisterModuleDependences("iblock","OnBeforeIBlockElementUpdate","logictim.balls","cUpdateElement","onBeforeElementUpdateHandler"); //update element
		UnRegisterModuleDependences("iblock","OnBeforeIBlockElementDelete","logictim.balls","cBeforeIBlockElementDelete","BeforeIBlockElementDelete"); //before element delete
		
		UnRegisterModuleDependences("main","OnBeforeUserUpdate","logictim.balls","cBeforeUserUpdate","BeforeUserUpdate"); //before update user
		UnRegisterModuleDependences("main","OnAfterUserRegister","logictim.balls","cAfterUserRegister","AfterUserRegister"); //After user register
		UnRegisterModuleDependences("main","OnBeforeEndBufferContent","logictim.balls","cRefLink","RefLink"); //Dlya otlova referalov
		
		UnRegisterModuleDependences("blog","OnCommentAdd","logictim.balls","cLogictimBlogComment","OnCommentAdd"); //add comment
		UnRegisterModuleDependences("blog","OnCommentUpdate","logictim.balls","cLogictimBlogComment","OnCommentUpdate"); //update element
		UnRegisterModuleDependences("blog","OnPostAdd","logictim.balls","cLogictimBlogPost","OnPostAdd"); //add post
		UnRegisterModuleDependences("blog","OnPostUpdate","logictim.balls","cLogictimBlogPost","OnPostUpdate"); //update post
		
		UnRegisterModuleDependences("forum","onAfterMessageAdd","logictim.balls","cLogictimForumMessage","AfterMessageAdd"); //add message to forum
		UnRegisterModuleDependences("forum","onAfterMessageUpdate","logictim.balls","cLogictimForumMessage","AfterMessageUpdate"); //update message to forum
		
		//for module sale up 16.0
		UnRegisterModuleDependences("sale","OnSaleOrderPaid","logictim.balls","cSaleOrderPaid","SaleOrderPaid"); //Change order paid
		UnRegisterModuleDependences("sale","OnSaleOrderCanceled","logictim.balls","cLTSaleOrderCanceled","CangeOrderCanceled"); //Change order canceled
		UnRegisterModuleDependences('sale', 'OnSaleOrderDeleted', 'logictim.balls', 'cLTSaleOrderDeleted', 'AfterOrderDeleted');
		UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepComplete', 'logictim.balls', 'cLTSaleComponentOrderOneStepComplete', 'AddOrderSaleOrderAjax');
		
		UnRegisterModuleDependences("main","OnAfterUserAdd","logictim.balls","cLTBAfterUserAdd","AfterUserAdd");
		
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->unRegisterEventHandler('sale', 'OnBeforeSalePaymentSetField', 'logictim.balls', 'cChangePaid', 'ChangePaid');
		$eventManager->unRegisterEventHandler('sale', 'OnSaleComponentOrderOneStepProcess', 'logictim.balls', 'cSaleOrderAjax', 'SaleOrderAjaxEvent');
		$eventManager->unRegisterEventHandler('sale', 'OnBeforeUserAccountUpdate', 'logictim.balls', 'cLogictimBeforeUserAccountUpdate', 'BeforeUserAccountUpdate');
		
		$eventManager->unRegisterEventHandler('main', 'OnBeforeEndBufferContent', 'logictim.balls', 'cLTBBeforeBuferContent', 'BeforeEndBufferContent');
		
		//VERSION 4
		UnRegisterModuleDependences("sale","OnSaleStatusOrderChange","logictim.balls", "\Logictim\Balls\Events\ChangeOrderStatus", "ChangeOrderStatus"); //Change order status
		UnRegisterModuleDependences("sale","OnSaleOrderBeforeSaved","logictim.balls","\Logictim\Balls\Events\OrderBeforeSaved","OrderBeforeSaved"); //Sale Order Before Saved
		$eventManager->unRegisterEventHandler('sale', 'OnSaleComponentOrderCreated', 'logictim.balls', '\Logictim\Balls\Events\OnSaleComponentOrderCreated', 'OnSaleComponentOrderCreated');
		UnRegisterModuleDependences("sale","OnSaleOrderSaved","logictim.balls","\Logictim\Balls\Events\OrderAfterSaved","OrderAfterSaved"); //Sale Order After Saved
		
		DeleteDirFilesEx("/bitrix/js/logictim.balls");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/install/themes/.default", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default", true, true);
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default/icons/logictim.balls");
		
		DeleteDirFilesEx("/personal_bonus");
		
		COption::RemoveOption("logictim.balls");
		
		//Delette iblok type, iblok, iblok propertys
		include(dirname(__FILE__)."/include/del_iblok.php");
		
		//Delette agents
		CModule::IncludeModule("main");
		CAgent::RemoveModuleAgents("logictim.balls");
		
        UnRegisterModule($this->MODULE_ID);
        return true;
    }


}
<?
$MESS["SALE_PROPERTIES_RECALCULATE_BASKET"] = "Eigenschaften, die eine Neuberechnung des Warenkorbs beeinflussen";
$MESS["CP_SBB_TPL_THEME_SITE"] = "Farbschema der Website benutzen (fur bitrix.eshop)";
$MESS["CP_SBB_TPL_THEME_BLUE"] = "Blau (standardma?ig)";
$MESS["CP_SBB_TPL_THEME_GREEN"] = "Grun";
$MESS["CP_SBB_TPL_THEME_RED"] = "rot";
$MESS["CP_SBB_TPL_THEME_WOOD"] = "Holz";
$MESS["CP_SBB_TPL_THEME_YELLOW"] = "Gelb";
$MESS["CP_SBB_TPL_THEME_BLACK"] = "dunkel";
$MESS["CP_SBB_TPL_TEMPLATE_THEME"] = "Farbschema";
$MESS["CP_SBB_TPL_USE_ENHANCED_ECOMMERCE"] = "E-commerce Daten an Google senden";
$MESS["USE_ENHANCED_ECOMMERCE_TIP"] = "Die Optionen von Google Analytics Enhanced Ecommerce mussen dafur konfiguriert werden";
$MESS["CP_SBB_TPL_DATA_LAYER_NAME"] = "Name des Datencontainers";
$MESS["CP_SBB_TPL_BRAND_PROPERTY"] = "Eigenschaft der Handelsmarken";
?>
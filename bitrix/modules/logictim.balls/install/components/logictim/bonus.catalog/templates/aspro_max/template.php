<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>


<? if($arParams["AJAX"] == 'Y'):?>

	<script type="text/javascript">
		BX.ready(function(){
			var arBonusItems_<?=$arParams["RAND"]?> = <?=CUtil::PhpToJSObject($arResult, false, true)?>;
			var arBonus_<?=$arParams["RAND"]?> = null;
			BX.ajax({
				url: '<?=$componentPath?>/ajax.php',
				method: 'POST',
				data: arBonusItems_<?=$arParams["RAND"]?>,
				dataType: 'json',
				onsuccess: function(result) {
					arBonus_<?=$arParams["RAND"]?> = result;
					for(id in result.ITEMS)
					{
						var item = result.ITEMS[id];
						//if(BX('lb_ajax_'+id) && item.ADD_BONUS > 0)
							//BX.adjust(BX('lb_ajax_'+id), {text: '+ '+item.ADD_BONUS+' '+result.TEXT.TEXT_BONUS_FOR_ITEM});
							
						if($("#lb_ajax_"+id) && item.VIEW_BONUS > 0)
				 			$("#lb_ajax_"+id).text('+ '+item.ADD_BONUS+' '+result.TEXT.TEXT_BONUS_FOR_ITEM);
						if($(".lb_ajax_"+id) && item.VIEW_BONUS > 0)
				 			$(".lb_ajax_"+id).text('+ '+item.ADD_BONUS+' '+result.TEXT.TEXT_BONUS_FOR_ITEM);
					}
				}
			});
			
			$(".cost.prices.clearfix").bind("DOMSubtreeModified",function(){
				var prodict_id = $(this).closest('.item_block').find(".lb_bonus").attr('data-item');
				var offer_id = $(this).closest('.item_block').find(".counter_block").attr('data-item');
				
				if(arBonus_<?=$arParams["RAND"]?> != null && arBonus_<?=$arParams["RAND"]?>['ITEMS'][offer_id])
				{
					var offer_item = arBonus_<?=$arParams["RAND"]?>['ITEMS'][offer_id];
					if(offer_item.VIEW_BONUS > 0)
					{
						//BX.adjust(BX('lb_ajax_'+prodict_id), {text: '+'+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>'});
						if($("#lb_ajax_"+prodict_id))
				 			$("#lb_ajax_"+prodict_id).text('+ '+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
						if($(".lb_ajax_"+prodict_id))
				 			$(".lb_ajax_"+prodict_id).text('+ '+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
							
					}
					else
					{
						//BX.adjust(BX('lb_ajax_'+prodict_id), {text: ''});
						if($("#lb_ajax_"+prodict_id))
							$("#lb_ajax_"+prodict_id).text('');
						if($(".lb_ajax_"+prodict_id))
							$(".lb_ajax_"+prodict_id).text('');
					}
				}
				
			});
		});
	</script>
    
<? else:?>

	<script>
		BX.ready(function(){
            var arBonus_<?=$arParams["RAND"]?> = <?=CUtil::PhpToJSObject($arResult["ITEMS_BONUS"], false, true)?>;
			
			for(id in arBonus_<?=$arParams["RAND"]?>) {
                var item = arBonus_<?=$arParams["RAND"]?>[id];
                
				 if($("#lb_ajax_"+id) && item.VIEW_BONUS > 0)
				 	$("#lb_ajax_"+id).text('+ '+item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
				 if($(".lb_ajax_"+id) && item.VIEW_BONUS > 0)	
					$(".lb_ajax_"+id).text('+ '+item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
            }
			
			$(".cost.prices").bind("DOMSubtreeModified",function(){
				
				//for catalog.section.list
				if($(this).closest('.item').find(".lb_bonus").attr('data-item') > 0)
				{
					var prodict_id = $(this).closest('.item').find(".lb_bonus").attr('data-item');
					if($(this).closest('.item').find(".counter_block").attr('data-item') > 0)
						var offer_id = $(this).closest('.item').find(".counter_block").attr('data-item');
					else
						var offer_id = $(this).closest('.item').find(".to-cart").attr('data-item');
				}
				
				
				//for catalog.element
				if($(this).closest('.product-main').find(".lb_bonus").attr('data-item') > 0)
				{
					var prodict_id = $(this).closest('.product-main').find(".lb_bonus").attr('data-item');
					
					if($(this).closest('.product-main').find(".counter_block").attr('data-item') > 0)
						var offer_id = $(this).closest('.product-main').find(".counter_block").attr('data-item');
					else
						var offer_id = $(this).closest('.product-main').find(".to-cart").attr('data-item');
				}
					
				
				if(arBonus_<?=$arParams["RAND"]?>[offer_id])
				{
					var offer_item = arBonus_<?=$arParams["RAND"]?>[offer_id];
					if(offer_item.VIEW_BONUS > 0)
					{
						//BX.adjust(BX('lb_ajax_'+prodict_id), {text: '+'+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>'});
						
						if($("#lb_ajax_"+prodict_id))
				 			$("#lb_ajax_"+prodict_id).text('+ '+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
						if($(".lb_ajax_"+prodict_id))
				 			$(".lb_ajax_"+prodict_id).text('+ '+offer_item.VIEW_BONUS+' '+'<?=COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", '')?>');
						
					}
					else
					{
						//BX.adjust(BX('lb_ajax_'+prodict_id), {text: ''});
						if($("#lb_ajax_"+prodict_id))
							$("#lb_ajax_"+prodict_id).text('');
						if($(".lb_ajax_"+prodict_id))
							$(".lb_ajax_"+prodict_id).text('');
					}
				}
			});
        });
    </script>
    

<? endif;?>


<? //echo '<pre>'; print_r($arResult); echo '</pre>';?>
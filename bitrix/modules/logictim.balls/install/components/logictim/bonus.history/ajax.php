<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('logictim.balls');
CModule::IncludeModule('sale');

CUtil::JSPostUnescape();


$arRes["POST"] = $_POST;
if($_POST["ACTION"] == 'ADD_COUPON'):
	$partnerId = $USER->GetID();
	$arCoupon = LBReferalsApi::AddPartnerCoupon($partnerId);
	$arRes["COUPON"] = $arCoupon;
endif;

if($_POST["ACTION"] == 'EXIT_BONUS'):
	$userId = $USER->GetID();
	$result = \Logictim\Balls\PayBonus\ExitBonus::QueryExitBonus($userId, $_POST["SUM"]);
	$arRes["RESULT"] = $result;
endif;






$APPLICATION->RestartBuffer();
header('Content-Type: application/json; charset='.LANG_CHARSET);
echo \Bitrix\Main\Web\Json::encode($arRes, JSON_BIGINT_AS_STRING);
die();

?>

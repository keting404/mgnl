<?
IncludeModuleLangFile(__FILE__);
if (CModule::IncludeModule("main")):

	$oUserTypeEntity    = new CUserTypeEntity();
		$aUserFields    = array(
		'ENTITY_ID'         => 'USER',
		'FIELD_NAME'        => 'UF_LOGICTIM_BONUS',
		'USER_TYPE_ID'      => 'double',
		'XML_ID'            => '',
		'SORT'              => 500,
		'MULTIPLE'          => 'N',
		'MANDATORY'         => 'N',
		'SHOW_FILTER'       => 'I',
		'SHOW_IN_LIST'      => '',
		'EDIT_IN_LIST'      => '',
		'IS_SEARCHABLE'     => 'N',
		'SETTINGS'          => array(
			'DEFAULT_VALUE' => '0',
			'SIZE'          => '20',
			'PRECISION'     => '2',
			'MIN_VALUE'    => '0',
			'MAX_VALUE'    => '0',
		),
			'EDIT_FORM_LABEL'   => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_BONUS_NAME"),
				'en'    => 'Bonus count', 
			),
			'LIST_COLUMN_LABEL' => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_BONUS_NAME"),
				'en'    => 'Bonus count', 
			),
			'LIST_FILTER_LABEL' => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_BONUS_NAME"),
		'en'    => 'Bonus count',
		),
			'ERROR_MESSAGE'     => array(
				'ru'    => GetMessage("logictim.balls_ORDER_PROP_PAYMENT_BONUS_ERROR"),
				'en'    => 'An error in completing the user field',
		),
			'HELP_MESSAGE'      => array(
				'ru'    => '',
		'en'    => '',
			),
		);
	$iUserFieldId   = $oUserTypeEntity->Add( $aUserFields ); // int
	
	
	//SUBSCTIBE
	$oUserTypeEntity    = new CUserTypeEntity();
		$aUserFields    = array(
		'ENTITY_ID'         => 'USER',
		'FIELD_NAME'        => 'UF_LGB_SUBSCRIBE',
		'USER_TYPE_ID'      => 'boolean',
		'XML_ID'            => '',
		'SORT'              => 500,
		'MULTIPLE'          => 'N',
		'MANDATORY'         => 'N',
		'SHOW_FILTER'       => 'I',
		'SHOW_IN_LIST'      => '',
		'EDIT_IN_LIST'      => '',
		'IS_SEARCHABLE'     => 'N',
		'SETTINGS'          => array(
			'DEFAULT_VALUE' => '1',
		),
			'EDIT_FORM_LABEL'   => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_UF_LGB_SUBSCRIBE"),
				'en'    => 'To receive alerts on the combustion of bonuses', 
			),
			'LIST_COLUMN_LABEL' => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_UF_LGB_SUBSCRIBE"),
				'en'    => 'To receive alerts on the combustion of bonuses', 
			),
			'LIST_FILTER_LABEL' => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_UF_LGB_SUBSCRIBE"),
		'en'    => 'To receive alerts on the combustion of bonuses',
		),
			'ERROR_MESSAGE'     => array(
				'ru'    => GetMessage("logictim.balls_USER_PROP_UF_LGB_SUBSCRIBE_ERROR"),
				'en'    => 'An error in completing the user field',
		),
			'HELP_MESSAGE'      => array(
				'ru'    => '',
		'en'    => '',
			),
		);
	$iUserFieldId   = $oUserTypeEntity->Add( $aUserFields ); // int
	
	
	global $USER;
	global $DB, $USER_FIELD_MANAGER;
	$rsUsers = CUser::GetList(($by="id"), ($order="desc"), array());
	while($arItem = $rsUsers->GetNext()) 
	{
		$USER_FIELD_MANAGER->Update("USER", $arItem['ID'], array("UF_LGB_SUBSCRIBE" => 1));
	}
	
	
	
endif;
				
				
				
				
				
	
?>
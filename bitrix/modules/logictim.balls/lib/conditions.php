<?
namespace Logictim\Balls;

class Conditions
{
	function SaveConditions($requestConditions)
	{
		$arIblocks = array();
		foreach($requestConditions as $arCondition):
			if(strpos($arCondition["controlId"], 'CondIBProp') !== false)
			{
				$arExp = explode(':', $arCondition["controlId"]);
				$arIblocks[] = $arExp[1];
			}
		endforeach;
		
		$arIBProps = array();
		foreach($arIblocks as $iblockId):
			$dbIbProps = \CIBlock::GetProperties($iblockId);
			while($dbProp = $dbIbProps->Fetch())
			{
				$arIBProps[$dbProp["ID"]] = $dbProp;
			}
		endforeach;
		
		$arConditions = array();		
		$arLevels = array(0=>0, 1=>0, 2=>0);
		foreach($requestConditions as $key => $arCond):
			$arKey = explode('__', $key);
			$level = count($arKey)-1;
			
			if($level < $lastLevel)
			{
				foreach($arLevels as $keyL => $ValL):
					if($keyL > $level)
						$arLevels[$keyL] = 0;
				endforeach;
			}
			
			
			$id = $arLevels[$level];
			
			$arBlock = array('id'=>$id, 'controlId'=>$arCond['controlId'], 'values'=>array());
			foreach($arCond as $keyVal => $val):
				if($keyVal == 'controlId')
					continue;
					
				if(is_array($val))
				{
					$arVal = $val;
					$val = array();
					foreach($arVal as $valAr):
						if($valAr != '')
							$val[] = $valAr;
					endforeach;
				}
				
				if($keyVal == 'value')
				{
					if(strpos($arCond['controlId'], 'CondIBProp') !== false)
					{
						$arExp = explode(':', $arCondition["controlId"]);
						$propertyId = $arExp[2];
						if($arIBProps[$propertyId]["PROPERTY_TYPE"] == 'N')
						{
							$val =str_replace(',', '.', $val);
							$val =(float)$val;
							$val =(string)$val;
						}
					}
					
					if($arCond['controlId'] == 'price' || $arCond['controlId'] == 'cartSum'  || $arCond['controlId'] == 'orderSum')
					{
						$val =str_replace(',', '.', $val);
						$val =(float)$val;
						$val =(string)$val;
					}
				}
				elseif($keyVal['controlId'] == 'bonus' || $keyVal['controlId'] == 'ordersSum')
				{
					$val =str_replace(',', '.', $val);
					$val =(float)$val;
					$val =(string)$val;
				}
				
	
				$arBlock['values'][$keyVal] = $val;
			endforeach;
			
			if(is_array($arBlock['values']['value']) && empty($arBlock['values']['value']) && $level > 0)
				continue;
				
			if(!isset($arBlock['children']))
				$arBlock['children'] = array();
			
			if($level == 0)
				$arConditions = array('id'=>$id, 'controlId'=>$arCond['controlId'], 'children'=>array());
			elseif($level == 1)
				$arConditions['children'][$id] = $arBlock;
			elseif($level == 2)
				$arConditions['children'][$arLevels[$level-1]-1]['children'][$id] = $arBlock;
			
			
			$lastLevel = $level;
			$arLevels[$level] = $arLevels[$level]+1;
		endforeach;
		
		/*$f = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/admin/profiles/111.txt", "a+"); 
		fwrite($f, print_r($requestConditions,true)); 
		fwrite($f, print_r($arConditions,true)); 
		fclose($f);*/
		
		return $arConditions;
		
		//$arConditions = \Bitrix\Main\Web\Json::encode($arConditions);
	}
	
	function SetLabels($arProductConditions)
	{
		if(empty($arProductConditions["children"]))
			return $arProductConditions;
		
		$sectionsId = array();
		$elementsId = array();
		foreach($arProductConditions["children"] as $conGroup):
			if(empty($conGroup["children"]))
				continue;
			foreach($conGroup["children"] as $cond):
				if($cond["controlId"] == 'product_categoty' && $cond["values"]["value"] > 0)
					$sectionsId[] = $cond["values"]["value"];
				if($cond["controlId"] == 'product' && !empty($cond["values"]["value"]))
					$elementsId = array_merge($elementsId, $cond["values"]["value"]);
			endforeach;
		endforeach;
		$sectionsId = array_unique($sectionsId);
		$elementsId = array_unique($elementsId);
		
		if(!empty($sectionsId)):
			$dbSections = \CIBlockSection::GetList(array('id' => 'asc'), array('ID'=>$sectionsId), false, array("ID", "NAME"));
			$arSections = array();
			while($arSection = $dbSections->fetch())
			{
				$arSections[$arSection["ID"]] = $arSection;
			}
		endif;
		if(!empty($elementsId)):
			$DBproducts = \CIBlockElement::GetList(array("ID"=>"ASC"), array("ID" => $elementsId), false, false, array("ID", "NAME", "IBLOCK_ID"));
			$arElements = array();
			while($el = $DBproducts->GetNextElement())
			{
				$arProd = $el->GetFields();
				$arElements[$arProd["ID"]] = $arProd;
			}
		endif;
		
		foreach($arProductConditions["children"] as $keyGroup => $conGroup):
			if(empty($conGroup["children"]))
				continue;
			foreach($conGroup["children"] as $keyKond => $cond):
				if($cond["controlId"] == 'product_categoty' && $cond["values"]["value"] > 0)
					$arProductConditions["children"][$keyGroup]["children"][$keyKond]["labels"] = array("value" => $arSections[$cond["values"]["value"]]["NAME"]);
				if($cond["controlId"] == 'product' && !empty($cond["values"]["value"]))
				{
					foreach($cond["values"]["value"] as $val):
						$arProductConditions["children"][$keyGroup]["children"][$keyKond]["labels"]["value"][] = $arElements[$val]["NAME"];
					endforeach;
					
				}
					
			endforeach;
		endforeach;
		
		return $arProductConditions;
	}
}



?>
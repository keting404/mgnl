<?
namespace Logictim\Balls\PayBonus;

IncludeModuleLangFile(__FILE__);

use Bitrix\Sale;

class OrderBeforeSaved {
	function AddBonusPayment($order)
	{
		/*$f = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/logictim.balls/lib/paybonus/111.txt", "a+"); 
		fwrite($f, print_r($order,true)); 
		fclose($f);*/
		
		if(\COption::GetOptionString("logictim.balls", "DISCOUNT_TO_PRODUCTS", 'N') == 'B')
			return;
			
		$is_new = $order->isNew();
		$fields = $order->GetFields();
		$values = $fields->GetValues();
		$basket = $order->getBasket();
		
		//Payments
		$paymentCollection = $order->getPaymentCollection();
		$arPayments = array();
		foreach($paymentCollection as $payment):
			$paymentId = $payment->getPaymentSystemId();
			$arPayments[$paymentId] = array("ID"=>$paymentId, "SUM"=>$payment->getSum(), "NAME"=>$payment->getPaymentSystemName(), "IS_PAYED"=>$payment->isPaid());
		endforeach;
		
		//GET ORDER PROPERTIES
		$props = $order->getPropertyCollection();
		foreach($props as $prop):
			$propFields = $prop->GetFields();
			$propValues = $propFields->GetValues();
			if($propValues["CODE"] == 'LOGICTIM_PAYMENT_BONUS')
			{
				$pay_bonus = $propValues["VALUE"];
				$payBonusPropId = $propValues["ORDER_PROPS_ID"];
			}
		endforeach;
		
		$discountData = $order->getDiscount()->getApplyResult();
		$arOrderParams = array(
								"ORDER_ID" => $order->getId(),
								"ORDER_NUM" => $values["ACCOUNT_NUMBER"],
								"SITE_ID" => $order->getSiteId(),
								"USER_ID" => $order->getUserId(),
								"ORDER_SUM" => $order->getPrice(),
								"CART_SUM" => $basket->getPrice(),
								"DELIVERY_SUM" => $order->getDeliveryPrice(),
								"PERSON_TYPE_ID" => $order->getPersonTypeId(),
								"CURRENCY" => $order->getCurrency(),
								"DISCOUNT" => $order->getDiscountPrice(),
								"DISCOUNT_DATA" => $discountData["DISCOUNT_LIST"],
								"PAYMENTS" => $arPayments,
								);
		$UserBallance = \Logictim\Balls\Helpers::UserBallance($arOrderParams["USER_ID"]);
								
		if($is_new && $UserBallance > 0 && $pay_bonus > 0):
		
			//Korzina zakaza
			$arItems = array();
			foreach ($basket as $basketItem):
				$arItem = array();
				$arItem["PRODUCT_ID"] = $basketItem->getProductId();
				$arItem["NAME"] = $basketItem->getField('NAME');
				$arItem["QUANTITY"] = $basketItem->getQuantity();
				$arItem["BASE_PRICE"] = $basketItem->getField('BASE_PRICE');
				$arItem["PRICE"] = $basketItem->getPrice();
				$arItem["DISCOUNT_PRICE"] = $basketItem->getField('DISCOUNT_PRICE');
				$arItem["POSITION_FINAL_PRICE"] = $basketItem->getFinalPrice();
				$arItems[$arItem["PRODUCT_ID"]] = $arItem;
			endforeach;
			
			$arOrderParams["PAY_BONUS"] = $pay_bonus;
			$arPayBonus = \Logictim\Balls\CalcBonus::OrderBonusPayment($arItems, $arOrderParams);
			
			
			
			if($arPayBonus["PAY_BONUS"] > 0)
			{
				$minBonusSum = $arPayBonus["MIN_ORDER_PAY"];
				$maxBonusSum = $arPayBonus["MAX_ORDER_PAY"];
				$pay_bonus = $arPayBonus["PAY_BONUS"];
				$bonusPayCart = $arPayBonus["PAY_CART"];
				$bonusPayDelivery = $arPayBonus["PAY_DELIVERY"];
				$newDeliveryPrice = $arPayBonus["NEW_DELIVERY_PRICE"];
			}
			else
			{
				$PayBonusProp = $props->getItemByOrderPropertyId($payBonusPropId);
				$PayBonusProp->setValue(0);
				return;
			}
				
			
			$PayBonusToDiscount = \COption::GetOptionString("logictim.balls", "DISCOUNT_TO_PRODUCTS", 'N');
			if($PayBonusToDiscount == 'Y'):
			
				//Raskidivaem skidku po tovaram v korzine	
				if($bonusPayCart > 0):
				
					//Gotovim ceni zaranee iz-za baga bitrix
					foreach($basket as $basketItem):
						$item = $basketItem->getFields();
						$arItem = $item->getValues();
						
						//Esli tovar nel'zya oplatit' bonusami
						if($arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"] > 0 || $arPayBonus["PAY_PRODUCTS"]["PROFILE"]["NO_PRODUCT_CONDITIONS"] == 'Y')
							$canPayProduct = $arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"];
						else
							continue;
							
						$productPart = $arItem["PRICE"] * $arItem["QUANTITY"] * 100 / $arPayBonus["PAY_PRODUCTS_SUM"]; //Procentnoe sootnoshenie pozicii tovara s obshhej summoj
						$discountPlus = $bonusPayCart * $productPart / 100; //Skol'ko rublej nado pripljusovat' k skidke tovara
						$newPrice = $arItem["PRICE"] - $discountPlus / $arItem["QUANTITY"]; //Cena s uchetom raskidanooj skidki
						$newDiscount = $arItem["DISCOUNT_PRICE"] + $discountPlus / $arItem["QUANTITY"]; //Skidka s uchetom dobavlennoj novoj skidki
						
						$arNewPrices[$arItem["PRODUCT_ID"]] = array("NEW_PRICE" => $newPrice, "NEW_DISCOUNT" => $newDiscount, "BASE_PRICE" => $arItem["BASE_PRICE"]);
					endforeach;
				
					//Menyaem ceni
					foreach($basket as $basketItem):
						$item = $basketItem->getFields();
						$arItem = $item->getValues();
						
						//Esli tovar nel'zya oplatit' bonusami
						if($arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"] > 0 || $arPayBonus["PAY_PRODUCTS"]["PROFILE"]["NO_PRODUCT_CONDITIONS"] == 'Y')
							$canPayProduct = $arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"];
						else
							continue;
						
						
						if(isset($arNewPrices[$arItem["PRODUCT_ID"]]))
						{
							$basketItem->setField('CUSTOM_PRICE', 'Y');
							$basketItem->setField('PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_PRICE"]);
							$basketItem->setField('BASE_PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["BASE_PRICE"]);
							$basketItem->setField('DISCOUNT_PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_DISCOUNT"]);
							
							//zapisivaem novie ceni v nash massiv
							$arItems[$arItem["PRODUCT_ID"]]["PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_PRICE"];
							$arItems[$arItem["PRODUCT_ID"]]["BASE_PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["BASE_PRICE"];
							$arItems[$arItem["PRODUCT_ID"]]["DISCOUNT_PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_DISCOUNT"];
							
						}
					endforeach;
				endif;
				
				//Esli bonusami oplacheno bol'she, chem stoimost korzini, to vichitaem ih iz dostavki
				if($bonusPayDelivery > 0):
					$shipmentCollection = $order->getShipmentCollection();
					foreach($shipmentCollection as $shipment):
						if(!$shipment->isSystem()) {
							$shipment->setFields(array(
								'PRICE_DELIVERY' => $newDeliveryPrice, 'BASE_PRICE_DELIVERY' => $newDeliveryPrice
							));
						}
					endforeach;
				endif;
				
				
				//Menyaem summu k oplate
				$pay_bonus = $arOrderParams["ORDER_SUM"] - $order->getPrice();
				foreach($paymentCollection as $arPayment):
					$fields = $arPayment->GetFields();
					$values = $fields->GetValues();
					
					if($values["SUM"] && $pay_bonus > 0 && $values["SUM"] > $order->getPrice()) //bitrix c versii 17.8-18.0 stal sam pereschitivat oplatu, poetomu dobavleno uslovie  $values["SUM"] > $order->getPrice()
					{
						if($arPayment->isInner())
						{
							if($pay_bonus + $values["SUM"] > $arOrderParams["ORDER_SUM"])
							{
								$new_pay_sum = $values["SUM"] - ($pay_bonus + $values["SUM"] - $arOrderParams["ORDER_SUM"]);
								$arPayment->setField("SUM", $new_pay_sum);
							}
							else
							{ continue;}
						}
						else
						{
							$new_pay_sum = $values["SUM"] - $pay_bonus;
							if($new_pay_sum < 0) 
								$new_pay_sum = 0;
							$arPayment->setField("SUM", $new_pay_sum);
							if($new_pay_sum <= 0) 
								$arPayment->setField("PAID", "Y");
						}
					}
				endforeach;
			
			else:
			
				//Menyaem summu k oplate
				foreach($paymentCollection as $arPayment):
					$fields = $arPayment->GetFields();
					$values = $fields->GetValues();
					
					if($values["SUM"] && $pay_bonus > 0)
					{
						if($arPayment->isInner())
						{
							if($pay_bonus + $values["SUM"] > $arOrderParams["ORDER_SUM"])
							{
								$new_pay_sum = $values["SUM"] - ($pay_bonus + $values["SUM"] - $arOrderParams["ORDER_SUM"]);
								$arPayment->setField("SUM", $new_pay_sum);
							}
							else
							{ continue;}
						}
						else
						{
							$new_pay_sum = $values["SUM"] - $pay_bonus;
							if($new_pay_sum < 0) 
								$new_pay_sum = 0;
							$arPayment->setField("SUM", $new_pay_sum);
							if($new_pay_sum <= 0) 
								$arPayment->setField("PAID", "Y");
						}
					}
				endforeach;
				
				//ADD PAYMENT BONUS
				if($pay_bonus > 0)
				{
					//Get ID of paysystem Bonus
					$paySystemId = \cHelper::PaySystemBonusId();
					$paymentCollection = $order->getPaymentCollection();
					$paymentBonus = $paymentCollection->createItem(\Bitrix\Sale\PaySystem\Manager::getObjectById($paySystemId));
					$paymentBonus->setField("SUM", $pay_bonus);
					$paymentBonus->setField("PAID", "Y");
				}
			
			endif;
		
		endif;
	}
}
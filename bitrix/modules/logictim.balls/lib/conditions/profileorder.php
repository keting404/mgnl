<?
namespace Logictim\Balls\Conditions;

class ProfileOrder
{
	function MainParams($mode='')
	{
			$arShowParams = array(
						"parentContainer" => 'ProfileConditions',
						"form" => '',
						"formName" => 'logictim_profile',
						"sepID" => '__',
						"prefix" => "profileCond",
						"messTree" => array(
											"SELECT_CONTROL" => GetMessage("logictim.balls_SELECT_COND"),
											"ADD_CONTROL" => GetMessage("logictim.balls_ADD_PROFILE_COND"),
											"DELETE_CONTROL" => GetMessage("logictim.balls_DEL_COND"),
											)
						);
						
			if($mode=='json'){
				return \Bitrix\Main\Web\Json::encode($arShowParams);
			}
			
			return $arShowParams;
	}
	
	function BaseConditions($mode='')
	{
		$params = array(
						"id" => '0',
						"controlId" => 'CondGroup',
						"children" => array(
										),
					);
					
		if($mode=='json'){
			return \Bitrix\Main\Web\Json::encode($params);
		}
		return $params;
	}
	
	function Controls($mode='', $type = 'order')
	{
		$arSites = \Logictim\Balls\Helpers::GetSites();
		$arUserGroups = \Logictim\Balls\Helpers::GetUserGroups();
		$basketRules = \Logictim\Balls\Helpers::getBasketRules();
		$arPaySystems = array();
		foreach(\Logictim\Balls\Helpers::getPaySystems() as $arPaySystem){
			$arPaySystems[$arPaySystem['ID']] = $arPaySystem['NAME'];
		}
		$arDelivery = array();
		foreach(\Logictim\Balls\Helpers::getDelivery() as $delivery){
			$arDelivery[$delivery['ID']] = $delivery['NAME'];
		}
		$arPersonTypes = \Logictim\Balls\Helpers::getPersonTypes();
		
		$params = array();
		
		$params[]=array(
				'controlId'=> 'CondGroup',
				'group'=> true,
				'label'=> '',
				'defaultText'=> '',
				'showIn'=> array(),
				'control'=> array('CONDITION_PERFORM_OPERATIONS')
			);
		
		if($type == 'order_referal'):
			$levels = array();
			while($x++ < (int)\COption::GetOptionString("logictim.balls", "REFERAL_LEVELS", 1))
			{
				$levels[$x] = $x;
			}
			$params[] = array(
							'controlgroup'=> '1',
							'group'=> true,
							'label'=> GetMessage("logictim.balls_COND_REFERAL_PARAMS"),
							'showIn'=> array('CondGroup'),
							'children'=> array(
			
												array(
														'controlId'=> 'PartnerGroups',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_PARTNER_GROUP"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_PARTNER_GROUP")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arUserGroups,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
												array(
														'controlId'=> 'PartnerLevel',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_PARTNER_LEVEL"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_PARTNER_LEVEL")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $levels,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'N',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
												array(
														'controlId'=> 'userGroups',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_ORDER_USER_GROUP"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_ORDER_USER_GROUP")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arUserGroups,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
													array(
														'controlId'=> 'sites',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_ORDER_SITE"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_ORDER_SITE")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arSites,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
													array(
														'controlId'=> 'pay_bonus',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_PAY_BONUS"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_PAY_BONUS")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_HAVE"),
																									'Not' => GetMessage("logictim.balls_COND_HAVE_NO")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_HAVE_NO"),
																				'defaultValue' => 'Not'
																			),
																		)
												),
													
											),
											
											
							);
		
		else:
		
		$params[] = array(
							'controlgroup'=> '1',
							'group'=> true,
							'label'=> GetMessage("logictim.balls_COND_MAIN_PARAMS"),
							'showIn'=> array('CondGroup'),
							'children'=> array(
												array(
														'controlId'=> 'sites',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_SITE"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_SITE")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arSites,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
													array(
														'controlId'=> 'userGroups',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_USER_GROUP"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_USER_GROUP")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																			array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arUserGroups,
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'first_option'=> '...',
																				'defaultText'=> '...',
																				'defaultValue'=> ''
																			)
																		)
													),
													array(
														'controlId'=> 'pay_bonus',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_PAY_BONUS"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_PAY_BONUS")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_HAVE"),
																									'Not' => GetMessage("logictim.balls_COND_HAVE_NO")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_HAVE_NO"),
																				'defaultValue' => 'Not'
																			),
																		)
												),
												
											)
						);
		
		endif;
						
		
		$params[] = array(
							'controlgroup'=> '1',
							'group'=> true,
							'label'=> GetMessage("logictim.balls_COND_OTHER_PARAMS"),
							'showIn'=> array('CondGroup'),
							'children'=> array(
												array(
														'controlId'=> 'orderRowNum',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW").($type == 'order_referal' ? ' '.GetMessage("logictim.balls_COND_USER_REFERALA_POSTFIX") : ''),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW_USE")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Evry' => GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW_EVRY"),
																									'Only' => GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW_ONLY"),
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW_EVRY"),
																				'defaultValue' => 'Evry'
																			),
																			array(
																				'type'=> 'input',
																				'id'=> 'ordersCount',
																				'name'=> 'ordersCount',
																				'show_value'=>'Y',
																				'defaultValue' => '2'
																			),
																			GetMessage("logictim.balls_COND_USER_POSTFIX_IY"),
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_USER_ORDERS_COUNT_ROW_ORDER")),
																		)
													),
												array(
														'controlId'=> 'ordersSum',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_USER_ORDERS_SUM").($type == 'order_referal' ? ' '.GetMessage("logictim.balls_COND_USER_REFERALA_POSTFIX") : ''),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_USER_ORDERS_SUM")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'EqGr' => GetMessage("logictim.balls_COND_EQGR"),
																									'Less' => GetMessage("logictim.balls_COND_LESS")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQGR"),
																				'defaultValue' => 'EqGr'
																			),
																			array(
																				'type'=> 'input',
																				'id'=> 'ordersSum',
																				'name'=> 'ordersSum',
																				'show_value'=>'Y',
																				'defaultValue' => '0'
																			),
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_FOR_PERIOD")),
																			array(
																				'type'=> 'input',
																				'id'=> 'period',
																				'name'=> 'period',
																				'defaultValue' => '1'
																			),
																			array(
																				'type'=> 'select',
																				'id'=> 'period_type',
																				'name'=> 'period_type',
																				'values' => array
																								(
																									'D' => GetMessage("logictim.balls_COND_DAY"),
																									'M' => GetMessage("logictim.balls_COND_MONTH"),
																									'Y' => GetMessage("logictim.balls_COND_YEAR")
																								),
																				'defaultValue' => 'Y',
																				'defaultText' => GetMessage("logictim.balls_COND_YEAR")
																			),
																		)
													),
												array(
														'controlId'=> 'cartSum',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_CART_SUM"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_CART_SUM")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'EqGr' => GetMessage("logictim.balls_COND_EQGR"),
																									'Less' => GetMessage("logictim.balls_COND_LESS")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQGR"),
																				'defaultValue' => 'EqGr'
																			),
																			array(
																				'type'=> 'input',
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'defaultValue' => '0'
																			)
																		)
													),
												array(
														'controlId'=> 'orderSum',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_ORDER_SUM"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																			array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_ORDER_SUM")),
																			array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'EqGr' => GetMessage("logictim.balls_COND_EQGR"),
																									'Less' => GetMessage("logictim.balls_COND_LESS")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQGR"),
																				'defaultValue' => 'EqGr'
																			),
																			array(
																				'type'=> 'input',
																				'id'=> 'value',
																				'name'=> 'value',
																				'show_value'=>'Y',
																				'defaultValue' => '0'
																			)
																		)
													),
												/*array(
														'controlId'=> 'basketRules',
														'group'=> false,
														'label'=> 'PRAVILO KORZINI',
														'showIn'=> array('CondGroup'),
														'control'=> array(
																		array('id'=>'prefix', 'type'=>'prefix', 'text'=>'PRAVILO KORZINI'),
																		array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																		array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $basketRules,
																				'show_value'=>'Y',
																				'id'=> 'value',
																				'name'=> 'value',
																			)
																		)
														),*/
												array(
														'controlId'=> 'paySystems',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_PAY_SYSTEM"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																		array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_PAY_SYSTEM")),
																		array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																		array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arPaySystems,
																				'show_value'=>'Y',
																				'id'=> 'value',
																				'name'=> 'value',
																			)
																		)
														),
												array(
														'controlId'=> 'delivery',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_DELIVERY_SYSTEM"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																		array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_DELIVERY_SYSTEM")),
																		array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																		array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arDelivery,
																				'show_value'=>'Y',
																				'id'=> 'value',
																				'name'=> 'value',
																			)
																		)
														),
												array(
														'controlId'=> 'personTypes',
														'group'=> false,
														'label'=> GetMessage("logictim.balls_COND_PERSON_TYPE"),
														'showIn'=> array('CondGroup'),
														'control'=> array(
																		array('id'=>'prefix', 'type'=>'prefix', 'text'=>GetMessage("logictim.balls_COND_PERSON_TYPE")),
																		array(
																				'id' => 'logic',
																				'name' => 'logic',
																				'type' => 'select',
																				'values' => array
																								(
																									'Equal' => GetMessage("logictim.balls_COND_EQUAL"),
																									'Not' => GetMessage("logictim.balls_COND_NOT")
																								),
																				'defaultText' => GetMessage("logictim.balls_COND_EQUAL"),
																				'defaultValue' => 'Equal'
																			),
																		array(
																				'type'=> 'select',
																				'multiple'=>'Y',
																				'values'=> $arPersonTypes,
																				'show_value'=>'Y',
																				'id'=> 'value',
																				'name'=> 'value',
																			)
																		)
														),
											),
						);				
						
		
		
		if($mode=='json'){
			return \Bitrix\Main\Web\Json::encode($params);
		}
		return $params;
	}
}



?>
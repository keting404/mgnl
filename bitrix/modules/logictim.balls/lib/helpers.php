<?
namespace Logictim\Balls;

class Helpers {
	
	public function GetSites()
	{
		$sites = array();
		$rsSites = \CSite::GetList($by="sort", $order="desc", array());
		while($arSite = $rsSites->Fetch())
		{
		  $sites[$arSite["ID"]] = $arSite["NAME"];
		}
		return $sites;
	}
	
	public function GetUserGroups()
	{
		$userGrups = array();
		$rsGroups = \CGroup::GetList(($by="id"), ($order="asc"), array("ACTIVE"  => "Y"));
		while($arUserGroups = $rsGroups->Fetch()) {
			$userGrups[$arUserGroups["ID"]] = $arUserGroups["NAME"];
		}
		return $userGrups;
	}
	
	public function getBasketRules(){
		$basketRules = array();
		$discountIterator = \Bitrix\Sale\Internals\DiscountTable::getList(array(
			'select' => array("ID", "NAME"),
			'filter' => array('ACTIVE' => 'Y'),
			'order' => array("NAME" => "ASC")
		));
		while ($discount = $discountIterator->fetch()){
			 $basketRules[$discount['ID']] = $discount['NAME'];
		}
		return $basketRules ;
	}
	
	public function getPaySystems(){
		$paySystems = array();
		$res = \Bitrix\Sale\Internals\PaySystemActionTable::GetList(array('order' => array("NAME" => "ASC")));
		while($row=$res->fetch()){
			$paySystems[]=$row;
		}
		return $paySystems;
	}
	
	public function getDelivery(){
		$delivery = array();
		$res = \Bitrix\Sale\Delivery\Services\Table::getList(array('order' => array("NAME" => "ASC")));
	   while($del = $res->Fetch()) {
		   $delivery[] = $del;
	   }
		return $delivery;
	}
	
	public function getPersonTypes(){
		$personTypes = array();
		$res = \CSalePersonType::GetList(array('NAME'=>'ASC'),array(),false,false,array());
		while($type = $res->Fetch()){
			$personTypes[$type['ID']]=$type['NAME'];
		}
		return $personTypes;
	}
	
	public function getCatalogs(){
		$arCatalogs = array();
		$dbCatalogs = \CIBlock::GetList(array(), array('ACTIVE'=>'Y', ), true);
		//take only torgoviy catalog
		while($arCatalog = $dbCatalogs->Fetch())
		{
			$catDb = \CCatalog::GetByID($arCatalog["ID"]);
			if($catDb)
				$arCatalogs[$catDb["ID"]] = $catDb["NAME"];
		}
		return $arCatalogs;
	}
	
	//Opredelyaem ID ibfobloka s operaciyami
	public function IblokOperationsId()
	{
		\CModule::IncludeModule("iblock");
		$dbiblokOpertion = \CIBlock::GetList(array(), array('ACTIVE'=>'Y', "CODE"=>'logictim_bonus_operations'), true);
		while($iblokOpertion = $dbiblokOpertion->Fetch())
		{
			$iblokOperationsId = $iblokOpertion["ID"];
		}
		return $iblokOperationsId;
	}
	
	//Opredelyaem ID ibfobloka s operaciyami ojidaniya
	public static function IblokWaitId()
	{
		\CModule::IncludeModule("iblock");
		$dbiblokWait = \CIBlock::GetList(array(), array('ACTIVE'=>'Y', "CODE"=>'logictim_bonus_wait'), true);
		while($iblokWait = $dbiblokWait->Fetch())
		{
			$iblokWaitId = $iblokWait["ID"];
		}
		return $iblokWaitId;
	}
	
	//Poluchaem vozmojnie znacheniya svoystava "OPERATION_TYPE"
	public function OperationsType()
	{
		\CModule::IncludeModule("iblock");
		$operationsType = array();
		$iblokOperationsId = \Logictim\Balls\Helpers::IblokOperationsId();
		$property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblokOperationsId, "CODE"=>"OPERATION_TYPE"));
		while($enum_fields = $property_enums->GetNext())
		{
			$operationsType[$enum_fields["XML_ID"]] = $enum_fields["ID"];
		}
		return $operationsType;
	}
	
	//Poluchaem vozmojnie znacheniya svoystava "OPERATION_TYPE" ibfobloka s operaciyami ojidaniya
	public static function OperationsTypeWait()
	{
		\CModule::IncludeModule("iblock");
		$operationsType = array();
		$iblokWaitId = \Logictim\Balls\Helpers::IblokWaitId();
		$property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblokWaitId, "CODE"=>"OPERATION_TYPE"));
		while($enum_fields = $property_enums->GetNext())
		{
			$operationsType[$enum_fields["XML_ID"]] = $enum_fields["ID"];
		}
		return $operationsType;
	}
	
	public function UserBallance($user_id)
	{
		global $USER;
		if(!$user_id || $user_id == '')
			$user_id = $USER->GetID();
			
		//Esli ispol'zuetsya bonusniy schet modulya
		if(\COption::GetOptionString("logictim.balls", "BONUS_BILL", '1') == 1)
		{
			$arParams["SELECT"] = array("UF_LOGICTIM_BONUS");
			$DBUserBonus = \CUser::GetList(($by="ID"),($order="desc"),array("ID" => $user_id),$arParams);
			if($arUserBonus = $DBUserBonus->Fetch()) 
			{
				$userBonus = $arUserBonus["UF_LOGICTIM_BONUS"];
			}
		}
		//Esli ispol'zuetsya vnutrenniy schet bitrix
		else
		{
			\CModule::IncludeModule("sale");
			//Opredelyaem valyutu
			$currency = \COption::GetOptionString("logictim.balls", "BONUS_CURRENCY", 'RUB');
			
			$dbAccountCurrency = \CSaleUserAccount::GetList(array(), array("USER_ID" => $user_id, "CURRENCY" => $currency), false, false, array());
			while($arAccountCurrency = $dbAccountCurrency->Fetch())
			{
				$userBonus = (float)$arAccountCurrency["CURRENT_BUDGET"];
			}
		}
		
		return $userBonus;
	}
	
	public function UserOrdersSum($arParams, $arCondition)
	{	
		//SOBITIE DO rascheta summi zakazkov
		$arFields = array("USER_ID" => $arParams["USER_ID"], "SITE_ID" => $arParams["SITE_ID"], "CUSTOM_ORDERS_SUM"=>'N');
		$event = new \Bitrix\Main\Event("logictim.balls", "BeforeUserOrdersSum", $arFields);
		$event->send();
		if($event->getResults())
		{
			foreach ($event->getResults() as $eventResult):
				$arFields = $eventResult->getParameters();
			endforeach;
		}
		if($arFields["CUSTOM_ORDERS_SUM"] != 'N' && $arFields["CUSTOM_ORDERS_SUM"] >= 0)
			return $arFields["CUSTOM_ORDERS_SUM"];
		//SOBITIE DO rascheta summi zakazkov
		
		\CModule::IncludeModule("sale");
		$calculator = new \Bitrix\Sale\Discount\CumulativeCalculator($arParams["USER_ID"], $arParams["SITE_ID"]);
		$sumConfiguration = array(
								'type_sum_period'=> 'relative',
								'sum_period_data' => array('period_value' => $arCondition["values"]["period"], 'period_type' => $arCondition["values"]["period_type"])
								);
		$calculator->setSumConfiguration($sumConfiguration);
		$ordersSum = $calculator->calculate();
		if($arParams["ORDER"]["ORDER_ID"] > 0 && $arParams["EVENT_ORDER_PAID"] == 'Y')
			$ordersSum = $ordersSum - $arParams["ORDER"]["ORDER_SUM"];
		
		return $ordersSum;
	}
	
	public function UserOrdersCount($arParams, $arCondition)
	{	
		
		\Bitrix\Main\Loader::includeModule('sale');
		$orders = \Bitrix\Sale\Order::getList(array(
                                    'select' => array('CNT'),
                                    'filter' => array('USER_ID' => $arParams["USER_ID"], "LID" => $arParams["SITE_ID"]),
                                    'order' => array(),
                                    'runtime' => array(new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)'))
			));
			$rows = array();
			if($row = $orders->fetch())
				$ordersCount = $row["CNT"];
			else
				$ordersCount = 0;
				
			return $ordersCount;
	}
	
}



?>
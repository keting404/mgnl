<?
namespace Logictim\Balls\Ajax;

\Bitrix\Main\Loader::includeModule("logictim.balls");

class SaleOrderAjax {
	
	function OnSaleComponentOrderCreated($order, &$arUserResult, $request, &$arParams, &$arResult)
	{
		if(!empty($_POST))
			$post = 'Y';
			
		
		$basket = $order->getBasket();
		
		//Payments
		$paymentCollection = $order->getPaymentCollection();
		$arPayments = array();
		foreach($paymentCollection as $payment):
			$paymentId = $payment->getPaymentSystemId();
			$arPayments[$paymentId] = array("ID"=>$paymentId, "SUM"=>$payment->getSum(), "NAME"=>$payment->getPaymentSystemName(), "IS_PAYED"=>$payment->isPaid());
		endforeach;
		
		//Delivery
		$deliveryCollection = $order->getDeliverySystemId();
		$arDelivery = array();
		foreach($deliveryCollection as $delivery):
			$arDelivery[$delivery] = array("ID"=>$delivery);
		endforeach;
		
		//Polya zakaza
		$discountData = $order->getDiscount()->getApplyResult();
		$arOrderParams = array(
								"USER_ID" => $order->getUserId(),
								"ORDER_SUM" => $order->getPrice(),
								"CART_SUM" => $basket->getPrice(),
								"DELIVERY_SUM" => $order->getDeliveryPrice(),
								"PERSON_TYPE_ID" => $order->getPersonTypeId(),
								"CURRENCY" => $order->getCurrency(),
								"DISCOUNT" => $order->getDiscountPrice(),
								"DISCOUNT_DATA" => $discountData["DISCOUNT_LIST"],
								"PAYMENTS" => $arPayments,
								"DELIVERY" => $arDelivery,
								);
								
		
		global $USER;
		if($USER->IsAuthorized())
			$UserBallance = \Logictim\Balls\Helpers::UserBallance($arOrderParams["USER_ID"]);
		else
			$UserBallance = 0;
		
		//Svoystava zakaza
		$props = $order->getPropertyCollection();
		foreach($props as $prop) {
			$fields = $prop->GetFields();
			$values = $fields->GetValues();
			if($values["CODE"] == 'LOGICTIM_PAYMENT_BONUS')
			{
				$payment_prop_id = $values["ORDER_PROPS_ID"];
				$input_bonus = $values["VALUE"];
				if(!is_numeric($input_bonus))
					$input_bonus = 0;
			}
			if($values["CODE"] == 'LOGICTIM_ADD_BONUS')
				$addBpnus_prop_id = $values["ORDER_PROPS_ID"];
		}
		
		//Korzina zakaza
		$arItems = array();
		foreach($basket as $basketItem):
			$arItem = array();
			$arItem["PRODUCT_ID"] = $basketItem->getProductId();
			$arItem["NAME"] = $basketItem->getField('NAME');
			$arItem["QUANTITY"] = $basketItem->getQuantity();
			$arItem["BASE_PRICE"] = $basketItem->getField('BASE_PRICE');
			$arItem["PRICE"] = $basketItem->getPrice();
			$arItem["DISCOUNT_PRICE"] = $basketItem->getField('DISCOUNT_PRICE');
			$arItem["POSITION_FINAL_PRICE"] = $basketItem->getFinalPrice();
			$arItems[$arItem["PRODUCT_ID"]] = $arItem;
		endforeach;
		
		
		if($post == 'Y')
		{
			$pay_bonus = $input_bonus;
			if(!is_numeric($pay_bonus))
				$pay_bonus = 0;
		}
		else
		{
			$pay_bonus = 'MAX';
			if(\COption::GetOptionString("logictim.balls", "ORDER_PAY_BONUS_AUTO", 'Y') != 'Y')
				$pay_bonus = 0;
		}
		
		$arOrderParams["PAY_BONUS"] = $pay_bonus;
		$arPayBonus = \Logictim\Balls\CalcBonus::OrderBonusPayment($arItems, $arOrderParams);
			$minBonusSum = $arPayBonus["MIN_ORDER_PAY"];
			$maxBonusSum = $arPayBonus["MAX_ORDER_PAY"];
			$pay_bonus = $arOrderParams["PAY_BONUS"] = $arPayBonus["PAY_BONUS"];
			$bonusPayCart = $arPayBonus["PAY_CART"];
			$bonusPayDelivery = $arPayBonus["PAY_DELIVERY"];
			$newDeliveryPrice = $arPayBonus["NEW_DELIVERY_PRICE"];
		
		
		//Raskidivaem oplatu bonusami v vide skidki
		$PayBonusToDiscount = \COption::GetOptionString("logictim.balls", "DISCOUNT_TO_PRODUCTS", 'N');
		if($PayBonusToDiscount == 'B' && $pay_bonus > 0):
		
			//Raskidivaem skidku po tovaram v korzine	
			if($bonusPayCart > 0):
			
				//Gotovim ceni zaranee iz-za baga bitrix
				foreach($basket as $basketItem):
					$item = $basketItem->getFields();
					$arItem = $item->getValues();
					
					//Esli tovar nel'zya oplatit' bonusami
					if($arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"] > 0 || $arPayBonus["PAY_PRODUCTS"]["PROFILE"]["NO_PRODUCT_CONDITIONS"] == 'Y')
						$canPayProduct = $arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"];
					else
						continue;
						
					$productPart = $arItem["PRICE"] * $arItem["QUANTITY"] * 100 / $arPayBonus["PAY_PRODUCTS_SUM"]; //Procentnoe sootnoshenie pozicii tovara s obshhej summoj
					$discountPlus = $bonusPayCart * $productPart / 100; //Skol'ko rublej nado pripljusovat' k skidke tovara
					$newPrice = $arItem["PRICE"] - $discountPlus / $arItem["QUANTITY"]; //Cena s uchetom raskidanooj skidki
					$newDiscount = $arItem["DISCOUNT_PRICE"] + $discountPlus / $arItem["QUANTITY"]; //Skidka s uchetom dobavlennoj novoj skidki
					
					$arNewPrices[$arItem["PRODUCT_ID"]] = array("NEW_PRICE" => $newPrice, "NEW_DISCOUNT" => $newDiscount, "BASE_PRICE" => $arItem["BASE_PRICE"]);
				endforeach;
				
				//Menyaem ceni
				foreach($basket as $basketItem):
					$item = $basketItem->getFields();
					$arItem = $item->getValues();
					
					//Esli tovar nel'zya oplatit' bonusami
					if($arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"] > 0 || $arPayBonus["PAY_PRODUCTS"]["PROFILE"]["NO_PRODUCT_CONDITIONS"] == 'Y')
						$canPayProduct = $arPayBonus["PAY_PRODUCTS"]["ITEMS"][$arItem["PRODUCT_ID"]]["ADD_BONUS"];
					else
						continue;
					
					
					if(isset($arNewPrices[$arItem["PRODUCT_ID"]]))
					{
						$basketItem->setField('CUSTOM_PRICE', 'Y');
						$basketItem->setField('PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_PRICE"]);
						$basketItem->setField('BASE_PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["BASE_PRICE"]);
						$basketItem->setField('DISCOUNT_PRICE', $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_DISCOUNT"]);
						
						//zapisivaem novie ceni v nash massiv
						$arItems[$arItem["PRODUCT_ID"]]["PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_PRICE"];
						$arItems[$arItem["PRODUCT_ID"]]["BASE_PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["BASE_PRICE"];
						$arItems[$arItem["PRODUCT_ID"]]["DISCOUNT_PRICE"] = $arNewPrices[$arItem["PRODUCT_ID"]]["NEW_DISCOUNT"];
						
					}
				endforeach;
			endif;
			
			//Esli bonusami oplacheno bol'she, chem stoimost korzini, to vichitaem ih iz dostavki
			if($bonusPayDelivery > 0):
				$shipmentCollection = $order->getShipmentCollection();
				foreach($shipmentCollection as $shipment):
					if(!$shipment->isSystem()) {
						$shipment->setFields(array(
							'PRICE_DELIVERY' => $newDeliveryPrice, 'BASE_PRICE_DELIVERY' => $newDeliveryPrice
						));
					}
				endforeach;
			endif;
			
			
		endif;
		
		//SKOL'KO BONUSOV BUDET NACHISLENO ZA ZAKAZ
		$arBonus = \Logictim\Balls\CalcBonus::getBonus($arItems, array("TYPE"=>'cart', "PROFILE_TYPE" => 'order', "ORDER"=>$arOrderParams));
		//echo '<pre>'; print_r($arBonus); echo '</pre>';
		
		$UserBonusSystemDostup = 'Y';
		$arResult["MIN_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["MIN_BONUS"] = $minBonusSum;
		$arResult["MAX_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["MAX_BONUS"] = $maxBonusSum;
		$arResult["USER_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["USER_BONUS"] = $UserBallance;
		$arResult["LOGICTIM_BONUS_USER_DOSTUP"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["LOGICTIM_BONUS_USER_DOSTUP"] = $UserBonusSystemDostup;
		
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["INPUT_BONUS"] = $input_bonus;
		$arResult["PAY_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["PAY_BONUS"] = $pay_bonus;
		$arResult["PAY_BONUS_FORMATED"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["PAY_BONUS_FORMATED"] = \SaleFormatCurrency($pay_bonus, $arOrderParams['CURRENCY']);
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["PAY_BONUS_NO_POST"] = $pay_bonus; //OLD
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["PAY_BONUS_NO_POST_FORMATED"] = SaleFormatCurrency($pay_bonus, $arOrderParams['CURRENCY']); //OLD
		
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["ORDER_SUM"] = $arOrderParams["ORDER_SUM"];
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["ORDER_SUM_FORMATED"] = SaleFormatCurrency($arOrderParams["ORDER_SUM"], $arOrderParams['CURRENCY']);
		
		$arResult["ARR_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["ARR_BONUS"] = $arBonus;
		$arResult["ADD_BONUS"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["ADD_BONUS"] = (string)$arBonus["ALL_BONUS"];
		
		$arResult["ORDER_PROP_PAYMENT_BONUS_ID"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["ORDER_PROP_PAYMENT_BONUS_ID"] = $payment_prop_id;
		$arResult["ORDER_PROP_ADD_BONUS_ID"] = $arResult["JS_DATA"]["LOGICTIM_BONUS"]["ORDER_PROP_ADD_BONUS_ID"] = $addBpnus_prop_id;
		
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["DISCOUNT_TO_PRODUCTS"] = \COption::GetOptionString("logictim.balls", "DISCOUNT_TO_PRODUCTS", 'N');
		
		//ADD_TEXT
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["TEXT_BONUS_BALLS"] = \COption::GetOptionString("logictim.balls", "TEXT_BONUS_BALLS", 'bonus:');
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["TEXT_BONUS_PAY"] = \COption::GetOptionString("logictim.balls", "TEXT_BONUS_PAY", 'pay from bonus:');
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["TEXT_BONUS_FOR_ITEM"] = \COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_ITEM", 'pay from bonus:');
		$arResult["JS_DATA"]["LOGICTIM_BONUS"]["MODULE_LANG"] = array(
															"HAVE_BONUS_TEXT" => \COption::GetOptionString("logictim.balls", "HAVE_BONUS_TEXT", 'Have bonus'),
															"CAN_USE_BONUS_TEXT" => \COption::GetOptionString("logictim.balls", "CAN_BONUS_TEXT", 'Can use bonus'),
															"MIN_BONUS_TEXT" => \COption::GetOptionString("logictim.balls", "MIN_BONUS_TEXT", 'Min use bonus').$arResult["MIN_BONUS"],
															"MAX_BONUS_TEXT" => \COption::GetOptionString("logictim.balls", "MAX_BONUS_TEXT", 'Max use bonus').$arResult["MAX_BONUS"],
															"PAY_BONUS_TEXT" => \COption::GetOptionString("logictim.balls", "PAY_BONUS_TEXT", 'Pay from bonus'),
															"TEXT_BONUS_FOR_PAYMENT" => \COption::GetOptionString("logictim.balls", "TEXT_BONUS_FOR_PAYMENT", 'Pay from bonus'),
															);
	}
}

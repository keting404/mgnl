<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

Class dlay_cookienotice extends CModule
{

    var $MODULE_ID = 'dlay.cookienotice';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct()
    {

        if (file_exists(__DIR__ . "/version.php")) {

            $arModuleVersion = array();

            include(__DIR__ . "/version.php");

            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME         = Loc::getMessage("DLAY_COOKIENOTICE_MODULE_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("DLAY_COOKIENOTICE_MODULE_DESCRIPTION");
            $this->PARTNER_NAME        = Loc::getMessage("DLAY_COOKIENOTICE_PARTNER_NAME");
            $this->PARTNER_URI         = Loc::getMessage("DLAY_COOKIENOTICE_PARTNER_URI");

        }

    }

    function InstallFiles() {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dlay.cookienotice/install/components",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
    }

    function UnInstallFiles() {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/components'))
        {
            if ($dir = opendir($p))
            {
                while (false !== $item = readdir($dir))
                {
                    if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
                        continue;

                    $dir0 = opendir($p0);
                    while (false !== $item0 = readdir($dir0))
                    {
                        if ($item0 == '..' || $item0 == '.')
                            continue;
                        DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
                    }
                    closedir($dir0);
                }
                closedir($dir);
            }
        }
    }

    function DoInstall() {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->InstallFiles();
        RegisterModule("dlay.cookienotice");
        $APPLICATION->IncludeAdminFile(GetMessage("DLAY_COOKIENOTICE_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/dlay.cookienotice/install/step.php");
    }

    function DoUninstall() {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        UnRegisterModule("dlay.cookienotice");
        $APPLICATION->IncludeAdminFile(GetMessage("DLAY_COOKIENOTICE_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/dlay.cookienotice/install/unstep.php");
    }

}

?>
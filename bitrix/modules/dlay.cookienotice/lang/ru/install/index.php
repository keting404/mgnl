<?php
$MESS["DLAY_COOKIENOTICE_MODULE_NAME"]        = "Уведомление об использовании куки";
$MESS["DLAY_COOKIENOTICE_MODULE_DESCRIPTION"] = "Выводит блок с информацией об использовании куки";
$MESS["DLAY_COOKIENOTICE_PARTNER_NAME"]       = "Веб-студия Dlay";
$MESS["DLAY_COOKIENOTICE_PARTNER_URI"]        = "https://dlay.ru";
$MESS["DLAY_COOKIENOTICE_INSTALL_TITLE"]      = "Установка модуля";
$MESS["DLAY_COOKIENOTICE_UNINSTALL_TITLE"]    = "Удаление модуля";
?>
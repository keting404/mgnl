<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader,
	\Bitrix\Main\Localization\Loc;

class ImComponentExternalCall extends CBitrixComponent
{
	private $chatId = 0;

	protected function checkModules()
	{
		if (!Loader::includeModule('im'))
		{
			\ShowError(Loc::getMessage('IM_COMPONENT_MODULE_IM_NOT_INSTALLED'));
			return false;
		}
		return true;
	}

	public function executeComponent()
	{
		$this->includeComponentLang('class.php');

		if (!$this->checkModules())
		{
			return false;
		}

		$this->chatId = intval($this->arParams['CHAT_ID']);
		if (!$this->chatId)
			return false;

		\Bitrix\Main\UI\Extension::load("im.application.recent");
		\Bitrix\Main\UI\Extension::load("im.application.call");

		$this->arResult['CHAT_ID'] = $this->chatId;

		$this->includeComponentTemplate();

		return true;
	}
};
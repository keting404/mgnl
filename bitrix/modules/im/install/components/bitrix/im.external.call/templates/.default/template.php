<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div id="placeholder"></div>
<script type="text/javascript">
	BX.Messenger.Application.Launch('call', {
		node: '#placeholder',
		chatId: '<?=$arResult['CHAT_ID']?>',
	});
</script>

import {VuexBuilder} from "ui.vue.vuex";
import {UsersModel} from '../src/users';

describe('Im model: Users', () => {

	let vuex = null;

	before(async () => {

		vuex = await new VuexBuilder()
			.addModel(
				UsersModel.create()
					.useDatabase(false)
					.setVariables({
						host: 'http://bitrix24.com',
						default: {name: 'Anonymous'}
					})
			)
			.build()
		;
	});

	it('Model is loaded', () => {
		assert(typeof UsersModel !== 'undefined');
	});

	it('Model is initialize', async () => {
		assert.equal(vuex.store.state.users.host, 'http://bitrix24.com');
	});

});
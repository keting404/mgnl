/**
 * Bitrix Im
 * Application External Call
 *
 * @package bitrix
 * @subpackage im
 * @copyright 2001-2020 Bitrix
 */

import {Vue} from "ui.vue";
import {Logger} from "im.lib.logger";
import "im.component.dialog";
import "pull.component.status";
import "./view.css";

Vue.component('bx-im-application-call',
{
	props:
	{
		userId: { default: 0 },
		chatId: { default: 0 },
		dialogId: { default: '0' },
	},
	methods:
	{
		logEvent(name, ...params)
		{
			Logger.info(name, ...params);
		},
	},
	template: `
		<div class="bx-im-application-call">
			<bx-pull-component-status/>
			<div>UserId: <b>{{userId}}</b></div>
			<div>ChatId: <b>{{chatId}}</b></div>
			<div>DialogId: <b>{{dialogId}}</b></div>
		</div>
	`
});
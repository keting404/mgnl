<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$aMenuLinksExt = array();

$catalog_id = \Bitrix\Main\Config\Option::get('aspro.max', 'CATALOG_IBLOCK_ID', CMaxCache::$arIBlocks[SITE_ID]['aspro_max_catalog']['aspro_max_catalog'][0]);

// для указания ID группы (только 1 уровень!)
// foreach ($aMenuLinks as $arMenuItem) {
//     $arMenuFilter[] = $arMenuItem[0];
// }
// $arSections = CIBlockSection::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), array('IBLOCK_ID' => $catalog_id, 'SECTION_ID' => $arMenuFilter, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', '<DEPTH_LEVEL' => \Bitrix\Main\Config\Option::get("aspro.max", "MAX_DEPTH_MENU", 2)), false, array('ID', 'ACTIVE', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'PICTURE', 'IS_PARENT', 'UF_REGION', 'UF_MENU_BANNER', 'UF_MENU_BRANDS', 'UF_CATALOG_ICON'));
// $k=0;
// while($ar_result = $arSections->GetNext())
//   {
//     $arSectionsList[]=$ar_result;
//     $aMenuLinksExt[$k][0]=$ar_result['NAME'];
//     $aMenuLinksExt[$k][1]=$ar_result['SECTION_PAGE_URL'];
//     $aMenuLinksExt[$k][2]=array();
//     $aMenuLinksExt[$k][3]=array(
//         'FROM_IBLOCK' => 1,
//         'DEPTH_LEVEL' => intval($ar_result['DEPTH_LEVEL'])-1,
//         'IBLOCK_ID' => $ar_result['IBLOCK_ID'],
//         'IS_PARENT' => 1,
//     );
//     $k++;
//   }
// 
// $arSectionsByParentSectionID = CMaxCache::GroupArrayBy($arSectionsRes, array('MULTI' => 'Y', 'GROUP' => array('IBLOCK_SECTION_ID')));
// 
// 
// $aMenuLinks = $aMenuLinksExt;

// просто поднимаем все на 1 уровень вверх:
$arSections = CMaxCache::CIBlockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => CMaxCache::GetIBlockCacheTag($catalog_id), 'MULTI' => 'Y')), array('IBLOCK_ID' => $catalog_id, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', '<DEPTH_LEVEL' => \Bitrix\Main\Config\Option::get("aspro.max", "MAX_DEPTH_MENU", 2)), false, array('ID', 'ACTIVE', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'PICTURE', 'UF_REGION', 'UF_MENU_BANNER', 'UF_MENU_BRANDS', 'UF_CATALOG_ICON'));
$arSectionsByParentSectionID = CMaxCache::GroupArrayBy($arSections, array('MULTI' => 'Y', 'GROUP' => array('IBLOCK_SECTION_ID')));

if($arSections)
	CMax::getSectionChilds(false, $arSections, $arSectionsByParentSectionID, $arItemsBySectionID, $aMenuLinksExt);
$k=0;
foreach($aMenuLinksExt as $elem){
    if (intval($elem[3]['DEPTH_LEVEL']) == 1) {
        unset($aMenuLinksExt[$k]);
    }else{
        $aMenuLinksExt[$k][3]['DEPTH_LEVEL']=intval($elem[3]['DEPTH_LEVEL'])-1;
    }
    
$k++;
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>

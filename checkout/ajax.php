<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("iblock")){
	echo "failure";
	return;
}
if(!empty($_SESSION['SALE_ORDER_ID']))
    $_SESSION['ORDER_ID'] = array_pop($_SESSION['SALE_ORDER_ID']);
if(!$USER->GetID() && $_SESSION['ORDER_ID']){
    $orderId = $_SESSION['ORDER_ID'];
    $arFilter = ["ACCOUNT_NUMBER" => $orderId];
}else if($USER->GetID()){
    $arFilter = ["USER_ID" => $USER->GetID(),"ACCOUNT_NUMBER" => $_POST["id"]];
}
\Bitrix\Main\Loader::includeModule('sale');
	$arSelect = array("ID","STATUS_ID","DELIVERY_ID");
	$rsOrder = \Bitrix\Sale\OrderTable::getList(
		array(
			"filter" => $arFilter,
			"select" => $arSelect,
		)
	);
	if($arOrder = $rsOrder->Fetch())
	{
		$orderArr = $arOrder;
	}
	if ($arStatus = CSaleStatus::GetByID($orderArr['STATUS_ID']))
    {
        $statusName = $arStatus['NAME'];
        $statusSort = $arStatus['SORT'];
    }
if($statusSort >= 100){
    $firstStatus = " complited";
    $secondStatus = " process";
}
if($statusSort >= 130){
    $secondStatus = " complited";
    $thirdStatus = " process";
}
if($statusSort >= 190)
    $thirdStatus = " complited";
    $thirdStatusName = "Доставлен";
if($statusSort == 200)
    $thirdStatusName = "Выполнен";
echo '<div class="row statuses-bar"><div class="col-xs-4 first-item"><div class="status-item'.$firstStatus.'">Принят</div></div><div class="col-xs-4 second-item"><div class="status-item'.$secondStatus.'">Собран</div></div><div class="col-xs-4 third-item"><div class="status-item'.$thirdStatus.'">'.$thirdStatusName.'</div></div></div></div>';
?>

<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->AddHeadScript($APPLICATION->GetCurPage(false).'/script.js', true);
$APPLICATION->SetAdditionalCSS($APPLICATION->GetCurPage(false).'/style.css', true);
$APPLICATION->SetTitle("Спасибо за покупку!");
use \Bitrix\Main\Application;
use \Bitrix\Sale\PaySystem;
if (isset($_GET['ORDER_ID'])){
    $orderId = $_GET['ORDER_ID'];
}else{
    parse_str($_SERVER['HTTP_REFERER'], $output);
    $orderId = $output['ORDER_ID'];
    if(!empty($_SESSION['ORDER']))
        $_SESSION['ORDER_ID'] = array_pop($_SESSION['ORDER']);
    if(!$orderId)
        $orderId = $_SESSION['ORDER_ID'];
    if($orderId)
        LocalRedirect("/checkout/?ORDER_ID=".$orderId);
}
// print_r($_SESSION);
if(!$USER->GetID() && $_SESSION['ORDER_ID']){
    $orderId = $_SESSION['ORDER_ID'];
    $filter = ["ACCOUNT_NUMBER" => $orderId];
}else if($USER->GetID()){
    $filter = ["USER_ID" => $USER->GetID(),"ACCOUNT_NUMBER" => $orderId];
}
if(!$USER->GetID() && !$_SESSION['ORDER_ID']){
    $APPLICATION->IncludeComponent(
		"bitrix:system.auth.form",
		"main",
		Array(
			"AUTH_URL" => "/auth/",
			"REGISTER_URL" => "/auth/registration/?register=yes&backurl=/checkout/?ORDER_ID=".$orderId,
			"FORGOT_PASSWORD_URL" => "/auth/forgot-password/?forgot-password=yes&backurl=/checkout/?ORDER_ID=".$orderId,
			"PROFILE_URL" => "/personal/",
			"SHOW_ERRORS" => "Y",
			"POPUP_AUTH" => "N",
		)
	);
}else{
$parameters = [
    'filter' => $filter,
    'select' => ["ID"],
    'order' => ["DATE_INSERT" => "ASC"]
];

$dbRes = \Bitrix\Sale\Order::getList($parameters);
while ($order = $dbRes->fetch())
{
    $orderArr = $order;
}
echo "<script>
    var post = {};
    post['id'] = ".$orderArr['ID'].";
</script>";
if ($orderArr['ID']) {?><div class="row">
	<div class="col-md-5">
		<div class="checkout-left-block">
 <img alt="2021-03-03_09-48.png" src="/upload/medialibrary/fff/fff5cee12f18d8b2de5293eb2d3e0849.jpg" title="2021-03-03_09-48.png">
			<h4>Заказ №<a href="/personal/orders/<?=$orderArr['ID']?>"><?=$orderArr['ID']?></a> оформлен</h4>
			<p>
				 Благодарим Вас за оформление заказа в интернет-магазине "Магнолия"
			</p>
 <a class="btn" href="/">Вернуться на главную</a>
		</div>
	</div>
	<div class="col-md-7">
		<div class="checkout-right-block">
			<h4>Статус Вашего заказа:</h4>
			<div class="insert-container">
			</div>
			<div class="checkout-width-container">
 <a class="btn" href="/personal/">в личный кабинет</a>
			</div>
		</div>
	</div>
</div><?}else{
echo '<div class="row">
	<div class="col-xs-12">
		<h4>Заказ не найден =(</h4>
		<a href="/personal/">Перейти в личный кабинет</a>
	</div>
</div>';
}
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

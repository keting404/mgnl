<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "сок, кофе, чай, кафе");
$APPLICATION->SetPageProperty("description", "В Магнолии приняты самые высокие стандарты качества обслуживания и жесткие требования к качеству продукции");
$APPLICATION->SetPageProperty("title", "Магазины Магнолия");
$APPLICATION->SetTitle("Магазины");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH ."/css/bootstrap.css");
$APPLICATION->AddHeadScript('//api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=ad83d37c-243b-4fac-a475-b20119779199');
$APPLICATION->AddHeadScript('/magnoliaList.js');
$APPLICATION->AddHeadScript('/shops/yandexMap.js');
$APPLICATION->SetAdditionalCSS("//use.fontawesome.com/releases/v5.0.13/css/all.css");
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/magnoliaList.js');
?><?
// 		$APPLICATION->IncludeComponent(
// 			"bitrix:search.title", 
// 			"", 
// 			array(
// 				"CATEGORY_0" => array(
// 					0 => "iblock_MapFooter",
// 				),
// 				"CATEGORY_0_TITLE" => "",
// 				"CATEGORY_0_iblock_MapFooter" => array(
// 					0 => "7",
// 				),
// 				"CATEGORY_0_iblock_service" => array(
// 					0 => "all",
// 				),
// 				"CHECK_DATES" => "N",
// 				"CONTAINER_ID" => "title-search",
// 				"CONVERT_CURRENCY" => "N",
// 				"INPUT_ID" => "title-search-input",
// 				"NUM_CATEGORIES" => "1",
// 				"ORDER" => "date",
// 				"PAGE" => "/shops-2/",
// 				"PREVIEW_HEIGHT" => "75",
// 				"PREVIEW_TRUNCATE_LEN" => "",
// 				"PREVIEW_WIDTH" => "75",
// 				"PRICE_CODE" => array(
// 				),
// 				"PRICE_VAT_INCLUDE" => "Y",
// 				"SHOW_INPUT" => "Y",
// 				"SHOW_OTHERS" => "N",
// 				"SHOW_PREVIEW" => "Y",
// 				"TEMPLATE_THEME" => "blue",
// 				"TOP_COUNT" => "5",
// 				"USE_LANGUAGE_GUESS" => "N",
// 				"COMPONENT_TEMPLATE" => "titleShopSearch"
// 			),
// 			false
// 		);?>
<?
if (!empty($_GET["q"]))
    {
        $arrFilter = array(
            "?SEARCHABLE_CONTENT" => $_GET["q"],
        );
    }
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"shops2",
	Array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array("",""),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array("BUY",""),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE_404" => "",
		"FILTER_FIELD_CODE" => array("",""),
		"FILTER_NAME" => "arrFilter",
		"FILTER_PROPERTY_CODE" => array("YCAFE","METRO","ADDR","BYU",""),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "12",
		// "IBLOCK_TYPE" => "MapFooter",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array("",""),
		"LIST_PROPERTY_CODE" => array("YCAFE","METRO","X","Y","DESCRIPT","TYPE","ADDR","PHOTE","BYU",""),
		"MEDIA_PROPERTY" => "",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/shops/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("detail"=>"#ELEMENT_CODE#/","news"=>"","section"=>""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SLIDER_PROPERTY" => "",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N"
	)
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
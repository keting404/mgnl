ymaps.ready(init);

function init() {

	var myMap1 = new ymaps.Map("map", {
		center: [55.76, 37.64],
		zoom: 12,
		controls: ['smallMapDefaultSet']
		}, {
		searchControlProvider: 'yandex#search'
	});
	shops.list.forEach(function(row) {
		var myPlacemark = new ymaps.Placemark(row.coord.split(','), {
			baloon: myMap1.baloon,

		}, {
			preset: 'islands#redDotIcon',
			iconColor: '#ff0000'
		});
		myMap1.geoObjects.add(myPlacemark);
	});
	
	myMap1.setBounds(myMap1.geoObjects.getBounds(),{checkZoomRange:true, zoomMargin:9});

	var myMap = new ymaps.Map("indexMap", {
		center: [55.76, 37.64],
		zoom: 12,
		controls: ['geolocationControl', 'fullscreenControl', 'zoomControl']
		}, {
		searchControlProvider: 'yandex#search'
	}),
	metro, street, searchIn;
	myMap.setBounds(myMap1.geoObjects.getBounds(),{checkZoomRange:true, zoomMargin:9});
	//myMap.behaviors.disable('scrollZoom');
	$('#map').remove();

// START Preset ballons size & img
	ymaps.option.presetStorage.add('custom#default', {
		iconLayout: 'default#image',
		iconImageHref: 'map_pin_white1.svg',
		iconImageSize: [36, 40],
		iconImageOffset: [-18, -40],
		hideIconOnBalloonOpen: false
	});
	ymaps.option.presetStorage.add('custom#active', {
		iconLayout: 'default#image',
		iconImageHref: 'map_pin_white1.svg',
		iconImageSize: [36, 40],
		iconImageOffset: [-18, -40],
		hideIconOnBalloonOpen: false
	});
// END Preset ballons size & img

	function findClosestObjects() {
		searchIn.getClosestTo(metro.get(0)).balloon.open();
	}

	function findClosestObjectsStreet() {
		searchIn.getClosestTo(street.get(0)).balloon.open();
	}

// START Check items Магазины - Открыт/Закрыт/Все
	function checkItems() {
		if ($(".indexMapYandex").hasClass("active")) {
			var shownObjects,
				all = new ymaps.GeoQueryResult(),
				open = new ymaps.GeoQueryResult(),
				hide = new ymaps.GeoQueryResult(),
				type;
			all = cafe.search('options.all = "yes"');
			type = $(this).find("option:checked").val();
			if (type == "all") {
				open = cafe.search('options.all = "yes"');
				searchIn = open;
				hide = cafe.search('options.all = "no"');
			}
			if (type == "open") {
				open = cafe.search('options.itemType = "open"');
				searchIn = open;
				hide = cafe.search('options.itemType = "soon"');
			}
			if (type == "soon") {
				open = cafe.search('options.itemType = "soon"');
				searchIn = open;
				hide = cafe.search('options.itemType = "open"');
			}
			shownObjects = all.intersect(open).addToMap(myMap);
			cafe.remove(shownObjects).removeFromMap(myMap);
		}
		else {
			var type = $(this).find("option:checked").val();
			if (type == "all") {
				$(".indexMapListContent tbody tr").removeClass("hidden");
			}
			if (type == "open") {
				$(".indexMapListContent tbody tr[data-type='open']").removeClass("hidden");
				$(".indexMapListContent tbody tr[data-type='soon']").addClass("hidden");
			}
			if (type == "soon") {
				$(".indexMapListContent tbody tr[data-type='soon']").removeClass("hidden");
				$(".indexMapListContent tbody tr[data-type='open']").addClass("hidden");
			}
		}
	}
// END Check Items

	$(".indexMapItemType").change(checkItems);
	var shopsObjectsArray = [];
	var canBuyArr = [];
	$(shops.list).each(function (i) {
		var el = shops.list[i],
			img = el.img,
			metro = el.metro,
			metroColor = el.metroColor,
			url = el.link,
			coord = el.coord,
			itemType = el.type,
			addr = el.addr,
			phone = el.phone,
			canBuy = "," + (el.canBy).toString() + ",",
			options = "";
		//phone = phone.replace(";","<br />");
		var indexInArr = canBuyArr.indexOf(canBuy);
		if (indexInArr == (-1) && canBuy != ",,") {
			canBuyArr.push(canBuy);
		}
		$(el.option).each(function (i) {
			if (this != "") {
				options += "<span>" + this + "</span>";
			}
		});
		var obj = {
			type: "Feature",
			properties: {
				balloonContent: "<div class='mapBox'><div class='mapBoxImg'><img class='mapBoxImgTag' data-src='" + img + "' alt=''></div><div class='mapBoxInfo'><div class='mapBoxAddr'>" + addr + "</div><div class='mapBoxMetro " + metroColor + "'><i></i>" + metro + "</div><div class='mapBoxPhone'>" + phone + "</div><div class='mapBoxOptions'>" + options + "</div>"
                +"<a class='mapBoxLink' href='" + url + "' target='_blank'>Подробнее</a>"
                +"</div></div>"
			},
			geometry: {
				type: "Point",
				coordinates: eval("[" + coord + "]")
			},
			options: {
				preset: 'custom#default',
				balloonOffset: [0, -36],
				itemType: itemType,
				all: 'yes',
				canBuy: canBuy
			}
		};
		shopsObjectsArray.push(obj);
	});

	window.cafe = ymaps.geoQuery({
		type: 'FeatureCollection',
		features: shopsObjectsArray
	})
	.addEvents('balloonopen', function (e) {
		e.get('target').options.set('preset', {
			preset: 'custom#active'
		});
		var src = document.getElementsByClassName('mapBoxImgTag')[0].getAttribute('data-src');
		document.getElementsByClassName('mapBoxImgTag')[0].setAttribute('src', src);
		document.getElementsByClassName('mapBoxImgTag')[0].setAttribute('class', 'loaded mapBoxImgTag');
	})
	.addEvents('balloonclose', function (e) {
		e.get('target').options.set('preset', {
			preset: 'custom#default'
		});
	})
	.addToMap(myMap);

	$(".indexMapItemType").change();

	$(".indexMapMetro select").change(function () {
		if ($(".indexMapYandex").hasClass("active")) {
			if ($(this).find("option:selected").hasClass("def")) return false;
			var val = $(this).find("option:selected").val();
			metro = ymaps.geoQuery(ymaps.geocode(eval([val]), {kind: 'metro'})).then(findClosestObjects);
		}
		else {
			if ($(this).find("option:selected").hasClass("def")) {
				$(".indexMapListContent tbody tr").show();
				return false;
			}
			var val = $(this).find("option:selected").text().toLowerCase();
			$(".indexMapListContent tbody tr").each(function () {
				var metro = $(this).find(".metroStation").text().toLowerCase();
				if (val == metro) {
					$(this).show();
				}
				else {
					$(this).hide();
				}
			});
		}
	});

	function searchByMetro(label, point) {
		if ($(".indexMapYandex").hasClass("active")) {
			metro = ymaps.geoQuery(ymaps.geocode(eval([point]), {kind: 'metro'})).then(findClosestObjects);
		}
		else {
			if ($(".indexMapMetroInput").val() == "") {
				$(".indexMapListContent tbody tr").show();
				return false;
			}
			var val = label.toLowerCase();
			$(".indexMapListContent tbody tr").each(function () {
				var metro = $(this).find(".metroStation").text().toLowerCase();
				if (val == metro) {
					$(this).show();
				}
				else {
					$(this).hide();
				}
			});
		}
	}

	$("form.indexMapMetro").submit(function () {
		var val = $(".indexMapMetroInput").val().toLowerCase(),
			connect = false;
		$(mapSearchMetro).each(function (i) {
			var arrLabel = mapSearchMetro[i].label.toLowerCase();
			if (val == arrLabel) {
				connect = true;
				searchByMetro(mapSearchMetro[i].label, mapSearchMetro[i].point);
			}
		});
		return false;
	});

	$(".indexMapMetroInput").on("keyup change", function () {
		var val = $(".indexMapMetroInput").val().toLowerCase(),
			connect = false;
		$(mapSearchMetro).each(function (i) {
			var arrLabel = mapSearchMetro[i].label.toLowerCase();
			if (val == arrLabel) {
				connect = true;
				searchByMetro(mapSearchMetro[i].label, mapSearchMetro[i].point);
			}
		});
	});

	$("form.indexMapStreet").submit(function () {
		searchByStreet();
		return false;
	});
	$(".indexMapStreet input").change(function () {
		searchByStreet();
		return false;
	});
	$(".indexMapTabs span").click(function () {
		if ($(this).hasClass("active")) return false;
		$(".indexMapTabs span, .indexMapYandex, .indexMapList").toggleClass("active");
		$(".indexMapItemType").change();
		$(".indexMapMetro select").change();
	});

	$(".showOnMap").click(function () {
		$.scrollTo($(".indexMap"), 500);
		var point = $(this).data("point"),
			pointArray = eval("[" + $(this).data("point") + "]"),
			text = $(this).data("text");
		myMap.setCenter(pointArray, 13, "map");
		myMap.balloon.open(pointArray, text, {
			// Опция: не показываем кнопку закрытия.
			// closeButton: false
		});
		return false;
	});

	function searchShops(search) {
		var shownObjects,
			all = new ymaps.GeoQueryResult(),
			open = new ymaps.GeoQueryResult(),
			hide = new ymaps.GeoQueryResult();
		all = cafe.search('options.all = "yes"');
		canBuyArr.forEach(function(item, i, arr) {
			if (item.search("," + search + ",") != (-1)) {
				open = cafe.search('options.canBuy = "' + item + '"').add(open);
			}
		});
		shownObjects = all.intersect(open).addToMap(myMap);
		cafe.remove(shownObjects).removeFromMap(myMap);
	}

	// Заполнение поля поиск по карте и Перемотка страницы до блока с картой
	$(".searchShops").click(function () {
		var search = $(this).data("search");
		searchShops(search);
		$("html, body").scrollTo($(".indexMap"), 500);
		return false;
	});
	// Показ на карте метки согласно поисковой строке
	$(".poisk_na_karte").bind('click', function () {
		var clname = $(this).closest('tr'),
			namer = clname.find('.indexMapListName').text();
		$(".indexMapStreet input").val(namer);
		$.scrollTo($(".indexMap"), 500);
		searchByStreet();
	});
	function searchByStreet() {
		if ($(".indexMapYandex").hasClass("active")) {
			var val = $(".indexMapStreet input").val(),
				valLower = val.toLowerCase();
			if(
				valLower.search("красной армии")!=(-1)
				|| valLower.search("октябрьский проспект")!=(-1)
				|| valLower.search("мирная")!=(-1)
				|| valLower.search("юбилейная")!=(-1)
				|| valLower.search("боголюбова")!=(-1)
				|| valLower.search("молодёжная")!=(-1)
				|| valLower.search("октябрьская")!=(-1)
				|| valLower.search("октябрьская")!=(-1)
				|| valLower.search("ленина")!=(-1)
				|| valLower.search("авиационная")!=(-1)
				|| valLower.search("подольских курсантов")!=(-1)
				|| valLower.search("говорова")!=(-1)
				|| valLower.search("мая")!=(-1)
				|| valLower.search("юбилейный ")!=(-1)
				|| valLower.search("чикина")!=(-1)
				|| valLower.search("чикина")!=(-1)
				|| valLower.search("одинцово")!=(-1)
				|| valLower.search("реутов")!=(-1)
				|| valLower.search("балашиха")!=(-1)
				|| valLower.search("лобня")!=(-1)
				|| valLower.search("подольск")!=(-1)
				|| valLower.search("лыткарино")!=(-1)
				|| valLower.search("белоозерский")!=(-1)
				|| valLower.search("дубна")!=(-1)
				|| valLower.search("щербинка")!=(-1)
				|| valLower.search("люберцы")!=(-1)
				|| valLower.search("сергеев")!=(-1)
			) {
				street = ymaps.geoQuery(ymaps.geocode("" + val)).then(findClosestObjectsStreet);
			}
			else {
				street = ymaps.geoQuery(ymaps.geocode("Москва " + val)).then(findClosestObjectsStreet);
			}
			return false;
		}
		else {
			var val = $(".indexMapStreet input").val().toLowerCase();
			$(".indexMapListContent tbody tr").each(function () {
				var street = $(this).find(".indexMapListName").text().toLowerCase();
				if (street.search(val) != (-1)) {
					$(this).show();
				}
				else {
					$(this).hide();
				}
			});
			return false;
		}
	}
}

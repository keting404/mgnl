<?php

use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\SystemException;
use \Bitrix\Sale\Order;
use \Bitrix\Sale\PaySystem;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define('NOT_CHECK_PERMISSIONS', true);
define("DisableEventsCheck", true);


global $APPLICATION;

if (CModule::IncludeModule("sale"))
{
	$context = Application::getInstance()->getContext();
	$request = $context->getRequest();

	$item = PaySystem\Manager::searchByRequest($request);

	if ($item !== false)
	{
		
		$service = new PaySystem\Service($item);
		if ($service instanceof PaySystem\Service)
		{
			$result = $service->processRequest($request);?>
			<div id="bx_incl_area_26_1_1">
				<div class="row col-md-12 col-sm-12">
					<a class="sale-order-history-link" href="/personal/orders/">
				Перейти к текущим заказам			</a>
				</div>
			<div class="row col-md-12 col-sm-12">
			<a href="/catalog/" class="sale-order-history-link">
				Перейти в каталог			</a>
		</div>
			<div class="clearfix"></div>
			<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderList.init({'url':'/bitrix/components/bitrix/sale.personal.order.list/ajax.php','templateFolder':'/bitrix/components/bitrix/sale.personal.order.list/templates/.default','templateName':'','paymentList':[]});
		</script>
		</div>

		<?}
	}
	else
	{
		$debugInfo = implode("\n", $request->toArray());
		PaySystem\Logger::addDebugInfo('Pay system not found. Request: '.$debugInfo);
	}
}

// $APPLICATION->FinalActions();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
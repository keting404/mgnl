<?
namespace Eutils\Mtscashback\Events;

use Bitrix\Sale;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

class MtsMain {

	public function sendRequest ($options,$order,$arOrderVals,$propertyCollection) {
        
        //если кешбек по заказу уже начислен то false
        foreach ($propertyCollection as $obProp) {
            $arProp = $obProp->getProperty();
//             if(in_array($arProp["CODE"], ["EUTILS_MTSCASHBACK_CASHBACK"])) {
//                     if ($obProp->getValue()>0)
//                         return false;
//             }
            if(in_array($arProp["CODE"], ["EUTILS_MTSCASHBACK_SUBSID"])) {
                    if(!$CustomerHash = $obProp->getValue())
                        return false;
            }
        }
        if($options['paysystems'])
            if(!in_array($arOrderVals["PAY_SYSTEM_ID"],explode(",", $options['paysystems'])))
                return false;
        $price = $arOrderVals["PRICE"];
        if($options['cashback_without_delivery'] == "Y")
            $price = $price - $order->getDeliveryPrice();
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer '.$options['token'];
        $headers[] = 'Connection: keep-alive';
        $array = array(
            'RequestDate' => date('c'),
            'AttributeVersion' => $options['attribute_version'],
            'IDTrans' => $arOrderVals["ID"],
            'PartnerCode' => $options['partner_code'],
            'ProductCode' => $options['product_code'],
            'EventDate' => date('c'),
            'Amount' => $price,
            'StatusUpdateDate' => date('c'),
            'Partner' => '',
            'Filename' => '',
            'Source' => '',
            'CustomerHash' => $CustomerHash,
            'Status' => 'Approved'
        );
        $ch = curl_init($options['url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $html = curl_exec($ch);
        curl_close($ch);
        $result = \Bitrix\Main\Web\Json::decode($html);
		
		foreach ($propertyCollection as $obProp) {
            $arProp = $obProp->getProperty();
            if(in_array($arProp["CODE"], ["EUTILS_MTSCASHBACK_CASHBACK"])) {
                if ($result['ChargeAmount'])
                    $obProp->setValue($result['ChargeAmount']);
            }
        }
        $saveResult = $order->getPropertyCollection()->save();
        if (!$saveResult->isSuccess()) {
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/log/mtscashback.log', date('[d-m-Y H:i] ') . print_r("Order: ".$arOrderVals["ID"].implode('; ', $saveResult->getErrorMessages()), true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
        if($options['log_on'] == "Y")
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/log/mtscashback.log', date('[d-m-Y H:i] ') . print_r("request: ".json_encode($array)." url: ".$options['url']." answer: ".$html, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
	}
	
    public function getOptions(): array
    {   
        if(Option::get("eutils.mtscashback", "test_on", "") == "Y"){
            $token = Option::get("eutils.mtscashback", "token_test", "");
            $url = 'https://api.mts.ru/Test-CashbackTarifficationAPI/3.7.0/Cashback';
        }else{
            $token = Option::get("eutils.mtscashback", "token", "");
            $url = 'https://api.mts.ru/CashbackTarrificationAPI/3.7.0/Cashback';
        }
        return $options = array(
                'token' => $token,
                'switch_on' => Option::get("eutils.mtscashback", "switch_on", ""),
                'test_on' => Option::get("eutils.mtscashback", "test_on", ""),
                'log_on' => Option::get("eutils.mtscashback", "log_on", ""),
                'cookie_lifetime' => Option::get("eutils.mtscashback", "cookie_lifetime", ""),
                'cashbak_on' => Option::get("eutils.mtscashback", "cashbak_on", ""),
                'is_payed' => Option::get("eutils.mtscashback", "is_payed", ""),
                'attribute_version' => Option::get("eutils.mtscashback", "attribute_version", ""),
                'partner_code' => Option::get("eutils.mtscashback", "partner_code", ""),
                'product_code' => Option::get("eutils.mtscashback", "product_code", ""),
                'status_id' => Option::get("eutils.mtscashback", "status_id", ""),
                'paysystems' => Option::get("eutils.mtscashback", "paysystems", ""),
                'cookie_type' => Option::get("eutils.mtscashback", "cookie_type", ""),
                'cashback_without_delivery' => Option::get("eutils.mtscashback", "cashback_without_delivery", ""),
                'url' => $url
            );
    }
    
    function OnBeforeEndBufferContent()
	{
        $options = self::getOptions();
        if ($options['switch_on'] != "Y")
            return false;
        if ($options['cookie_lifetime']){
            $timelife = time()+floatval($options['cookie_lifetime']);
        }else{
            $timelife = time()+60*60*24*1;
        }
        if ($options['cookie_type'] == "Y" && $_GET['utm_source'] == "mts"){
            if ($_GET['utm_source'])
                setcookie("mts_utm_source",$_GET['utm_source'],$timelife,"/");
            if ($_GET['utm_term'])
                setcookie("mts_utm_term",$_GET['utm_term'],$timelife,"/");
            if ($_GET['subid'])
                setcookie("subid",$_GET['subid'],$timelife,"/");
        }else{
            if ($_GET['utm_source']){
                setcookie("subid","",time()-3600,"/");
                setcookie("utm_source",$_GET['utm_source'],$timelife,"/");
                }
            if ($_GET['utm_term'])
                {
                setcookie("subid","",time()-3600,"/");
                setcookie("utm_term",$_GET['utm_term'],$timelife,"/");
                }
            if ($_GET['subid'])
                setcookie("subid",$_GET['subid'],$timelife,"/");
        }
        
	}

	function OnSaleComponentOrderCreated($order)
	{
        $options = self::getOptions();
        if (
            $options['switch_on'] != "Y" ||
            ($_COOKIE['utm_source'] != 'mts' && $_COOKIE['mts_utm_source'] != 'mts')
        )
            return false;
            
        $isNew = $order->isNew();
        $arOrderVals = $order->getFields()->getValues();
        $propertyCollection = $order->getPropertyCollection();
//         $discountCollection = $order->getDiscount();
        if($isNew == '1'){
            foreach ($propertyCollection as $obProp) {
                $arProp = $obProp->getProperty();
                $arProps[] = $arProp;
                if(in_array($arProp["CODE"], ["EUTILS_MTSCASHBACK_CHANNEL"]))
                    $obProp->setValue($_COOKIE['mts_utm_term'] ? $_COOKIE['mts_utm_term'] : $_COOKIE['utm_term']);
                if(in_array($arProp["CODE"], ["EUTILS_MTSCASHBACK_SUBSID"])) 
                    $obProp->setValue($_COOKIE['subid']);
            }
        }
	}

	function OnSaleStatusOrderChange($order)
	{
        $options = self::getOptions();
        if ($options['switch_on'] != "Y" || 
            $options['cashbak_on'] != "status_change"
        )
            return false;
        
        $isNew = $order->isNew();
        $arOrderVals = $order->getFields()->getValues();
        $propertyCollection = $order->getPropertyCollection();
//         $discountCollection = $order->getDiscount();
        if($arOrderVals['STATUS_ID'] != $options['status_id'])
            return false;
        if($options['is_payed'] == 'Y')
            if($arOrderVals["PAYED"] != 'Y')
                return false;
        
        if($isNew != '1')
            self::sendRequest($options,$order,$arOrderVals,$propertyCollection);
		
	}

	function OnSaleOrderPaid($order)
	{
        $isNew = $order->isNew();
        $arOrderVals = $order->getFields()->getValues();
        $propertyCollection = $order->getPropertyCollection();
//         $discountCollection = $order->getDiscount();
        $options = self::getOptions();
        if (
            ($options['switch_on'] != "Y" ||
            $options['cashbak_on'] != "payed")
        )
            if ($arOrderVals['STATUS_ID'] != $options['status_id'] && $options['is_payed'] == 'Y')
                return false;
            
        if($arOrderVals["PAYED"] != 'Y')
            return false;
            
		if($isNew != '1'){
            self::sendRequest($options,$order,$arOrderVals,$propertyCollection);
		}
	}
    
}

<?

use Bitrix\Sale;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);
$saleStatusIterator = CSaleStatus::GetList(array("SORT" => "ASC"), array(), false, false, array("ID","NAME"));
         while ($row = $saleStatusIterator->Fetch()) {
             $statuses[$row['ID']] = $row['NAME'];
         }
$db_ptype = CSalePaySystem::GetList($arOrder = Array("ID"=>"ASC", "PSA_NAME"=>"ASC"), Array("ACTIVE"=>"Y"));
while ($ptype = $db_ptype->Fetch())
{
   $paySystems[$ptype['ID']] = $ptype['NAME'];
}
$aTabs = array(
    array(
        "DIV"     => "edit",
        "TAB"     => Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_NAME"),
        "TITLE"   => Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_NAME"),
        "OPTIONS" => array(
            Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_COMMON"),
            array(
                "switch_on",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_SWITCH_ON"),
                "N",
                array("checkbox")
            ),
            array(
                "test_on",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_TESTVAR"),
                "Y",
                array("checkbox")
            ),
            array(
                "log_on",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_LOG_ENABLED"),
                "N",
                array("checkbox")
            ),
            array(
                "token",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_TOKEN_IMPORTANT"),
                "",
                array("text", 35)
            ),
            array(
                "token_test",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_TOKEN_TEST"),
                "",
                array("text", 35)
            ),
            array(
                "attribute_version",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_ATTRIBUTE"),
                "",
                array("text", 35)
            ),
            array(
                "partner_code",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_PARTNER"),
                "",
                array("text", 35)
            ),
            array(
                "product_code",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_PRODUCT"),
                "",
                array("text", 35)
            ),
            array(
                "cookie_type",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_COOKIE_TYPE"),
                "N",
                array("checkbox")
            ),
            array(
                "cookie_lifetime",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_COOKIE"),
                "86400",
                array("text", 35)
            ),
            Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_COND"),
            array(
                "cashbak_on",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_SEND"),
                "status_change",
                array("selectbox", array(
                    "status_change"  => Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_STATUS"),
                    "payed" => Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_PAYED")
                    ))
            ),
            array(
                "status_id",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_STATUS_ID"),
                "F",
                array("selectbox", $statuses)
            ),
            array(
                "paysystems",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_PAY_ID"),
                null,
                array("multiselectbox", $paySystems)
            ),
            array(
                "cashback_without_delivery",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_NO_DELIVERY"),
                "N",
                array("checkbox")
            ),
            array(
                "is_payed",
                Loc::getMessage("EUTILS_MTSCASHBACK_OPTIONS_TAB_IS_PAYED"),
                "Y",
                array("checkbox")
            ),
       )
   )
);
if($request->isPost() && check_bitrix_sessid()){

    foreach($aTabs as $aTab){

       foreach($aTab["OPTIONS"] as $arOption){

           if(!is_array($arOption)){

               continue;
           }

           if($arOption["note"]){

                continue;
           }

           if($request["apply"]){

                $optionValue = $request->getPost($arOption[0]);

              if($arOption[0] == "switch_on" || $arOption[0] == "test_on" || $arOption[0] == "is_payed" || $arOption[0] == "cookie_type" || $arOption[0] == "cashback_without_delivery"){

                  if($optionValue == ""){

                       $optionValue = "N";
                   }
               }

               Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
            }elseif($request["default"]){

             Option::set($module_id, $arOption[0], $arOption[2]);
            }
       }
   }

   LocalRedirect($APPLICATION->GetCurPage()."?mid=".$module_id."&lang=".LANG);
}
$tabControl = new CAdminTabControl(
  "tabControl",
 $aTabs
);

$tabControl->Begin();?>
<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post">

  <?
   foreach($aTabs as $aTab){

       if($aTab["OPTIONS"]){

         $tabControl->BeginNextTab();

         __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
      }
   }?>
   <?$tabControl->Buttons();
  ?>

   <input type="submit" name="apply" value="<? echo(Loc::GetMessage("EUTILS_MTSCASHBACK_OPTIONS_INPUT_APPLY")); ?>" class="adm-btn-save" />
    <input type="submit" name="default" value="<? echo(Loc::GetMessage("EUTILS_MTSCASHBACK_OPTIONS_INPUT_DEFAULT")); ?>" />

   <?
   echo(bitrix_sessid_post());
 ?>

</form>
<?$tabControl->End();?>

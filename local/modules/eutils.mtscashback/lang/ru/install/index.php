<?
$MESS["EUTILS_MTSCASHBACK_NAME"]         = "МТС кешбек";
$MESS["EUTILS_MTSCASHBACK_DESCRIPTION"]  = "Отправляет информацию о совершенных покупках при переходе с сервиса https://cashback.mts.ru/";
$MESS["EUTILS_MTSCASHBACK_PARTNER_NAME"] = "Easy Utils";
$MESS["EUTILS_MTSCASHBACK_PARTNER_URI"]  = "";
$MESS["EUTILS_MTSCASHBACK_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["EUTILS_MTSCASHBACK_INSTALL_TITLE"]      = "Установка модуля";
$MESS["EUTILS_MTSCASHBACK_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["EUTILS_MTSCASHBACK_STEP_BEFORE"]        = "Модуль";
$MESS["EUTILS_MTSCASHBACK_STEP_AFTER"]         = "установлен";
$MESS["EUTILS_MTSCASHBACK_UNSTEP_AFTER"]       = "удален";
$MESS["EUTILS_MTSCASHBACK_STEP_SUBMIT_BACK"]   = "Вернуться в список";
?>

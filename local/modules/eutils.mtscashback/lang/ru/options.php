<?
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_NAME"]             = "Настройки";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_COMMON"]          = "Общие";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_COND"]          = "Условия";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_SWITCH_ON"]       = "Модуль включен";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_TOKEN_IMPORTANT"] = "Токен (продуктовая среда)";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_TOKEN_TEST"] = "Токен (тестовая среда)";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_ATTRIBUTE"] = "AttributeVersion";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_PARTNER"] = "PartnerCode";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_PRODUCT"] = "ProductCode";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_TESTVAR"] = "Режим теста";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_LOG_ENABLED"] = "Писать лог";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_COOKIE_TYPE"] = "<b>Уникальные cookie</b> (если включено - информация с сылки МТС будет записываться в свою куку (mts_utm_**) и жить указаное количество времени а запрос на кешбек будет отправлен на все покупки в течении жизни cookie, если отключено - запись произведется в общую cookie utm_** и перезаписываться при переходе с других источников (например рекламы), в таком случае при последующем переходе на сайт с другого источника с utm метками - запрос на кешбек отправлен не будет)";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_COOKIE"] = "Время жизни utm-куки (сек)";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_SEND"] = "Отправлять запрос при";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_STATUS"] = "Смене статуса заказа";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_WNEN_PAYED"] = "Получении оплаты";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_STATUS_ID"] = "Статус заказа (если ранее выбрано при смене статуса)";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_PAY_ID"] = "Платежные системы для кешбека";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_IS_PAYED"] = "Проверять, оплачен ли заказ";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_TAB_NO_DELIVERY"] = "Сумма Cashbak без учета доставки";

$MESS["EUTILS_MTSCASHBACK_OPTIONS_INPUT_APPLY"]   = "Применить";
$MESS["EUTILS_MTSCASHBACK_OPTIONS_INPUT_DEFAULT"] = "По умолчанию";

?>

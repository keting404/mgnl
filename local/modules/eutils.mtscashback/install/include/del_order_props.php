<?
		if (CModule::IncludeModule("sale"))
			{
				//Delette props
				$db_props = CSaleOrderProps::GetList(
					array("ID" => "ASC"),
					array("CODE" => array('EUTILS_MTSCASHBACK_CHANNEL', 'EUTILS_MTSCASHBACK_SUBSID', 'EUTILS_MTSCASHBACK_CASHBACK')),
					false,
					false,
					array()
				);
					while ($props = $db_props->Fetch())
					{
						CSaleOrderProps::Delete($props["ID"]);
					}
				$PropsGroupID = array();
					while ($props = $db_props->Fetch())
					{
						$PropsGroupID[] = $props["PROPS_GROUP_ID"];
						CSaleOrderProps::Delete($props["ID"]);
					}
				//Delette groups of Bonus
				foreach($PropsGroupID as $PropGroup) {
					CSaleOrderPropsGroup::Delete($PropGroup);
				}
			}
?>

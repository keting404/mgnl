<?
$MESS["EUTILS_YAGO_OPTIONS_TAB_NAME"]             = "Настройки";
$MESS["EUTILS_YAGO_OPTIONS_TAB_COMMON"]          = "Общие";
$MESS["EUTILS_YAGO_OPTIONS_TAB_COND"]          = "Условия";
$MESS["EUTILS_YAGO_OPTIONS_TAB_SWITCH_ON"]       = "Модуль включен";
$MESS["EUTILS_YAGO_OPTIONS_TAB_TOKEN"] = "Токен";
$MESS["EUTILS_YAGO_OPTIONS_TAB_CALLBACK_TOKEN"] = "Токен для обращений на сайт (задать самостоятельно строку любого формата)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_COURIER_W"] = "Максимальный вес для курьера, гр.";
$MESS["EUTILS_YAGO_OPTIONS_TAB_EXPRESS_W"] = "Максимальный вес для такси (больше - уже карго), гр.";
$MESS["EUTILS_YAGO_OPTIONS_TAB_HTTPS_ON"] = "сайт работает по https";
$MESS["EUTILS_YAGO_OPTIONS_TAB_LOG_ENABLED"] = "Писать лог";
$MESS["EUTILS_YAGO_OPTIONS_TAB_PVZ_ON"] = "Режим доставки в ВПЗ (в случае выбора самовывоза)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_PICKUP_NAME"] = "Название магазина";
$MESS["EUTILS_YAGO_OPTIONS_TAB_EMERGENCY_NAME"] = "Контакт для связи (имя)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_EMERGENCY_PHONE"] = "Контакт для связи (телефон, с +7)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_STATUS_ID"] = "Статус заказа";
$MESS["EUTILS_YAGO_OPTIONS_TAB_PAY_ID"] = "Платежная система";
$MESS["EUTILS_YAGO_OPTIONS_TAB_DEL_ID"] = "Служба доставки";
$MESS["EUTILS_YAGO_OPTIONS_TAB_DISTANCE_MIN"] = "Дистанция доставки ОТ (метров)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_DISTANCE_MAX"] = "Дистанция доставки ДО (метров)";
$MESS["EUTILS_YAGO_OPTIONS_TAB_REGION_ID"] = "ID регионов работы доставки через запятую (из модуля <a href='/bitrix/admin/settings.php?lang=ru&mid=ctweb.yandexdelivery&mid_menu=1'>Стоимости доставки по зонам</a>). Пустое значение - везде";

$MESS["EUTILS_YAGO_OPTIONS_INPUT_APPLY"]   = "Применить";
$MESS["EUTILS_YAGO_OPTIONS_INPUT_DEFAULT"] = "По умолчанию";

?>

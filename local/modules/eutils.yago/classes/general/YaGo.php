<?
use Bitrix\Sale;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

class YaGo {

	public function sendRequest ($options,$order,$arOrderVals,$propertyCollection) {
        $basketWeight = floatval($order->getBasket()->getWeight());
        //если курьер по заказу уже назанчен то false
        foreach ($propertyCollection as $obProp) {
            $arProp = $obProp->getProperty();
            $arProps[$arProp["CODE"]]["VALUE"] = $obProp->getFields()->getValues();
        }
        
        if($options['delsystems'])
            if(!in_array($arOrderVals["DELIVERY_ID"],explode(",", $options['delsystems'])))
                return false;
        if($options['paysystems'])
            if(!in_array($arOrderVals["PAY_SYSTEM_ID"],explode(",", $options['paysystems'])))
                return false;
        if($options["distance_min"])
            if(floatval($arProps["DISTANCE"]["VALUE"]["VALUE"]) < floatval($options["distance_min"]))
                return false;
        if($options["distance_max"])
            if(floatval($arProps["DISTANCE"]["VALUE"]["VALUE"]) > floatval($options["distance_max"]))
                return false;
        if($arProps["PARTNER_DELIVERY"]["VALUE"]["VALUE"] != "Y")
            return false;
        $regionsOption = explode(",", $options['region_id']);
        if($options['region_id'] && !in_array($arProps["REGION_ID"]["VALUE"]["VALUE"],explode(",", $options['region_id'])))
            return false;
            
        //магазин
        $arStoreFrom = \Bitrix\Catalog\StoreTable::getList(array(
            'filter'=>array('=ID'=>$arProps["STORE_ID"]["VALUE"]["VALUE"]),
            'select'=>array('*','UF_*'),
        ))->fetch();

        //доставка в ПВЗ коллекция
        if($options['pvz_on'] == "Y"){
            $shipmentCollection = $order->getShipmentCollection();
            foreach ($shipmentCollection as $ship) {
                $store_id = $ship->getStoreId();//ID склада
                if ($store_id) {
                    break;
                }
            }
            if($store_id && $store_id != $arProps["STORE_ID"]["VALUE"]["VALUE"]){
                $arStore = \Bitrix\Catalog\StoreTable::getList(array(
                    'filter'=>array('=ID'=>$store_id),
                    'select'=>array('*','UF_*'),
                ))->fetch();
            }
            if($arStore){
                $arProps["ADDRESS"]["VALUE"]["VALUE"] = $arStore['ADDRESS'];
                $arProps["D_LONG"]["VALUE"]["VALUE"] = $arStore['GPS_N'];
                $arProps["D_LAT"]["VALUE"]["VALUE"] = $arStore['GPS_S'];
                $arProps["PODEZD"]["VALUE"]["VALUE"] = "";
                $arProps["ROOM"]["VALUE"]["VALUE"] = "";
                $arProps["FLOOR"]["VALUE"]["VALUE"] = "";
                $arOrderVals['USER_DESCRIPTION'] = "Доставка в пункт выдачи";
                $arProps["EMAIL"]["VALUE"]["VALUE"] = $arStore['EMAIL'];
                $arProps["FIO"]["VALUE"]["VALUE"] = "Магазин ".$arStore['TITLE'];
                $arProps["PHONE"]["VALUE"]["VALUE"] = $arStore['PHONE'];
            }
        }
        
        if($basketWeight <= floatval($options["courier_weight"])){
                    $type = "courier";
                }else if($basketWeight <= floatval($options["express_weight"])){
                    $type = "express";
                }else if ($basketWeight <= 1400000){
                    $type = "cargo";
                }
        $client_requirements = array(
                    "taxi_class" => $type
                );
        if ($basketWeight > floatval($options["express_weight"])){
            if($basketWeight <= 300000){
                            $cargoType = "van";
                        }else if($basketWeight <= 700000){
                            $cargoType = "lcv_m";
                        }else if ($basketWeight <= 1400000){
                            $cargoType = "lcv_l";
                        }
        }
        if($cargoType)
            $client_requirements["cargo_type"] = $cargoType;
        
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: Bearer '.$options['token'];
        $headers[] = 'Connection: keep-alive';
        $array = array(
            "callback_properties" =>
                [
                    "callback_url" => $options['http'].SITE_SERVER_NAME.'/statusy-dostavki/callbackscript.php?app=yago&'
                ],
            "client_requirements" => $client_requirements,
            "comment" => $options["pickup_name"],
            "emergency_contact" => [
                    "name" => $options['emergency_name'],
                    "phone" => $options['emergency_phone']
                ],
            "items" => [
                    array(
                        "cost_currency" => "RUB",
                        "cost_value" => $order->getPrice(),
                        "droppof_point" => 2,
                        "pickup_point" => 1,
                        "quantity" => 1,
                        "title" => "Продукты",
                        "weight" => $basketWeight / 1000
                    ),
                ],
            "optional_return" => false,
            "route_points" => [
                    array(
                        "address" => array(
                            "comment" => "Доставка из магазина ".$arStoreFrom["TITLE"].".
Сообщите менеджеру, что заказ по доставке Яндекс.Такси.
Назовите номер заказа ".$arOrderVals["ID"]." и заберите посылку.
Об оплате заказа уточните в магазине при получении заказа.",
                            "coordinates" => [
                                $arStoreFrom["GPS_N"],
                                $arStoreFrom["GPS_S"]
                            ],
                            "fullname" => $arStoreFrom['ADDRESS'],
                        ),
                        "contact" => array(
                            "email" => $arStoreFrom["EMAIL"],
                            "name" => "Магазин ".$arStoreFrom['TITLE'],
                            "phone" => $arStoreFrom["PHONE"]
                        ),
                        "point_id" => 1,
                        "skip_confirmation" => true,
                        "type" => "source",
                        "visit_order" => 1
                    ),
                     array(
                        "address" => array(
                            "comment" => $arOrderVals['USER_DESCRIPTION'],
                            "coordinates" => [
                                $arProps["D_LONG"]["VALUE"]["VALUE"],
                                $arProps["D_LAT"]["VALUE"]["VALUE"]
                            ],
                            "fullname" => $arProps["ADDRESS"]["VALUE"]["VALUE"],
                            "porch" => $arProps["PODEZD"]["VALUE"]["VALUE"],
                            "sflat" => $arProps["ROOM"]["VALUE"]["VALUE"],
                            "sfloor" => $arProps["FLOOR"]["VALUE"]["VALUE"],
                        ),
                        "contact" => array(
                            "email" => $arProps["EMAIL"]["VALUE"]["VALUE"],
                            "name" => $arProps["FIO"]["VALUE"]["VALUE"],
                            "phone" => $arProps["PHONE"]["VALUE"]["VALUE"]
                        ),
                        "external_order_id" => $arOrderVals["ID"],
                        "payment_on_delivery" => array(
                            "client_order_id" => $arOrderVals["ID"],
                            "cost" => $order->getPrice(),
                            "currency" => "RUB",
                            "customer" => [
                                "email" => $arProps["EMAIL"]["VALUE"]["VALUE"],
                                "full_name" => $arProps["FIO"]["VALUE"]["VALUE"],
                                "phone" => $arProps["PHONE"]["VALUE"]["VALUE"]
                            ],
                            "tax_system_code" => 1
                        ),
                        "point_id" => 2,
                        "skip_confirmation" => true,
                        "type" => "destination",
                        "visit_order" => 2
                    ),
                ],
            "referral_source" => "bitrix",
            "skip_act" => false,
            "skip_client_notify" => false,
            "skip_door_to_door" => false,
            "skip_emergency_notify" => true
        );
//         $ch = curl_init($options['url_request'].md5($arOrderVals["ID"].$arProps["EUTILS_YAGO_ISORDERED"]["VALUE"]["VALUE"]));
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array, JSON_UNESCAPED_UNICODE));
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//         // curl_setopt($ch, CURLOPT_HEADER, 1);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//          
//         $html = curl_exec($ch);
//         curl_close($ch);
        $html = '{
  "available_cancel_state": "free",
  "callback_properties": {
    "callback_url": "https://www.example.com"
  },
  "carrier_info": {
    "address": "г. Цветочный, ул. Незабудковая, д.1",
    "inn": "123456789",
    "name": "ООО Ромашка"
  },
  "client_requirements": {
    "cargo_loaders": 0,
    "cargo_options": "thermobag",
    "cargo_type": "lcv_m",
    "taxi_class": "express"
  },
  "comment": "Ресторан",
  "corp_client_id": "cd8cc018bde34597932855e3cfdce927",
  "created_ts": "2020-01-01T00:00:00+00:00",
  "current_point_id": 6987,
  "due": "2020-01-01T00:00:00+00:00",
  "emergency_contact": {
    "name": "Рик",
    "phone": "+79099999999"
  },
  "error_messages": [
    {
      "code": "some_error",
      "message": "Some error"
    }
  ],
  "eta": 10,
  "id": "741cedf82cd464fa6fa16d87155c636",
  "items": [
    {
      "cost_currency": "RUB",
      "cost_value": "2.00",
      "droppof_point": 2,
      "extra_id": "БП-208",
      "pickup_point": 1,
      "quantity": 1,
      "size": {
        "height": 0.1,
        "length": 0.1,
        "width": 0.1
      },
      "title": "Плюмбус",
      "weight": 2
    }
  ],
  "matched_cars": [
    {
      "cargo_loaders": 0,
      "cargo_type": "lcv_m",
      "cargo_type_int": "2 is equal to \"lcv_m\"",
      "client_taxi_class": "cargo",
      "door_to_door": false,
      "taxi_class": "express"
    }
  ],
  "optional_return": false,
  "performer_info": {
    "car_color": "красный",
    "car_color_hex": "FF00000",
    "car_model": "Hyundai Solaris",
    "car_number": "А100РА100",
    "courier_name": "Личность",
    "legal_name": "ИП Птичья личность",
    "transport_type": "car"
  },
  "pricing": {
    "currency": "RUB",
    "currency_rules": {
      "code": "RUB",
      "sign": "₽",
      "template": "$VALUE$ $SIGN$$CURRENCY$",
      "text": "руб."
    },
    "final_price": "12.50",
    "offer": {
      "offer_id": "28ae5f1d72364468be3f5e26cd6a66bf",
      "price": "12.50",
      "price_raw": 12,
      "valid_until": "2020-01-01T00:00:00+00:00"
    }
  },
  "revision": 1,
  "route_points": [
    {
      "address": {
        "building": "23к1А",
        "city": "Санкт-Петербург",
        "comment": "Домофон не работает",
        "country": "Российская Федерация",
        "description": "Одинцово, Московская область, Россия",
        "door_code": "169",
        "flat": 1,
        "floor": 1,
        "fullname": "Санкт-Петербург, Большая Монетная улица, 1к1А",
        "porch": "A",
        "sflat": "1",
        "sfloor": "1",
        "shortname": "Большая Монетная улица, 1к1А",
        "street": "Большая Монетная улица",
        "uri": "ymapsbm1://geo?ll=38.805%2C55.084"
      },
      "contact": {
        "email": "morty@yandex.ru",
        "name": "Морти",
        "phone": "+79099999998"
      },
      "external_order_id": "100",
      "id": 1,
      "pickup_code": "893422",
      "skip_confirmation": false,
      "type": "source",
      "visit_order": 1,
      "visit_status": "pending"
    },
    {
      "address": {
        "building": "23к1А",
        "city": "Санкт-Петербург",
        "comment": "Домофон не работает",
        "country": "Российская Федерация",
        "description": "Одинцово, Московская область, Россия",
        "door_code": "169",
        "flat": 1,
        "floor": 1,
        "fullname": "Санкт-Петербург, Большая Монетная улица, 1к1А",
        "porch": "A",
        "sflat": "1",
        "sfloor": "1",
        "shortname": "Большая Монетная улица, 1к1А",
        "street": "Большая Монетная улица",
        "uri": "ymapsbm1://geo?ll=38.805%2C55.084"
      },
      "contact": {
        "email": "morty@yandex.ru",
        "name": "Морти",
        "phone": "+79099999998"
      },
      "external_order_id": "100",
      "id": 1,
      "pickup_code": "893422",
      "skip_confirmation": false,
      "type": "destination",
      "visit_order": 1,
      "visit_status": "pending"
    }
  ],
  "skip_act": false,
  "skip_client_notify": false,
  "skip_door_to_door": false,
  "skip_emergency_notify": false,
  "status": "new",
  "taxi_offer": {
    "offer_id": "28ae5f1d72364468be3f5e26cd6a66bf",
    "price": "12.50",
    "price_raw": 12
  },
  "updated_ts": "2020-01-01T00:00:00+00:00",
  "warnings": [
    {
      "code": "not_fit_in_car",
      "message": "Предупреждение",
      "source": "client_requirements"
    }
  ]
}';
        $result = json_decode($html);
		
        if($options['log_on'] == "Y")
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/log/yago.log', date('[d-m-Y H:i] ') . print_r("request: ".json_encode($array, JSON_UNESCAPED_UNICODE)." url: ".$options['url_request'].md5($arOrderVals["ID"].$arProps["EUTILS_YAGO_ISORDERED"]["VALUE"]["VALUE"])." answer: ", true) . print_r($html, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        
        CModule::IncludeModule("iblock");
        
        $res = CIBlock::GetList(
            Array(), 
            Array(
                'TYPE'=>'eutils_yandex_go',
                "!CODE"=>'yandex_go_deliverys'
            ), true
        );
        while($ar_res = $res->Fetch())
        {
            $iblockId = $ar_res['ID'];
        }
        
        $el = new CIBlockElement;

        $PROP = array();
        $fullName = 'Доставка Яндекс.GO заказа #'.$arOrderVals['ID'];
        $PROP['ORDER_ID'] = $arOrderVals['ID'];
        $PROP['CLAIM_ID'] = $result->id;
        $PROP['eta'] = $result->eta;
        $PROP['status'] = $result->status;
        $PROP['PICKUP'] = $result->route_points[0]->address->fullname;
        $PROP['DESTINATION'] = $result->route_points[1]->address->fullname;
        //транслит симв кода
        $params = Array(
            "max_len" => "100", // обрезает символьный код до 100 символов
            "change_case" => "L", // буквы преобразуются к нижнему регистру
            "replace_space" => "-", // меняем пробелы на нижнее подчеркивание
            "replace_other" => "-", // меняем левые символы на нижнее подчеркивание
            "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
            "use_google" => "false", // отключаем использование google
            ); 
            
        $arEventFields = Array(
            "IBLOCK_ID"      => $iblockId,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $fullName,
            "CODE" => CUtil::translit($fullName, "ru" , $params),
            "ACTIVE"         => "Y",            // активен
        //"PREVIEW_TEXT"   => $message,
        );

        if( $el->Add($arEventFields) ){
            //запись в файл
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/log/yago.log', date('[d-m-Y H:i] ') . "Элемент успешно создан" . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
        else{
            //запись в файл
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/log/yago.log', date('[d-m-Y H:i] ') . "Элемент не создан" . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
	}
	
    public function getOptions(): array
    { 
        return $options = array(
                'token' => Option::get("eutils.yago", "token", ""),
                'switch_on' => Option::get("eutils.yago", "switch_on", ""),
                'test_on' => Option::get("eutils.yago", "test_on", ""),
                'log_on' => Option::get("eutils.yago", "log_on", ""),
                'pvz_on' => Option::get("eutils.yago", "pvz_on", ""),
                'status_id' => Option::get("eutils.yago", "status_id", ""),
                'paysystems' => Option::get("eutils.yago", "paysystems", ""),
                'http' => (Option::get("eutils.yago", "http", "") == "Y") ? "https://" : "http://",
                'courier_weight' => Option::get("eutils.yago", "courier_weight", "10000"),
                'express_weight' => Option::get("eutils.yago", "express_weight", "20000"),
                'pickup_name' => Option::get("eutils.yago", "pickup_name", ""),
                'emergency_name' => Option::get("eutils.yago", "emergency_name", ""),
                'emergency_phone' => Option::get("eutils.yago", "emergency_phone", ""),
                'distance_min' => Option::get("eutils.yago", "distance_min", ""),
                'distance_max' => Option::get("eutils.yago", "distance_max", ""),
                'delsystems' => Option::get("eutils.yago", "delsystems", ""),
                'region_id' => Option::get("eutils.yago", "region_id", ""),
                'url_request' => 'b2b.taxi.yandex.net/b2b/cargo/integration/v2/claims/create?request_id='
            );
    }

	function OnSaleStatusOrderChange($order)
	{
	
        $options = self::getOptions();
        if ($options['switch_on'] != "Y")
            return false;
        
        $isNew = $order->isNew();
        $arOrderVals = $order->getFields()->getValues();
        $propertyCollection = $order->getPropertyCollection();
//         $discountCollection = $order->getDiscount();

        if($arOrderVals['STATUS_ID'] != $options['status_id'])
            return false;

        if($isNew != '1')
            self::sendRequest($options,$order,$arOrderVals,$propertyCollection);
		
	}
    
}

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Правовая информация");
?><div class="whiteBox office">
	<div class="officeBox">
		<div class="officeTitle">Политика в отношении обработки персональных данных</div>
		<div>
			<h3>Сбор информации</h3>
			<p>
			Персональные данные обрабатываются в соответствии с ФЗ “О персональных данных” №152-ФЗ.<br>
			Мы собираем информацию, поступающую от вас, в следующих случаях:
			</p>
			<ul>
				<li>когда вы регистрируетесь на сайте;</li>
				<li>заходите в личный кабинет;</li>
				<li>заполняете форму обратной связи, в т.ч. анкету или отзыв;</li>
				<li>и/или выходите из аккаунта.</li>
			</ul>
			<p>
			Информация может включать ваше имя, фамилию, отчество, адрес электронной почты, номер телефона, дату рождения, возраст, пол, адреса магазинов которые вы посещаете или посещали ранее. <br>
			Кроме того, мы автоматически регистрируем данные о вашем компьютере и браузере, включая ip-адрес, используемое программное обеспечение для доступа к сайту, аппаратные данные, а также адреса запрашиваемых страниц.
			</p>
		</div>
		<div>
			<h3>Использование и обработка информации</h3>
			<p>
			Информация, которую мы получаем от вас, может быть использована в следующих целях:
			</p>
			<ul>
				<li>предложения персонализированной рекламы;</li>
				<li>улучшения работы нашего сайта;</li>
				<li>улучшения системы поддержки пользователей;</li>
				<li>для повышения качества услуг, и их соответствия вашим индивидуальным запросам;</li>
				<li>оформления договорных и иных отношений;</li>
				<li>организации и проведения программ лояльности;</li>
				<li>рекламных и маркетинговых исследований.</li>
			</ul>
			<p>Для исполнения вышеуказанных целей, мы можем связываться с вами с помощью различных каналов связи, в т.ч. по телефону, электронной почте, мессенджерам.</p>
		</div>
		<div>
			<h3>Защита личных данных</h3>
			<p>Мы являемся единственным владельцем информации, собранной на данном сайте. Ваши личные данные не будут проданы или каким-либо образом переданы третьим лицам.</p>
			<h3>Раскрытие информации третьим лицам</h3>
			<p>
			Мы не продаем, не обмениваем и не передаем личные данные сторонним компаниям. Это не относится к надежным компаниям, которые помогают нам в работе сайта, в проведении программы лояльности и ведении бизнеса при условии, что они соглашаются сохранять конфиденциальность информации.<br>
			Мы готовы делиться информацией, чтобы предотвратить преступления или помочь в их расследовании, если речь идет о подозрении на мошенничество, действиях, физически угрожающих безопасности людей, нарушениях правил использования или в случаях, когда это предусмотрено законом.<br>
			Не конфиденциальная информация может быть предоставлена другим компаниям в целях маркетинга и рекламы.
			</p>
		</div>
		<div>
			<h3>Согласие</h3>
			<p>Пользуясь услугами нашего сайта, вы автоматически соглашаетесь с нашей политикой конфиденциальности.</p>
		</div>
	</div>

	<div class="officeBox">
		<div class="officeTitle">«Cookie files»</div>
		<div>
			<h3>Информация в cookie файлах</h3>
			<p>Помимо персональной информации, мы автоматически регистрируем данные о вашем браузере, и адресах запрашиваемых страниц.</p>
		</div>
		<div>
			<h3>Использование данных</h3>
			<p>Мы используем файлы «cookie» и сходные технологии для предоставления вам персонализированной информации, подбора подходящей рекламы, в статических и исследовательских целях, а также для улучшения работы сайта.</p>
			<p>Файлы «cookie» могут использоваться для определения повторных посещений, улучшения взаимодействия с контентом, и для удобства посещения нашего сайта. Файлы «cookie» не передают никакую конфиденциальную информацию.</p>
		</div>
		<div>
			<h3>Согласие</h3>
			<p>При использовании данного сайта, вы подтверждаете свое согласие на использование файлов cookie в соответствии с настоящей политикой в отношении данного типа файлов. Если вы не согласны с использованием файлов cookie, вы можете отключить их в настройках своего браузера или покинуть наш сайт.</p>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
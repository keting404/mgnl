<img src="images/header__section.png" alt="" class="header__bg">
<div class="card__wrapper">
	<div class="card-tabs">
		<div class="card-tab card-tab1 active">Карта лояльности</div>
		<div class="card-tab card-tab2 ">Бонусный счет</div>
	</div>


	<div class="card__content card-loyalty active">
		<div class="wraps">
			<div class="maxwidth-theme">
				<div class="w-section__title">Бонусная карта Магнолия для удачных покупок</div>
				<p><b>Уважаемые покупатели, напоминаем Вам, что срок действия бонусов Магнолия составляет 1 год. Активируйте карту, чтобы воспользоваться бонусными баллами. 1 января 2021 года баллы, накопленные более 1 года назад, будут аннулированы!</b></p>

				<div class="card_bonus__wrapper">
					<div class="card_bonus__left order-2">
						<p>Совершая покупки с картой Магнолия, Вы можете накапливать бонусы и оплачивать ими покупки. Копите бонусы за каждую покупку и оплачивайте до 99% суммы.</p>

						<p>Специально для участников программы мы предлагаем дополнительные скидки на целые категории товаров. А также в каждом каталоге мы предлагаем Вам 9 товаров по удивительно низким ценам!</p>

						<p>Заполните свой профиль в личном кабинете, и получайте еще больше бонусов в День рождения.</p>
						<a href="https://lk.mgnl.ru/#/registration" class="btn">Активировать карту</a>
					</div>
					<div class="card_bonus__right order-1">
						<img src="images/card_bonus/card.png" alt="" class="card_bonus__img">
					</div>
				</div>


				<div class="card_bonus__wrapper">
					<div class="card_bonus__left order-1">
						<img src="images/card_bonus/monitor.png" alt="" class="card_bonus__img">
					</div>
					<div class="card_bonus__right order-2">
						<p><b>Личный кабинет</b></p>
						<ul>
							<li>Управление профилем - обновляйте информацию о себе</li>
							<li>Просмотр баланса бонусного счета</li>
							<li>Полная история покупок</li>
							<li>Управление картой - блокировка карты и ее замена</li>
							<li>Управление контактными данными</li>
						</ul>
						<a href="https://lk.mgnl.ru/#/login" class="btn">Войти в личный кабинет</a>
					</div>
				</div>

				<p class="card-bonus__title">Преимущества бонусной карты Магнолия</p>

				<div class="card-bonus-icon__wrapper">
					<div class="card-bonus-icon__item">
						<img src="images/card_bonus/icon-1.png" alt="">
						<p>Оплачивайте покупки бонусами</p>
					</div>
					<div class="card-bonus-icon__item">
						<img src="images/card_bonus/icon-2.png" alt="">
						<p>Скидки на целые<br>категории товаров</p>
					</div>
					<div class="card-bonus-icon__item">
						<img src="images/card_bonus/icon-3.png" alt="">
						<p>Лучшие товары по лучшим<br>ценам каждые 2 недели</p>
					</div>
					<div class="card-bonus-icon__item">
						<img src="images/card_bonus/icon-4.png" alt="">
						<p>Особенное<br>время</p>
					</div>
					<div class="card-bonus-icon__item">
						<img src="images/card_bonus/icon-5.png" alt="">
						<p>Больше бонусов в день<br>рождения</p>
					</div>
				</div>


				<div class="card_bonus__wrapper">
					<div class="card_bonus__left">
						<p class="card-bonus__title">Начисление бонусов</p>


						<div class="card-bonus__info accrual-bonuses">
							<div class="card-bonus__info-item --top">
								<div class="card-bonus__info-img">
									<img src="images/card_bonus/icon-6.png" alt="">
								</div>
								<div class="card-bonus__info-text">
									<p>Бонусы начисляются за все!<sup>*</sup>
									Получайте больше бонусов в зависимости от суммы чека:</p>
									<p><b>до 500 руб. - начисляется 3% бонусов<br>
										от 500 и до 1000 руб. - начисляется 3,5% бонусов<br>
										от 1000 руб. и больше - начисляется 4% бонусов!</b><br><br>
										кроме табака и табачной продукции<sup>*</sup></p>
								</div>
							</div>

							<div class="card-bonus__info-item --center">
								<div class="card-bonus__info-img">
									<img src="images/card_bonus/icon-7.png" alt="">
								</div>
								<div class="card-bonus__info-text">
									<p><b>Время жизни бонусов 1 год</b></p>
								</div>
							</div>
							
						</div>

						
					</div>
					<div class="card_bonus__right">
						<p class="card-bonus__title">Списание бонусов</p>

						<div class="card-bonus__info">

							<div class="debiting_bonuses">
								<div class="debiting_bonuses__item">
									<img src="images/card_bonus/icon-8.png" alt="">
									<span>Предъявите пластиковую карту кассиру и сообщите о желании списать бонусы</span>
								</div>
								<div class="debiting_bonuses__item">
									<img src="images/card_bonus/icon-9.png" alt="">
									<span>До 99% суммы чека можно оплатить бонусами<sup>*</sup></span>
								</div>
								<div class="debiting_bonuses__item">
									<img src="images/card_bonus/icon-10.png" alt="">
									<span>1 бонус = 1 рубль<sup>**</sup></span>
								</div>
							</div>

							<span>Списание возможно только при предъявлении пластиковой карты.<br>
							Карта должна быть активирована;<br>
							<sup>*</sup>Списание бонусов ограничено в случае акционных товаров, табака <br>
							и табачной продукции, алкоголя при наличии у него мрц;<br>
							<sup>**</sup>Бонусы не имеют денежного выражения и не могут быть выданы деньгами.</span>
						</div>
						
					</div>
				</div>

				<p class="card-bonus__title">Как получить бонусную карту?</p>

				<div class="get-bonus-card">
					<div class="get-bonus-card__item">
						<img src="images/card_bonus/icon-11.png" alt="">
						<span>Получите карту бесплатно <br>за покупку от 1000 рублей</span>
					</div>

					<div class="get-bonus-card__item">
						<img src="images/card_bonus/icon-12.png" alt="">
						<span>Активируйте карту на странице<br>личного кабинета</span>
					</div>

					<div class="get-bonus-card__item">
						<img src="images/card_bonus/icon-13.png" alt="">
						<span>Копите бонусы и оплачивайте<br>бонусами покупки!</span>
					</div>
				</div>

				<p class="card-bonus__title">Часто задаваемые вопросы</p>
				<div class="questions__title">Вопросы по карте</div>

				<div class="questions__wrapper">
					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Что это за карта и зачем она нужна?</p>
							<p  class="questions__value">Участие в программе и использование карты Магнолия позволяет приобретать товары по специальным ценам, накапливать и расплачиваться бонусами, получать скидки на целые категории товаров, участвовать в промо-акциях.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Как восстановить счет после утери/порчи карты?</p>
							<p  class="questions__value">Восстановить счет можно сделав замену карты в личном кабинете. Для этого потребуется перейти в личный кабинет, и в разделе меню "Карта", сначала заблокировать старую карту, потом в поле "Замена карты" указать номер новой карты.</p>
						</div>


					</div>
					<div class="questions__colum">
						
						<div class="questions__item">
							<p class="questions__name">Где и как можно получить карту?</p>
							<p  class="questions__value">Карта выдается за совершение покупки на сумму от 1000 рублей.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Что делать если моя карта испорчена?</p>
							<p  class="questions__value">Вы можете получить новую карту в обмен на старую в любом из магазинов сети.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Кажется я потерял карту, что делать?</p>
							<p  class="questions__value">В первую очередь рекомендуем заблокировать утерянную карту в личном кабинете. Получить новую карту Вы можете в ближайшем магазине, сообщив администрации магазина об утере карты.</p>
						</div>

					</div>
					<div class="questions__colum">
						
						<div class="questions__item">
							<p class="questions__name">Как активировать карту?</p>
							<p  class="questions__value">Активировать карту можно пройдя регистрацию в <a href="https://lk.mgnl.ru/#/registration">Личном кабинете</a>.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Мою карту заблокировали, почему?</p>
							<p class="questions__value">Карта может быть заблокирована в случае нарушения правил программы. Проверить состояние карты можно в личном кабинете.</p>
						</div>


<!-- 						 -->



					</div>
				</div>	

				<hr class="questions__hr">


				<div class="questions__title">Вопросы по начислению бонусов</div>
				
				<div class="questions__wrapper">
					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Как получить бонусы?</p>
							<p class="questions__value">Бонусы начисляются за каждую покупку, и за все товары, кроме табака и табачной продукции. Бонусы за покупку расчитываются после расчета скидок.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Как получить больше бонусов?</p>
							<p class="questions__value">Начисление 10% бонусами можно получить совершая покупки в день рождения.</p>
						</div>

						
					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">А можно сканировать карту с телефона?</p>
							<p class="questions__value">Конечно. Но если Вы хотите расплатиться бонусами, необходимо предоставить пластиковую карту.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Как получить 10% бонусов в День рождения?</p>
							<p class="questions__value">Акция в день рождения 10% бонусами действует за 1 день до дня рождения, в день рождения, и 1 день после дня рождения. Карта должна быть активирована, проверить дату рождения или указать ее можно в разделе "Профиль" личного кабинета.</p>
						</div>

					</div>

					<div class="questions__colum">

						<div class="questions__item">
							<p class="questions__name">Сколько я получу бонусов?</p>
							<p class="questions__value">
								Бонусы начисляются в зависимости от суммы чека.<br>
								За покупку<br>
								до 500 рублей начисляется 3% бонусов,<br>
								от 500 до 1000 рублей - 3,5% бонусов,<br>
								от 1000 рублей - 4% бонусов</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Как проверить сколько у меня бонусов?</p>
							<p class="questions__value">Проверить баланс бонусного счета и историю операций можно в <a href="https://lk.mgnl.ru/#/">Личном кабинете</a>.</p>
						</div>
					</div>
				</div>

				<hr class="questions__hr">

				<div class="questions__title">Вопросы по начислению бонусов</div>
				
				<div class="questions__wrapper">
					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Как списать бонусы?</p>
							<p class="questions__value">Предоставьте кассиру пластиковую карту и сообщите о своем желании расплатиться бонусами. Для списания бонусов карта должна быть активирована.</p>
						</div>
					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Сколько можно списать бонусов?</p>
							<p class="questions__value">Вы можете выбрать количество бонусов, которыми желаете оплатить покупку. До 99% от стоимости покупки можно оплатить бонусами, минимальная сумма к оплате всего 1 рубль.</p>
						</div>
					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Почему я не могу списать все бонусы?</p>
							<p class="questions__value">Касса по-умолчанию расчитывает максимально возможное количество бонусов к списанию, с учетом ограничений.</p>
						</div>
					</div>
				</div>

				<div class="questions__wrapper">
					<div class="questions__colum --65">
						<div class="questions__item">
							<p class="questions__name">Какие ограничения при списании бонусов?</p>
							<p class="questions__value">Акционные товары - предлагаются по сниженной цене на период действия буклета/каталога (такие товары отмечены на полке специальными выделителями)<br>
							Крепкий алкоголь ниже его минимальной розничной цены. По приказу Росалкорегулирования продавать алкоголь ниже цены мрц нельзя<br>
							Табак и табачная продукция - запрещено стимулирование продаж табака согласно федеральному закону, на эту продукцию не действуют акции, скидки и бонусы.</p>
						</div>
					</div>

					<div class="questions__colum --30">
						<div class="questions__item">
							<p class="questions__name">Что будет если я не воспользуюсь бонусами?</p>
							<p class="questions__value">Срок действия бонусов 12 месяцев со дня начисления. По истечении этого срока все неиспользованные бонусы аннулируются.</p>
						</div>
					</div>
				</div>

				<hr class="questions__hr">

				<div class="questions__title">Вопросы по картам партнеров</div>

				<div class="questions__wrapper">
					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">А для пенсионеров скидки есть?</p>
							<p class="questions__value">Да, мы предоставляем скидку 5% по социальной карте Москвича и социальной карте Московской области. Скидка по социальным картам действует с 9:00 и до 17:00 во всех магазинах</p>
						</div>

					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">У меня есть Социальная карта, можно использовать еe вместе с картой Магнолия?</p>
							<p class="questions__value">Нет, одновременно можно использовать только одну карту</p>
						</div>
					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Какие еще вы принимаете карты?</p>
							<p class="questions__value">Только карты Магнолия, социальные карты Москвы и Московской области</p>
						</div>
					</div>
				</div>

				<hr class="questions__hr">

				<div class="questions__title">Вопросы по картам партнеров</div>

				<div class="questions__wrapper">
					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Что такое купон?</p>
							<p class="questions__value">Это билет по которому можно получить дополнительную скидку.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Купон можно использовать вместе с картой?</p>
							<p class="questions__value">Да, если в условиях акции с применением купона не указано обратное.</p>
						</div>

					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Где и как можно получить купоны?</p>
							<p class="questions__value">Купоны могут распространяться как в бумажном так и в электронном виде.</p>
						</div>

						<div class="questions__item">
							<p class="questions__name">Сколько раз можно воспользоваться купоном?</p>
							<p class="questions__value">Купоны могут быть одноразовые погашаемые или многократного использования. Информация о типе купона располагается на самом купоне.</p>
						</div>
					</div>

					<div class="questions__colum">
						<div class="questions__item">
							<p class="questions__name">Как воспользоваться купоном?</p>
							<p class="questions__value">Предоставьте купон кассиру перед оплатой покупки.</p>
						</div>
					</div>
				</div>

				<hr class="questions__hr">









			</div>
			
		</div>

	</div>


	<div class="card__content card_bonus ">
		<div class="wraps">
			<div class="maxwidth-theme">
				<div class="w-section__title">Копите бонусы от покупок онлайн. Оплачивайте до 99% от покупки бонусами!</div>
				<p>
					<b>Бонусная программа "Магнолия" - это возможность получать до 4% от суммы ваших покупок на ваш счет на сайте интернет-магазина "Магнолия".</b>
				Это значит, что следующая покупка будет для вас выгоднее и вы сможете потратить бонусы на сайте интернет-магазина "Магнолия". Просто совершайте покупки и бонусы будут копиться на вашем личном счету. </p>



				<div class="card-loyalty-icon__wrapper">
					<div class="card-loyalty-icon__item">
						<img src="images/card_loyalty/icon-1.png" alt="">
						<p>Покупки с выгодой</p>
					</div>
					<div class="card-loyalty-icon__item">
						<img src="images/card_loyalty/icon-2.png" alt="">
						<p>Уникальные скидки</p>
					</div>
					<div class="card-loyalty-icon__item">
						<img src="images/card_loyalty/icon-3.png" alt="">
						<p>Делитесь с близкими</p>
					</div>
					<div class="card-loyalty-icon__item">
						<img src="images/card_loyalty/icon-4.png" alt="">
						<p>Копите и тратьте</p>
					</div>
					<div class="card-loyalty-icon__item">
						<img src="images/card_loyalty/icon-5.png" alt="">
						<p>Дарите</p>
					</div>
				</div>

				<div class="card-loyalty__wrapper">
					<div class="card-loyalty__left">

						<div class="card-loyalty-info">
							<div class="card-loyalty-title">Как копить?</div>
							<p>Совершайте покупки на сайте интернет-магазина "Магнолия" и копите бонусные баллы. </p>
							<ul>
								<li>до 500 руб. - начисляется 3% бонусов</li>
								<li>от 500 и до 1000 руб. - начисляется 3,5% бонусов</li>
								<li>от 1000 руб. и больше - начисляется 4% бонусов!</li>
								<li>получайте больше бонусов в зависимости от суммы чека.</li>
							</ul>
						</div>
						
					</div>
					<div class="card-loyalty__right">

						<div class="card-loyalty-info">
							<div class="card-loyalty-title">Как потратить?</div>
							<p>Вы можете потратить бонусы в интернет-магазине "Магнолия". Совершайте покупки, оплачивайте их накопленными бонусами. Обратите внимание, что бонусные баллы конвертируются в скидку на товары при оформлении заказа. Вы можете получить скидку на товары до 99% в зависимости от количества накопленных вами баллов.</p>
							<p>Потратить баллы, накопленные онлайн вы можете только при онлайн-покупках, программа лояльности розничной сети "Магнолия" не распространяется на интернет-магазин "Магнолия".</p>
						</div>
						
					</div>
				</div>

				<div class="card-loyalty__wrapper">
					<div class="card-loyalty__left">
						<img src="images/card_loyalty/monitor.png" alt="">
					</div>

					<div class="card-loyalty__right">
						<div class="card-loyalty-info2">
							<p>Вы можете отслеживать ваши бонусные баллы в личном кабинете, для этого необходимо зарегистрироваться и зайти в раздел "Бонусы"</p>

							<a href="/auth/">ВОЙТИ В ЛИЧНЫЙ КАБИНЕТ</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wraps">
	<div class="maxwidth-theme">


		<div class="card__section mt-4 mb-4">
			<div class="card__section__title">Правила программы и примечания</div>
			<p>Бонусный счет "Магнолия" является собственностью ЗАО "Т и К Продукты". Используя бонуснусный счет  "Магнолия" на сайте www.shop.mgnl.ru, вы подтверждаете, что согласны с условиями участия в программе. Бонусы не имеют денежного выражения и не могут быть выданы в виде денежного эквивалента.</p>
			<p>Оплата бонусами не распространяется на товары участвующие в акции. Такие товары отмечены на сайте интернет-магазина специальными выделителями. В цену товара по специальной цене уже заложена скидка, и они изначально предлагаются по сниженной цене, поэтому мы не предоставляем дополнительной скидки на такие товары. За покупку таких товаров бонусы начисляются, но не списываются в счет их оплаты.</p>
			<p>Мы не начисляем и не списываем бонусы при покупке табачной продукции. (Не допускается оплата бонусами табака и табачной продукции. Данное ограничение введено на основании Федерального закона от 23.02.2013 N 15-ФЗ "Об охране здоровья граждан от воздействия окружающего табачного дыма и последствий потребления табака". Статья 16 (Запрет рекламы и стимулирования продажи табака, спонсорства табака) часть 1.,Статья 13 (Ценовые и налоговые меры, направленные на сокращение спроса на табачные изделия) часть 2.)</p>
			<p>С полными правилами программы Вы можете ознакомиться перейдя по ссылке ниже:  </p>

			<a href="/upload/Бонусы%20онлайн.pdf" target="_blank">Правила программы</a>

		</div>

		<div class="bottom-links-block">
		<a class="muted back-url url-block" href="/blog/">
			<i class="svg inline  svg-inline-return_to_the_list" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><defs><style>.cls-1{fill-rule:evenodd;}</style></defs><path data-name="Rounded Rectangle 993" class="cls-1" d="M295,4656a9,9,0,1,1,9-9A9,9,0,0,1,295,4656Zm0-16a7,7,0,1,0,7,7A7,7,0,0,0,295,4640Zm1.694,10.7a0.992,0.992,0,0,1-1.407,0l-3-3a1,1,0,0,1,0-1.41l3-3.02a1,1,0,0,1,1.407,1.41l-2.3,2.31,2.3,2.29A1.01,1.01,0,0,1,296.694,4650.7Z" transform="translate(-286 -4638)"></path></svg>
</i>		<span class="font_upper back-url-text">Назад к списку</span></a>
		
																<div class="share hover-block bottom">
					<div class="shares-block hover-block__item text-center colored_theme_hover_bg-block">
																	</div>
				</div>
								</div>

		<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "inline_bonus", Array(
		    "AJAX_MODE" => "Y",
		        "AJAX_OPTION_ADDITIONAL" => "",
		        "AJAX_OPTION_HISTORY" => "N",
		        "AJAX_OPTION_JUMP" => "N",
		        "AJAX_OPTION_STYLE" => "Y",
		        "CACHE_TIME" => "3600",    // Время кеширования (сек.)
		        "CACHE_TYPE" => "A",    // Тип кеширования
		        "CHAIN_ITEM_LINK" => "",    // Ссылка на дополнительном пункте в навигационной цепочке
		        "CHAIN_ITEM_TEXT" => "",    // Название дополнительного пункта в навигационной цепочке
		        "COMPONENT_TEMPLATE" => "inline",
		        "EDIT_URL" => "",    // Страница редактирования результата
		        "IGNORE_CUSTOM_TEMPLATE" => "N",    // Игнорировать свой шаблон
		        "LIST_URL" => "",    // Страница со списком результатов
		        "SEF_MODE" => "N",    // Включить поддержку ЧПУ
		        "SUCCESS_URL" => "",    // Страница с сообщением об успешной отправке
		        "USE_EXTENDED_ERRORS" => "N",    // Использовать расширенный вывод сообщений об ошибках
		        "VARIABLE_ALIASES" => array(
		            "WEB_FORM_ID" => "WEB_FORM_ID",
		            "RESULT_ID" => "RESULT_ID",
		        ),
		        "WEB_FORM_ID" => "5",    // ID веб-формы
		    ),
		    false
		);?>

	</div>
</div>



<script>
	(function($) {
		$(function() {
  
			$('.card-tabs').on('click', 'div:not(.active)', function() {
				$(this).addClass('active').siblings().removeClass('active').closest('div.card__wrapper').find('div.card__content').removeClass('active').eq($(this).index()).addClass('active');
			});
 
		});
	})(jQuery);
</script>	